﻿using System;
using MonkeyArms;

namespace MyDiemCore
{
    public class ResetPasswordOutputInvoker : Invoker
    {
        public ResetPasswordOutputInvoker ()
        {
        }
    }

    public class ResetPasswordOutputArgs : InvokerArgs
    {
        public readonly string Message;

        public ResetPasswordOutputArgs (string message)
        {
            Message = message;
        }
    }
}

