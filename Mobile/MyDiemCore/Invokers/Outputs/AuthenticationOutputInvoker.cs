﻿using System;
using MonkeyArms;

namespace MyDiemCore
{
	public class AuthenticationOutputInvoker:Invoker
	{
		public AuthenticationOutputInvoker ()
		{
		}
	}

	public class AuthenticationOutputArgs:InvokerArgs
	{
		public readonly bool Success;

		public AuthenticationOutputArgs (bool success)
		{
			Success = success;
		}
	}
}

