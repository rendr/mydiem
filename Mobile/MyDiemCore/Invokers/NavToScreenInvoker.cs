using System;
using MonkeyArms;
using MyDiem.Core.BL;

namespace MyDiem.Core.Invokers
{
	public enum NavToScreenEnum
	{
		CalendarMonthView,
		CalendarWeekView,
		CalendarDayView,
		GoSettingsView,
		GoSubscribablesView,
		GoAboutView,
        GoParentDirectoryView,
        GoFacebookView,
        GoTwitterView,
		ParentDetailsView}
	;
	/*
	 * Invoker
	 */
	public class NavToScreenInvoker:Invoker
	{
		public NavToScreenInvoker () : base ()
		{
		}
	}
	/*
	 * Invoker Args
	 */
	public class NavToScreenInvokerArgs:InvokerArgs
	{
		//getters/setters
		public readonly NavToScreenEnum ScreenToNavTo;
		public readonly SchoolGroup SelectedGroup;
		public readonly string Title;
		//constructor
		public NavToScreenInvokerArgs (NavToScreenEnum screenToNavTo, SchoolGroup group = null, string title = null) : base ()
		{
			this.ScreenToNavTo = screenToNavTo;
			this.SelectedGroup = group;
			this.Title = title;
		}
	}
}

