using System;
using MonkeyArms;

namespace MyDiem.Core.Invokers
{
	public class SelectCalendarMonthDateInvoker:Invoker
	{
		public SelectCalendarMonthDateInvoker ():base()
		{
		}
	}

	public class SelectDateInvokerArgs:InvokerArgs
	{
		private DateTime selectedDate;

		public DateTime SelectedDate {
			get {
				return selectedDate;
			}
		}

		public SelectDateInvokerArgs(DateTime selectedDate)
		{
			this.selectedDate = selectedDate;
		}
	}
}

