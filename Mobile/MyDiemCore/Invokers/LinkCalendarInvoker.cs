using System;

using MonkeyArms;

namespace MyDiem.Core.Invokers
{
	public enum CalendarLinkType{iCal, gCal};

	/*
	 * Invoker
	 */

	public class LinkCalendarInvoker:Invoker
	{
		public LinkCalendarInvoker ():base()
		{
		}
	}

	/*
	 * Invoker Args
	 */

	public class LinkCalendarInvokerArgs:InvokerArgs{

		private CalendarLinkType linkType;

		public CalendarLinkType LinkType{
			get{
				return linkType;
			}
		}

		private string subscriptionURL;

		public string SubscriptionURL {
			get {
				return subscriptionURL;
			}
		}

		public LinkCalendarInvokerArgs(CalendarLinkType linkType, string subscriptionURL):base(){
			this.linkType = linkType;
			this.subscriptionURL = subscriptionURL;
		}
	}
}

