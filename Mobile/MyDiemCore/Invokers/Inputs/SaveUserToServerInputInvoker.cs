﻿using MonkeyArms;
using MyDiem.Core.BL;

namespace Invokers.Inputs
{
	public class SaveUserToServerInputInvoker : Invoker
	{
		public SaveUserToServerInputInvoker ()
		{
		}
	}

	public class SaveUserToServerInputArgs : InvokerArgs
	{
        public readonly User User;

        public SaveUserToServerInputArgs (User userToSave)
		{
            this.User = userToSave;
		}
	}
}

