﻿using System;
using MyDiem.Core.BL;
using MonkeyArms;

namespace MyDiemCore
{
	public class SaveUserToDBInputInvoker : Invoker
	{
		public SaveUserToDBInputInvoker()
		{
			//asdf
		}
	}

	public class SaveUserToDBInputArgs : InvokerArgs
	{
		public readonly User UserToSave;

		public SaveUserToDBInputArgs(User user)
		{
			UserToSave = user;
		}
	}
}

