﻿using System;
using MonkeyArms;

namespace MyDiemCore
{
	public class AuthenticationInputInvoker:Invoker
	{
		public AuthenticationInputInvoker ()
		{
		}
	}

	public class AuthenticationInvokerArgs:InvokerArgs
	{
		public readonly string Username, Password;

		public AuthenticationInvokerArgs (string username, string password)
		{
			Username = username;
			Password = password;
		}
	}
}

