using System;
using MonkeyArms;

namespace MyDiem.Core.Invokers
{
	public class SetNetworkActivityIndicatorVisibilityInvoker:Invoker
	{
		public SetNetworkActivityIndicatorVisibilityInvoker ()
		{
		}
	}

	public class SetNetworkActivityIndicatorVisibilityInvokerArgs:InvokerArgs{


		private bool visible = false;
		public bool IndicatorVisible{
			get{
				return visible;
			}
		}

		public SetNetworkActivityIndicatorVisibilityInvokerArgs(bool visible)
		{
			this.visible = visible;
		}

	}
}

