﻿using System;
using MonkeyArms;

namespace MyDiemCore
{
	public class RequestInvokerArgs:InvokerArgs
	{
		public readonly int ID;

		public RequestInvokerArgs (int id)
		{
			ID = id;
		}
	}
}

