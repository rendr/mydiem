﻿using MonkeyArms;

namespace MyDiemCore
{
    public class ResetPasswordRequestInvoker : Invoker
    {
        public ResetPasswordRequestInvoker ()
        {
        }
    }

    public class ResetPasswordRequestInvokerEventArgs : InvokerArgs
    {
        public string Email;

        public ResetPasswordRequestInvokerEventArgs (string email)
        {
            Email = email;
        }
    }
}

