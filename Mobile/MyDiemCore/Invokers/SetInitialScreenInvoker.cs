using System;
using MonkeyArms;

namespace MyDiem.Core.Invokers
{
	public enum InitialScreenEnum{LoadingScreen, HomeScreen};

	public class SetInitialScreenInvoker:Invoker
	{
		public SetInitialScreenInvoker ()
		{
		}
	}

	public class SetInitialScreenInvokerArgs:InvokerArgs
	{
		private InitialScreenEnum initialScreen;

		public InitialScreenEnum InitialScreen{
			get{
				return initialScreen;
			}
		}

		public SetInitialScreenInvokerArgs(InitialScreenEnum screen)
		{
			this.initialScreen = screen;
		}
	}


}

