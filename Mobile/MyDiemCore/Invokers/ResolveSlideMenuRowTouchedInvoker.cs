using System;

using MonkeyArms;

namespace MyDiem.Core.Invokers
{
	/*
	 * Invoker
	 */

	public class ResolveSlideMenuRowTouchedInvoker:Invoker
	{
		public ResolveSlideMenuRowTouchedInvoker ():base()
		{
		}
	}

	/*
	 * Invoker Args
	 */

	public class ResolveSlideMenuRowTouchedInvokerArgs:InvokerArgs
	{
		private string selectedRowLabel;

		public string SelectedRowLabel {
			get {
				return selectedRowLabel;
			}

		}

		public ResolveSlideMenuRowTouchedInvokerArgs(string selectedRowLabel):base(){
			this.selectedRowLabel = selectedRowLabel;
		}
	}
}

