using System;
using MonkeyArms;
using MyDiem.Core.Invokers;
using MyDiem.Core.ViewModel;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using System.Collections.Generic;

namespace MyDiem.Core.Commands
{
	public class ResolveSlideMenuRowTouchedCommand:Command
	{
		[Inject]
		public UserManager UM;
		[Inject]
		public SubscribableManager SM;

		[Inject]
		public NavToScreenInvoker NavToScreenInvoker{ get; set; }

		[Inject]
		public LinkCalendarInvoker LinkCalendarInvoker{ get; set; }

		[Inject]
		public PurchaseCalendarConnectInvoker PuchaseCCInvoker{ get; set; }

		[Inject]
		public User AppUser;

		public ResolveSlideMenuRowTouchedCommand () : base ()
		{
		}

		public override void Execute (InvokerArgs args)
		{
			ResolveSlideMenuRowTouchedInvokerArgs invokerArgs = args as ResolveSlideMenuRowTouchedInvokerArgs;
			RowTouched (invokerArgs.SelectedRowLabel);
		}

		protected void RowTouched (string rowLabel)
		{
			switch (rowLabel) {
				
			case SlideMenuViewModel.SUBSCRIBE_LABEL:
				SchoolGroup group = SM.CreateNewMainSubscriptionCategory ("Schools", UM.GetCalendarIDs (), false);
				GoScreen (NavToScreenEnum.GoSubscribablesView, group, SlideMenuViewModel.SUBSCRIBE_LABEL);
				break;
				
			case SlideMenuViewModel.MONTH_LABEL:
				GoScreen (NavToScreenEnum.CalendarMonthView);
				break;
			case SlideMenuViewModel.WEEK_LABEL:
				GoScreen (NavToScreenEnum.CalendarWeekView);
				break;
				
			case SlideMenuViewModel.LIST_LABEL:
				GoScreen (NavToScreenEnum.CalendarDayView);
				break;
			case SlideMenuViewModel.PROFILE_LABEL:
				GoScreen (NavToScreenEnum.GoSettingsView);
				break;
			case SlideMenuViewModel.ABOUT_LABEL:
				GoScreen (NavToScreenEnum.GoAboutView);
				break;
			case SlideMenuViewModel.MY_CALENDARS_LABEL:
				SchoolGroup userCalendarCategory = SM.CreateNewMainSubscriptionCategory (SlideMenuViewModel.MY_CALENDARS_LABEL, UM.GetCalendarIDs (), true);
				GoScreen (NavToScreenEnum.GoSubscribablesView, userCalendarCategory, SlideMenuViewModel.MY_CALENDARS_LABEL);
				break;
				
			case SlideMenuViewModel.LINK_CALENDAR_LABEL:
				LinkCalendarInvoker.Invoke (new LinkCalendarInvokerArgs (CalendarLinkType.iCal, AppUser.SubscriptionUrl));
				break;
				
			case SlideMenuViewModel.LINK_GOOGLE_CAL_LABEL:
				if (AppUser.CalendarConnect) {
					LinkCalendarInvoker.Invoke (new LinkCalendarInvokerArgs (CalendarLinkType.gCal, AppUser.SubscriptionUrl));
				} else {
					PuchaseCCInvoker.Invoke ();
				}
				break;
				
			case SlideMenuViewModel.DIRECTORY_LABEL:
				GoScreen (NavToScreenEnum.GoParentDirectoryView);
				break;

            case SlideMenuViewModel.FACEBOOK:
                GoScreen (NavToScreenEnum.GoFacebookView);
                break;

            case SlideMenuViewModel.TWITTER:
                GoScreen (NavToScreenEnum.GoTwitterView);
                break;
            }
        }

		protected void GoScreen (NavToScreenEnum screen, SchoolGroup selectedGroup = null, string title = null)
		{
			NavToScreenInvoker.Invoke (new NavToScreenInvokerArgs (screen, selectedGroup, title));
		}
	}
}

