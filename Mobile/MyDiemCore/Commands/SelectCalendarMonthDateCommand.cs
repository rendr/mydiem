using System;
using MonkeyArms;
using MyDiem.Core.Invokers;
using MyDiem.Core.ViewModel;
using MyDiem.Core.BL.Managers;

namespace MyDiem.Core.Commands
{
	public class SelectCalendarMonthDateCommand:Command
	{
		[Inject]
		public CalendarMonthViewModel VM;

		[Inject]
		public EventManager EM;

		public SelectCalendarMonthDateCommand ()
		{
		}

		public override void Execute (InvokerArgs args)
		{
			var date = (args as SelectDateInvokerArgs).SelectedDate;
			VM.SelectedDate = date;
			VM.SelectedDateEvents = EM.GetEventsForDate(date);
		}
	}
}

