﻿using Invokers.Inputs;
using Invokers.Notification;
using MonkeyArms;
using MyDiem.Core.SAL;

namespace Commands
{
	public class SaveUserToServerCommand:Command
	{
		[Inject]
		public UpdateUserProfileServiceDelegate del;

		[Inject]
		public UserSavedOnServerInvoker UserSaved;


		#region implemented abstract members of Command

		public override void Execute (InvokerArgs args)
		{
			SaveUserToServerInputArgs sargs = (SaveUserToServerInputArgs)args;
            del.Update (sargs.User, delegate { UserSaved.Invoke (InvokerArgs.Empty); });
		}

		#endregion


	}
}

