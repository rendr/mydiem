﻿using MonkeyArms;

namespace MyDiemCore
{
    public class DeleteUserFromDB:Command
	{
		#region implemented abstract members of Command

		public override void Execute (InvokerArgs args)
		{
            Settings.User = "";
		}

		#endregion
	}
}