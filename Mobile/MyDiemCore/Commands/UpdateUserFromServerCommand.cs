﻿using System;
using MonkeyArms;
using MyDiem.Core.SAL;
using Newtonsoft.Json.Linq;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using System.Collections.Generic;

namespace MyDiemCore
{
	public class UpdateUserFromServerCommand:Command
	{
		[Inject]
		public GetUserProfileServiceDelegate Service;
		[Inject]
		public SaveUserToDBInputInvoker SaveUserInvoker;
		[Inject]
		public User AppUser;
		[Inject]
		public UserManager UM;
		[Inject]
		public UserUpdatedFromServerNotificationInvoker UserUpdatedNotification;

		#region implemented abstract members of Command

		public override void Execute (InvokerArgs args)
		{

			Service.GetProfile (AppUser.ID,
				() => {
					UpdateAppUserFromProfilePayload (Service.Payload);
					UpdateUserCalendarSubscriptionsFromProfilePayload (Service.Payload);
					UpdateUserDirectorySubscriptionsFromProfilePayload (Service.Payload);
					UserUpdatedNotification.Invoke ();
				}
			);
		}

		#endregion

		protected void UpdateAppUserFromProfilePayload (JObject payload)
		{
			AppUser.Update (User.FromJObj (payload));
			SaveUserInvoker.Invoke (new SaveUserToDBInputArgs (AppUser));
		}

		protected void UpdateUserCalendarSubscriptionsFromProfilePayload (JObject payload)
		{
			UM.UserCalendarSubscriptions = new List<Subscription> ();
			foreach (JObject c in (JArray)payload["CALENDARS"]) {
				UM.UserCalendarSubscriptions.Add (Subscription.FromJObject (c));
			}
		}

		protected void UpdateUserDirectorySubscriptionsFromProfilePayload (JObject payload)
		{
			UM.UserDirectorySubscriptions = new List<Subscription> ();
			foreach (JObject c in (JArray)payload["DIRECTORIES"]) {
				UM.UserDirectorySubscriptions.Add (Subscription.FromJObject (c));
			}
		}
	}
}

