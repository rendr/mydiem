﻿using MonkeyArms;
using MyDiem.Core.SAL;
using MyDiemCore;
using MyDiem.Core.Util;

namespace Commands
{
    public class ResetPasswordCommand : Command
    {
        [Inject]
        public ResetPasswordServiceDelegate del;
       
        [Inject]
        public ResetPasswordOutputInvoker OutputInvoker;

        public override void Execute (InvokerArgs args)
        {
            var sargs = (ResetPasswordRequestInvokerEventArgs)args;
            var email = sargs.Email;

            if (!FormDataValidator.IsValidEmail (email)) {
                DispatchOutput ("Please enter a valid email");
                return;
            }

            del.Reset (email, () => DispatchOutput(del.Message));
        }

        protected void DispatchOutput (string message)
        {
            OutputInvoker.Invoke (new ResetPasswordOutputArgs (message));
        }
    }
}

