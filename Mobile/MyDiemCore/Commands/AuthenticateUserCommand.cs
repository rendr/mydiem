﻿using System;
using MonkeyArms;
using MyDiem.Core.Util;
using MyDiem.Core.SAL;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.BL;
using MyDiem.Core.ViewModel;

namespace MyDiemCore
{
	public class AuthenticateUserCommand:Command
	{
		[Inject]
		public AuthenticationOutputInvoker OutputInvoker;
		[Inject]
		public UserNotValidatedNotificationInvoker NotValidatedInvoker;
		[Inject]
		public AuthenticateUserServiceDelegate ServiceDelegate;
		[Inject]
		public User AppUser;

		#region implemented abstract members of Command

		public override void Execute (InvokerArgs args)
		{
			AuthenticationInvokerArgs authArgs = args as AuthenticationInvokerArgs;
			string email = authArgs.Username;
			string password = authArgs.Password;

			if (!FormDataValidator.IsValidEmail (email)) {
				DispatchOutput (false);
				return;	
			}

			ServiceDelegate.Authenticate (email, password,
				delegate {

					if (!ServiceDelegate.loggedIn) {
						DispatchOutput (false);
						return;
					}
					AppUser.Update (ServiceDelegate.AppUser);
					if (AppUser.IsValidated) {
						DispatchOutput (true);
					} else {
						NotValidatedInvoker.Invoke ();
					}
				}
			);
		}

		#endregion

		protected void DispatchOutput (bool success)
		{
			OutputInvoker.Invoke (new AuthenticationOutputArgs (success));
		}
	}
}

