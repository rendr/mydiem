using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MonkeyArms;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.Interfaces;
using MyDiem.Core.Invokers;
using MyDiemCore;
using Newtonsoft.Json;

namespace MyDiem.Core.Commands
{
	public class BaseBootStrapCommand:Command
	{
		[Inject]
		public UserManager UM;
		[Inject]
		public EventManager EM;
		[Inject]
		public RegionManager RM;
		[Inject]
        public CountyManager CM;
        [Inject]
        public StateManager StM;
		[Inject]
		public SubscribableManager SM;
		[Inject]
		public SetNetworkActivityIndicatorVisibilityInvoker SetNetworkIndicatorInvoker;
		[Inject]
		public SetInitialScreenInvoker SetInitialScreenInvoker;
		[Inject]
		public NavToScreenInvoker NavToScreenInvoker;
		[Inject]
		public UpdateUserFromDBRequestInvoker UpdatUserFromDBRequest;
		[Inject]
		public User AppUser;
		[Inject]
		public UpdateUserFromServerRequestInvoker UpdateUserFromServerRequest;
		[Inject]
		public UserUpdatedFromServerNotificationInvoker UserUpdatedFromServerNotification;

		public BaseBootStrapCommand ()
		{
			Detain ();
		}

		public override void Execute (InvokerArgs args)
		{
            User user = JsonConvert.DeserializeObject<User>(Settings.User);

            if (user != null) {
				Debug.WriteLine ("User is logged in");
				SetNetworkIndicatorInvoker.Invoke (new SetNetworkActivityIndicatorVisibilityInvokerArgs (true));
				UpdatUserFromDBRequest.Invoke ();

				UserUpdatedFromServerNotification.Invoked += HandleGetProfileFinished;
				EM.GetEventsForCalendarsFinished.Invoked += HandleGetEventsForCalendarsFinished;
				UpdateUserFromServerRequest.Invoke (new RequestInvokerArgs (AppUser.ID));
				SetInitialScreenInvoker.Invoke (new SetInitialScreenInvokerArgs (InitialScreenEnum.LoadingScreen));
			} 
            else 
            {
				Debug.WriteLine ("User is not logged in");
				SetInitialScreenInvoker.Invoke (new SetInitialScreenInvokerArgs (InitialScreenEnum.HomeScreen));
			}

            StM.GetStates(() => {
                var state = StM.States.FirstOrDefault ();
                CM.Counties = state.Counties.Any () ? state.Counties : new List<County> () { new County (-1, "No Matching Counties") };
                RM.Regions = state.Regions.Any () ? state.Regions : new List<Region> () { new Region (-1, "No Matching Regions") };
            });
		}

		protected void HandleGetProfileFinished (object sender, EventArgs e)
		{
			EM.GetEventsForCalendars (UM.GetCalendarIDs ());
		}

		protected void HandleGetEventsForCalendarsFinished (object sender, EventArgs e)
		{
			UserUpdatedFromServerNotification.Invoked -= HandleGetProfileFinished;
			EM.GetEventsForCalendarsFinished.Invoked -= HandleGetEventsForCalendarsFinished;
			NavToScreenInvoker.Invoke (new NavToScreenInvokerArgs (NavToScreenEnum.CalendarMonthView));
			Release ();
		}
	}
}

