﻿using MonkeyArms;
using MyDiem.Core.BL;
using Newtonsoft.Json;

namespace MyDiemCore
{
    public class UpdateUserFromDBCommand:Command
	{ 
		[Inject]
		public User AppUser;

		#region implemented abstract members of Command

		public override void Execute (InvokerArgs args)
		{
            User user = JsonConvert.DeserializeObject<User> (Settings.User);
            AppUser.Update (user);
		}

		#endregion
	}
}

