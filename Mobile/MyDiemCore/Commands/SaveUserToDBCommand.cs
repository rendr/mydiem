﻿using MonkeyArms;
using MyDiem.Core.BL;
using Newtonsoft.Json;

namespace MyDiemCore
{
    public class SaveUserToDBCommand:Command
	{
		#region implemented abstract members of Command

		public override void Execute (InvokerArgs args)
		{
			User userToSave = (args as SaveUserToDBInputArgs).UserToSave;
            Settings.User = JsonConvert.SerializeObject (userToSave);
		}

		#endregion
	}
}

