using System;

namespace MyDiem.Core
{
	public interface IValidateSubscribablePasswordServiceDelegate
	{
		void ValidatePassword (long subscribableID, string password, Action action);
		bool Success{get;set;}
	}
}

