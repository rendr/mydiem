using System;

using Newtonsoft.Json.Linq;

namespace MyDiem.Core.SAL
{
	public class UpdateUserLinkAccess:BaseAPIServiceDelegate
	{
		public bool Success;

		public UpdateUserLinkAccess ()
		{
			this.APIMethodName = "updateUserCalendarConnect";
		}

		public void Update(long userID, Action action)
		{
			this.action = action;
			this.SendRequest("&userID=" + userID);
		}

		override public void HandleResult(string resultString){
			try{
			JObject o = JObject.Parse(resultString);
			this.Success = (bool)o["SUCCESS"];
			this.action();
			}catch{

		}
		}
	}
}

