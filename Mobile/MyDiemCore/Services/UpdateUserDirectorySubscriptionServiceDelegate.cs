using System;

using Newtonsoft.Json.Linq;

using MyDiem.Core.BL;

namespace MyDiem.Core.SAL
{
	public class UpdateUserDirectorySubscriptionServiceDelegate:BaseAPIServiceDelegate
	{
		public bool Success;

		public UpdateUserDirectorySubscriptionServiceDelegate ()
		{
			this.APIMethodName = "updateUserDirectorySubscription";
		}

		public void UpdateUserDirectorySubscription(long userID, long directoryID, bool suppressed, bool isSubscribed, Action action){
			this.action = action;
			this.Success = false;
			var suppressParam = (suppressed)?"1":"0";
			var subscribedParam = (isSubscribed) ? "1" : "0";
			this.SendRequest("&userID=" + userID + "&directoryID=" + directoryID + "&suppressed=" + suppressParam + "&isSubscribed=" + subscribedParam);

			
			
		}
		
		
		override public void HandleResult(string resultString){
			JObject o = JObject.Parse(resultString);
			this.Success = (bool)o["SUCCESS"];
			this.action();
		}
	}
}

