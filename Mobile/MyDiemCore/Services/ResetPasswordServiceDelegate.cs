﻿using System;
using MyDiem.Core.SAL;
using Newtonsoft.Json.Linq;

namespace MyDiem.Core.SAL
{
    public class ResetPasswordServiceDelegate : BaseAPIServiceDelegate
    {
        public string Message;

        public ResetPasswordServiceDelegate ()
        {
            this.APIMethodName = "resetPassword";
        }

        public void Reset (string email, Action action)
        {
            this.action = action;
            this.SendRequest ("&email=" + email);
        }

        public override void HandleResult (string resultString)
        {
            var obj = JObject.Parse (resultString);
            Message = (string)obj ["MESSAGE"];

            this.action ();
        }
    }
}