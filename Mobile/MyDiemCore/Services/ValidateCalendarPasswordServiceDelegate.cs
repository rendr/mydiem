using System;

using MyDiem.Core.SAL;
using Newtonsoft.Json.Linq;


namespace MyDiem.Core
{
	public class ValidateCalendarPasswordServiceDelegate:BaseAPIServiceDelegate, IValidateSubscribablePasswordServiceDelegate
	{
		private bool success;

		public bool Success {
			get {
				return success;
			}
			set {
				success = value;
			}
		}
		
		public ValidateCalendarPasswordServiceDelegate ()
		{
			this.APIMethodName = "validateCalendarPassword";
		}
		
		public void ValidatePassword(long calendarID, string submittedPassword, Action action){
			this.action = action;
			this.success = false;
			this.SendRequest("&calID="  + calendarID + "&password=" + submittedPassword);
			
			
		}
		
		
		override public void HandleResult(string resultString){
			JObject o = JObject.Parse(resultString);
			this.success = (bool)o["SUCCESS"];
			this.action();
		}
	}
}

