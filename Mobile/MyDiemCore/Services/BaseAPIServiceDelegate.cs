using System;
using System.Diagnostics;
using MonkeyArms;
using Newtonsoft.Json.Linq;

namespace MyDiem.Core.SAL
{
	public class BaseAPIServiceDelegate
	{
		public static event EventHandler ServiceError = delegate {};

		protected string APIBaseURL = "https://app.mydiem.com/index.cfm?event=mydiemAccess.";
		protected string APIMethodName;
		protected Action action;
		protected Uri requestURI;
		protected int Attempts = 0;

		public BaseAPIServiceDelegate ()
		{
		}

        protected void SendRequest (string paramString, bool post = false)
		{
			var service = DI.Get<IWebAPIService> ();
			var requestURL = APIBaseURL + this.APIMethodName;
			Debug.WriteLine ("API Request: " + requestURL);
			Attempts++;
            service.Send (requestURL, post, paramString);
			service.Result += (object sender, EventArgs e) => {

				var responseIsJson = true;

				try {
					JArray.Parse (service.ResponseText);
				} catch {
					try {
						JObject.Parse (service.ResponseText);
					} catch (Exception ex) {
						responseIsJson = false;
						Debug.WriteLine ("Exception message: " + ex.Message);
						Debug.WriteLine ("API Error:" + service.APIErrorMessage);
						Debug.WriteLine ("API Error URL:" + service.APIErrorURL);
						if (Attempts > 2) {
							ServiceError (this, EventArgs.Empty);
						} else {
                            SendRequest (paramString, post);
							Debug.WriteLine ("Trying again...");
						}
					}
				}

				Debug.WriteLine ("API Result:" + service.ResponseText);
				if (!String.IsNullOrEmpty (service.ResponseText) && responseIsJson) {
					this.HandleResult (service.ResponseText);
				}
			};
		}

		public virtual void HandleResult (string resultString)
		{
		}
	}
}

