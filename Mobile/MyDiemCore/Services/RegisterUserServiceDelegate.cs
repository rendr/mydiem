using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MyDiem.Core.BL;


namespace MyDiem.Core.SAL
{
	public class RegisterUserServiceDelegate:BaseAPIServiceDelegate
	{
		public bool userRegistered;
		public string responseMessage;
		
		public RegisterUserServiceDelegate ()
		{
			this.APIMethodName = "register";
		}
		
        public void Register(User userToRegister, Action action)
		{
            this.action = action;
            this.userRegistered = false;

            var serializerOptions = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };

            string paramStr = JsonConvert.SerializeObject(userToRegister, Formatting.None, serializerOptions);
            this.SendRequest(paramStr, true);
		}
		
		
		override public void HandleResult(string resultString){
			JObject o = JObject.Parse(resultString);
			this.userRegistered = (bool)o["SUCCESS"];
			this.responseMessage = (string)o["MESSAGE"];
			this.action();
		}
	}
}

