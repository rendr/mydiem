using System;
using System.Collections.Generic;

using Newtonsoft.Json.Linq;

using MyDiem.Core.BL;

namespace MyDiem.Core.SAL
{
	public class GetCalendarsWithEventsServiceDelegate:BaseAPIServiceDelegate
	{
		public List<CalendarEvent> CalendarEvents;

		public GetCalendarsWithEventsServiceDelegate ()
		{
			this.APIMethodName = "getCalendarsWithEvents";
		}


		public void GetCalendarsAndEvents(int[] calendarIDs, Action action)
		{
			this.action = action;
			this.SendRequest("&calendarID=" + String.Join(",", calendarIDs));
		}
		
		
		override public void HandleResult(string resultString){
			CalendarEvents = new List<CalendarEvent>();
			JArray results = JArray.Parse(resultString);
			foreach(JObject co in results){
				foreach(JObject eo in (JArray)co["EVENTS"]){
					CalendarEvents.Add(CalendarEvent.fromJObject(Convert.ToInt32((string)co["ID"]), eo, (string)co ["TITLE"]));
				}
			}
			this.action();
		}
	}
}

