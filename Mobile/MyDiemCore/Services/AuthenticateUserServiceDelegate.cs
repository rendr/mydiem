using System;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using MyDiem.Core.BL;

namespace MyDiem.Core.SAL
{
	public class AuthenticateUserServiceDelegate:BaseAPIServiceDelegate
	{
		public bool loggedIn = false;
		public User AppUser;

		public AuthenticateUserServiceDelegate ()
		{
			this.APIMethodName = "login";
		}

		public void Authenticate (string email, string password, Action action)
		{
			this.action = action;
			this.loggedIn = false;
			this.SendRequest ("&email=" + email + "&password=" + password);
			
			
		}

		override public void HandleResult (string resultString)
		{

			JObject o = JObject.Parse (resultString);

			this.loggedIn = (bool)o ["SUCCESS"];
			if (this.loggedIn) {
				this.AppUser = User.FromJObj (o);
			}
			Debug.WriteLine ("hi:" + this.loggedIn);
			this.action ();
		}
	}
}

