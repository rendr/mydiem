using System;

using Newtonsoft.Json.Linq;

using MyDiem.Core.BL;
using System.Collections.Generic;

namespace MyDiem.Core.SAL
{
	public class GetSchoolSubscribablesServiceDelegate:BaseAPIServiceDelegate
	{
		public List<SchoolGroup> SchoolGroups;

		public GetSchoolSubscribablesServiceDelegate ()
		{
			this.APIMethodName = "getSchoolSubscribables";
		}

		public virtual void GetSubscribables(long userID, Action action)
		{
			
			this.action = action;
			this.SendRequest("&userID=" + userID );
		}

		public virtual void GetSubscribables(long regionID, long countyID, long userID, Action action)
		{
			
			this.action = action;
			this.SendRequest("&regionID=" + regionID + "&countyID=" + countyID + "&userID=" + userID);
		}

		override public void HandleResult(string resultString){
			JArray items = JArray.Parse(resultString);
			SchoolGroups = new List<SchoolGroup>();
			//parsing schools
			foreach (JObject schoolObj in items){
				SchoolGroup schoolGroup = new SchoolGroup();
				schoolGroup.Title = (string)schoolObj["TITLE"];
				schoolGroup.RegionID = Convert.ToInt32((string)schoolObj["REGIONID"]);
				schoolGroup.CountyID = Convert.ToInt32((string)schoolObj["COUNTYID"]);
				schoolGroup.Type = (string)schoolObj["TYPE"];
				schoolGroup.Subscribables = new List<Subscribable>();


				//parsing subscribables
				foreach(JObject calendarObj in (JArray)schoolObj["SUBSCRIBABLES"]){
					schoolGroup.Subscribables.Add(Subscribable.FromJObj(calendarObj));
				}

				
				SchoolGroups.Add(schoolGroup);
				
			}
			this.action();
		}
	}
}

