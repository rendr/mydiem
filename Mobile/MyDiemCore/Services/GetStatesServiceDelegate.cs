﻿using System;
using MyDiem.Core.BL;
using System.Collections.Generic;
using MyDiem.Core.SAL;
using Newtonsoft.Json;

namespace MyDiemCore
{
    public class GetStatesServiceDelegate : BaseAPIServiceDelegate
    { 
        public List<State> States;

        public GetStatesServiceDelegate ()
        {
            this.APIMethodName = "getStates";
        }

        public void GetStates(Action action)
        {
            this.action = action;
            this.SendRequest("");
        }

        public override void HandleResult(string resultString)
        {
            States = JsonConvert.DeserializeObject<List<State>>(resultString);
            this.action();
        }
    }
}

