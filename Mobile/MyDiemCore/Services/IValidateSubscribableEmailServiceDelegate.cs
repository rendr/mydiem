using System;

namespace MyDiem.Core
{
	public interface IValidateSubscribableEmailServiceDelegate
	{
		void ValidateEmail (long id, string email, Action action);
		bool Success{get;set;}
	}
}

