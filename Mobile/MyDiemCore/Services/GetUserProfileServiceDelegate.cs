using System;
using System.Diagnostics;
using Newtonsoft.Json.Linq;

namespace MyDiem.Core.SAL
{
	public class GetUserProfileServiceDelegate:BaseAPIServiceDelegate
	{
		public JObject Payload;

		public GetUserProfileServiceDelegate ()
		{
			this.APIMethodName = "getUserProfile";
		}

		public void GetProfile(long userID, Action action)
		{

			this.action = action;
			this.SendRequest("&userID=" + userID);
		}
		
		override public void HandleResult(string resultString){
			this.Payload = JObject.Parse(resultString);
			this.action();
		}
	}
}

