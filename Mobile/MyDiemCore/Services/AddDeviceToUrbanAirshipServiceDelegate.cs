using System;

using Newtonsoft.Json.Linq;

using MyDiem.Core.BL;

namespace MyDiem.Core.SAL
{
	public class AddDeviceToUrbanAirshipServiceDelegate:BaseAPIServiceDelegate
	{
		public AddDeviceToUrbanAirshipServiceDelegate ()
		{
			this.APIMethodName = "addDevice";
		}

		public void Add(long userID, string deviceToken, string deviceName, Action action)
		{
			this.action = action;
			this.SendRequest("&userID=" + userID.ToString() + "&deviceToken=" + deviceToken + "&deviceName=" + deviceName);
		}


		override public void HandleResult(string resultString){
			JObject o = JObject.Parse(resultString);

			this.action();
		}
	}
}

