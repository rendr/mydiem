using System;

namespace MyDiem.Core.SAL
{
	public interface IWebAPIService
	{
		event EventHandler Result;
		event EventHandler Error;

		string ResponseText{ get; }

		string APIErrorMessage { get; }

		string APIErrorURL { get; }

        void Send (string baseURL, bool post, string payload);
	}
}
