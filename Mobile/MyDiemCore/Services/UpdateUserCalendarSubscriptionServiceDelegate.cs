using System;

using Newtonsoft.Json.Linq;

using MyDiem.Core.BL;

namespace MyDiem.Core.SAL

{
	public class UpdateUserCalendarSubscriptionServiceDelegate:BaseAPIServiceDelegate
	{
		public bool Success;

		public UpdateUserCalendarSubscriptionServiceDelegate ()
		{
			this.APIMethodName = "updateUserCalendarSubscription";
		}

		public void UpdateUserCalendarSubscription(long userID, Subscription calendarSubscription, long colorID, Action action){
			this.action = action;
			this.Success = false;
			//hotfix
			if (colorID == 0) {
				colorID = 1;
			}
			this.SendRequest("&userID=" + userID + "&calendarID="  + calendarSubscription.SubscribableID + "&colorID=" + colorID + "&suppressed=" + calendarSubscription.Suppressed + "&isSubscribed=" + calendarSubscription.Enabled);
			
			
		}
		
		
		override public void HandleResult(string resultString){
			JObject o = JObject.Parse(resultString);
			this.Success = (bool)o["SUCCESS"];
			this.action();
		}
	}
}

