using System;
using System.Diagnostics;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using MyDiem.Core.BL;
using System.Linq;


namespace MyDiem.Core.SAL
{
	public class GetDirectoriesWithContactsServiceDelegate:BaseAPIServiceDelegate
	{


		public List<Parent> Parents;

		public GetDirectoriesWithContactsServiceDelegate ()
		{
			this.APIMethodName = "getDirectoriesWithContacts";
		}

		public void GetParents (long userID, Action action)
		{
			
			this.action = action;
			this.SendRequest ("&userID=" + userID);
//			this.SendRequest("&userID=1");
		}

		override public void HandleResult (string resultString)
		{
			List<Parent> Results = new List<Parent> ();
			int parseCounter = 0;
			if (resultString != "[]") {
				JArray items = JArray.Parse (resultString);

				foreach (JObject obj in items) {
					foreach (JObject p in obj["CONTACTS"]) {
						Results.Add (Parent.FromJObj (p, (string)obj ["TITLE"]));
					}


				}
			}


			Parents = Results.OrderBy (o => o.LastName).ToList ();
			;
			this.action ();
		}
	}
}

