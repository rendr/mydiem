using System;
using MyDiem.Core.SAL;
using Newtonsoft.Json.Linq;
using MyDiem.Core.BL;
using Newtonsoft.Json;

namespace MyDiem.Core.SAL
{
    public class UpdateUserProfileServiceDelegate : BaseAPIServiceDelegate
    {
        public bool userUpdated;
        public string responseMessage;

        public UpdateUserProfileServiceDelegate ()
        {
            this.APIMethodName = "updateUser";
        }

        public void Update (User updatedUser, Action action)
        {
            this.action = action;
            this.userUpdated = false;

            var serializerOptions = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };

            string paramStr = JsonConvert.SerializeObject (updatedUser, Formatting.None, serializerOptions);

            SendRequest (paramStr, true);
        }

        override public void HandleResult (string resultString)
        {
            JObject o = JObject.Parse (resultString);
            this.userUpdated = (bool)o ["SUCCESS"];
            this.responseMessage = (string)o ["MESSAGE"];
            this.action ();
        }
    }
}

