using System;
using System.Collections.Generic;

using Newtonsoft.Json.Linq;

using MyDiem.Core.BL;

namespace MyDiem.Core.SAL
{
	public class GetColorServiceDelegate:BaseAPIServiceDelegate
	{
		public List<CalendarColor> Colors;

		public GetColorServiceDelegate ()
		{
			this.APIMethodName = "getColors";
		}

		public void GetColors(Action action)
		{
			
			this.action = action;
			this.SendRequest("");
		}
		
		override public void HandleResult(string resultString){
			JArray items = JArray.Parse(resultString);
			Colors = new List<CalendarColor>();
			foreach (JObject obj in items){
				Colors.Add(CalendarColor.FromJObject(obj));
			}
			this.action();
		}
	}
}

