using System;

using MyDiem.Core.SAL;
using Newtonsoft.Json.Linq;


namespace MyDiem.Core
{
	public class ValidateDirectoryPasswordServiceDelegate:BaseAPIServiceDelegate, IValidateSubscribablePasswordServiceDelegate
	{
		public bool success;

		public bool Success {
			get {
				return success;
			}
			set {
				success = value;
			}
		}
		
		public ValidateDirectoryPasswordServiceDelegate ()
		{
			this.APIMethodName = "validateDirectoryPassword";
		}
		
		public void ValidatePassword(long directoryID, string submittedPassword, Action action){
			this.action = action;
			this.success = false;
			this.SendRequest("&directoryID="  + directoryID + "&password=" + submittedPassword);
			
			
		}
		
		
		override public void HandleResult(string resultString){
			JObject o = JObject.Parse(resultString);
			this.success = (bool)o["SUCCESS"];
			this.action();
		}
	}
}

