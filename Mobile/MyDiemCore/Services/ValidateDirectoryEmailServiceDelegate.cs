using System;

using MyDiem.Core.SAL;
using Newtonsoft.Json.Linq;


namespace MyDiem.Core
{
	public class ValidateDirectoryEmailServiceDelegate:BaseAPIServiceDelegate, IValidateSubscribableEmailServiceDelegate
	{
		public bool success;

		public bool Success {
			get {
				return success;
			}
			set {
				success = value;
			}
		}
		
		public ValidateDirectoryEmailServiceDelegate ()
		{
			this.APIMethodName = "validateDirectoryEmail";
		}
		
		public void ValidateEmail(long directoryID, string submittedEmail, Action action){
			this.action = action;
			this.success = false;
			this.SendRequest("&directoryID="  + directoryID + "&email=" + submittedEmail);
			
			
		}
		
		
		override public void HandleResult(string resultString){
			JObject o = JObject.Parse(resultString);
			this.success = (bool)o["SUCCESS"];
			this.action();
		}
	}
}

