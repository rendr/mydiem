using System;
using MonkeyArms;
using MyDiem.Core.Interfaces;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.BL;
using MyDiemCore;
using Invokers.Notification;
using Invokers.Inputs;

namespace MyDiem.Core.Mediators
{
	public class ProfileViewMediator:Mediator
	{
		[Inject]
		public UserManager UM;
		[Inject]
		public SubscribableManager SM;
		[Inject]
		public StateManager StM;
        [Inject]
        public RegionManager RM;
		[Inject]
		public CountyManager CM;
		[Inject]
		public User AppUser;
		[Inject]
		public LogUserOutRequestInvoker LogoutRequest;
		[Inject]
		public SaveUserToServerInputInvoker SaveUser;
		[Inject]
		public UserSavedOnServerInvoker UserSaved;
		protected IProfileView Target;

		public ProfileViewMediator (IProfileView target) : base (target)
		{
			this.Target = target;
		}

		public override void Register ()
		{
			this.Target.Logout += HandleLogout;
			this.Target.UpdateSubmitted += HandleUpdateSubmitted;
			this.Target.SetUserData (AppUser);
            this.Target.States = StM.States;
			this.Target.Regions = RM.Regions;
			this.Target.Counties = CM.Counties;

			InvokerMap.Add (UserSaved, HandleUpdateProfileFinished, Target);
		}

		public override void Unregister ()
		{
			base.Unregister ();
			this.Target.Logout -= HandleLogout;
			this.Target.UpdateSubmitted -= HandleUpdateSubmitted;
		}

		void HandleUpdateSubmitted (object sender, EventArgs e)
		{
            var userToSave = CreateUpdatedUserFromFormFields();
            AppUser.Update (userToSave);

            var args = new SaveUserToServerInputArgs(AppUser);
            SaveUser.Invoke (args);
		}

        private User CreateUpdatedUserFromFormFields()
        {
            var updatedUser = new User() 
            {
                ID = AppUser.ID,
                IsParent = AppUser.IsParent,
                IsValidated = AppUser.IsValidated,
                CalendarConnect = AppUser.CalendarConnect,
                CarpoolConnect = AppUser.CarpoolConnect,
                SubscriptionUrl = AppUser.SubscriptionUrl,

                Fname = Target.SubmittedFirstName,
                Lname = Target.SubmittedLastName,
                Email = Target.SubmittedEmail,
                RegionID = RM.GetRegionID(Target.SubmittedRegion),
                CountyID = CM.GetCountyID(Target.SubmittedCounty),
                Password = string.IsNullOrEmpty(Target.SubmittedPassword) ? null : Target.SubmittedPassword,
            };

            if (updatedUser.IsParent)
            {
                updatedUser.HidePhone1 = Convert.ToBoolean(Target.SubmittedHidePhone1);
                updatedUser.HidePhone2 = Convert.ToBoolean(Target.SubmittedHidePhone2);
                updatedUser.HideAddress = Convert.ToBoolean(Target.SubmittedHideAddress);
                updatedUser.Phone1 = Target.SubmittedPhone1;
                updatedUser.Phone2 = Target.SubmittedPhone2;
                updatedUser.Street = Target.SubmittedStreet;
                updatedUser.State = Target.SelectedState;
            }

            return updatedUser;
        }


		void HandleLogout (object sender, EventArgs e)
		{
			LogoutRequest.Invoke ();
			Target.GoLogout ();
		}

		void HandleUpdateProfileFinished (object sender, EventArgs e)
		{
			UM.UpdateProfileFinished -= HandleUpdateProfileFinished;
			SM.GetAllSubscribables (AppUser.ID);
			this.Target.ShowUpdateSuccess ();
		}
	}
}

