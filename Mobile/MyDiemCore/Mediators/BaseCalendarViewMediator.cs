using System;
using MonkeyArms;
using MyDiem.Core.ViewModel;
using MyDiem.Core.Interfaces;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.CustomEvents;
using MyDiem.Core.Invokers;
using MyDiem.Core.BL;

namespace MyDiem.Core.Mediators
{
	public class BaseCalendarViewMediator:Mediator
	{
		[Inject]
		public UserManager UM;

		[Inject]
		public EventListViewModel EventListVM{ get; set; }

		[Inject]
		public SubscribableListViewModel SubscribableListVM{ get; set; }

		[Inject]
		public NavToScreenInvoker NavToScreen;
		[Inject]
		public User AppUser;
		protected IBaseCalendarView Map;

		public BaseCalendarViewMediator (IBaseCalendarView target) : base (target)
		{
			Map = target;
		}

		public override void Register ()
		{

			NavToScreen.Invoked += HandleNavToScreenInvokerInvokedEvent;
			EventListVM.GoEventDetailsEvent += HandleGoEventDetailsEvent;

		}

		public override void Unregister ()
		{
			base.Unregister ();
			NavToScreen.Invoked -= HandleNavToScreenInvokerInvokedEvent;
			EventListVM.GoEventDetailsEvent -= HandleGoEventDetailsEvent;


		}

		protected void HandleGoEventDetailsEvent (object sender, EventArgs e)
		{
			Map.GoEventDetailsView (EventListVM.SelectedEvent);	
		}

		protected void HandleNavToScreenInvokerInvokedEvent (object sender, EventArgs e)
		{
			var navToScreenInvokerArgs = e as NavToScreenInvokerArgs;
			switch (navToScreenInvokerArgs.ScreenToNavTo) {
			case NavToScreenEnum.CalendarDayView:
				Map.GoCalendarDayView ();
				break;
			case NavToScreenEnum.CalendarMonthView:
				Map.GoCalendarMonthView ();
				break;

			case NavToScreenEnum.CalendarWeekView:
				Map.GoCalendarWeekView ();
				break;

			case NavToScreenEnum.GoAboutView:
				Map.GoAboutView ();
				break;

			case NavToScreenEnum.GoSubscribablesView:
				SubscribableListVM.SetMainCalendarCategory (navToScreenInvokerArgs.SelectedGroup);
				Map.GoSubscribablesView (navToScreenInvokerArgs.SelectedGroup, navToScreenInvokerArgs.Title);
				break;

			case NavToScreenEnum.GoParentDirectoryView:
				Map.GoParentDirectory ();
				break;

			case NavToScreenEnum.GoSettingsView:
				Map.GoSettingsView (AppUser);
				break;

            case NavToScreenEnum.GoFacebookView:
                Map.GoFacebookDirectory ();
                break;

            case NavToScreenEnum.GoTwitterView:
                Map.GoTwitterDirectory ();
                break;
			}
		}
	}
}

