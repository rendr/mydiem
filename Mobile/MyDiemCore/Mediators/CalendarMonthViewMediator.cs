using System;

using MonkeyArms;
using MyDiem.Core.ViewModel;
using MyDiem.Core.Interfaces;
using MyDiem.Core.Invokers;
using MyDiem.Core.BL.Managers;
using System.Diagnostics;
using CustomEvents;

namespace MyDiem.Core.Mediators
{
	public class CalendarMonthViewMediator:BaseCalendarViewMediator
	{
		[Inject]
		public CalendarMonthViewModel VM{get;set;}

		[Inject]
		public SelectCalendarMonthDateInvoker SelectDateInvoker{get;set;}

	
		[Inject]
		public EventManager EM;

		[Inject]
		public UserManager UM;

		[Inject]
		public EventListViewModel ELVM;

		protected new ICalendarMonthView Target;

		public CalendarMonthViewMediator (ICalendarMonthView target):base(target as IBaseCalendarView)
		{
			this.Target = target;
		}

		public override void Register ()
		{
			base.Register ();

			Target.DateSelected += HandleTargetDateSelected;
			Target.MonthChanged += HandleMonthChanged;

		
				var list = EM.GetEventsForDatesMonth (Target.SelectedDate);
				AddColorsToEvents (ref list);
				Target.MonthEvents = list;


				var events = EM.GetEventsForDate (Target.SelectedDate);
				AddColorsToEvents (ref events);
				Target.SelectedDateEvents = events;

			Target.EventSelected += HandleEventSelected; 

		}

		public override void Unregister ()
		{
			base.Unregister ();
			Target.DateSelected -= HandleTargetDateSelected;
			Target.MonthChanged -= HandleMonthChanged;
			Target.EventSelected -= HandleEventSelected; 
		}


		void HandleEventSelected (object sender, EventArgs e)
		{
			ELVM.GoEventDetails((e as SelectedCalendarEventArgs).SelectedEvent);
		}

		void HandleMonthChanged (object sender, EventArgs e)
		{
			var list = EM.GetEventsForDatesMonth (Target.SelectedMonth);
			AddColorsToEvents (ref list);
			Target.MonthEvents = list;
		}


		void AddColorsToEvents (ref System.Collections.Generic.List<MyDiem.Core.BL.CalendarEvent> list)
		{
			foreach (var calendarEvent in list) {
				calendarEvent.SwatchColorString = UM.GetSubscriptionForCalendar ((int)calendarEvent.CalendarID).Color;
			}
		}


		//Target handlers
		protected void HandleTargetDateSelected (object sender, EventArgs e)
		{
			var d = Target.SelectedDate;
			var events = EM.GetEventsForDate (d);
			AddColorsToEvents (ref events);
			Target.SelectedDateEvents = events;
		}



	}
}

