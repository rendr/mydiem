using System;
using MonkeyArms;
using MyDiem.Core.Interfaces;
using System.Diagnostics;

namespace MyDiem.Core.Mediators
{
	public class HomeViewMediator:Mediator
	{
		protected new IHomeView Target;

		public HomeViewMediator (IHomeView target):base(target)
		{
			this.Target = target;
		}

		public override void Register ()
		{
			Target.Login += HandleLogin;
            Target.Register += HandleRegister;
            Target.Reset += HandleReset;
		}

        public override void Unregister ()
        {
            Target.Login -= HandleLogin;
            Target.Register -= HandleRegister;
            Target.Reset -= HandleReset;
        }

        private void HandleRegister (object sender, EventArgs e)
		{
			Target.GoRegister ();
		}

		private void HandleLogin (object sender, EventArgs e)
		{
			Target.GoLogin ();
		}

        private void HandleReset (object sender, EventArgs e)
        {
            Target.GoReset ();
        }
	}
}

