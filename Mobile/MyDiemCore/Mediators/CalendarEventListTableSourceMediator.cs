using System;

using MonkeyArms;
using MyDiem.Core.ViewModel;
using MyDiem.Core.Interfaces;
using CustomEvents;

namespace MyDiem.Core.Mediators
{
	public class CalendarEventListTableSourceMediator:Mediator
	{
		[Inject]
		public EventListViewModel VM;

		protected ICalendarEventListTableSource Target;

		public CalendarEventListTableSourceMediator (ICalendarEventListTableSource target):base(target)
		{
			this.Target = target;
		}

		public override void Register ()
		{
			Target.Init(VM.InitDaysDictionary(Target.Events), VM.TodaySectionIndex, VM.TodayRowIndex);
			Target.EventSelected += HandleEventSelected;
		}

		public override void Unregister ()
		{
			Target.EventSelected -= HandleEventSelected;
		}

		protected void HandleEventSelected (object sender, EventArgs e)
		{
			VM.GoEventDetails ((e as SelectedCalendarEventArgs).SelectedEvent);

		}
	}
}

