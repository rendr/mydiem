using System;
using MonkeyArms;
using MyDiem.Core.ViewModel;
using MyDiem.Core.Interfaces;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.BL;

namespace MyDiem.Core.Mediators
{
	public class SubscribableListViewMediator:BaseCalendarViewMediator
	{

		[Inject]
		public FilterMenuViewModel FilterMenuVM;

		[Inject]
		public SubscribableManager SM;

		[Inject]
		public UserManager UM;
	
		protected new ISubscribableListView Target;

		public SubscribableListViewMediator (ISubscribableListView target) : base (target)
		{
			this.Target = target;
		}

		public override void Register ()
		{
			base.Register ();


			SubscribableListVM.SubscribableSelected += HandleSubscribableSelected;
			SubscribableListVM.SubscribablesChanged += HandleSubscribablesChanged;
			SubscribableListVM.PromptForSubscribablePassword += HandlePrompForSubscribablePassword;
			SubscribableListVM.SubscribablePasswordFailed += HandleSubscribablePasswordFailed;
			SubscribableListVM.SubscribableEmailFailed += HandleSubscribableEmailFailed;
			SubscribableListVM.PromptForSubscribablePurchase += HandlePromptForSubscribablePurchase;
			SubscribableListVM.PromptForSubscribableEmail += HandlePromptForSubscribableEmail;

			Target.ResetSubscribables += HandleResetSubscribables;
			Target.FilterSubscribables += HandleFilterSubscribables;
			Target.PasswordSubmitted += HandlePasswordSubmitted;
			Target.EmailSubmitted += HandleEmailSubmitted;
			Target.SubscribableSelected += HandleTableItemSelected;

			//init actions
			SM.GetAllSubscribablesFinished += HandleGetAllSubscribablesFinished;

			Target.SearchPlaceHolderText = SubscribableListVM.SEARCH_PLACEHOLDER;

		

			Target.UpdateSubscribablesList (SubscribableListVM.SchoolGroups, UM.GetCalendarIDs (), UM.GetDirectoryIDs ());
		}


		public override void Unregister ()
		{
			base.Unregister ();
			

			SubscribableListVM.SubscribableSelected -= HandleSubscribableSelected;
			SubscribableListVM.SubscribablesChanged -= HandleSubscribablesChanged;
			SubscribableListVM.PromptForSubscribablePassword -= HandlePrompForSubscribablePassword;
			SubscribableListVM.SubscribablePasswordFailed -= HandleSubscribablePasswordFailed;
			SubscribableListVM.SubscribableEmailFailed -= HandleSubscribableEmailFailed;
			SubscribableListVM.PromptForSubscribablePurchase -= HandlePromptForSubscribablePurchase;
			SubscribableListVM.PromptForSubscribableEmail -= HandlePromptForSubscribableEmail;
			
			Target.ResetSubscribables -= HandleResetSubscribables;
			Target.FilterSubscribables -= HandleFilterSubscribables;
			Target.PasswordSubmitted -= HandlePasswordSubmitted;
			Target.EmailSubmitted -= HandleEmailSubmitted;
			Target.SubscribableSelected -= HandleTableItemSelected;
		}

		void HandleGetAllSubscribablesFinished (object sender, EventArgs e)
		{
			SubscribableListVM.InitCalendars ();
		}

		protected void HandleEmailSubmitted (object sender, EventArgs e)
		{
			SubscribableListVM.SubmitSubscribableEmail (Target.SubmittedEmail);
		}

		protected void HandlePasswordSubmitted (object sender, EventArgs e)
		{
			SubscribableListVM.SubmitSubscribablePassword (Target.SubmittedPassword);
		}

		protected void HandleFilterSubscribables (object sender, EventArgs e)
		{
			SubscribableListVM.FilterCalendars (Target.SearchQuery);
		}

		protected void HandleResetSubscribables (object sender, EventArgs e)
		{
			SubscribableListVM.ResetCalendars ();
		}

		protected void HandleSubscribablesChanged (object sender, EventArgs e)
		{
			Target.UpdateSubscribablesList (SubscribableListVM.SchoolGroups, UM.GetCalendarIDs (), UM.GetDirectoryIDs ());
		}

		protected void HandlePrompForSubscribablePassword (object sender, EventArgs e)
		{
			Target.PromptForSubscribablePassword ();
		}

		protected void HandleSubscribableSelected (object sender, EventArgs e)
		{
			var subscribable = SubscribableListVM.SelectedSubscribable;
			if (subscribable.type == Subscribable.TYPE_CALENDAR) {
				Target.HandleSubscribableSelected (SubscribableListVM.SelectedSubscribable, UM.GetSubscriptionForCalendar (subscribable.ID));
			}
			if (subscribable.type == Subscribable.TYPE_DIRECTORY) {
				Target.HandleSubscribableSelected (SubscribableListVM.SelectedSubscribable, UM.GetSubscriptionForDirectory (subscribable.ID));
			}

		}

		protected void HandleSubscribablePasswordFailed (object sender, EventArgs e)
		{
			Target.SubscribablePasswordFailed ();
		}

		protected void HandleSubscribableEmailFailed (object sender, EventArgs e)
		{
			Target.HandleSubscribableEmailFailed ();
		}

		protected void HandlePromptForSubscribablePurchase (object sender, EventArgs e)
		{
			Target.PromptForSubscribablePurchase (SubscribableListVM.SelectedSubscribable.sku);
		}

		protected void HandlePromptForSubscribableEmail (object sender, EventArgs e)
		{

		}



		protected void HandleTableItemSelected (object sender, EventArgs e)
		{
			var subscribable = Target.SelectedSubscribable;
			if (subscribable.isEmailProtected) {
				Target.ShowWhiteListPrompt ();
			} else if (SubscribableListViewModel.UserDoesNotHaveAccess (UM.GetCalendarIDs (), UM.GetDirectoryIDs (), subscribable)) {
				SubscribableListVM.SelectPasswordProtectedSubscribable (subscribable);
			} else if (subscribable.isPremium && !UM.IsCalendarUsers (subscribable.ID)) {
				SubscribableListVM.SelectPremiumSubscribable (subscribable);
			} else {
				SubscribableListVM.SelectSubscribable (subscribable);
			}
			
			
		}


	}
}

