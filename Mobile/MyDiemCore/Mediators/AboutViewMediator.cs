using System;

using MonkeyArms;

using MyDiem.Core.Interfaces;
using MyDiem.Core.ViewModel;

namespace MyDiem.Core.Mediators
{
	public class AboutViewMediator:BaseCalendarViewMediator
	{
		[Inject]
		public AboutViewModel VM{get;set;}

		protected new IAboutView Target;

		public AboutViewMediator (IAboutView target):base(target as IBaseCalendarView)
		{
			this.Target = target;
		}

		public override void Register ()
		{
			base.Register ();

		}

		public override void Unregister ()
		{
			base.Unregister ();
		}
	}
}

