using System;
using MonkeyArms;
using MyDiem.Core.Interfaces;
using MyDiem.Core.ViewModel;

namespace MyDiem.Core.Mediators
{
	public class ParentDetailsViewMediator:BaseCalendarViewMediator
	{
		[Inject]
		public DirectoryViewModel VM;

		protected new IParentDetailsView Target;

		public ParentDetailsViewMediator (IParentDetailsView target):base(target as IBaseCalendarView)
		{
			this.Target = target;
		}

		public override void Register ()
		{
			base.Register ();
			Target.ScreenTitle = String.Format ("{0}, {1}", VM.SelectedParent.LastName, VM.SelectedParent.FirstName);
			Target.UpdateTableSource(VM.SelectedParent);
		}
	}
}

