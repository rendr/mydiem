﻿using System;
using MonkeyArms;
using MyDiemCore;

namespace MyDiem.Core.Mediators
{
    public class ResetPasswordMediator : Mediator
    {
        [Inject]
        public ResetPasswordRequestInvoker ResetPasswordRequest;

        [Inject]
        public ResetPasswordOutputInvoker OutputInvoker;

        protected IResetPasswordView Target;

        public ResetPasswordMediator(IResetPasswordView target) : base(target)
        {
            this.Target = target;
        }

        public override void Register ()
        {
            InvokerMap.Add (OutputInvoker, HandleReply, Target);
            Target.ResetSubmitted += HandleResetSubmitted;
        }

        void HandleResetSubmitted (object sender, EventArgs e)
        {
            ResetPasswordRequest.Invoke (new ResetPasswordRequestInvokerEventArgs (Target.SubmittedEmail));
        }

        void HandleReply (object sender, EventArgs e)
        {
            var args = e as ResetPasswordOutputArgs;
            Target.ShowMessage (args.Message);
        }
   }
}

