using System;
using MonkeyArms;
using MyDiem.Core.Interfaces;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.BL.Managers.Events;

namespace MyDiem.Core.Mediators
{
	public class RegisterViewMediator:Mediator
	{
		[Inject]
		public UserManager UM;

		[Inject]
		public RegionManager RM;

		[Inject]
		public CountyManager CM;

        [Inject]
        public StateManager StM;

		protected IRegisterView Target;

		public RegisterViewMediator (IRegisterView target):base(target)
		{
			this.Target = target;
		}

		public override void Register ()
		{
			UM.RegisterUserFinished += HandleRegisterUserFinished;

			Target.RegistrationSubmitted += HandleRegistrationSubmitted;
			Target.Regions = RM.Regions;
            Target.Counties = CM.Counties;
            Target.States = StM.States;
		}

		public override void Unregister ()
		{
			UM.RegisterUserFinished -= HandleRegisterUserFinished;
			
			Target.RegistrationSubmitted -= HandleRegistrationSubmitted;
		}

		void HandleRegistrationSubmitted (object sender, EventArgs e)
		{
			UM.Register(Target.SubmittedFirstName, 
			            Target.SubmittedLastName, 
			            Target.SubmittedEmail, 
			            Target.SubmittedZip, 
			            Target.SubmittedPassword, 
			            Target.SubmittedPasswordVerify, 
			            RM.GetRegionID(Target.SubmittedRegionTitle), 
			            CM.GetCountyID(Target.SubmittedCountyTitle));
		}

		void HandleRegisterUserFinished (object sender, EventArgs e)
		{
			var args = e as RegisterUserFinishedEvent;
			if(args.success){
				Target.HandleSuccess(args.message);
			}else{
				Target.ShowRegistrationError(args.message);
			}
		}
	}
}

