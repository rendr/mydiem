using System;
using MonkeyArms;
using MyDiem.Core.Interfaces;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.ViewModel;
using CustomEvents;

namespace MyDiem.Core.Mediators
{
	public class CalendarWeekViewMediator:BaseCalendarViewMediator
	{
		[Inject]
		public EventManager EM;
		[Inject]
		public EventListViewModel ELVM;
		protected ICalendarWeekView WeekView;

		public CalendarWeekViewMediator (ICalendarWeekView target) : base (target as IBaseCalendarView)
		{
			this.WeekView = target;
		}

		public override void Register ()
		{
			base.Register ();
			WeekView.SelectedDateChanged += HandleDateSelected;
			WeekView.WeekDateChanged += HandleWeekDateChanged;
			WeekView.EventSelected += HandleEventSelected;
			var list = EM.GetEventsForDatesWeek (WeekView.WeekDate);
			AddColorsToEvents (ref list);
			WeekView.WeekEvents = list;
		}

		public override void Unregister ()
		{
			base.Unregister ();
			WeekView.SelectedDateChanged -= HandleDateSelected;
			WeekView.WeekDateChanged -= HandleWeekDateChanged;
			WeekView.EventSelected -= HandleEventSelected;
		}

		void HandleEventSelected (object sender, EventArgs e)
		{
			ELVM.GoEventDetails ((e as SelectedCalendarEventArgs).SelectedEvent);
		}

		void HandleWeekDateChanged (object sender, EventArgs e)
		{
			var list = EM.GetEventsForDatesWeek (WeekView.WeekDate);
			AddColorsToEvents (ref list);
			WeekView.WeekEvents = list;
		}

		void HandleDateSelected (object sender, EventArgs e)
		{
			var list = EM.GetEventsForDate (WeekView.SelectedDate);
			AddColorsToEvents (ref list);
			WeekView.SelectedDateEvents = list;
		}

		void AddColorsToEvents (ref System.Collections.Generic.List<MyDiem.Core.BL.CalendarEvent> list)
		{
			foreach (var calendarEvent in list) {
				calendarEvent.SwatchColorString = UM.GetSubscriptionForCalendar ((int)calendarEvent.CalendarID).Color;
			}
		}
	}
}

