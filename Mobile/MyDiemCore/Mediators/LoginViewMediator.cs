using System;
using System.Diagnostics;
using MonkeyArms;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.BL.Managers.Events;
using MyDiem.Core.Interfaces;
using MyDiem.Core.SAL;
using MyDiemCore;
using MyDiem.Core.BL;

namespace MyDiem.Core.Mediators
{
	public class LoginViewMediator:Mediator
	{
		[Inject]
		public AuthenticationOutputInvoker OutputInvoker;
		[Inject]
		public UserNotValidatedNotificationInvoker NotValidatedInvoker;
		[Inject]
		public AuthenticationInputInvoker AuthenticateInvoker;
		[Inject]
		public UserManager UM;
		[Inject]
		public EventManager EM;
		[Inject]
		public UpdateUserFromServerRequestInvoker UpdateRequest;
		[Inject]
		public UserUpdatedFromServerNotificationInvoker UserUpdatedNotification;
		[Inject]
		public User AppUser;
		protected ILoginView Target;

		public LoginViewMediator (ILoginView target) : base (target)
		{
			this.Target = target;
		}

		public override void Register ()
		{
			InvokerMap.Add (OutputInvoker, HandleUserManagerAuthenticateUserFinished, Target);
			InvokerMap.Add (AuthenticateInvoker, HandleUserManagerAuthenticateUserStarted, Target);
			InvokerMap.Add (NotValidatedInvoker, HandleUserNotValidated, Target);
			InvokerMap.Add (UserUpdatedNotification, HandleUserUpdated, Target);
			InvokerMap.Add (EM.GetEventsForCalendarsFinished, HandleGetEventsForCalendarsFinished, Target);




			Target.LoginSubmitted += HandleLoginSubmitted;

			BaseAPIServiceDelegate.ServiceError += HandleServiceError;

		}

		public override void Unregister ()
		{
			base.Unregister ();
			Target.LoginSubmitted -= HandleLoginSubmitted;
			
			BaseAPIServiceDelegate.ServiceError -= HandleServiceError;
		}
		/*
		 * HANDLERS
		 */
		void HandleServiceError (object sender, EventArgs e)
		{
			Target.EnableControls ();
		}

		void HandleLoginSubmitted (object sender, EventArgs e)
		{
			Debug.WriteLine ("HandleLoginSubmitted");
			AuthenticateInvoker.Invoke (new AuthenticationInvokerArgs (Target.SubmittedEmail, Target.SubmittedPassword));

		}

		void HandleUserManagerAuthenticateUserFinished (object sender, EventArgs e)
		{

			Debug.WriteLine ("HandleUserManagerAuthenticateUserFinished");
			var args = e as AuthenticationOutputArgs;
			if (args.Success) {
				UpdateRequest.Invoke (new RequestInvokerArgs (AppUser.ID));
			} else {
				Target.ShowBadPasswordPrompt ();
				Target.EnableControls ();
			}

		}

		void HandleUserManagerAuthenticateUserStarted (object sender, EventArgs e)
		{
			Target.DisableUIAndShowNetworkIndicator ();
		}

		void HandleUserUpdated (object sender, EventArgs e)
		{
			EM.GetEventsForCalendars (UM.GetCalendarIDs ());
		}

		void HandleUserNotValidated (object sender, EventArgs e)
		{
			Target.PromptInvalidatedUser ();
			Target.EnableControls ();
		}

		void HandleGetEventsForCalendarsFinished (object sender, EventArgs e)
		{
			Target.GoCalendar ();
		}
	}
}

