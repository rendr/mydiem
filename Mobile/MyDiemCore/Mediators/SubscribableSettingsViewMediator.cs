using System;
using MyDiem.Core.Interfaces;
using MyDiem.Core.BL.Managers;
using MonkeyArms;
using MyDiem.Core.ViewModel;
using MyDiem.Core.BL;
using MyDiem.Core.Invokers;

namespace MyDiem.Core.Mediators
{
	public class SubscribableSettingsViewMediator:BaseCalendarViewMediator
	{
		[Inject]
		public CalendarColorManager CCM;
		[Inject]
		public SubscribableListViewModel SVM;
		[Inject]
		public SubscribableManager SM;
		[Inject]
		public UserManager UM;
		[Inject]
		public EventManager EM;
		[Inject]
		public ResolveSlideMenuRowTouchedInvoker NavInvoker;
		[Inject]
		public User AppUser;
		protected new ISubscribableSettingsView Target;

		public SubscribableSettingsViewMediator (ISubscribableSettingsView target) : base (target)
		{
			this.Target = target;


		}

		public override void Register ()
		{
			base.Register ();


			Target.SelectedSubscribable = SVM.SelectedSubscribable;
			Target.SelectedSubscription = UM.GetSubscriptionForCalendar (SVM.SelectedSubscribable.ID);

			Target.SubscriptionChanged += HandleSubscriptionChanged;

			UM.UpdateUserCalendarSubscriptionFinished += HandleSubscriptionUpdateFinished;


			if (CCM.Colors == null) {
				CCM.GetColorsFinished += HandleGetColorsFinished;
				CCM.GetAllColors ();
			} else {
				Target.Colors = CCM.Colors;
			}

		}

		void HandleSubscriptionUpdateFinished (object sender, EventArgs e)
		{
			Target.ShowUpdatedPrompt ();
		}

		public override void Unregister ()
		{
			base.Unregister ();
			Target.SubscriptionChanged -= HandleSubscriptionChanged;
			UM.UpdateUserCalendarSubscriptionFinished -= HandleSubscriptionUpdateFinished;
		}

		void HandleSubscriptionChanged (object sender, EventArgs e)
		{
			if (Target.SelectedSubscribable.type == Subscribable.TYPE_DIRECTORY) {
				UM.UpdateUserDirectorySubscription (AppUser.ID, Target.SelectedSubscribable.ID, Target.SelectedSubscription);
			}
			if (Target.SelectedSubscribable.type == Subscribable.TYPE_CALENDAR) {
				UM.UpdateUserCalendarSubscription (AppUser.ID, Target.SelectedSubscription, Target.SelectedColorID);

				RemoveSubscribableFromSubscribablesListVM ();


				EM.GetEventsForCalendars (UM.GetCalendarIDs ());
			}
			Target.ShowUpdatingPrompt ();
		}

		void RemoveSubscribableFromSubscribablesListVM ()
		{
			if (!Target.SelectedSubscription.Enabled) {
				foreach (var schoolGroup in SubscribableListVM.SchoolGroups) {
					foreach (var subscribable in schoolGroup.Subscribables) {
						if (subscribable.ID == Target.SelectedSubscription.SubscribableID) {
							schoolGroup.Subscribables.Remove (subscribable);
							break;
						}
					}
				}
			}
		}

		protected void HandleGetColorsFinished (object sender, EventArgs e)
		{
			Target.Colors = CCM.Colors;
		}
	}
}

