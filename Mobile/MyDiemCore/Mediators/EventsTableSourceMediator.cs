using System;
using MonkeyArms;
using MyDiem.Core.ViewModel;
using MyDiem.Core.Interfaces;
using CustomEvents;

namespace MyDiem.Core.Mediators
{
	public class EventsTableSourceMediator:Mediator
	{
		[Inject]
		public EventListViewModel VM;

		protected IEventsTableSource Target;

		public EventsTableSourceMediator (IEventsTableSource target):base(target)
		{
			this.Target = target;
		}

		public override void Register ()
		{
			Target.EventSelected += HandleRowSelected;

			VM.InitDaysDictionary(Target.TableItems);
		}

		public override void Unregister ()
		{
			Target.EventSelected -= HandleRowSelected;
		}

		protected void HandleRowSelected (object sender, EventArgs e)
		{
			VM.GoEventDetails ((e as SelectedCalendarEventArgs).SelectedEvent);
		}
	}
}

