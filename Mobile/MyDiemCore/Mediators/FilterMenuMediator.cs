using System;
using MonkeyArms;
using MyDiem.Core.ViewModel;
using MyDiem.Core.Interfaces;
using MyDiem.Core.BL.Managers;
using System.Collections.Generic;

namespace MyDiem.Core.Mediators
{
	public class FilterMenuMediator:Mediator
	{
		[Inject]
		public SubscribableListViewModel ViewModel;
		[Inject]
		public RegionManager RM;
		[Inject]
		public CountyManager CM;
		protected IFilterMenu Target;

		public FilterMenuMediator (IFilterMenu target) : base (target)
		{
			this.Target = target;
		}

		public override void Register ()
		{


			Target.Regions = RM.Regions;
			Target.Counties = CM.Counties;
			Target.Types = new List<String> () { "Any", "Calendar", "Directory" };
			Target.Categories = ViewModel.GetCategories ();

			Target.CountySelected += HandleCountySelected;
			Target.RegionSelected += HandleRegionSelected;
			Target.TypeSelected += HandleTypeSelected;
			Target.CategorySelected += HandleCategorySelected;
		}

		void HandleCategorySelected (object sender, EventArgs e)
		{
			ViewModel.SelectedCategories = Target.SelectedCategories;
			UpdateViewModel ();
		}

		public override void Unregister ()
		{
			Target.CountySelected -= HandleCountySelected;
			Target.RegionSelected -= HandleRegionSelected;
			Target.TypeSelected -= HandleTypeSelected;
			Target.CategorySelected -= HandleCategorySelected;
		}

		void HandleTypeSelected (object sender, EventArgs e)
		{
			ViewModel.SelectedFilterType = Target.SelectedType;

			UpdateViewModel ();
		}

		void HandleRegionSelected (object sender, EventArgs e)
		{
			UpdateViewModel ();
		}

		void HandleCountySelected (object sender, EventArgs e)
		{
			UpdateViewModel ();
		}

		void UpdateViewModel ()
		{
			var countyID = ViewModel.AppUser.CountyID;
			if (Target.SelectedCounty != null) {
				countyID = Target.SelectedCounty.ID;
			}

			var regionID = ViewModel.AppUser.RegionID;
			if (Target.SelectedRegion != null) {
				regionID = Target.SelectedRegion.ID;
			}
			ViewModel.GetAllSubscribables (regionID, countyID);
		}
	}
}

