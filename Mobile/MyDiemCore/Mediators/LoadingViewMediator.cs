using System;
using System.Diagnostics;
using MonkeyArms;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.BL.Managers.Events;
using MyDiem.Core.Interfaces;
using MyDiem.Core.SAL;
using MyDiem.Core.Invokers;

namespace MyDiem.Core.Mediators
{
	public class LoadingViewMediator:Mediator
	{
		[Inject]
		public NavToScreenInvoker NavToScreenInvoker;
		protected ILoadingView Target;

		public LoadingViewMediator (ILoadingView target) : base (target)
		{
			this.Target = target;
		}

		public override void Register ()
		{
			InvokerMap.Add (NavToScreenInvoker, HandleInvoked, Target);

		}

		void HandleInvoked (object sender, EventArgs e)
		{
			var invokerArgs = e as NavToScreenInvokerArgs;
			if (invokerArgs.ScreenToNavTo == NavToScreenEnum.CalendarMonthView) {
				Target.GoMainView ();
			}
		}

		public override void Unregister ()
		{

			base.Unregister ();
		}
	}
}

