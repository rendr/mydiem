using System;
using System.Diagnostics;
using System.Collections.Generic;
using MonkeyArms;
using MyDiem.Core.ViewModel;
using MyDiem.Core.Interfaces;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.Invokers;
using System.Linq;

namespace MyDiem.Core.Mediators
{
	public class SettingsMenuTableSourceMediator:Mediator
	{
		[Inject]
		public SlideMenuViewModel VM{ get; set; }

		[Inject]
		public ResolveSlideMenuRowTouchedInvoker RowTouchedInvoker{ get; set; }

		[Inject]
		public User AppUser;
		protected ISettingsMenuTableSource TableSource;

		public SettingsMenuTableSourceMediator (ISettingsMenuTableSource target) : base (target)
		{
			TableSource = target;

		}

		public override void Register ()
		{
			InvokerMap.Add (TableSource.SettingsRowSelected, HandleSettingsRowSelected);
			InvokerMap.Add (VM.CalendarCategoriesChangedEvent, HandleCalendarCategoriesChangedEvent);
			TableSource.IsCalendarConnectPurchased = AppUser.CalendarConnect;
			 

			if (VM.CalendarCategories != null) {
				TableSource.EnableSubscribableOptions ();
			}

		}

		void HandleCalendarCategoriesChangedEvent (object sender, EventArgs e)
		{
			TableSource.EnableSubscribableOptions ();
		}

		protected void HandleSettingsRowSelected (object sender, EventArgs e)
		{

			Dictionary<string, List<string>> Options = VM.OptionsDictionary;
			string[] keys = Options.Keys.ToArray ();
			RowTouchedInvoker.Invoke (new ResolveSlideMenuRowTouchedInvokerArgs (Options [keys [TableSource.SelectedSectionIndex]] [TableSource.SelectedRowIndex]));

		}
	}
}

