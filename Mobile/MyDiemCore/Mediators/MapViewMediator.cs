﻿using System;
using MonkeyArms;
using MyDiem.Core.Interfaces;
using MyDiem.Core.ViewModel;
using System.Linq;

namespace MyDiem.Core.Mediators
{
	public class MapViewMediator:Mediator
	{
		[Inject]
		public DirectoryViewModel VM;

		protected IMapView Map;

		public MapViewMediator (IMapView target) : base (target as IMediatorTarget)
		{
			this.Map = target;

		}


		public override void Register ()
		{

			this.Map.Contacts = VM.Contacts;
			try {
				this.Map.OriginUser = VM.Contacts.First (contact => contact.Email == VM.AppUser.Email);
			} catch {
				this.Map.OriginUser = VM.Contacts [0];
			}

			this.Map.ParentSelected += HandleParentSelected;

		}

		public override void Unregister ()
		{
			base.Unregister ();
			this.Map.ParentSelected -= HandleParentSelected;
		}

		void HandleParentSelected (object sender, EventArgs e)
		{
			this.VM.SelectParent (Map.SelectedParent);
			Map.GoParentDetails ();
		}
	}
}

