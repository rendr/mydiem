using System;
using MonkeyArms;
using MyDiem.Core.Interfaces;
using MyDiem.Core.ViewModel;
using MyDiem.Core.Invokers;

namespace MyDiem.Core.Mediators
{
    public class ParentDirectoryViewMediator : BaseCalendarViewMediator
    {
        [Inject]
        public DirectoryViewModel VM;
        protected new IParentDirectoryView Target;

        public ParentDirectoryViewMediator (IParentDirectoryView target) : base (target)
        {
            this.Target = target;
        }

        public override void Register ()
        {
            base.Register ();

            //if (VM.Contacts == null || VM.Contacts.Count == 0) {
            VM.GetContacts ();
            //} else {
            //ShowContacts ();
            //}

            Target.SearchPlaceHolderText = VM.SEARCH_PLACEHOLDER;

            InvokerMap.Add (VM.ContactsUpdated, (object sender, EventArgs args) => ShowContacts ());
            InvokerMap.Add (Target.Search, HandleSearch);
            InvokerMap.Add (Target.Reset, (object sender, EventArgs args) => VM.ResetContacts ());
            InvokerMap.Add (Target.ParentSelected, HandleParentSelected);

        }

        protected void HandleSearch (object sender, EventArgs args)
        {
            VM.FilterContacts (Target.SearchQuery);

        }

        protected void HandleParentSelected (object sender, EventArgs e)
        {
            VM.SelectParent (Target.SelectedParent);
            Target.GoParentDetails ();
        }
        //VM Listeners
        protected void HandleContactsUpdated (object sender, EventArgs e)
        {
            ShowContacts ();
        }

        void ShowContacts ()
        {
            Target.UpdateContacts (VM.Contacts);
            Target.HideLoadingIndicator ();
            if (VM.Contacts.Count == 0 && Target.SearchQuery == String.Empty) {
                Target.ShowNotSubscribedPrompt ();
            } else if (VM.Contacts.Count == 0 && Target.SearchQuery != String.Empty) {
                Target.ShowNoMatchesPrompt ();
            }
        }
    }
}

