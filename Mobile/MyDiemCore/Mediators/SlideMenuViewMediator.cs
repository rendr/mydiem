using System;
using System.Diagnostics;
using MonkeyArms;
using MyDiem.Core.ViewModel;
using MyDiem.Core.Interfaces;

namespace MyDiem.Core.Mediators
{
	public class SlideMenuViewMediator:Mediator
	{
		[Inject]
		public SlideMenuViewModel VM{ get; set; }

		protected ISlideMenuView Target;

		public SlideMenuViewMediator (IMediatorTarget target) : base (target)
		{
			Target = target as ISlideMenuView;
		}

		public override void Register ()
		{

			InvokerMap.Add (VM.CalendarCategoriesChangedEvent, HandleCalendarCategoriesChangedEvent, Target);


			if (VM.CalendarCategories != null) {
				Target.UpdateOptionsTableSource (VM.CalendarCategories);
				Target.Enable ();
			}
		}

		protected void HandleCalendarCategoriesChangedEvent (object sender, EventArgs e)
		{
			Debug.WriteLine ("HandleCalendarCategoriesChangedEvent mediator");
			if (VM.CalendarCategories == null || VM.CalendarCategories.Count == 0) {
				return;
			}
			Target.UpdateOptionsTableSource (VM.CalendarCategories);
			Target.Enable ();
		}
	}
}

