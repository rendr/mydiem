using System;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace MyDiem.Core.BL
{
	public class Parent
	{
        public string FirstName, LastName, State, Zip, Address, ID, Phone1, Phone2, City, Email, SearchKeywords, DirectoryTitle, PhotoUrl;
        public List<Child> Children;
		public static string NOT_LISTED = "Not Listed";
		public double Lat, Long;
        public bool HidePhone1, HidePhone2, HideAddress;

		public string ChildrenNames{
			get{
				return String.Join (", ", Children.Select (c => (c.FirstName + " " + c.LastName)).ToList ().ToArray ());
			}
		}

		public string LastFirstName{
			get{
				return String.Format ("{0}, {1}", LastName, FirstName);
			}
		}

		public Parent ()
		{
		}

		public static Parent FromJObj (JObject obj, string directoryTitle)
		{
			Parent p = new Parent ();
            p.DirectoryTitle = directoryTitle;

            p.ID = (string)obj ["ID"];
			p.FirstName = (string)obj ["FIRSTNAME"];
            p.LastName = (string)obj ["LASTNAME"];
            p.SearchKeywords = p.FirstName + "," + p.LastName;
			
            p.HideAddress = Convert.ToBoolean((int)obj["HIDEADDRESS"]);
            p.Address = (string)obj ["ADDRESS"];
            p.City = (string)obj ["CITY"];
            p.Zip = (string)obj ["ZIP"];
            p.State = (string)obj ["STATE"];

            p.HidePhone1 = Convert.ToBoolean((int)obj["HIDEPHONE1"]);
            p.HidePhone2 = Convert.ToBoolean((int)obj["HIDEPHONE2"]);
            p.Phone1 = FormatPhoneNumber((string)obj["PHONE1"], p.HidePhone1);
            p.Phone2 = FormatPhoneNumber((string)obj["PHONE2"], p.HidePhone2);

            p.PhotoUrl = (string)obj ["PHOTOURL"];

            p.Email = (string)obj ["EMAIL"];
			if (String.IsNullOrEmpty (p.Email)) {
				p.Email = NOT_LISTED;
			}

			try {
				p.Lat = Double.Parse ((string)obj ["LAT"]);
				p.Long = Double.Parse ((string)obj ["LONG"]);
			} catch {
				Debug.WriteLine ("Parent doesn't have LAT/LONG");
			}

			JArray children = (JArray)obj ["CHILDREN"];
			p.Children = new List<Child> ();

			foreach (JObject c in children) {
				var ch = Child.FromJObj (c);
				p.Children.Add (ch);
				p.SearchKeywords += "," + ch.FirstName + "," + ch.LastName + "," + ch.Grade + "," + p.Zip;
			}

			return p;
		}

        private static string FormatPhoneNumber(string phoneNumber, bool hidePhone)
		{
			phoneNumber = phoneNumber.Trim ();
            if (string.IsNullOrEmpty(phoneNumber) || hidePhone) {
				return NOT_LISTED;
			}
			var cleanPhoneNumber = Regex.Replace (phoneNumber, "[^.0-9]", "");
			return String.Format ("{0:(###) ###-####}", long.Parse (cleanPhoneNumber));
		}

		public static string FormatNumberForIOSCall (string phoneNumber)
		{
			phoneNumber = phoneNumber.Trim ();
			if (string.IsNullOrEmpty (phoneNumber)) {
				return NOT_LISTED;
			}
			var cleanPhoneNumber = Regex.Replace (phoneNumber, "[^.0-9]", "");
			return String.Format ("{0:###-###-####}", long.Parse (cleanPhoneNumber));
		}
	}
}