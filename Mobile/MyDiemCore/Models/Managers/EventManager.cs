using System;
using System.Diagnostics;
using System.Collections.Generic;
using MyDiem.Core.SAL;
using System.Linq;
using MyDiemCore;
using MonkeyArms;

namespace MyDiem.Core.BL.Managers
{
	public class EventManager
	{
		public event EventHandler GetEventsForCalendarsStarted = delegate{};

		private Invoker getEventsForCalendarsFinished = new Invoker ();

		public Invoker GetEventsForCalendarsFinished {
			get {
				return getEventsForCalendarsFinished;
			}
		}

		public  List<CalendarEvent> CalendarEvents;

		public  void GetEventsForCalendars (int[] calendarIDs)
		{
			Debug.WriteLine ("GetEventsForCalendars");
			GetEventsForCalendarsStarted (null, new EventArgs ());
			GetCalendarsWithEventsServiceDelegate del = new GetCalendarsWithEventsServiceDelegate ();
			del.GetCalendarsAndEvents (calendarIDs, delegate {
				CalendarEvents = del.CalendarEvents;
				GetEventsForCalendarsFinished.Invoke ();
			});
		}

		public  List<CalendarEvent> GetEventsForDatesWeek (DateTime date)
		{

			DateTime startDate = date.AddDays (0 - (int)date.DayOfWeek);
			DateTime endDate = date.AddDays (7 - (int)date.DayOfWeek);
			return (CalendarEvents.Where (ce => (DateBetween (startDate, endDate, ce.StartDateTime) || DateBetween (startDate, endDate, ce.EndDateTime)))).OrderBy (ce => ce.StartDateTime).ToList ();
		}

		public  List<CalendarEvent> GetEventsForDate (DateTime date)
		{
			return (CalendarEvents.Where (ce => (DatesMatch (date.Date, ce.StartDateTime) || DatesMatch (ce.EndDateTime, date.Date) || DateBetween (ce.StartDateTime, ce.EndDateTime, date)))).OrderBy (ce => ce.StartDateTime).ToList ();
		}

		public  List<CalendarEvent> GetEventsForDatesMonth (DateTime date)
		{


			DateTime startDate = new DateTime (date.Year, date.Month, 1);
			DateTime endDate = startDate.AddMonths (1).AddDays (-1);

			var events = (CalendarEvents.Where (ce => (DateBetween (startDate, endDate, ce.StartDateTime) || DateBetween (startDate, endDate, ce.EndDateTime)))).OrderBy (ce => ce.StartDateTime).ToList ();
//			events.Add (new CalendarEvent (events [events.Count - 1].CalendarID, "Ben's Birthday", 1394988295, 1395161095, true, "", ""));
			return events;
		}

		public MyDiem.Core.BL.CalendarEvent GetEventByID (long eventID)
		{
			return CalendarEvents.First (ce => ce.ID == eventID);
		}

		public  bool DateBetween (DateTime startDate, DateTime endDate, DateTime date)
		{

			return startDate.Date <= date.Date && endDate.Date >= date.Date;
		}

		public  bool DatesMatch (DateTime date1, DateTime date2)
		{
			return (date1.Day == date2.Day && date1.Month == date2.Month && date1.Year == date2.Year);
		}
	}
}

