using System.Collections.Generic;
using MyDiemCore;
using System.Linq;
using System;

namespace MyDiem.Core.BL.Managers
{
    public class StateManager
	{
        public virtual List<State> States 
        {
            get;
            set;
        }

        public void GetStates(Action act)
        {
            var del = new GetStatesServiceDelegate();
            del.GetStates( () => {
                this.States = del.States.Where (x => x.Counties.Any () && x.Regions.Any ()).ToList ();
                act ();
            });
        }
	}
}

