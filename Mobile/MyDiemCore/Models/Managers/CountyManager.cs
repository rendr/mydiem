using System.Collections.Generic;

namespace MyDiem.Core.BL.Managers
{
    public class CountyManager
    {
        public virtual List<County> Counties
        {
            get;
            set;
        }

        public County GetCountyByID(long countyID)
        {
            foreach (County c in Counties)
            {
                if (c.ID == countyID)
                {
                    return c;
                }
            }
            return null;
        }

        public int GetCountyID(string value)
        {
            foreach (County c in Counties)
            {
                if (c.Title == value)
                {
                    return c.ID;
                }
            }
            return -1;
        }
    }
}

