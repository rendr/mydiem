﻿using System;
using System.Collections.Generic;
using MyDiem.Core.BL;
using MyDiem.Core.SAL;
using Newtonsoft.Json;

namespace MyDiem.Core.BL.Managers
{
    public static class SocialMediaManager
    {
        public static List<SocialMediaGroup> SocialMediaGroupList;
        public static event EventHandler GetFinished = delegate { };

        public static void GetData (int id)
        {
            GetSocialMediaDataServiceDelegate del = new GetSocialMediaDataServiceDelegate ();
            del.GetSocialMediaData (id,
                delegate {
                    SocialMediaGroupList = del.SocialMediaGroupList;
                    GetFinished (null, new EventArgs ());
                }
            );
        }
    }
}

namespace MyDiem.Core.SAL
{
    public class GetSocialMediaDataServiceDelegate : BaseAPIServiceDelegate
    {
        public List<SocialMediaGroup> SocialMediaGroupList;

        public GetSocialMediaDataServiceDelegate ()
        {
            this.APIMethodName = "getUserLocations";
        }

        public void GetSocialMediaData (int userId, Action action)
        {
            this.action = action;

            var paramStr = "{\"userid\":" + userId + "}";
            this.SendRequest (paramStr, true);
        }

        override public void HandleResult (string resultString)
        {
            SocialMediaGroupList = JsonConvert.DeserializeObject<List<SocialMediaGroup>> (resultString);
            this.action ();
        }
    }
}


