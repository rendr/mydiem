using System;
using System.Collections.Generic;
using MyDiem.Core.SAL;
using System.Linq;



namespace MyDiem.Core.BL.Managers
{
	public class SubscribableManager
	{
		public  event EventHandler GetAllSubscribablesStarted = delegate {};
		public  event EventHandler GetAllSubscribablesFinished = delegate {};
		public  event EventHandler SubscribeToiCalInvoked = delegate {};

		public  event EventHandler ReminderSet = delegate{};

		public  event EventHandler PasswordValidated = delegate {};

		public  event EventHandler PasswordValidationFailed = delegate {};


		public  event EventHandler EmailValidated = delegate {};

		public  event EventHandler EmailValidationFailed = delegate {};

		public  event EventHandler SubscribeToGCalInvoked = delegate{};

		public List<SchoolGroup> Organizations;

		public virtual void GetAllSubscribables(long userID)
		{


			GetAllSubscribablesStarted(null, new EventArgs());
			GetSchoolSubscribablesServiceDelegate del = new GetSchoolSubscribablesServiceDelegate ();
//			GetAllCalendarsServiceDelegate del = new GetAllCalendarsServiceDelegate();

			del.GetSubscribables(userID,
				delegate{
					Organizations = del.SchoolGroups;
					GetAllSubscribablesFinished(null, new EventArgs());
				}
			);
		}


		public virtual void GetAllSubscribables(long regionID, long countyID, long userID)
		{


			GetAllSubscribablesStarted(null, new EventArgs());
			GetSchoolSubscribablesServiceDelegate del = new GetSchoolSubscribablesServiceDelegate ();
			//			GetAllCalendarsServiceDelegate del = new GetAllCalendarsServiceDelegate();

			del.GetSubscribables(regionID, countyID, userID,
				delegate{
					Organizations = del.SchoolGroups;
					GetAllSubscribablesFinished(null, new EventArgs());
				}
			);
		}

		public virtual SchoolGroup CreateNewMainSubscriptionCategory(string newTitle,int[]ids, bool inIDsList){


			SchoolGroup newSubscriptionGroup = new SchoolGroup ();
			newSubscriptionGroup.Title = newTitle;

			newSubscriptionGroup.SubCategories = GetOrganizationsCopy ();
			foreach (var schoolGroup in newSubscriptionGroup.SubCategories) {
				if (inIDsList) {
					schoolGroup.Subscribables = schoolGroup.Subscribables.Where (s => ids.Contains (s.ID)).ToList ();
				} else {
					schoolGroup.Subscribables = schoolGroup.Subscribables.Where (s => !ids.Contains (s.ID)).ToList ();
				}
			}
			return newSubscriptionGroup;
		}

		public virtual void EnableAllSubscribablesForGroup(SchoolGroup sGroup)
		{
			foreach (var schoolGroup in sGroup.SubCategories) {
				foreach (var subscribable in schoolGroup.Subscribables) {
					subscribable.isEmailProtected = subscribable.isPasswordProtected = false;
				}
			}
		}

		 List<SchoolGroup> GetOrganizationsCopy()
		{
			var newList = new List<SchoolGroup> ();
			foreach (var schoolGroup in Organizations) {
				var newSchoolGroup = schoolGroup.Clone ();
				newSchoolGroup.Subscribables = schoolGroup.Subscribables.ToList ();
				newList.Add (newSchoolGroup);

			}
			return newList;
		}

		public virtual void ValidateSubscribablePassword(long calendarID, string password, string type)
		{
			IValidateSubscribablePasswordServiceDelegate del;
			if (type == Subscribable.TYPE_CALENDAR) {
				del = new ValidateCalendarPasswordServiceDelegate ();
			} else {
				del = new ValidateDirectoryPasswordServiceDelegate ();
			}
			del.ValidatePassword (calendarID, password, 
				delegate {

					if(del.Success){
						PasswordValidated(null, new EventArgs());
					}else{
						PasswordValidationFailed(null, new EventArgs());
					}
				}
			);

		}

		public virtual void ValidateSubscribableEmail (int id, string email, string type)
		{
			IValidateSubscribableEmailServiceDelegate del;
			if (type == Subscribable.TYPE_CALENDAR) {
				del = new ValidateCalendarEmailServiceDelegate ();
			} else {
				del = new ValidateDirectoryEmailServiceDelegate ();
			}
			del.ValidateEmail (id, email, 
				delegate {

					if(del.Success){
						EmailValidated(null, new EventArgs());
					}else{
						EmailValidationFailed(null, new EventArgs());
					}
				}
			);
		}

		public virtual List<Subscribable> GetSubscribables(int[] ids)
		{

			List<Subscribable> subscribables = new List<Subscribable>();
			foreach(SchoolGroup cc in Organizations){

				foreach(Subscribable cal in cc.Subscribables){

					if(Array.IndexOf(ids,cal.ID) != -1){
						subscribables.Add(cal);
					}
					if(subscribables.Count == ids.Length){
						return subscribables;
					}
				}

			}
			return subscribables;
		}

		public virtual void BroadcastReminderSet ()
		{
			ReminderSet(null, new EventArgs());
		}
		public virtual void SubscribeToiCal()
		{
			SubscribeToiCalInvoked(null, new EventArgs());
		}



		public virtual void SubscribeToGCal()
		{
			SubscribeToGCalInvoked(null, new EventArgs());
		}
	}
}

