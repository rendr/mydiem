using System;
using System.Collections.Generic;
using MyDiem.Core.SAL;
using MonkeyArms;

namespace MyDiem.Core.BL.Managers
{
	public class ParentDirectoryManager
	{
		public event EventHandler ParentsUpdated = delegate {};

		public List<Parent> Parents;

		public void GetParentDirectory (int appUserID)
		{
			
			

			GetDirectoriesWithContactsServiceDelegate del = new GetDirectoriesWithContactsServiceDelegate ();
			del.GetParents (
				appUserID,
				delegate {
					Parents = del.Parents;
					ParentsUpdated (null, new EventArgs ());
				}
			);
		}
	}
}

