using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using MyDiem.Core.SAL;
using MyDiem.Core.Util;
using MyDiem.Core.Interfaces;
using MonkeyArms;
using System.Diagnostics;
using MyDiem.Core.ViewModel;

namespace MyDiem.Core.BL.Managers
{
	public class UserManager:IInjectingTarget
	{
		//		[Inject]
		//		public DirectoryViewModel DirectoryVM;
		//
		//		[Inject]
		//		public SubscribableManager SM;
		public event EventHandler RegisterUserStarted = delegate {};
		public event EventHandler RegisterUserFinished = delegate {};
		public event EventHandler UpdateUserCalendarSubscriptionStarted = delegate{};
		public event EventHandler UpdateUserCalendarSubscriptionFinished = delegate{};
		public event EventHandler UpdateProfileStarted = delegate{};
		public event EventHandler UpdateProfileFinished = delegate{};

		public List<Subscription> UserCalendarSubscriptions;
		public List<Subscription> UserDirectorySubscriptions;

		public UserManager ()
		{
			try {
				DIUtil.InjectProps (this);
			} catch (Exception e) {
				Debug.WriteLine ("something went wrong in the constructor");
			}
		}

		public Subscription GetSubscriptionForCalendar (int calendarID)
		{
			return GetSubscription (UserCalendarSubscriptions, calendarID);

		}

		public Subscription GetSubscriptionForDirectory (int directoryID)
		{
			return GetSubscription (UserDirectorySubscriptions, directoryID);

		}

		private Subscription GetSubscription (List<Subscription> subscriptions, int id)
		{
			foreach (Subscription cs in subscriptions) {
				if (cs.SubscribableID == id) {
					return cs;
				}
			}
			return new Subscription (-1, id, DateTime.Now.Millisecond, "000000", false, false);
		}

        public void Register (string firstName, string lastName, string email, string zipCode, string password1, string password2, int regionID, int countyID)
		{
			RegisterUserStarted (null, null);
			
            var shouldContinue = ValidateFields(email, zipCode, password1, password2, regionID, countyID);
            if (!shouldContinue)
                return;
            
            var userToRegister = new User()
            {
                Fname = firstName,
                Lname = lastName,
                Email = email, 
                Zip = zipCode,
                Password = password1,
                RegionID = regionID,
                CountyID = countyID
            };

			RegisterUserServiceDelegate del = new RegisterUserServiceDelegate ();
            del.Register (userToRegister,
				          delegate { RegisterUserFinished (null, new Events.RegisterUserFinishedEvent (del.userRegistered, del.responseMessage)); }
			);
		}

        private bool ValidateFields(string email, string zipCode, string password1, string password2, int regionID, int countyID)
        {
            if (!FormDataValidator.IsValidEmail(email))
            {
                RegisterUserFinished(null, new Events.RegisterUserFinishedEvent(false, "Please enter a valid email address."));
                return false;
            }

            if (!FormDataValidator.IsValidZipCode(zipCode))
            {
                RegisterUserFinished(null, new Events.RegisterUserFinishedEvent(false, "Zip Code is required and must be 5 digits."));
                return false;
            }

            if (password1 != password2)
            {
                RegisterUserFinished(null, new Events.RegisterUserFinishedEvent(false, "Password entries did not match"));
                return false;
            }

            if (!FormDataValidator.IsValidPassword(password1))
            {
                RegisterUserFinished(null, new Events.RegisterUserFinishedEvent(false, "Password must be 6 characters or greater."));
                return false;
            }

            if (regionID == -1 && countyID == -1)
            {
                RegisterUserFinished(null, new Events.RegisterUserFinishedEvent(false, "Must select a private and/or public school."));
                return false;
            }

            return true;
        }

		public void UpdateUserCalendarSubscription (long userID, Subscription subscription, long colorID)
		{
			UpdateUserCalendarSubscriptionServiceDelegate del = new UpdateUserCalendarSubscriptionServiceDelegate ();

			bool calendarFound = false;
			for (int i = 0; i < UserCalendarSubscriptions.Count; i++) {
				Subscription storedSubscription = UserCalendarSubscriptions [i];
				if (storedSubscription.SubscribableID == subscription.SubscribableID) {
					if (subscription.Enabled) {
						UserCalendarSubscriptions [i] = subscription;
					} else {
						UserCalendarSubscriptions.RemoveAt (i);
					}
					calendarFound = true;
					break;
				}
			}
			if (!calendarFound) {
				UserCalendarSubscriptions.Add (subscription);
			}

			del.UpdateUserCalendarSubscription (userID, subscription, colorID,
				delegate {
					UpdateUserCalendarSubscriptionFinished (null, new UpdateUserCalendarSubscriptionFinishedEvent (del.Success));
					
				}
			);
		
		}

		public void UpdateUserDirectorySubscription (int userID, int directoryID, Subscription selectedSubscription)
		{
			UpdateUserDirectorySubscriptionServiceDelegate del = new UpdateUserDirectorySubscriptionServiceDelegate ();
			
			bool directoryFound = false;
			for (int i = 0; i < UserDirectorySubscriptions.Count; i++) {
				Subscription storedSubscription = UserDirectorySubscriptions [i];
				if (storedSubscription.SubscribableID == selectedSubscription.SubscribableID) {
					UserDirectorySubscriptions [i] = selectedSubscription;
					directoryFound = true;
					break;
				}
			}
			if (!directoryFound) {
				UserDirectorySubscriptions.Add (selectedSubscription);
			}
			
			del.UpdateUserDirectorySubscription (userID, directoryID, selectedSubscription.Suppressed, selectedSubscription.Enabled,
				delegate {
					UpdateUserCalendarSubscriptionFinished (null, new UpdateUserCalendarSubscriptionFinishedEvent (del.Success));
				}
			);
		}

		public bool IsCalendarUsers (int calendarID)
		{
			foreach (Subscription sub in UserCalendarSubscriptions) {
				if (sub.SubscribableID == calendarID) {
					return true;
				}
			}
			return false;
		}

		public int[] GetCalendarIDs ()
		{
			int[] ids = new int[UserCalendarSubscriptions.Count];
			for (int i = 0; i < ids.Length; i++) {
				ids [i] = UserCalendarSubscriptions [i].SubscribableID;
			}
			return ids;

		}

		public int[] GetDirectoryIDs ()
		{
			int[] ids = new int[UserDirectorySubscriptions.Count];
			for (int i = 0; i < ids.Length; i++) {
				ids [i] = UserDirectorySubscriptions [i].SubscribableID;
			}
			return ids;
		}
	}
}

