using System;
using System.Collections.Generic;

namespace MyDiem.Core.BL.Managers
{
	public class RegionManager
	{
		
		public event EventHandler GetRegionsStarted = delegate {};
		public event EventHandler GetRegionsFinished = delegate {};
	
		public virtual List<Region> Regions {
			get;
			set;
		}

		public virtual Region GetRegionByID (long regionID)
		{
			foreach (Region r in Regions) {
				if(r.ID == regionID){
					return r;
				}
			}
			return null;
		}

		public virtual int GetRegionID (string value)
		{
			foreach (Region r in Regions) {
				if(r.Title == value){
					return r.ID;
				}
			}
			return -1;
		}
	}
}

