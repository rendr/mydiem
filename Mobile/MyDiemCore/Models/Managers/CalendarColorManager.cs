using System;
using System.Collections.Generic;

using MyDiem.Core.SAL;

namespace MyDiem.Core.BL.Managers
{
	public class CalendarColorManager
	{

		public event EventHandler GetColorsStarted = delegate {};
		public event EventHandler GetColorsFinished = delegate {};

		public List<CalendarColor> Colors;

		public void GetAllColors()
		{
			GetColorsStarted(null, new EventArgs());
			GetColorServiceDelegate del = new GetColorServiceDelegate();
			del.GetColors(
				delegate{
					Colors = del.Colors;
					GetColorsFinished(null, new EventArgs());
				}
			);
		}

		public long GetIDForColor (string color)
		{
			foreach(CalendarColor c in Colors){
				if(c.Color == color){
					return c.ID;
				}
			}
			return 1;
		}

	}
}

