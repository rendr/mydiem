using System;

namespace MyDiem.Core.BL.Managers.Events
{
	public class RegisterUserFinishedEvent:EventArgs
	{
		public bool success;
		public string message;
		
		public RegisterUserFinishedEvent (bool success, string message)
		{
			this.success = success;
			this.message = message;
		}
	}
}

