using System;

namespace MyDiem.Core
{
	public class UpdateUserCalendarSubscriptionFinishedEvent:EventArgs
	{
		public bool Success;

		public UpdateUserCalendarSubscriptionFinishedEvent (bool success)
		{
			this.Success = success;
		}
	}
}

