using System;
using System.Collections.Generic;

using Newtonsoft.Json.Linq;
using MyDiem.Core.BL;

namespace MyDiem.Core.BL
{
	public class SchoolGroup
	{
		public static string TYPE_PRIVATE = "private";
		public static string TYPE_PUBLIC = "public";

		public string Title;



		public List<SchoolGroup> SubCategories;
		public List<Subscribable> Subscribables;
		public int RegionID;
		public int CountyID;
		public string Type;

		public string KeyWords{
			get{
				var s = Title;
				foreach (var subscribable in Subscribables) {
					s += " " + subscribable.title;
				}
				return s;
			}
		}

		public SchoolGroup ()
		{

		}

		public SchoolGroup Clone ()
		{
			var g = new SchoolGroup ();
			g.Title = this.Title;
			g.SubCategories = this.SubCategories;
			g.Subscribables = this.Subscribables;
			g.RegionID = this.RegionID;
			g.CountyID = this.CountyID;
			g.Type = this.Type;
			return g;

		}

	}
}

