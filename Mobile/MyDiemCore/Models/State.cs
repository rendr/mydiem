﻿using System.Collections.Generic;
using MyDiem.Core.Interfaces;
using Newtonsoft.Json;

namespace MyDiem.Core.BL
{
    public class State : IArea
    {
        public State (int id, string title, List<County> counties, List<Region> regions)
        {
            this.ID = id;
            this.Title = title;
            this.Counties = counties;
            this.Regions = regions;
        }

        #region IArea implementation

        public int ID
        {
            get;
        }

        [JsonProperty("NAME")]
        public string Title
        {
            get;
        }

        [JsonProperty ("COUNTIES")]
        public List<County> Counties
        {
            get;
        }

        [JsonProperty ("REGIONS")]
        public List<Region> Regions
        {
            get;
        }

        #endregion
    }
}

