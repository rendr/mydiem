using System;
using Newtonsoft.Json.Linq;
using MyDiem.Core.Interfaces;

namespace MyDiem.Core.BL
{
	public class County:IArea
	{
		public string Title {get; set;}
		public int ID {get; set;}

		public County (int id, string title)
		{
			this.ID = id;
			this.Title = title;
		}

		public static County FromJObj (JObject obj)
		{
			return new County(Convert.ToInt32((string)obj["ID"]), (string)obj["TITLE"]);
		}
	}
}

