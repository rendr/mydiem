using System;

namespace BL.Contracts
{
	public interface IBusinessEntity
	{
		int ID { get; set; }
	}
}

