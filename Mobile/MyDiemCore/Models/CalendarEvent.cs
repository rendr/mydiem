using System;

using Newtonsoft.Json.Linq;

namespace MyDiem.Core.BL
{
	public class CalendarEvent
	{
		public int ID {
			get {
				return CalendarID + StartDateTime.Month + StartDateTime.Year + StartDateTime.Hour + StartDateTime.Day + StartDateTime.Minute;
			}
		}

		public int CalendarID;
		public string Title {get; set;}
		public string CalendarTitle{ get; set;}
		public long Start {get; set;}
		public long End {get; set;}
		public bool AllDay {get; set;}

		public string Description {
			get;
			set;
		}

		public string Location {
			get;
			set;
		}

		public DateTime StartDateTime;
		public DateTime EndDateTime;

		private string swatchColorString;

		public string SwatchColorString {
			get {
				return swatchColorString.Trim();
			}
			set {
				swatchColorString = value;
			}
		}

		public long StartDateTimeMilliseconds{

			get{
				return (long)StartDateTime.ToUniversalTime ().Subtract (new DateTime (1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
			}
		}

		public CalendarEvent (int calendarID, string title, long start, long end, bool allDay, string description, string location, string calendarTitle = "")
		{
			this.CalendarID = calendarID;
			this.Title = title;
			this.Start = start;
			this.End = end;
			this.AllDay = allDay;
			this.Description = description;
			this.Location = location;
			this.CalendarTitle = calendarTitle;

			var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

			StartDateTime = epoch.AddSeconds(start).ToLocalTime();
			EndDateTime = epoch.AddSeconds(end).ToLocalTime();

		}

		public static CalendarEvent fromJObject(int calendarID, JObject obj, string calendarTitle)
		{
			return new CalendarEvent(calendarID, (string)obj["TITLE"], Convert.ToInt32((string)obj["START"]), Convert.ToInt32((string)obj["END"]), (bool)obj["ALLDAY"], 
				(string)obj["DESCRIPTION"], (string)obj["LOCATE"], calendarTitle);
		}
	}
}

