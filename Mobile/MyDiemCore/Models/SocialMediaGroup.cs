﻿using System;

namespace MyDiem.Core.BL
{
    public class SocialMediaGroup
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
    }
}