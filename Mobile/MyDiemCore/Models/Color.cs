using System;
using BL.Contracts;

namespace MyDiem.Core.BL
{
	public class Color:BusinessEntityBase
	{
		public string HexString;
		public UInt32 HexValue;

		public Color (string hexString)
		{
			this.HexString = hexString;
			this.HexValue = Convert.ToUInt32 ("0x" + hexString);

		}
	}
}

