using System;

using Newtonsoft.Json.Linq;

namespace MyDiem.Core.BL
{
	public class Subscription
	{
		public int ID;
		public int SubscribableID;
		public int LastModified;
		private string color;

		public string Color {
			get {
				return color.Trim();
			}
			set {
				color = value;
			}
		}

		public bool Enabled;
		public bool Suppressed;

		public Subscription (int id, int calendarId, int lastModified, string color, bool enabled, bool suppressed)
		{

			this.ID = id;
			this.SubscribableID = calendarId;
			this.LastModified = lastModified;
			this.color = color;
			this.Enabled = enabled;
			this.Suppressed = suppressed;
		}

		public static Subscription FromJObject(JObject o)
		{
			return new Subscription(-1, 
			                                Convert.ToInt32((string)o["ID"]), 
			                                Convert.ToInt32((string)o["LASTMODIFIED"]), 
			                                (string)o["COLOR"], 
			                                true, 
			                                (bool)o["SUPPRESSED"]);
		}
	}
}

