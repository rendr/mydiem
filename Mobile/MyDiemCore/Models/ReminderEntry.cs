using System;
using Newtonsoft.Json.Linq;
using BL.Contracts;

namespace MyDiem.Core.BL
{
	public class ReminderEntry:BusinessEntityBase
	{
		public string Label{ get; set; }

		public ReminderEntry ()
		{

		}

		public ReminderEntry (int id, string label)
		{
			this.ID = id;
			this.Label = label;
		}
	}
}

