using System;
using Newtonsoft.Json.Linq;
using MyDiem.Core.Interfaces;

namespace MyDiem.Core.BL
{
	public class Region:IArea
	{
		public long IAppDomainSetup { get; set; }

		public string Title { get; set; }

		public int ID { get; set; }

		public Region (int id, string title)
		{
			this.ID = id;
			this.Title = title;
		}

		public static Region FromJObj (JObject obj)
		{
			return new Region (Convert.ToInt32 ((string)obj ["ID"]), (string)obj ["TITLE"]);
		}

		public override string ToString ()
		{
			return Title;
		}
	}
}

