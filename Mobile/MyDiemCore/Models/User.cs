using System;
using BL.Contracts;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace MyDiem.Core.BL
{
	public class User:BusinessEntityBase
	{
		public string Fname { get; set; }

		public string Lname { get; set; }

		public string Email { get; set; }

        //[JsonProperty("zipcode")]
		public string Zip;

        [JsonIgnore]
        public bool IsValidated { get; set; }

		public long RegionID { get; set; }

		public long CountyID { get; set; }

        public string State { get; set; }

		public bool CalendarConnect { get; set; }

		public bool CarpoolConnect { get; set; }

		public bool CalendarConnectPrompted { get; set; }

        public string SubscriptionUrl { get; set; }

        public string Phone1 { get; set; }

        public string Phone2 { get; set; }

        public string Street { get; set; }

        public string PhotoUrl { get; set; }

        public bool IsParent { get; set; }

        public bool? HidePhone1 { get; set; }

        public bool? HidePhone2 { get; set; }

        public bool? HideAddress { get; set; }

        public string Password { get; set; }
       
        [JsonIgnore]
        public bool IsEmpty 
        {
            get { return ID <= 0; }
        }

		public User ()
		{
		}

		public static User FromJObj (JObject obj)
		{
			User u = new User ();
			u.ID = Convert.ToInt32 ((string)obj ["USERID"]);
			u.RegionID = Convert.ToInt32 ((string)obj ["REGIONID"]);
			u.IsValidated = ((string)obj ["ISVALIDATED"] == "1");
			u.Zip = (string)obj ["ZIP"];
			if (obj ["COUNTYID"] != null) {
				u.CountyID = Convert.ToInt32 ((string)obj ["COUNTYID"]);
			}

			u.Fname = (string)obj ["FIRSTNAME"];
			u.Lname = (string)obj ["LASTNAME"];
            u.Email = (string)obj ["EMAIL"];
            u.PhotoUrl = (string)obj ["PHOTOURL"];

            u.IsParent = (bool)obj["ISPARENT"];
            if (u.IsParent)
            {
                try
                {
                    u.State = (string)obj ["STATE"];
                    u.HideAddress = (bool)obj ["HIDEADDRESS"];
                    u.Street = (string)obj ["ADDRESS"];
                    u.HidePhone1 = (bool)obj ["HIDEPHONE1"];
                    u.Phone1 = (string)obj ["PHONE1"];
                    u.HidePhone2 = (bool)obj ["HIDEPHONE2"];
                    u.Phone2 = (string)obj ["PHONE2"];
                } catch (Exception ex) 
                {
                    System.Diagnostics.Debug.WriteLine (ex.Message);
                }
            }


			if (obj.GetValue ("CALENDARCONNECT") != null) {
				u.CalendarConnect = (bool)obj ["CALENDARCONNECT"];
			}

			if (obj.GetValue ("CARPOOLCONNECT") != null) {
				u.CarpoolConnect = (bool)obj ["CARPOOLCONNECT"];
			}
			u.SubscriptionUrl = (string)obj ["ICALSUBSCRIPTIONURL"];

			return u;
		}

		public void Update (User user)
		{
			if (user.ID != 0) {
				this.ID = user.ID;
			}
			this.Fname = user.Fname;
			this.Lname = user.Lname;
			this.Email = user.Email;
			this.Zip = user.Zip;
			this.IsValidated = user.IsValidated;
			this.RegionID = user.RegionID;
			this.CountyID = user.CountyID;
			this.CalendarConnect = user.CalendarConnect;
			this.CarpoolConnect = user.CarpoolConnect;
            this.HidePhone1 = user.HidePhone1;
            this.Phone1 = user.Phone1;
            this.HidePhone2 = user.HidePhone2;
            this.Phone2 = user.Phone2;
            this.HideAddress = user.HideAddress;
            this.Street = user.Street;
            this.SubscriptionUrl = user.SubscriptionUrl;
            this.IsParent = user.IsParent;
            this.Password = user.Password;
            this.State = user.State;
            this.PhotoUrl = user.PhotoUrl;
			if (user.CalendarConnectPrompted) {
				this.CalendarConnectPrompted = user.CalendarConnectPrompted;
			}
		}

        public void Update (string firstName, string lastName, string email, string zip, long regionID, long countyID, string phone1, string phone2, string street, bool hidephone1, bool hidephone2, bool hidestreet, bool isParent, string state, string password = null)
		{
			this.Fname = firstName;
			this.Lname = lastName;
			this.Email = email;
			this.Zip = zip;
			this.RegionID = regionID;
			this.CountyID = countyID;
            this.Phone1 = phone1;
            this.Phone2 = phone2;
            this.Street = street;
            this.HidePhone1 = hidephone1;
            this.HidePhone2 = hidephone2;
            this.HideAddress = hidestreet;
            this.Password = password;
            this.IsParent = isParent;
            this.State = state;
		}
	}
}

