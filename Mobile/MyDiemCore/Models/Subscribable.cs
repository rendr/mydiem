using System;
using BL.Contracts;
using Newtonsoft.Json.Linq;

namespace MyDiem.Core.BL
{
	public partial class Subscribable:BusinessEntityBase
	{
		public static string TYPE_CALENDAR = "calendar";
		public static string TYPE_DIRECTORY = "directory";

		public long lastModified { get; set; }

		public string title{ get; set; }

		public string color { get; set; }

		public bool suppressed { get; set; }

		public bool isPremium { get; set; }

		public bool isPasswordProtected { get; set; }

		public bool isEmailProtected { get; set; }

		public string category { get; set; }

		public string type;

		public string sku { get; set; }

		public string price { get; set; }

		public Subscribable (int id, string title, bool isPremium, bool isPasswordProtected, string sku, string price, string type = null, string category = null, bool isEmailProtected = false)
		{
			this.ID = id;
			this.type = type;
			this.title = title;
			this.isPremium = isPremium;
			this.isPasswordProtected = isPasswordProtected;
			this.isEmailProtected = isEmailProtected;
			this.category = category;
			this.sku = sku;
			this.price = price;



			/*
			if (this.type == TYPE_DIRECTORY) {
				this.isPremium = false;
				this.isPasswordProtected = false;
				this.isEmailProtected = false;
			}
			*/

			/*
			this.isPremium = true;
			this.price = "1.99";
			this.sku = "com.mydiem.ios.calendars.calendar1";
			*/

		}

		public static Subscribable FromJObj (JObject obj)
		{
			//temporary check to see if we are still getting old calendars.

			return new Subscribable (Convert.ToInt32 ((string)obj ["ID"]), (string)obj ["TITLE"], (bool)obj ["ISPREMIUM"], (bool)obj ["ISPASSWORDPROTECTED"], (string)obj ["SKU"], (string)obj ["PRICE"], (string)obj ["TYPE"], (string)obj ["CATEGORY"], (string)obj ["ISEMAILPROTECTED"] != "0");
			
			
		}
	}
}

