using System;
using Newtonsoft.Json.Linq;

namespace MyDiem.Core.BL
{
	public class Child
	{
		public string FirstName, Location, Grade, ChildID, LastName;

		public Child ()
		{
		}

		public static Child FromJObj(JObject obj)
		{
			Child c = new Child ();
			
			c.FirstName = (string)obj ["FIRSTNAME"];
			c.LastName = (string)obj ["LASTNAME"];
			c.Grade = (string)obj ["GRADE"];
			c.ChildID = (string)obj ["CHILDID"];
			c.LastName = (string)obj ["LASTNAME"];
			return c;
		}
	}
}

