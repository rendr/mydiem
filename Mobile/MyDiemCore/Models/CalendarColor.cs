using System;
using Newtonsoft.Json.Linq;

namespace MyDiem.Core.BL
{
	public class CalendarColor
	{
		public long ID;
		public string Color;

		public CalendarColor (long id, string color)
		{
			this.ID = id;
			this.Color = color;
		}

		public static CalendarColor FromJObject(JObject obj)
		{
			return new CalendarColor(Convert.ToInt32((string)obj["ID"]), (string)obj["COLOR"]);
		}
	}
}

