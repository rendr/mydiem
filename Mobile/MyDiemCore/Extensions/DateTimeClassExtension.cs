﻿using System;

namespace MyDiemCore
{
	public static class DateTimeClassExtension
	{
		public static long EpochTicks (this DateTime date)
		{
			var epoch = new DateTime (1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			return date.Ticks - epoch.Ticks;

		}
	}
}

