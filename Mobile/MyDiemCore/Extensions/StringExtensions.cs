﻿using System;
using System.Text.RegularExpressions;

namespace MyDiemCore.Extensions
{
    public static class StringExtensions
    {
        public static string FormatPhoneNumber (this string number)
        {
            var origValue = number;
            number = Regex.Replace (number, "[^0-9.]", "");
            if (string.IsNullOrEmpty (number))
                return origValue;
            return String.Format ("{0:(###) ###-####}", Convert.ToInt64 (number));

        }
    }
}

