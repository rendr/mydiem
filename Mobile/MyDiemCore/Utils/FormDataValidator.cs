using System;
//using System.Text.RegularExpressions;

namespace MyDiem.Core.Util
{
	public static class FormDataValidator
	{
		public static bool IsValidZipCode(string zip){
			if(zip.Length != 5){
				return false;
			}
			try{
				Convert.ToInt64(zip);
			}catch(Exception e){
				return false;
			}
			
			return true;
		}
		
		public static bool IsValidEmail(string strIn)
	   {
	       
	       if (String.IsNullOrEmpty(strIn))
	          return false;

			/*
	       return Regex.IsMatch(strIn, 
	              @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" + 
	              @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$", 
	              RegexOptions.IgnoreCase);
	              */
			return true;
	   }
		
		public static bool IsValidPassword(string password)
		{
			return (password.Length >= 6);	
		}
	}
}

