﻿using System;

namespace MyDiemCore
{
	public class DateUtil
	{
		public DateUtil ()
		{
		}

		public bool DateBetweenTwoDates (DateTime date, DateTime startDate, DateTime endDate)
		{
			return (date.Day >= startDate.Day && date.Day <= endDate.Day) && (date.Month == startDate.Month || date.Month == endDate.Month);
		}
	}
}

