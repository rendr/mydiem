﻿using System;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace MyDiemCore
{
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        private const string UserKey = "user_key";
        private static readonly string UserDefault = string.Empty;

        public static string User
        {
            get
            {
                return AppSettings.GetValueOrDefault (UserKey, UserDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue (UserKey, value);
            }
        }

        private const string ReminderKey = "reminder_key";
        private static readonly string ReminderDefault = string.Empty;

        public static string Reminder
        {
            get
            {
                return AppSettings.GetValueOrDefault (ReminderKey, ReminderDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue (ReminderKey, value);
            }
        }
    }
}

