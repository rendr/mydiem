using System;
using MyDiem.Core.BL;

namespace CustomEvents
{
	public class SelectedCalendarEventArgs:EventArgs
	{
		private CalendarEvent selectedEvent;

		public CalendarEvent SelectedEvent {
			get {
				return selectedEvent;
			}
		}

		public SelectedCalendarEventArgs (CalendarEvent selectedEvent )
		{
			this.selectedEvent = selectedEvent;
		}
	}
}

