using System;

using MyDiem.Core.BL;

namespace MyDiem.Core.CustomEvents
{
	public class CalendarCategoryEventArgs:EventArgs
	{
		public SchoolGroup Category;
		public CalendarCategoryEventArgs (SchoolGroup cat)
		{
			this.Category = cat;
		}
	}
}

