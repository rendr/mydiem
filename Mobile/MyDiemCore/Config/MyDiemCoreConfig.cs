using System;
using MonkeyArms;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.ViewModel;
using MyDiem.Core.Invokers;
using MyDiem.Core.Commands;
using MyDiem.Core.Mediators;
using MyDiem.Core.Interfaces;
using MyDiemCore;
using MyDiem.Core.BL;
using Invokers.Notification;
using Commands;
using Invokers.Inputs;

namespace MyDiem.Core.Config
{
	public class MyDiemCoreContext:Context
	{
		public MyDiemCoreContext () : base ()
		{
		}

		public override void Config ()
		{
			base.Config ();

			//Singleton Managers
			DI.MapSingleton<UserManager> ();
			DI.MapSingleton<ParentDirectoryManager> ();
			DI.MapSingleton<EventManager> ();
			DI.MapSingleton<CalendarColorManager> ();
			DI.MapSingleton<RegionManager> ();
            DI.MapSingleton<CountyManager> ();
            DI.MapSingleton<StateManager> ();
			DI.MapSingleton<SubscribableManager> ();

			//Misc Singletons
			DI.MapSingleton<User> ();

			//Singleton VMs
			DI.MapSingleton<SlideMenuViewModel> ();
			DI.MapSingleton<AboutViewModel> ();
			DI.MapSingleton<CalendarMonthViewModel> ();
			DI.MapSingleton<DirectoryViewModel> ();
			DI.MapSingleton<EventListViewModel> ();
			DI.MapSingleton<FilterMenuViewModel> ();
			DI.MapSingleton<SetInitialScreenInvoker> ();
			DI.MapSingleton<PurchaseCalendarConnectInvoker> ();
            DI.MapSingleton<SubscribableListViewModel> ();
            DI.MapSingleton<SocialMediaViewModel> ();

			//Singleton Invokers
			DI.MapSingleton<NavToScreenInvoker> ();
			DI.MapSingleton<LinkCalendarInvoker> ();
			DI.MapSingleton<SetNetworkActivityIndicatorVisibilityInvoker> ();
			DI.MapSingleton<UserNotValidatedNotificationInvoker> ();
            DI.MapSingleton<AuthenticationOutputInvoker> ();
            DI.MapSingleton<ResetPasswordOutputInvoker> ();
			DI.MapSingleton<UserUpdatedFromServerNotificationInvoker> ();
			DI.MapSingleton<UserSavedOnServerInvoker> ();

			//Invoker/Commands
			DI.MapCommandToInvoker<ResolveSlideMenuRowTouchedCommand, ResolveSlideMenuRowTouchedInvoker> ();
			DI.MapCommandToInvoker<SelectCalendarMonthDateCommand, SelectCalendarMonthDateInvoker> ();
			DI.MapCommandToInvoker<AuthenticateUserCommand, AuthenticationInputInvoker> ();
			DI.MapCommandToInvoker<UpdateUserFromServerCommand, UpdateUserFromServerRequestInvoker> ();
			DI.MapCommandToInvoker<SaveUserToDBCommand, SaveUserToDBInputInvoker> ();
			DI.MapCommandToInvoker<UpdateUserFromDBCommand, UpdateUserFromDBRequestInvoker> ();
			DI.MapCommandToInvoker<DeleteUserFromDB, LogUserOutRequestInvoker> ();
            DI.MapCommandToInvoker<SaveUserToServerCommand, SaveUserToServerInputInvoker> ();
            DI.MapCommandToInvoker<ResetPasswordCommand, ResetPasswordRequestInvoker> ();

			//Mediator Mappings
			DI.MapMediatorToClass<HomeViewMediator, IHomeView> ();
            DI.MapMediatorToClass<LoginViewMediator, ILoginView> ();
            DI.MapMediatorToClass<ResetPasswordMediator, IResetPasswordView> ();
			DI.MapMediatorToClass<LoadingViewMediator, ILoadingView> ();
			DI.MapMediatorToClass<SettingsMenuTableSourceMediator, ISettingsMenuTableSource> ();
			DI.MapMediatorToClass<EventsTableSourceMediator, IEventsTableSource> ();
			DI.MapMediatorToClass<SlideMenuViewMediator, ISlideMenuView> ();

			DI.MapMediatorToClass<AboutViewMediator, IAboutView> ();
			DI.MapMediatorToClass<ParentDirectoryViewMediator, IParentDirectoryView> ();
			DI.MapMediatorToClass<ParentDetailsViewMediator, IParentDetailsView> (); 
			DI.MapMediatorToClass<ProfileViewMediator, IProfileView> ();
			DI.MapMediatorToClass<RegisterViewMediator, IRegisterView> ();
			DI.MapMediatorToClass<SubscribableListViewMediator, ISubscribableListView> ();
			DI.MapMediatorToClass<SubscribableSettingsViewMediator, ISubscribableSettingsView> ();
			DI.MapMediatorToClass<CalendarMonthViewMediator, ICalendarMonthView> ();
			DI.MapMediatorToClass<CalendarWeekViewMediator, ICalendarWeekView> ();
			DI.MapMediatorToClass<CalendarEventListTableSourceMediator, ICalendarEventListTableSource> ();
			DI.MapMediatorToClass<FilterMenuMediator, IFilterMenu> ();

			DI.MapMediatorToClass<BaseCalendarViewMediator, IBaseCalendarView> ();

			DI.MapMediatorToClass<MapViewMediator, IMapView> ();

			//DI.MapSingleton<LocalNotificationUtil>();

		}
	}
}

