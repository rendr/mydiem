using System;
using System.Collections.Generic;
using MyDiem.Core.BL;

namespace MyDiem.Core.Interfaces
{
	public interface ISubscribableListView:IBaseCalendarView
	{
		event EventHandler FilterSubscribables, ResetSubscribables, SubscribableSelected, PasswordSubmitted, EmailSubmitted;

	

		void UpdateSubscribablesList (List<SchoolGroup> schoolGroups, int[] userCalendarIDs, int[] userDirectoryIDs);

		void PromptForSubscribablePassword ();

		void PromptForSubscribablePurchase (string sku);

		void ShowWhiteListPrompt ();

		void SubscribablePasswordFailed ();

		void HandleSubscribableEmailFailed ();


		void HandleSubscribableSelected (Subscribable subscribable, Subscription subscription);

		string SearchPlaceHolderText{ get; set; }

		string SearchQuery{ get; }

		string SubmittedPassword{ get; }

		string SubmittedEmail{ get; }

		Subscribable SelectedSubscribable{ get; set; }

		void ShowLoadingPrompt ();
	}
}

