using System;
using MonkeyArms;
using MyDiem.Core.BL;

namespace MyDiem.Core.Interfaces
{
	public interface IBaseCalendarView:IMediatorTarget
	{
		void GoCalendarMonthView ();

		void GoCalendarWeekView ();

		void GoCalendarDayView ();

		void GoSettingsView (User user);

		void GoSubscribablesView (SchoolGroup category, string title);

		void GoEventDetailsView (CalendarEvent selectedEvent);

		void GoAboutView ();

        void GoParentDirectory ();

        void GoFacebookDirectory ();

        void GoTwitterDirectory ();
	}
}

