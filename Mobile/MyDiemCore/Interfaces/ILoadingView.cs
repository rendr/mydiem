using System;
using MonkeyArms;

namespace MyDiem.Core.Interfaces
{
	public interface ILoadingView:IMediatorTarget
	{
		void GoMainView();
	}
}

