using System;
using System.Collections.Generic;
using MyDiem.Core.BL;

namespace MyDiem.Core.Interfaces
{
	public interface ISubscribableSettingsView:IBaseCalendarView
	{
		event EventHandler SubscriptionChanged;

		List<CalendarColor> Colors { get; set; }

		Subscribable SelectedSubscribable {
			get;
			set;
		}

		Subscription SelectedSubscription {
			get;
			set;
		}

		long SelectedColorID {
			get;
			set;
		}

		void ShowUpdatedPrompt ();

		void ShowUpdatingPrompt ();
	}
}

