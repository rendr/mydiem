using System;
using System.Collections.Generic;
using MonkeyArms;
using MyDiem.Core.BL;

namespace MyDiem.Core.Interfaces
{
	public interface ISlideMenuView:IMediatorTarget
	{
		void UpdateOptionsTableSource (List<SchoolGroup> categories);

		void Enable ();
	}
}

