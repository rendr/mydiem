using System;

namespace MyDiem.Core.Interfaces
{
	public interface IArea
	{
		int ID{ get;}
		string Title{get;}
	}
}

