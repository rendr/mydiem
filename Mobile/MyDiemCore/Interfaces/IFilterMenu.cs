using System;
using MonkeyArms;
using System.Collections.Generic;
using MyDiem.Core.BL;

namespace MyDiem.Core.Interfaces
{
	public interface IFilterMenu:IMediatorTarget
	{
		event EventHandler RegionSelected;
		event EventHandler CountySelected;
		event EventHandler TypeSelected;
		event EventHandler CategorySelected;

		List<Region> Regions{ get; set; }
		List<County> Counties{ get; set; }
		List<String> Types{ get; set; }

		List<string> Categories {get;set;}

		List<string> SelectedCategories {get;}

		Region SelectedRegion{ get; }
		County SelectedCounty{ get; }
		string SelectedType{ get;}


	}
}

