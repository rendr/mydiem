using System;
using MonkeyArms;
using System.Collections.Generic;
using MyDiem.Core.BL;

namespace MyDiem.Core.Interfaces
{
	public interface ICalendarMonthView:IBaseCalendarView
	{
		event EventHandler DateSelected;
		event EventHandler MonthChanged;
		event EventHandler EventSelected;

		DateTime SelectedDate{get;}
		DateTime SelectedMonth{get;}

		List<CalendarEvent> SelectedDateEvents{ get; set;}
		List<CalendarEvent> MonthEvents {get;set;}


	}
}

