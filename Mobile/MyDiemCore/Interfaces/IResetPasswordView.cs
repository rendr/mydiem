﻿using System;
using MonkeyArms;

namespace MyDiemCore
{
    public interface IResetPasswordView : IMediatorTarget
    {
        string SubmittedEmail { get; }
        event EventHandler ResetSubmitted;
        void ShowMessage (string message);
    }
}

