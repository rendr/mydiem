using System;
using MonkeyArms;
using System.Collections.Generic;

namespace MyDiem.Core.Interfaces
{
	public interface IParentDirectoryView:IBaseCalendarView
	{
		Invoker ParentSelected{ get; }

		Invoker Search{ get; }

		Invoker Reset{ get; }

		string SearchPlaceHolderText{ get; set; }

		string SearchQuery{ get; }

		MyDiem.Core.BL.Parent SelectedParent{ get; }

		void UpdateContacts (List<MyDiem.Core.BL.Parent> contacts);

		void GoParentDetails ();

		void HideLoadingIndicator ();

		void ShowNotSubscribedPrompt ();

		void ShowNoMatchesPrompt ();
	}
}

