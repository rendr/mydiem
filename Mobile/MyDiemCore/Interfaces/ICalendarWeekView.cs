using System;
using MyDiem.Core.BL;
using System.Collections.Generic;

namespace MyDiem.Core.Interfaces
{
	public interface ICalendarWeekView:IBaseCalendarView
	{
		event EventHandler WeekDateChanged;
		event EventHandler SelectedDateChanged;
		event EventHandler EventSelected;

		DateTime WeekDate{ get; }

		DateTime SelectedDate { get; }

		List<CalendarEvent> WeekEvents{ get; set;}

		List<CalendarEvent> SelectedDateEvents{ get; set;}
	}
}

