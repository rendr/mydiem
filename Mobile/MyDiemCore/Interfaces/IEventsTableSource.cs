using System;
using MonkeyArms;
using MyDiem.Core.BL;
using System.Collections.Generic;

namespace MyDiem.Core.Interfaces
{
	public interface IEventsTableSource:IMediatorTarget
	{
		event EventHandler EventSelected;

		List<CalendarEvent> TableItems{ get; set; }
	}
}

