using System;
using MonkeyArms;
using System.Collections.Generic;
using MyDiem.Core.BL;

namespace MyDiem.Core.Interfaces
{
	public interface IRegisterView:IMediatorTarget
	{
		event EventHandler RegistrationSubmitted;

		List<County> Counties{ get; set;}
		List<Region> Regions{ get; set;}
        List<State> States{ get; set;}

		string SubmittedFirstName{get;}
		string SubmittedLastName{get;}
		string SubmittedEmail{get;}
		string SubmittedZip{get;}
		string SubmittedPassword{get;}
		string SubmittedPasswordVerify{get;}
		string SubmittedRegionTitle{get;}
		string SubmittedCountyTitle{get;}


		void HandleSuccess(string message);
		void ShowRegistrationError(string message);
	}
}

