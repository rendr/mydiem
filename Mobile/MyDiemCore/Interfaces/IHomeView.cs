using System;
using MonkeyArms;

namespace MyDiem.Core.Interfaces
{
	public interface IHomeView:IMediatorTarget
	{
        event EventHandler Login, Register, Reset;
		void GoLogin();
		void GoRegister();
        void GoReset ();
    }
}

