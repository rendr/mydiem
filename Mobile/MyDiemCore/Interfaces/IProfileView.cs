using System;
using MyDiem.Core.BL;
using System.Collections.Generic;

namespace MyDiem.Core.Interfaces
{
	public interface IProfileView:IBaseCalendarView
	{
		event EventHandler Logout;
		event EventHandler UpdateSubmitted;

		void SetUserData (User appUser);

		void ShowUpdateSuccess ();

		string SubmittedPassword {
			get;
		}

		string SubmittedFirstName {
			get;

		}

		string SubmittedLastName {
			get;
		}

		string SubmittedEmail {
			get;
		}

		string SubmittedRegion {
			get;
		}

		string SubmittedCounty {
			get;
		}

		string SubmittedZip {
			get;
		}

        bool SubmittedHidePhone1 {
            get;
        }

        string SubmittedPhone1 {
            get;
        }

        bool SubmittedHidePhone2 {
            get;
        }

        string SubmittedPhone2 {
            get;
        }

        bool SubmittedHideAddress {
            get;
        }

        string SubmittedStreet {
            get;
        }
       
        string SelectedState
        {
            get;
        }

        bool IsParent {
            get;
        }

        List<State> States {
            set;
        }

		List<Region> Regions {
			set;
		}

		List<County> Counties {
			set;
		}

		void GoLogout ();
	}
}

