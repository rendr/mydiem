using System;

using MonkeyArms;
using System.Collections.Generic;
using MyDiem.Core.BL;

namespace MyDiem.Core.Interfaces
{
	public interface ICalendarEventListTableSource:IMediatorTarget
	{
		event EventHandler EventSelected;

		List<CalendarEvent> Events{ get; set; }

		void Init (Dictionary<string, List<CalendarEvent>> days, int todaySectionIndex, int todayRowIndex);

	}
}

