﻿using System;
using MonkeyArms;
using System.Collections.Generic;
using MyDiem.Core.BL;

namespace MyDiem.Core.Interfaces
{
	public interface IMapView:IMediatorTarget
	{
		event EventHandler ParentSelected;

		Parent SelectedParent{ get; }

		List<Parent> Contacts{ get; set; }

		Parent OriginUser{ get; set; }

		void GoParentDetails ();
	}
}

