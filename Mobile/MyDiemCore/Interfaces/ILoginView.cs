using System;
using MonkeyArms;

namespace MyDiem.Core.Interfaces
{
	public interface ILoginView:IMediatorTarget
	{

		event EventHandler LoginSubmitted;

		string SubmittedPassword{get;}
		string SubmittedEmail{get;}

		void ShowBadPasswordPrompt();
		void DisableUIAndShowNetworkIndicator ();
		void PromptInvalidatedUser();
		void GoCalendar ();
		void EnableControls();

	}
}

