using System;
using System.Collections.Generic;
using MonkeyArms;

namespace MyDiem.Core.Interfaces
{
	public interface ISettingsMenuTableSource:IMediatorTarget
	{
		Invoker SettingsRowSelected{ get; }

		int SelectedSectionIndex{ get; set; }

		int SelectedRowIndex{ get; set; }

		bool IsCalendarConnectPurchased{ get; set; }

		void SetOptions (Dictionary<string, List<string>> options);

		void EnableSubscribableOptions ();
	}
}

