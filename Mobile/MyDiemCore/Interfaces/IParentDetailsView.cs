using System;
using MonkeyArms;

namespace MyDiem.Core.Interfaces
{
	public interface IParentDetailsView:IBaseCalendarView
	{
		string ScreenTitle{get;set;}
		void UpdateTableSource(MyDiem.Core.BL.Parent selectedParent);
	}
}

