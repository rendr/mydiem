using System;
using System.Collections.Generic;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.CustomEvents;

namespace MyDiem.Core.ViewModel
{
	public class FilterMenuViewModel
	{
		public event EventHandler OpenRegionPicker = delegate {}, OpenCountyPicker = delegate {}, OpenTypePicker = delegate {}, CategoriesSelected = delegate {};

		private  Region selectedRegion;
		public static List<string> SelectedCategories;

		public void RegionClicked ()
		{
			OpenRegionPicker (null, new EventArgs ());
		}

		public void CountyClicked ()
		{
			OpenCountyPicker (null, new EventArgs ());
		}

		public void TypeClicked ()
		{
			OpenTypePicker (null, new EventArgs ());
		}

		public void FilterCategories (List<string> enabledCategories)
		{
			SelectedCategories = enabledCategories;

			CategoriesSelected (null, EventArgs.Empty);
		}
	}
}

