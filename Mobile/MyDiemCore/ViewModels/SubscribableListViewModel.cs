using System;
using System.Collections.Generic;
using System.Linq;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.CustomEvents;
using System.Diagnostics;
using MonkeyArms;

namespace MyDiem.Core.ViewModel
{
	public class SubscribableListViewModel:IInjectingTarget
	{
		public List<string> SelectedCategories;
		string selectedFilterType = "Any";

		public virtual string SelectedFilterType {
			get {
				return selectedFilterType;
			}
			set {
				selectedFilterType = value;
			}
		}

		private SchoolGroup MainCalendarCategory;
		public Subscribable SelectedSubscribable;

		public event EventHandler SubscribablesChanged = delegate{}, SubscribableSelected = delegate {}, PromptForSubscribableEmail = delegate{}, SubscribableEmailFailed = delegate{}, PromptForSubscribablePassword = delegate {}, SubscribablePasswordFailed = delegate{}, PromptForSubscribablePurchase = delegate {};

		[Inject]
		public SubscribableManager SM;
		[Inject]
		public UserManager UM;

		[Inject]
		public virtual User AppUser {
			get;
			set;
		}

		public SubscribableListViewModel ()
		{
			DIUtil.InjectProps (this);
			SM.GetAllSubscribablesFinished += HandleGetAllSubscribablesFinished;
		}

		void HandleGetAllSubscribablesFinished (object sender, EventArgs e)
		{
			SchoolGroup group = new SchoolGroup ();
			group.Title = "Calendars";
			group.SubCategories = SM.Organizations;
			MainCalendarCategory = group;
			InitCalendars ();
		}

		public virtual void GetAllSubscribables (long RegionID = -1, long CountyID = -1)
		{
			Debug.WriteLine ("GetAllSubscribables {0}, {1}", RegionID, CountyID);

			if (RegionID != -1 || CountyID != -1) {
				SM.GetAllSubscribables (RegionID, CountyID, AppUser.ID);
			} else {
				SM.GetAllSubscribables (AppUser.ID);
			}
		}

		public List<SchoolGroup> SchoolGroups {
			get;
			set;
		}

		public string SEARCH_PLACEHOLDER = "Search";

		public void SetMainCalendarCategory (SchoolGroup cat)
		{
			MainCalendarCategory = cat;
			InitCalendars ();
		}

		private string SearchQuery = "";

		public void FilterCalendars (string query)
		{
			SearchQuery = query.ToLower ();
			ApplyFilters ();
		}

		public void ApplyFilters ()
		{
			if (SelectedCategories == null) {
				SelectedCategories = GetCategories ();
			}

			List<SchoolGroup> TempSubCategories = MainCalendarCategory.SubCategories.Select (i => i.Clone ()).ToList ();
			//			List<SchoolGroup> TempSubCategories = MainCalendarCategory.SubCategories.Where(sc => sc.Subscribables.Count > 0 ).ToList();
			List<SchoolGroup> FilteredCategories = new List<SchoolGroup> ();

			var subscribableIDs = UM.UserCalendarSubscriptions.Select (s => s.SubscribableID).Distinct ().ToList ();

			foreach (var schoolGroup in TempSubCategories) {
				if (schoolGroup.Title.ToLower ().Contains (SearchQuery)
				    || schoolGroup.Subscribables.Any (s => s.title.ToLower ().Contains (SearchQuery))) {
					//check if subscribables are of correct type
					if (selectedFilterType != "Any") {
						schoolGroup.Subscribables = schoolGroup.Subscribables.Where (s => s.type.ToLower () == selectedFilterType.ToLower ()).ToList ();
					}

					//check if subscribable is in correct category
					schoolGroup.Subscribables = schoolGroup.Subscribables.Where (s => SelectedCategories.Contains (s.category)).ToList ();


					if (!schoolGroup.Title.ToLower ().Contains (SearchQuery)) {
						//check if matches search criteria
						schoolGroup.Subscribables = schoolGroup.Subscribables.Where (s => s.title.ToLower ().Contains (SearchQuery)).ToList ();
					}

//					schoolGroup.Subscribables = schoolGroup.Subscribables.Where (s => !subscribableIDs.Contains(s.ID)).ToList ();

					if (schoolGroup.Subscribables.Count > 0) {
						FilteredCategories.Add (schoolGroup);
					}
				}
			}

			SchoolGroups = FilteredCategories;

			SubscribablesChanged (null, new EventArgs ());
		}

		public void InitCalendars ()
		{
			//			SchoolGroups = new List<SchoolGroup> ();
			//			SchoolGroups = MainCalendarCategory.SubCategories.Where(sc => sc.Subscribables.Count > 0 ).ToList();

			//			if(SelectedFilterType != "Any"){
			//				foreach(SchoolGroup group in SchoolGroups){
			//					group.Subscribables = group.Subscribables.Where(ce =>(ce.type.Contains(SelectedFilterType.ToLower()) && SelectedCategories.Contains(ce.category)   )).ToList();
			//				}
			//				SchoolGroups = MainCalendarCategory.SubCategories.Where(sc => sc.Subscribables.Count > 0 ).ToList();
			//			}
			//
			//			SubscribablesChanged(null, new EventArgs());
			ApplyFilters ();
		}

		public void ResetCalendars ()
		{
			SearchQuery = "";
			InitCalendars ();
		}

		public void SelectSubscribable (Subscribable selectedCalendar)
		{
			SelectedSubscribable = selectedCalendar;
			SubscribableSelected (null, new EventArgs ());
		}

		public void SelectPasswordProtectedSubscribable (Subscribable selectedCalendar)
		{
			SelectedSubscribable = selectedCalendar;
			PromptForSubscribablePassword (null, new EventArgs ());
			
		}

		public void SelectPremiumSubscribable (Subscribable selectedCalendar)
		{
			SelectedSubscribable = selectedCalendar;
			PromptForSubscribablePurchase (null, new EventArgs ());
		}

		public void SubmitSubscribableEmail (string text)
		{
			SM.EmailValidated += HandleEmailValidated;
			SM.EmailValidationFailed += HandleEmailValidationFailed;
			SM.ValidateSubscribableEmail (SelectedSubscribable.ID, text, SelectedSubscribable.type);
		}

		void HandleEmailValidationFailed (object sender, EventArgs e)
		{
			SM.EmailValidated -= HandleEmailValidated;
			SM.EmailValidationFailed -= HandleEmailValidationFailed;

			SubscribableEmailFailed (null, new EventArgs ());
		}

		void HandleEmailValidated (object sender, EventArgs e)
		{
			SM.EmailValidated -= HandleEmailValidated;
			SM.EmailValidationFailed -= HandleEmailValidationFailed;
			SubscribableSelected (null, new EventArgs ());
		}

		public void SelectEmailProtectedSubscribable (Subscribable selectedSubscribable)
		{
			SelectedSubscribable = selectedSubscribable;
			PromptForSubscribableEmail (null, new EventArgs ());
		}

		public void SubmitSubscribablePassword (string password)
		{
			SM.PasswordValidated += HandlePasswordValidated;
			SM.PasswordValidationFailed += HandlePasswordValidationFailed;

			SM.ValidateSubscribablePassword (SelectedSubscribable.ID, password, SelectedSubscribable.type);
		}

		void HandlePasswordValidationFailed (object sender, EventArgs e)
		{
			SM.PasswordValidated -= HandlePasswordValidated;
			SM.PasswordValidationFailed -= HandlePasswordValidationFailed;

			SubscribablePasswordFailed (null, new EventArgs ());
		}

		void HandlePasswordValidated (object sender, EventArgs e)
		{
			SM.PasswordValidated -= HandlePasswordValidated;
			SM.PasswordValidationFailed -= HandlePasswordValidationFailed;

			SubscribableSelected (null, new EventArgs ());
		}

		public List<string> GetCategories ()
		{
			return MainCalendarCategory.SubCategories.SelectMany (s => s.Subscribables).Select (s => s.category).Distinct ().ToList ();
		}

		public static bool UserDoesNotHaveAccess (int[] userCalendarIDs, int[] userDirectoryIDs, Subscribable subscribable)
		{
			return subscribable.isEmailProtected || (subscribable.isPasswordProtected && !userCalendarIDs.Contains (subscribable.ID) && !userDirectoryIDs.Contains (subscribable.ID));
		}
	}
}

