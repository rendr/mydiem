﻿using System;
using System.Collections.Generic;
using MonkeyArms;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;

namespace MyDiem.Core.ViewModel
{
    public class SocialMediaViewModel : IInjectingTarget
    {
        [Inject]
        public static User AppUser;

        public static List<SocialMediaGroup> SocialMediaGroupList;

        public SocialMediaViewModel ()
        {
            DIUtil.InjectProps (this);
        }

        public static void GetData (Action action)
        {
            SocialMediaManager.GetFinished += (s, e) => {
                SocialMediaGroupList = SocialMediaManager.SocialMediaGroupList;
                SocialMediaGroupList.RemoveAll (x => x.Name.Contains ("U.S. Holidays"));
                action ();
            };
            SocialMediaManager.GetData (DI.Get<User> ().ID);
        }
    }
}

