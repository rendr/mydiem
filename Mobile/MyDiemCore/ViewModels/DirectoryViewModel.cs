using System;
using System.Collections.Generic;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.CustomEvents;
using System.Linq;
using MonkeyArms;

namespace MyDiem.Core.ViewModel
{
	public  class DirectoryViewModel:IInjectingTarget
	{
		[Inject]
		public ParentDirectoryManager PDM;
		[Inject]
		public User AppUser;

		public DirectoryViewModel ()
		{
			DIUtil.InjectProps (this);
		}

		public  string SEARCH_PLACEHOLDER = "Search";
		public readonly Invoker ContactsUpdated = new Invoker ();
		public  List<MyDiem.Core.BL.Parent> Contacts;

		public event EventHandler GoParentDetails = delegate {};

		private  Parent _selectedParent;

		public virtual void GetContacts ()
		{
			PDM.ParentsUpdated += (sender, e) => {
				Contacts = PDM.Parents;

				var distinctContactsByEmail = from c in Contacts
				                              group c by c.Email into g
				                              select g.First ();

				Contacts = distinctContactsByEmail.ToList ();

				ContactsUpdated.Invoke (InvokerArgs.Empty);
			};
			PDM.GetParentDirectory (AppUser.ID);
		}

		public  Parent SelectedParent {
			get {
				return _selectedParent;
			}
		}

		public virtual void SelectParent (Parent p)
		{
			_selectedParent = p;
			GoParentDetails (null, new EventArgs ());
		}

		private  string SearchQuery;

		public virtual void FilterContacts (string query)
		{
			SearchQuery = query;

			if (query.Length == 0) {
				ResetContacts ();
				return;
			}
			if (query.Length < 3) {
				return;
			}
			SearchAndFilterContacts ();
		}

		private  List<string> FilterParams;

		public  void FilterContacts (List<string> parameters)
		{
			FilterParams = parameters;
			SearchAndFilterContacts ();
		}

		private  void SearchAndFilterContacts ()
		{

			Contacts = PDM.Parents;

			if (FilterParams != null && FilterParams.Count > 0 && !string.IsNullOrEmpty (SearchQuery)) {
				Contacts = Contacts.Where (p => 
					FilterParams.Contains (p.DirectoryTitle)
				&& p.SearchKeywords.ToLower ().Contains (SearchQuery)
				&& p.Children.Any (c => FilterParams.Contains (c.Grade))).ToList ();
			} else if (string.IsNullOrEmpty (SearchQuery) && FilterParams != null && FilterParams.Count > 0) {
				Contacts = Contacts.Where (p => 
					FilterParams.Contains (p.DirectoryTitle)
				&& p.Children.Any (c => FilterParams.Contains (c.Grade))).ToList ();
			} else if (FilterParams == null || FilterParams.Count == 0 && !string.IsNullOrEmpty (SearchQuery)) {
				Contacts = Contacts.Where (ce => (ce.SearchKeywords.ToLower ().Contains (SearchQuery))).ToList ();
			} 

			var distinctContactsByEmail = from e in Contacts
			                              group e by e.Email into g
			                              select g.First ();

			Contacts = distinctContactsByEmail.ToList ();

			ContactsUpdated.Invoke (InvokerArgs.Empty);
		}

		public  void ResetContacts ()
		{
			SearchQuery = "";
			SearchAndFilterContacts ();

		}

		public  List<string> GetDirectoryNames ()
		{
			if (Contacts == null) {
				return new List<string> ();
			}
			return Contacts.Select (c => c.DirectoryTitle).Distinct ().ToList ();
		}

		public  List<string> GetGrades ()
		{
			if (Contacts == null) {
				return new List<string> ();
			}
			var list = Contacts.SelectMany (c => c.Children).Select (k => k.Grade).Distinct ().ToList ();
			list.Sort (delegate(string c1, string c2) { 
				var numberGrades = new string[]{ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
				if (c1 == "K" && c2 == "FAC") {
					return 1;
				} else if (c1 == "K") {
					return -1;
				} else if (numberGrades.Contains (c1) && numberGrades.Contains (c2)) {
					return (int.Parse (c1) > int.Parse (c2)) ? 1 : -1;
				} else {
					return 1;

				}
			}
			);
			return list;
		}
	}
}

