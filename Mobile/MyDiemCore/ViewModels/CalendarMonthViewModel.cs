using System;
using System.Collections.Generic;

using MyDiem.Core.BL;


namespace MyDiem.Core.ViewModel
{
	public class CalendarMonthViewModel
	{
		public event EventHandler SelectedDateChanged = delegate{}, SelectedDateEventsChanged = delegate{};

		private DateTime selectedDate;

		public DateTime SelectedDate {
			get {
				return selectedDate;
			}
			set {
				selectedDate = value;
				SelectedDateChanged(this, new EventArgs());
			}
		}

		private List<CalendarEvent> selectedDateEvents;
		public List<CalendarEvent> SelectedDateEvents {
			get {
				return selectedDateEvents;
			}
			set {
				selectedDateEvents = value;
				SelectedDateEventsChanged(this, new EventArgs());
			}
		}
	}
}

