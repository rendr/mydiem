using System;
using System.Collections.Generic;

using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.CustomEvents;
using MonkeyArms;

namespace MyDiem.Core.ViewModel
{


	public class EventListViewModel:IInjectingTarget
	{
		[Inject]
		public EventManager EM;

		public CalendarEvent SelectedEvent;
		public event EventHandler GoEventDetailsEvent = delegate{};

		public Dictionary<string, List<CalendarEvent>> Days;

		public List<CalendarEvent> AllEvents;

		public int TodaySectionIndex = 0;
		public int TodayRowIndex = 0;

		public EventListViewModel(){
			DIUtil.InjectProps(this);
		}


		public Dictionary<string, List<CalendarEvent>> InitDaysDictionary(List<CalendarEvent> events)
		{
			Days = new Dictionary<string, List<CalendarEvent>>();

			AllEvents = new List<CalendarEvent> ();
			if(events == null){
				AllEvents = EM.CalendarEvents;
			}else{
				AllEvents = events;
			}
			AllEvents.Sort(delegate(CalendarEvent p1, CalendarEvent p2) { return p1.StartDateTime.CompareTo(p2.StartDateTime); });
			foreach(CalendarEvent cEvent in AllEvents){
				
				if (!Days.ContainsKey (cEvent.StartDateTime.ToString ("D"))) {
					Days [cEvent.StartDateTime.ToString ("D")] = new List<CalendarEvent> ();
					
				}
				Days[cEvent.StartDateTime.ToString("D")].Add(cEvent);
				
				if(cEvent.StartDateTime < DateTime.Now){
					TodaySectionIndex = Days.Keys.Count - 1;
					TodayRowIndex = Days[cEvent.StartDateTime.ToString("D")].Count -1;

				}
			}

			return Days;

		}

		public void GoEventDetails(CalendarEvent selectedEvent)
		{
			this.SelectedEvent = selectedEvent;
			GoEventDetailsEvent(null, new EventArgs());
		}

	
	}
}

