using System;
using System.Collections.Generic;
using System.Diagnostics;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.CustomEvents;
using MonkeyArms;


namespace MyDiem.Core.ViewModel
{
	public class SlideMenuViewModel: IInjectingTarget
	{
		[Inject]
		public UserManager UM;
		[Inject]
		public SubscribableManager SM;
		[Inject]
		public User AppUser;
		public Invoker CalendarCategoriesChangedEvent = new Invoker ();
		public const string VIEW_OPTIONS_LABEL = "View Options";
		public const string MONTH_LABEL = "Calendar Month";
		public const string WEEK_LABEL = "Calendar Week";
		public const string LIST_LABEL = "Calendar List";
		public const string DIRECTORY_LABEL = "Directory";
		public const string SUBSCRIBE_LABEL = "Add Subscription";
		public const string SETTINGS_LABEL = "Settings";
		public const string MY_CALENDARS_LABEL = "My Subscriptions";
		public const string PROFILE_LABEL = "Profile";
		public const string LINK_CALENDAR_LABEL = "Link To Calendar app";
		public const string LINK_GOOGLE_CAL_LABEL = "Link to Google Cal";
		public const string ABOUT_LABEL = "About My Diem";
		public const string CALENDARS_LABEL = "Calendars";
        public const string FACEBOOK = "Facebook";
        public const string TWITTER = "Twitter";

        public SlideMenuViewModel ()
		{
			DIUtil.InjectProps (this);
			Init ();
		}

		public virtual List<SchoolGroup> CalendarCategories {
			get {

				return SM.Organizations;
			}
		}

		public Dictionary<string, List<string>> OptionsDictionary {
			get {
				return GenerateOptionsDictionary ();
			}
		}

		protected void Init ()
		{
			if (SM.Organizations == null || SM.Organizations.Count == 0) {
				SM.GetAllSubscribablesFinished += HandleGetAllSubscribables;
				SM.GetAllSubscribables (AppUser.ID);
			}
		}

		private Dictionary<string, List<string>> GenerateOptionsDictionary ()
		{
			var Options = new Dictionary<string, List<string>> ();
			
			string VIEW_OPTIONS = VIEW_OPTIONS_LABEL;
			Options.Add (VIEW_OPTIONS, new List<string> ());
			Options [VIEW_OPTIONS].Add (MONTH_LABEL);
			Options [VIEW_OPTIONS].Add (WEEK_LABEL);
			Options [VIEW_OPTIONS].Add (LIST_LABEL);
			Options [VIEW_OPTIONS].Add (DIRECTORY_LABEL);
            Options [VIEW_OPTIONS].Add (FACEBOOK);
            Options [VIEW_OPTIONS].Add (TWITTER);

            string USER = SETTINGS_LABEL;
			Options.Add (USER, new List<string> ());
			Options [USER].Add (SUBSCRIBE_LABEL);
			Options [USER].Add (MY_CALENDARS_LABEL);
			Options [USER].Add (PROFILE_LABEL);
			Options [USER].Add (LINK_CALENDAR_LABEL);
			Options [USER].Add (LINK_GOOGLE_CAL_LABEL);
			Options [USER].Add (ABOUT_LABEL);

			return Options;
		}

		private void HandleGetAllSubscribables (object sender, EventArgs e)
		{
			Debug.WriteLine ("HandleGetAllSubscribables");
			CalendarCategoriesChangedEvent.Invoke (InvokerArgs.Empty);
			
		}
	}
}

