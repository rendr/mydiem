using System;
using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using MonkeyArms;
using MyDiem.Core.BL;
using MyDiem.Core.Interfaces;

namespace MyDiem.Droid.Fragments
{
    public class ProfileFragment : BaseFragment, IProfileView
	{
		protected UserFormFragment UserForm;
		protected User AppUser;

		public override void OnCreate (Bundle savedInstanceState)
		{
			this.ScreenTitle = GetString (Resource.String.profileScreenTitle);
			this.LayoutID = Resource.Layout.Profile;

			this.rightHeaderButtonTitle = "Save";
			this.RightHeaderButtonVisiblity = ViewStates.Visible;

			base.OnCreate (savedInstanceState);
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = base.OnCreateView (inflater, container, savedInstanceState);
            RightHeaderButton.SetBackgroundResource (Resource.Drawable.GreenButtonBackground);

            var transaction = FragmentManager.BeginTransaction ();

			UserForm = new UserFormFragment ();
            UserForm.ShowZipCodeRow = true;
            UserForm.EnableZip = false;
            UserForm.EnableEmail = false;
			transaction.Replace (Resource.Id.userFormContainer, UserForm);

			transaction.Commit ();

            var logoutBtn = view.FindViewById (Resource.Id.logoutButton) as Button;
			logoutBtn.Click += (s, e) => Logout (this, new EventArgs ());

            return view;
		}

		public override void OnStart ()
		{
			base.OnStart ();
			DI.RequestMediator (this);
		}

		public override void OnStop ()
		{
			base.OnStop ();
			DI.DestroyMediator (this);
		}

		protected override void RightHeaderButtonClicked ()
		{
            UpdateSubmitted (this, new EventArgs ());
            ShowSavingIndicator ();
		}

		ProgressDialog SavingIndicator;

		void ShowSavingIndicator ()
		{
			if (SavingIndicator == null) {
				SavingIndicator = new ProgressDialog (this.Activity);
				SavingIndicator.SetCancelable (false);
				SavingIndicator.SetMessage ("Saving");
			}
			SavingIndicator.Show ();
		}

		#region IProfileView implementation

		public void SetUserData (MyDiem.Core.BL.User appUser)
		{
			this.AppUser = appUser;
			UserForm.User = appUser;
		}

		public List<MyDiem.Core.BL.Region> Regions {
			set {
				UserForm.Regions = value;
			}
		}

		public List<MyDiem.Core.BL.County> Counties {
			set {
				UserForm.Counties = value;
			}
		}

        public List<MyDiem.Core.BL.State> States {
            set {
                UserForm.States = value;
            }
        }

		public event EventHandler Logout;
		public event EventHandler UpdateSubmitted;

		public string SubmittedPassword {
			get {

				return UserForm.GetEditTextValue (Resource.Id.passwordEditText);
			}
		}

		public string SubmittedFirstName {
			get {
				return UserForm.GetEditTextValue (Resource.Id.firstNamEditText);
			}
		}

		public string SubmittedLastName {
			get {
				return UserForm.GetEditTextValue (Resource.Id.lastNameEditText);
			}
		}

		public string SubmittedEmail {
			get {
				return UserForm.GetEditTextValue (Resource.Id.emailEditText);
			}
		}

		public string SubmittedZip {
			get {
				return UserForm.GetEditTextValue (Resource.Id.zipEditText);
			}
		}

		public string SubmittedRegion {
			get {
				return UserForm.GetSpinnerRowValue (Resource.Id.privateSchoolsRadioButton, Resource.Id.regionSpinner);
			}
		}

		public string SubmittedCounty {
			get {
				return UserForm.GetSpinnerRowValue (Resource.Id.publicSchoolsRadioButton, Resource.Id.countySpinner);
			}
		}

        public string SubmittedPhone1 {
            get {
                return UserForm.GetEditTextValue (Resource.Id.phone1EditText);
            }
        }

        public string SubmittedPhone2 {
            get {
                return UserForm.GetEditTextValue (Resource.Id.phone2EditText);
            }
        }

        public string SubmittedStreet {
            get {
                return UserForm.GetEditTextValue (Resource.Id.streetEditText);
            }
        }

        public bool SubmittedHidePhone1
        {
            get
            {
                return !View.FindViewById<ToggleButton>(Resource.Id.sharePhone1Toggle).Checked;
            }
        }

        public bool SubmittedHidePhone2
        {
            get
            {
                return !View.FindViewById<ToggleButton>(Resource.Id.sharePhone2Toggle).Checked;
            }
        }

        public bool SubmittedHideAddress
        {
            get
            {
                return !View.FindViewById<ToggleButton>(Resource.Id.shareStreetRadioButton).Checked;
            }
        }

        public bool IsParent
        {
            get
            {
                return AppUser.IsParent;
            }
        }

        public string SelectedState
        {
            get
            {
                return AppUser.State;
            }
        }

        public void GoLogout ()
		{
			Activity.StartActivity (typeof(HomeActivity));

		}

		public void ShowUpdateSuccess ()
		{
			this.Activity.RunOnUiThread (() => {
				Toast.MakeText (this.Activity, Resource.String.profileUpdateSuccessPrompt, ToastLength.Long).Show ();
				SavingIndicator.Hide();
			});
		}

		#endregion
	}
}

