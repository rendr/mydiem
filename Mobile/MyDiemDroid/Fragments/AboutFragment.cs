using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using MyDiem.Core.Interfaces;
using Android.Webkit;
using MonkeyArms;
using Android.Text;
using Android.Text.Method;
using Android.Content.PM;

namespace MyDiem.Droid.Fragments
{
	public class AboutFragment : BaseFragment, IAboutView
	{


		#region IAboutView implementation



		#endregion

		protected TextView MainTextView;

		public override void OnCreate (Bundle savedInstanceState)
		{
			this.LayoutID = Resource.Layout.About;

			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			this.ScreenTitle = Activity.GetString (Resource.String.settingsAboutMyDiemLabel);
			var view = base.OnCreateView (inflater, container, savedInstanceState);
			MainTextView = (TextView)view.FindViewById (Resource.Id.aboutTextView);
			MainTextView.MovementMethod = LinkMovementMethod.Instance;

			UpdateText ();
			return view;
		}

		void UpdateText ()
		{

			MainTextView.SetText (Html.FromHtml (this.Activity.GetString(Resource.String.aboutText) + "<br><br>App Version: " + Activity.PackageManager.GetPackageInfo(Activity.PackageName,0).VersionName), TextView.BufferType.Normal);
		}

		public override void OnStart ()
		{
			base.OnStart ();
			DI.RequestMediator (this);

		}

		public override void OnStop ()
		{
			base.OnStop ();
			DI.DestroyMediator (this);
		}

	}
}

