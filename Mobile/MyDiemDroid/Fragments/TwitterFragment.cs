﻿using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.ViewModel;
using MyDiem.Droid.Fragments;

namespace MyDiem.Droid
{
    public class TwitterFragment : BaseFragment
    {
        ProgressDialog LoadingIndicator;
       
        public override void OnCreate (Bundle savedInstanceState)
        {
            this.LayoutID = Resource.Layout.Twitter;
            base.OnCreate (savedInstanceState);
        }

        public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
			ScreenTitle = "Twitter/Instagram";

			var view = base.OnCreateView (inflater, container, savedInstanceState);

            var h = view.FindViewById<LinearLayout> (Resource.Id.header);
            h.SetBackgroundColor (Android.Graphics.Color.Rgb (64, 153, 255));

            var list = view.FindViewById<ListView> (Resource.Id.twitterList);
            list.ItemClick += (sender, e) => {
                var index = e.Position;
                var url = SocialMediaManager.SocialMediaGroupList [index].Twitter;

                if (string.IsNullOrEmpty (url))
                {
                    Toast.MakeText (view.Context, Resource.String.noUrlEntered, ToastLength.Long).Show ();
                } else
                {
                    var browserIntent = new Intent (Intent.ActionView, Android.Net.Uri.Parse (url));
                    StartActivity (browserIntent);
                }
            };

            if (LoadingIndicator == null)
            {
                LoadingIndicator = new ProgressDialog (this.Activity);
                LoadingIndicator.SetCancelable (false);
                LoadingIndicator.SetMessage (GetString (Resource.String.loadingFeeds));
                LoadingIndicator.Show ();
            }

            SocialMediaViewModel.GetData (() => {
                try
                {
                    this.Activity.RunOnUiThread (() => {
                        list.Adapter = new ArrayAdapter<string> (container.Context, Resource.Layout.SimpleSpinnerItem, SocialMediaManager.SocialMediaGroupList.Select (x => x.Name).ToList ());
                    });
                }
                catch (System.Exception ex)
                {
                    System.Diagnostics.Debug.Write (ex.Message);
                }
                LoadingIndicator.Hide ();
            });

            return view;
        }
    }
}

