﻿using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.ViewModel;
using MyDiem.Droid.Fragments;

namespace MyDiem.Droid
{
    public class FacebookFragment : BaseFragment
    {
        ProgressDialog LoadingIndicator;
        private List<string> schools = new List<string>();
        ListView list;

        public override void OnCreate (Bundle savedInstanceState)
        {
            this.LayoutID = Resource.Layout.Facebook;
            base.OnCreate (savedInstanceState);
        }

        public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            ScreenTitle = "Facebook";
            var view = base.OnCreateView (inflater, container, savedInstanceState);
           
            var h = view.FindViewById <LinearLayout>(Resource.Id.header);
            h.SetBackgroundColor (Android.Graphics.Color.Rgb (59, 89, 152));

            list = view.FindViewById<ListView> (Resource.Id.facebookList);
            list.ItemClick += (sender, e) => {
                var index = e.Position;
                var url = SocialMediaManager.SocialMediaGroupList [index].Facebook;

                if (string.IsNullOrEmpty (url))
                { 
                    Toast.MakeText (view.Context, Resource.String.noUrlEntered, ToastLength.Long).Show ();
                } 
                else
                {
                    var browserIntent = new Intent (Intent.ActionView, Android.Net.Uri.Parse (url));
                    StartActivity (browserIntent);
                }
            };

            if (LoadingIndicator == null)
            {
                LoadingIndicator = new ProgressDialog (this.Activity);
                LoadingIndicator.SetCancelable (false);
                LoadingIndicator.SetMessage (GetString (Resource.String.loadingFeeds));
                LoadingIndicator.Show ();
            }

            SocialMediaViewModel.GetData (() => {
                schools = SocialMediaManager.SocialMediaGroupList.Select (x => x.Name).ToList ();
                try
                {
                    this.Activity.RunOnUiThread (() => {
                        list.Adapter = new ArrayAdapter<string> (container.Context, Resource.Layout.SimpleSpinnerItem, schools);
                    });
                } 
                catch (System.Exception ex)
                {
                    System.Diagnostics.Debug.Write (ex.Message);
                }
                LoadingIndicator.Hide ();
            }); 

            return view;
        }
    }
}
