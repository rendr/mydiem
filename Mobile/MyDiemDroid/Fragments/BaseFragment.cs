using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace MyDiem.Droid.Fragments
{
    public class BaseFragment : Fragment
    {
        public event EventHandler ToggleSettingsView = delegate{};

        protected string ScreenTitle;
        protected SearchView SearchInput;
        protected ViewStates SearchVisibility = ViewStates.Gone;
        protected ViewStates FilterVisibility = ViewStates.Gone;
        protected ViewStates MapVisibility = ViewStates.Gone;
        protected ViewStates RightHeaderButtonVisiblity = ViewStates.Gone;
        protected ViewStates ArrowButtonVisiblity = ViewStates.Gone;
        protected string rightHeaderButtonTitle = "";
        protected ImageView FilterButton;
        protected ImageView MapButton;
        protected Button RightHeaderButton;

        public string RightHeaderButtonTitle
        {
            get
            {
                return rightHeaderButtonTitle;
            }
            set
            {
                rightHeaderButtonTitle = value;
                if (RightHeaderButton != null)
                {
                    RightHeaderButton.Text = value;
                }
            }
        }

        protected TextView TitleText;

        #region IBaseCalendarView implementation

        public void GoCalendarMonthView()
        {
            //do nothing
        }

        public void GoCalendarWeekView()
        {
            //do nothing
        }

        public void GoCalendarDayView()
        {
            //do nothing
        }

        public void GoSettingsView(MyDiem.Core.BL.User user)
        {
            //do nothing
        }

        public void GoSubscribablesView(MyDiem.Core.BL.SchoolGroup category, string title)
        {
            //do nothing
        }

        public void GoEventDetailsView(MyDiem.Core.BL.CalendarEvent selectedEvent)
        {
            //do nothing
        }

        public void GoAboutView()
        {
            //do nothing
        }

        public void GoParentDirectory()
        {
            //do nothing
        }

        public void GoFacebookDirectory ()
        {
            //do nothing
        }

        public void GoTwitterDirectory ()
        {
            //do nothing
        }

        #endregion

        protected int LayoutID;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            if (container == null)
            {
                return null;
            }

            var frame = new LinearLayout(Activity);
            frame.Orientation = Orientation.Vertical;

            InitHeader(inflater, frame);

		
            var view = inflater.Inflate(LayoutID, null);
            frame.AddView(view);
		
            return frame;
        }

        void InitHeader(LayoutInflater inflater, LinearLayout frame)
        {
            var headerView = inflater.Inflate(Resource.Layout.ScreenHeader, null);
            frame.AddView(headerView);

            TitleText = headerView.FindViewById(Resource.Id.screenTitle) as TextView;
            TitleText.Text = ScreenTitle;

            var settingsButton = headerView.FindViewById(Resource.Id.settingsButton) as ImageView;

            settingsButton.Click += (object sender, EventArgs e) =>
            {
                ToggleSettingsView(this, new EventArgs());
            };


            TouchDelegate touchDelegate = new TouchDelegate(new Android.Graphics.Rect()
                {
                    Left = 0,
                    Right = 100,
                    Top = 0,
                    Bottom = 100
                }, settingsButton);

            // Sets the TouchDelegate on the parent view, such that touches 
            // within the touch delegate bounds are routed to the child.
            if (settingsButton.Parent is View)
            {
                (settingsButton.Parent as View).TouchDelegate = touchDelegate;
            }

            InitSearchInput(headerView, TitleText, settingsButton);
            InitFilterButton(headerView);
            InitMapButton(headerView);
            InitRightHeaderButton(headerView);

            headerView.FindViewById<ImageView>(Resource.Id.leftArrowButton).Visibility = headerView.FindViewById<ImageView>(Resource.Id.rightArrowButton).Visibility = ArrowButtonVisiblity;

        }

        virtual protected void SubmitSearch(string query)
        {
//            throw(new NotImplementedException("OnSearchSubmitted needs to be overridden if header search field is going to be used"));
        }

        virtual protected void ResetSearch()
        {
//            throw(new NotImplementedException("OnSearchReset needs to be overridden if header search field is going to be used"));
        }

        virtual protected void RightHeaderButtonClicked()
        {
//            throw(new NotImplementedException("RightHeaderButtonClicked needs to be overridden if right header button is going to be used"));
        }

        virtual protected void FilterButtonClicked()
        {
//            throw(new NotImplementedException("FilterButtonClicked needs to be overridden if right header button is going to be used"));
        }

        virtual protected void MapButtonClicked()
        {
//            throw(new NotImplementedException("MapButtonClicked needs to be overridden if map button is going to be used"));
        }

        void InitSearchInput(View headerView, TextView titleText, ImageView settingsButton)
        {
            SearchInput = headerView.FindViewById(Resource.Id.searchInput) as SearchView;
            SearchInput.QueryTextFocusChange += (object sender, View.FocusChangeEventArgs e) =>
            {
                if (!e.HasFocus)
                {
                    settingsButton.Visibility = titleText.Visibility = ViewStates.Visible;
                    ResetSearch();
                }
            };
            SearchInput.QueryTextChange += (object sender, SearchView.QueryTextChangeEventArgs e) =>
            {
                if (!String.IsNullOrEmpty(SearchInput.Query))
                {
                    SubmitSearch(SearchInput.Query);
                }
                else
                {
                    ResetSearch();
                }
                    
            };
            SearchInput.SearchClick += (object sender, EventArgs e) =>
            {
                settingsButton.Visibility = titleText.Visibility = ViewStates.Gone;
            };
            SearchInput.QueryTextSubmit += (object sender, SearchView.QueryTextSubmitEventArgs e) =>
            {
                SubmitSearch(SearchInput.Query);
            };

            int id = SearchInput.Context.Resources.GetIdentifier("android:id/search_src_text", null, null);
            TextView textView = (TextView)SearchInput.FindViewById(id);
            textView.SetTextColor(Color.White);

            SearchInput.Visibility = SearchVisibility;
        }

        void InitFilterButton(View headerView)
        {
            FilterButton = headerView.FindViewById(Resource.Id.filterButton) as ImageView;
            FilterButton.Visibility = FilterVisibility;
            FilterButton.Click += (object sender, EventArgs e) =>
            {
                FilterButtonClicked();
            };
        }

        void InitMapButton(View headerView)
        {
            MapButton = headerView.FindViewById(Resource.Id.mapButton) as ImageView;
            MapButton.Visibility = MapVisibility;
            MapButton.Click += (object sender, EventArgs e) =>
            {
                MapButtonClicked();
            };
        }

        void InitRightHeaderButton(View headerView)
        {
            RightHeaderButton = headerView.FindViewById(Resource.Id.rightHeaderButton) as Button;
            RightHeaderButton.Text = rightHeaderButtonTitle;
            RightHeaderButton.Visibility = RightHeaderButtonVisiblity;

            RightHeaderButton.Click += (object sender, EventArgs e) =>
            {
                RightHeaderButtonClicked();
            };
        }
    }
}

