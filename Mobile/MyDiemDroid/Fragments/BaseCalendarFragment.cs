using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using MyDiem.Views;
using MyDiem.Core.BL;
using MyDiem.Droid.Adapters;
using System.Globalization;
using MyDiemCore;

namespace MyDiem.Droid.Fragments
{
	public class BaseCalendarFragment : BaseFragment
	{
		/*
		*	FIELDS
		*/
		protected ImageButton NextButton, PreviousButton;
		protected ListView EventsListView;
		protected int[] CellIDs = new int[] {
			Resource.Id.cell1,
			Resource.Id.cell2,
			Resource.Id.cell3,
			Resource.Id.cell4,
			Resource.Id.cell5,
			Resource.Id.cell6,
			Resource.Id.cell7
		};
		protected DateUtil DateUtil = new DateUtil ();
		/*
		*	HELPER FUNCTIONS
		*/
		public int GetSectionIndexForItem (IDictionary<string, object> SelectedItem, Dictionary<string, List<CalendarEvent>> Days)
		{
			if (SelectedItem == null) {
				return 0;
			}
			var list = Days.Keys.ToList ();
			var iDictionary = SelectedItem ["day"] as string;
			return list.IndexOf (iDictionary);
		}

		public int GetRowIndexForItem (IDictionary<string, object> SelectedItem, Dictionary<string, List<CalendarEvent>> Days)
		{

			if (SelectedItem == null) {
				return 0;
			}
			var dayKey = SelectedItem ["day"] as string;
			return Days [dayKey].IndexOf (SelectedItem ["event"] as CalendarEvent);

		}

		protected virtual  void SetReferences (View view)
		{
			NextButton = view.FindViewById<ImageButton> (Resource.Id.rightArrowButton);
			PreviousButton = view.FindViewById<ImageButton> (Resource.Id.leftArrowButton);
			EventsListView = view.FindViewById<ListView> (Resource.Id.eventsListView);
		}

		protected void DeselectWeekCells (View week1View)
		{
			foreach (var id in CellIDs) {
				var notatedCalendarCell = week1View.FindViewById<NotatedCalendarCell> (id);
				if (notatedCalendarCell.Highlighted) {
					notatedCalendarCell.Highlighted = false;
				}
			}
		}

		protected List<CellSwatch> GetCellSwatches (NotatedCalendarCell cell, List<CalendarEvent> events)
		{
			var swatches = new List<CellSwatch> ();

			if (events == null) {
				return swatches;
			}

			foreach (var calendarEvent in events) {
				if (DateUtil.DateBetweenTwoDates (cell.Date, calendarEvent.StartDateTime, calendarEvent.EndDateTime)) {
					var cellSwatch = new CellSwatch () {
						Color = Android.Graphics.Color.ParseColor ("#" + calendarEvent.SwatchColorString.Trim ()),
						SpanWidth = calendarEvent.AllDay
					};
					swatches.Add (cellSwatch);
				}
			}
			return swatches;
		}

		protected virtual void SelectCellByDate (List<View> weekViews, DateTime date)
		{
			foreach (var view in weekViews) {
				foreach (var id in CellIDs) {
					var cell = view.FindViewById<NotatedCalendarCell> (id);
					if (cell.Date.DayOfYear == date.DayOfYear && cell.Date.Year == date.Year) {
						cell.Highlighted = true;
					} else {
						cell.Highlighted = false;
					}
				}
			}
		}

		protected GroupedSimpleAdapter InitMultiDayEventListAdapter (JavaList<IDictionary<string, object>> dayList)
		{
			return new GroupedSimpleAdapter (this.Activity.BaseContext, dayList, Resource.Layout.GroupedListItem, new string[] {
				"startTime",
				"itemTitle",
				"headerTitle",
				"detailText",
				"leftIcon"
			}, new int[] {
				Resource.Id.groupListItemLeftLabelTextView,
				Resource.Id.groupListItemCenterLabelTextView,
				Resource.Id.groupHeaderTextView,
				Resource.Id.groupListItemRightTextView,
				Resource.Id.leftIcon
			});
		}

		protected JavaList<IDictionary<string, object>> InitMultiDayEventList (Dictionary<string, List<CalendarEvent>> daysDictionary)
		{
			var dayList = new JavaList<IDictionary<string, object>> ();
			foreach (var keyValuePair in daysDictionary) {
				var d = new JavaDictionary<string, object> ();
				d ["headerTitle"] = keyValuePair.Key;
				d ["detailText"] = "";
				foreach (var cEvent in keyValuePair.Value as List<CalendarEvent>) {
					if (!cEvent.AllDay) {
						d ["startTime"] = cEvent.StartDateTime.ToShortTimeString ();
					} else {
						d ["startTime"] = GetString (Resource.String.allDayLabel);
					}
					d ["itemTitle"] = cEvent.Title;
					d ["day"] = keyValuePair.Key;
					d ["leftIcon"] = null;
					d ["rightIcon"] = null;
					d ["event"] = cEvent;
					dayList.Add (d);
					d = new JavaDictionary<string, object> ();
					d ["headerTitle"] = null;
				}
			}
			return dayList;

		}

		public Dictionary<string, List<CalendarEvent>> InitDaysDictionary (List<CalendarEvent> events)
		{
			var Days = new Dictionary<string, List<CalendarEvent>> ();
			events.Sort (delegate(CalendarEvent p1, CalendarEvent p2) {
				return p1.StartDateTime.CompareTo (p2.StartDateTime);
			});
			foreach (CalendarEvent cEvent in events) {

				if (!Days.ContainsKey (cEvent.StartDateTime.ToString ("D"))) {
					Days [cEvent.StartDateTime.ToString ("D")] = new List<CalendarEvent> ();

				}
				Days [cEvent.StartDateTime.ToString ("D")].Add (cEvent);

			}

			return Days;

		}

		protected CalendarEvent GetEventFromSectionAndRowIndex (int sectionIndex, int rowIndex, Dictionary<string, List<CalendarEvent>> days)
		{
			string[] keys = new string[days.Keys.Count];
			days.Keys.CopyTo (keys, 0);
			string keyName = keys [sectionIndex];
			List<CalendarEvent> events = days [keyName];
			return events [rowIndex];
		}

		protected void UpdateTitle (DateTime date)
		{
			TitleText.Text = date.ToString ("MMM yyyy", CultureInfo.InvariantCulture).ToUpper ();
		}
	}
}

