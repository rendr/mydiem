using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using MyDiem.Core.Interfaces;
using MyDiem.Droid.Adapters;
using MonkeyArms;
using MyDiem.Core.BL;
using Delegates;
using MyDiem.Core.ViewModel;

namespace MyDiem.Droid.Fragments
{
	public class SubscribablesFragment : BaseFragment, ISubscribableListView
	{
		ProgressDialog FilteringDialog;

		///////////////////////////
		//INTERFACE IMPLEMENTATIONS
		///////////////////////////

		#region ISubscribableListView implementation

		public event EventHandler FilterSubscribables = delegate {};
		public event EventHandler ResetSubscribables = delegate {};
		public event EventHandler SubscribableSelected = delegate {};
		public event EventHandler PasswordSubmitted = delegate {};
		public event EventHandler EmailSubmitted = delegate {};

		public SubscribablesFragment (string title)
		{
			this.ScreenTitle = title;
		}

		public void ShowLoadingPrompt ()
		{
			Toast.MakeText (this.Activity.BaseContext, Resource.String.loading, ToastLength.Long).Show ();
		}

		GroupedSimpleAdapter SubscribablesAdapter;
		ListView SubscribablesList;
		JavaList<IDictionary<string, object>> ListItems;

		public void UpdateSubscribablesList (List<MyDiem.Core.BL.SchoolGroup> schoolGroups, int[] userCalendarIDs, int[] userDirectoryIDs)
		{

			if (schoolGroups == null) {
				return;
			}


			ListItems = new  JavaList<IDictionary<string, object>> ();



			foreach (var schoolGroup in schoolGroups) {
				var d = new JavaDictionary<string,object> ();
				d ["headerTitle"] = schoolGroup.Title;
				d ["detailText"] = "";

				foreach (var subscribable in schoolGroup.Subscribables) {
					d ["itemTitle"] = subscribable.title;

					if (subscribable.type == Subscribable.TYPE_CALENDAR) {
//						d ["leftIcon"] = Resource.Drawable.CalendarIcon;
						d ["leftIcon"] = Android.Resource.Drawable.IcMenuToday;
					} else if (subscribable.type == Subscribable.TYPE_DIRECTORY) {
//						d ["leftIcon"] = Resource.Drawable.DirectoryIcon;
						d ["leftIcon"] = Android.Resource.Drawable.IcMenuCall;
					}

					if (subscribable.isPremium) {
						d ["rightIcon"] = Resource.Drawable.LockIcon;
					} else if (SubscribableListViewModel.UserDoesNotHaveAccess (userCalendarIDs, userDirectoryIDs, subscribable)) {

						d ["rightIcon"] = Resource.Drawable.LockIcon;
					} else {
						d ["rightIcon"] = null;
					}

					d ["subscribable"] = subscribable;

					ListItems.Add (d);

					d = new JavaDictionary<string,object> ();
					d ["headerTitle"] = null;

				}


			}



			SubscribablesAdapter = new GroupedSimpleAdapter (this.Activity.BaseContext, ListItems, Resource.Layout.GroupedListItem, new string[] {
				"itemTitle",
				"headerTitle",
				"detailText",
				"leftIcon",
				"rightIcon"
			}, new int[] {
				Resource.Id.groupListItemCenterLabelTextView,
				Resource.Id.groupHeaderTextView,
				Resource.Id.groupListItemRightTextView,
				Resource.Id.leftIcon,
				Resource.Id.rightIcon

			});

			if (SubscribablesList != null) {
				SubscribablesList.ItemClick -= HandleItemClick;
			}

			SubscribablesList = View.FindViewById (Resource.Id.listView) as ListView;
			SubscribablesList.ItemClick += HandleItemClick;
			this.Activity.RunOnUiThread (() => {
				SubscribablesList.Adapter = SubscribablesAdapter;
				FilteringDialog.Hide ();
			});

		}

		void HandleItemClick (object sender, AdapterView.ItemClickEventArgs e)
		{
			this.selectedSubscribable = ListItems [e.Position] ["subscribable"] as Subscribable;
			SubscribableSelected (this, new EventArgs ());
		}

		public void PromptForSubscribablePurchase (string sku)
		{
			throw new NotImplementedException ();
		}

		public void PromptForSubscribablePassword ()
		{
			var view = this.Activity.LayoutInflater.Inflate (Resource.Layout.PasswordInput, null);

			AlertDialog.Builder alert = new AlertDialog.Builder (this.Activity);                 
			alert.SetTitle (Resource.String.unlock);
			alert.SetMessage (Resource.String.enterPassword);  
			alert.SetPositiveButton (Resource.String.submitButtonLabel, (object sender, DialogClickEventArgs args) => {
				submittedPassword = view.FindViewById<EditText> (Resource.Id.passwordTextField).Text;
				PasswordSubmitted (this, EventArgs.Empty);
			});
			// Set an EditText view to get user input   

			alert.SetView (view);
			alert.Show ();
		}

		public void SubscribablePasswordFailed ()
		{
			this.Activity.RunOnUiThread (() => {
				Toast.MakeText (this.Activity, Resource.String.incorrectPassword, ToastLength.Long).Show ();
			});
		}

		public void ShowWhiteListPrompt ()
		{
			var alert = new AlertDialog.Builder (this.Activity);
			alert.SetTitle (Resource.String.accessRestricted);
			alert.SetMessage (Resource.String.whiteListMessage);
			alert.Show ();
		}

		public void HandleSubscribableEmailFailed ()
		{
			throw new NotImplementedException ();
		}

		public void HandleSubscribableSelected (MyDiem.Core.BL.Subscribable subscribable, MyDiem.Core.BL.Subscription subscription)
		{
			this.Activity.StartActivity (typeof(SubscribableSettingsActivity));
		}

		public string SearchPlaceHolderText {
			get;
			set;
		}

		public string SearchQuery {
			get {
				return (View.FindViewById (Resource.Id.searchInput) as SearchView).Query;
			}
		}

		private string submittedPassword;

		public string SubmittedPassword {
			get {
				return submittedPassword;
			}
		}

		public string SubmittedEmail {
			get {
				throw new NotImplementedException ();
			}
		}

		private Subscribable selectedSubscribable;

		public MyDiem.Core.BL.Subscribable SelectedSubscribable {
			get {
				return selectedSubscribable;
			}
			set {
				selectedSubscribable = value;
			}
		}

		#endregion

		///////////////////////////
		//OVERRIDES
		///////////////////////////
		public override void OnCreate (Bundle savedInstanceState)
		{
			this.FilterVisibility = ViewStates.Visible;
			this.SearchVisibility = ViewStates.Visible;
			this.LayoutID = Resource.Layout.Subscribables;
			base.OnCreate (savedInstanceState);

			FilteringDialog = new ProgressDialog (this.Activity);
			FilteringDialog.SetMessage (this.Activity.GetString (Resource.String.filtering));
			// Create your fragment here
		}

		FilterMenuDelegate filterMenu;

		public override void OnResume ()
		{
			base.OnResume ();
			DI.RequestMediator (this);
			if (filterMenu == null) {
				filterMenu = new FilterMenuDelegate (FilterButton, this.Activity);
				filterMenu.ShowFilteringPrompt += (object sender, EventArgs e) => {
					FilteringDialog.Show ();
				};
			}
		}

		public override void OnPause ()
		{
			base.OnPause ();
			DI.DestroyMediator (this);
		}

		protected override void SubmitSearch (string query)
		{
			FilterSubscribables (this, new EventArgs ());
		}

		protected override void ResetSearch ()
		{
			ResetSubscribables (this, new EventArgs ());
		}

		protected override void FilterButtonClicked ()
		{

		}


	}
}

