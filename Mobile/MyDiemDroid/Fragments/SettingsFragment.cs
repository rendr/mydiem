using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using MyDiem.Core.Interfaces;
using MonkeyArms;
using MyDiem.Core.Invokers;
using MyDiem.Core.BL;
using MyDiemCore;

namespace MyDiem.Droid.Fragments
{

	public class SettingsFragment : Fragment, ISettingsMenuTableSource
	{
		ScrollView ViewScroller;

		View OverlayView;

		#region ISettingsMenuTableSource implementation

		protected static bool PromptLinkCalendar = true;

		private bool isCalendarConnectPurchased;

		public bool IsCalendarConnectPurchased {
			get {
				return isCalendarConnectPurchased;
			}
			set {
				isCalendarConnectPurchased = value;
				UpdateCalendarConnectLabel ();
			}
		}

		public Invoker SettingsRowSelected{ get; private set; }

		public void SetOptions (Dictionary<string, List<string>> options)
		{
			//ignoring on Android
		}

		private int selectedSectionIndex;

		public int SelectedSectionIndex {
			get {
				return selectedSectionIndex;
			}
			set {
				selectedSectionIndex = value;
			}
		}

		private int selectedRowIndex;

		public int SelectedRowIndex {
			get {
				return selectedRowIndex;
			}
			set {
				selectedRowIndex = value;
			}
		}

		public void EnableSubscribableOptions ()
		{
			Activity.RunOnUiThread (() => {
				SetSubscribableOptionsEnabled (true);
			});
		}

		#endregion

		public static SettingsFragment NewInstance ()
		{

			var frag = new SettingsFragment { Arguments = new Bundle () };
			return frag;
		}

		public SettingsFragment ()
		{
			SettingsRowSelected = new Invoker ();
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			if (container == null) {
				return null;
			}
			ViewScroller = new ScrollView (Activity);
			ViewScroller.FillViewport = true;
			ViewScroller.AddView (inflater.Inflate (Resource.Layout.Settings, null));
			SetSubscribableOptionsEnabled (false);
			AddEventListeners (ViewScroller);
			initOverlay ();


			return ViewScroller;
		}

		protected void initOverlay ()
		{
			OverlayView = ViewScroller.FindViewById<View> (Resource.Id.overlay);
			View closeButton = ViewScroller.FindViewById<View> (Resource.Id.closebutton);
			closeButton.Click += (object sender, EventArgs e) => {
				UpdatePrompts (false);
			};


			UpdatePrompts (!DI.Get<User> ().CalendarConnectPrompted);


		}


		protected void UpdatePrompts (Boolean showPrompt)
		{
			PromptLinkCalendar = showPrompt;
			if (PromptLinkCalendar) {
				(OverlayView.FindViewById<TextView> (Resource.Id.calendarPromptText)).Text = DI.Get<User> ().CalendarConnect ? GetString (Resource.String.calendarPromptLink) : GetString (Resource.String.calendarPromptBuy);
			}
			OverlayView.Enabled = PromptLinkCalendar;
			OverlayView.Visibility = PromptLinkCalendar ? ViewStates.Visible : ViewStates.Invisible;

			DI.Get<User> ().CalendarConnectPrompted = true;
			DI.Get<SaveUserToDBInputInvoker> ().Invoke (new SaveUserToDBInputArgs (DI.Get<User> ()));
		}


		protected void SetSubscribableOptionsEnabled (bool enabled)
		{
			ViewScroller.FindViewById<TextView> (Resource.Id.subscribeRow).Enabled = enabled;
			ViewScroller.FindViewById<TextView> (Resource.Id.myCalendarsRow).Enabled = enabled;

			ViewScroller.FindViewById<TextView> (Resource.Id.subscribeRow).Alpha = enabled ? 1 : .25f;
			ViewScroller.FindViewById<TextView> (Resource.Id.myCalendarsRow).Alpha = enabled ? 1 : .25f;
		}

		public override void OnStart ()
		{
			base.OnStart ();
			DI.RequestMediator (this);
			DI.Get<CalendarConnectPurchasedInvoker> ().Invoked += HandleCalendarConnectPurchased;
			;
		}

		void HandleCalendarConnectPurchased (object sender, EventArgs e)
		{
			UpdateCalendarConnectLabel ();
		}

		public override void OnStop ()
		{
			base.OnStop ();
			DI.DestroyMediator (this);
			DI.Get<CalendarConnectPurchasedInvoker> ().Invoked -= HandleCalendarConnectPurchased;
			;
		}

		void AddEventListeners (ScrollView scroller)
		{
			(scroller.FindViewById (Resource.Id.MonthRow) as TextView).Click += (object sender, EventArgs e) => {
				onRowClicked (0, 0);
			};
			(scroller.FindViewById (Resource.Id.WeekRow) as TextView).Click += (object sender, EventArgs e) => {
				onRowClicked (0, 1);
			};
			(scroller.FindViewById (Resource.Id.ListRow) as TextView).Click += (object sender, EventArgs e) => {
				onRowClicked (0, 2);
			};
			(scroller.FindViewById (Resource.Id.directoryRow) as TextView).Click += (object sender, EventArgs e) => {
				onRowClicked (0, 3);
			};
            (scroller.FindViewById (Resource.Id.facebookRow) as TextView).Click += (object sender, EventArgs e) => {
                onRowClicked (0, 4);
            };
            (scroller.FindViewById (Resource.Id.twitterRow) as TextView).Click += (object sender, EventArgs e) => {
                onRowClicked (0, 5);
            };

			(scroller.FindViewById (Resource.Id.subscribeRow) as TextView).Click += (object sender, EventArgs e) => {
				onRowClicked (1, 0);
			};
			(scroller.FindViewById (Resource.Id.myCalendarsRow) as TextView).Click += (object sender, EventArgs e) => {
				onRowClicked (1, 1);
			};
			(scroller.FindViewById (Resource.Id.profileRow) as TextView).Click += (object sender, EventArgs e) => {
				onRowClicked (1, 2);
			};

			UpdateCalendarConnectLabel ();

			(scroller.FindViewById (Resource.Id.linkToGoogleCalRow) as TextView).Click += (object sender, EventArgs e) => {
				onRowClicked (1, 4);
			};
			(scroller.FindViewById (Resource.Id.aboutRow) as TextView).Click += (object sender, EventArgs e) => {
				onRowClicked (1, 5);
			};

		}

		void UpdateCalendarConnectLabel ()
		{
			if ((ViewScroller.FindViewById (Resource.Id.linkToGoogleCalRow) as TextView) == null) {
				return;
			}
			(ViewScroller.FindViewById (Resource.Id.linkToGoogleCalRow) as TextView).Text = (isCalendarConnectPurchased) ? GetString (Resource.String.calendarConnectPurchased) : GetString (Resource.String.calendarConnectNotPurchased);
		}

		void onRowClicked (int sectionIndex, int rowIndex)
		{
			this.selectedSectionIndex = sectionIndex;
			this.SelectedRowIndex = rowIndex;
			SettingsRowSelected.Invoke (InvokerArgs.Empty);
		}
	}
}

