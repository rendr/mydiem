using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using MyDiem.Core.Interfaces;
using MonkeyArms;
using MyDiem.Core.ViewModel;
using MyDiem.Views;
using System.Globalization;
using MyDiem.Core.BL;
using CustomEvents;
using Java.Lang.Annotation;

namespace MyDiem.Droid.Fragments
{
	public class CalendarMonthFragmentScreen : BaseCalendarFragment, ICalendarMonthView, IInjectingTarget
	{
		/*
		*	FIELDS
		*/
		EventListAdapter ListAdapter;
		FrameLayout Week1View, Week2View, Week3View, Week4View, Week5View, Week6View;
		DateTime firstDayOfMonth;
		int MonthOffset;
		/*
		*	CONSTRUCTOR
		*/
		public CalendarMonthFragmentScreen ()
		{
			this.ArrowButtonVisiblity = ViewStates.Visible;
			this.RightHeaderButtonVisiblity = ViewStates.Visible;
			this.rightHeaderButtonTitle = "Today";

		}
		/*
		*	INTERFACE IMPLEMENTATIONS
		*/

		#region ICalendarMonthView implementation

		public event EventHandler DateSelected = delegate {};
		public event EventHandler MonthChanged = delegate {};
		public event EventHandler EventSelected = delegate {};

		public DateTime SelectedMonth {
			get {
				return firstDayOfMonth;
			}
		}

		private DateTime selectedDate = DateTime.Now;

		public DateTime SelectedDate {
			get {
				return selectedDate;

			}

		}

		private List<CalendarEvent> events;

		public List<CalendarEvent> SelectedDateEvents {
			get {
				return events;
			}
			set {
				this.events = value;
				ListAdapter = new EventListAdapter (this.Activity, events);
				EventsListView.Adapter = ListAdapter;
			}
		}

		private List<CalendarEvent> monthEvents;

		public List<CalendarEvent> MonthEvents {
			get {
				return monthEvents;
			}
			set {
				monthEvents = value;
				UpdateCellSwatchesForWeekView (Week1View);
				UpdateCellSwatchesForWeekView (Week2View);
				UpdateCellSwatchesForWeekView (Week3View);
				UpdateCellSwatchesForWeekView (Week4View);
				UpdateCellSwatchesForWeekView (Week5View);
				UpdateCellSwatchesForWeekView (Week6View);
			}
		}

		#endregion

		/*
		*	OVERRIDES
		*/
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			this.LayoutID = Resource.Layout.CalendarMonthScreen;
			var view = base.OnCreateView (inflater, container, savedInstanceState);
			SetReferences (view);
			AddEventListeners ();
			return view;
		}

		public override void OnStart ()
		{

			base.OnStart ();
			if (selectedDate == DateTime.MinValue) {
				UpdateCalendar (DateTime.Today);
			} else {
				UpdateCalendar (selectedDate);
			}
			DI.RequestMediator (this);

		}

		public override void OnStop ()
		{
			base.OnStop ();
			DI.DestroyMediator (this);

		}

		override protected void SetReferences (View view)
		{
			base.SetReferences (view);

			Week1View = view.FindViewById<FrameLayout> (Resource.Id.week1);
			Week2View = view.FindViewById<FrameLayout> (Resource.Id.week2);
			Week3View = view.FindViewById<FrameLayout> (Resource.Id.week3);
			Week4View = view.FindViewById<FrameLayout> (Resource.Id.week4);
			Week5View = view.FindViewById<FrameLayout> (Resource.Id.week5);
			Week6View = view.FindViewById<FrameLayout> (Resource.Id.week6);


		}

		protected override void RightHeaderButtonClicked ()
		{
			MonthOffset = 0;
			UpdateCalendar (DateTime.Now);
		}
		/*
		*	HELPER FUNCTIONS
		*/
		void UpdateCellSwatchesForWeekView (FrameLayout weekView)
		{
			
			for (var i = 0; i < 7; i++) {
				var cell = weekView.FindViewById<NotatedCalendarCell> (CellIDs [i]);
				cell.Swatches = GetCellSwatches (cell, monthEvents);
				
			}
			
			
		}

		void AddEventListeners ()
		{

			EventsListView.ItemClick += HandleEventClicked;

			PreviousButton.Click += (object sender, EventArgs e) => {

				MonthOffset--;
				UpdateCalendar (DateTime.Today.AddMonths (MonthOffset));

			};
			NextButton.Click += (object sender, EventArgs e) => {
				MonthOffset++;
				UpdateCalendar (DateTime.Today.AddMonths (MonthOffset));
			};



			AddEventListenersForWeek (Week1View);
			AddEventListenersForWeek (Week2View);
			AddEventListenersForWeek (Week3View);
			AddEventListenersForWeek (Week4View);
			AddEventListenersForWeek (Week5View);
			AddEventListenersForWeek (Week6View);
		}

		void AddEventListenersForWeek (FrameLayout week1View)
		{
			foreach (var id in CellIDs) {
				var cell = week1View.FindViewById<NotatedCalendarCell> (id);
				cell.Click += (object sender, EventArgs e) => {
					DeselectAllCells ();
					var nc = sender as NotatedCalendarCell;
					selectedDate = nc.Date;
					nc.Highlighted = true;
					DateSelected (this, EventArgs.Empty);
				};
			}
		}

		void DeselectAllCells ()
		{
			DeselectWeekCells (Week1View);
			DeselectWeekCells (Week2View);
			DeselectWeekCells (Week3View);
			DeselectWeekCells (Week4View);
			DeselectWeekCells (Week5View);
			DeselectWeekCells (Week6View);
		}

		void UpdateCalendar (DateTime targetDate)
		{
			firstDayOfMonth = new DateTime (targetDate.Year, targetDate.Month, 1);

			UpdateTitle (firstDayOfMonth);
			selectedDate = targetDate;
			var firstVisibleDay = firstDayOfMonth;
			while (firstVisibleDay.DayOfWeek != DayOfWeek.Sunday) {
				firstVisibleDay = firstVisibleDay.AddDays (-1);
			}
			UpdateWeekLabels (firstVisibleDay, Week1View);
			UpdateWeekLabels (firstVisibleDay.AddDays (7), Week2View);
			UpdateWeekLabels (firstVisibleDay.AddDays (14), Week3View);
			UpdateWeekLabels (firstVisibleDay.AddDays (21), Week4View);
			UpdateWeekLabels (firstVisibleDay.AddDays (28), Week5View);
			UpdateWeekLabels (firstVisibleDay.AddDays (35), Week6View);

			if (selectedDate.Month == targetDate.Month && selectedDate.Year == targetDate.Year) {
				SelectCellByDate (new List<View> {
					Week1View, Week2View, Week3View, Week4View, Week5View, Week6View
				}, selectedDate);
			} else {
				DeselectAllCells ();
			}

			EventsListView.Adapter = new EventListAdapter (this.Activity, new List<CalendarEvent> ());

			MonthChanged (this, EventArgs.Empty);

		}

		void HandleEventClicked (object sender, AdapterView.ItemClickEventArgs e)
		{
			EventSelected (this, new SelectedCalendarEventArgs (SelectedDateEvents [e.Position]));
		}

		void UpdateWeekLabels (DateTime date, FrameLayout weekView)
		{



			if (date.Month > firstDayOfMonth.Month || date.Year > firstDayOfMonth.Year) {
				weekView.Visibility = ViewStates.Gone;
				return;
			} else {
				weekView.Visibility = ViewStates.Visible;
			}
			


			for (var i = 0; i < 7; i++) {
				var cell = weekView.FindViewById<NotatedCalendarCell> (CellIDs [i]);
				cell.Date = date;
				if (date.Month != firstDayOfMonth.Month) {
					cell.BackgroundColor = new Android.Graphics.Color (200, 200, 200, 100);
				} else {
					cell.BackgroundColor = new Android.Graphics.Color (255, 255, 255);
				}
				date = date.AddDays (1);
			}
		}
	}
}

