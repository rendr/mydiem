using System;
using System.Collections.Generic;
using System.IO;
using Android.App;
using Android.Graphics;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.Lang;
using MyDiem.Core.BL;
using MyDiem.Core.Interfaces;
using Newtonsoft.Json.Linq;
using RestSharp;
using Xamarin.Media;

namespace MyDiem.Droid.Fragments
{
    public class UserFormFragment : Fragment
	{
        public bool HidePic { get; set; }

        ProgressDialog LoadingIndicator;
       
        public bool ShowZipCodeRow = true;
		
        private bool enableEmail = true;
		public bool EnableEmail {
			get {
				return enableEmail;
			}
			set {
				enableEmail = value;
				UpdateEmailRowState ();
			}
		}

        private bool enableZip = true;
        public bool EnableZip
        {
            get
            {
                return enableZip;
            }
            set
            {
                enableZip = value;
                UpdateZipField ();
            }
        }

		public User User {
			get;
			set;
		}

		public List<Core.BL.Region> Regions {
			get;
			set;
		}

		public List<County> Counties {
			get;
			set;
		}

        public List<Core.BL.State> States {
            get;
            set;
        }

		protected FrameLayout FormHolder;

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			if (container == null) {
				return null;
			}

			FormHolder = new FrameLayout (Activity);
			FormHolder.AddView (inflater.Inflate (Resource.Layout.UserForm, null));

			SetupRowToggle (Resource.Id.privateSchoolsRadioButton, Resource.Id.regionRow);
			SetupRowToggle (Resource.Id.publicSchoolsRadioButton, Resource.Id.countyRow);

			SetupExclusiveToggle (new List<int>{Resource.Id.privateSchoolsRadioButton, Resource.Id.publicSchoolsRadioButton});

			return FormHolder;
		}

		public override void OnStart ()
		{
			base.OnStart ();

			InitSpinnerRows (FormHolder);
			if (!User.IsEmpty) 
            {
				UpdateEditTextValues (FormHolder);
                UpdateRowStates ();
                UpdateEmailRowState ();
			}
            UpdateProfilePicture ();
            UpdateParentFields(FormHolder);
            UpdateZipField();
		}

		public string GetEditTextValue (int editTextID)
		{
			var et = View.FindViewById (editTextID) as EditText;
			return et.Text;
		}

		public string GetSpinnerRowValue (int checkBoxID, int spinnerID)
		{
			var checkBox = View.FindViewById (checkBoxID) as RadioButton;
			if (checkBox.Checked) {
				var spinner = View.FindViewById (spinnerID) as Spinner;
				return (spinner.SelectedItem as JavaDictionary) ["itemTitle"] as string;
			} else {
				return "";
			}

		}

        private void UpdateProfilePicture()
        {
            var pic = View.FindViewById<CircleImageView>(Resource.Id.profilePicture);
            pic.Visibility = HidePic ? ViewStates.Gone : ViewStates.Visible;

            var label = View.FindViewById<TextView> (Resource.Id.tapToEdit);
            label.Visibility = HidePic ? ViewStates.Gone : ViewStates.Visible;

            if (!User.IsEmpty && !string.IsNullOrEmpty (User.PhotoUrl))
            {
                pic.SetImageUrl(User.PhotoUrl);
            }
            else
            {
                pic.SetImageResource(Resource.Drawable.user);
            }

            pic.Touch += async (object sender, View.TouchEventArgs e) => {
                if(e.Event.Action == MotionEventActions.Up)
                {
                    try
                    {
                        var picker = new MediaPicker(this.View.Context);
                        var newPhoto = await picker.PickPhotoAsync();

                        if (LoadingIndicator == null)
                        {
                            LoadingIndicator = new ProgressDialog (this.Activity);
                            LoadingIndicator.SetCancelable (false);
                            LoadingIndicator.SetMessage (GetString (Resource.String.loading));
                            LoadingIndicator.Show ();
                        }

                        var resized = BitmapHelpers.LoadAndResizeBitmap (newPhoto.Path, 100, 100);
                        
                        ExifInterface exif = new ExifInterface (newPhoto.Path);
                        int orientation = exif.GetAttributeInt (ExifInterface.TagOrientation, (int)Android.Media.Orientation.Undefined);

                        if (float.Parse(Build.VERSION.Sdk) < 23)
                        {
                            resized = BitmapHelpers.RotateBitmap (resized, orientation);
                        }

                        var path = ExportBitmapAsPNG (resized);

                        var uploadUrl = UploadFile (path);
                        User.PhotoUrl = uploadUrl;
                        pic.SetImageUrl (uploadUrl);
                   
                        LoadingIndicator.Hide ();
                    }
                    catch(System.Exception ex)
                    {
                        Console.WriteLine(ex.Message); 
                    }
                }
            };
        }

        private string ExportBitmapAsPNG (Bitmap bitmap)
        {
            var sdCardPath = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
            var filePath = System.IO.Path.Combine (sdCardPath, "test.png");
            var stream = new FileStream (filePath, FileMode.Create);
            bitmap.Compress (Bitmap.CompressFormat.Png, 100, stream);
            stream.Close ();

            return filePath;
        }

        private string UploadFile (string path)
        {
            var client = new RestClient ("http://app.mydiem.com/index.cfm?event=mydiemAccess.uploadPhoto");
            var request = new RestRequest ("", Method.POST);

            request.AddParameter ("userID", User.ID);
            request.AddFile ("Photo", path);

            var response = client.Execute (request);
            var url = (string)(JObject.Parse (response.Content)) ["MESSAGE"];

            return url;
        }

		void UpdateRowStates ()
        {
            bool privateSchool = (User.RegionID > 0);
            bool publicSchool = (User.CountyID > 0);

			var privateSchoolRadioButton = View.FindViewById<RadioButton> (Resource.Id.privateSchoolsRadioButton);
            var regionRow = View.FindViewById (Resource.Id.regionRow);
			
            var countyRadioButton = View.FindViewById<RadioButton> (Resource.Id.publicSchoolsRadioButton);
            var countyRow = View.FindViewById (Resource.Id.countyRow);

			if (privateSchool)
            {
				privateSchoolRadioButton.Checked = true;
                regionRow.Visibility = ViewStates.Visible;
			}
            else if (publicSchool)
            {
				countyRadioButton.Checked = true;
                countyRow.Visibility = ViewStates.Visible;
			}
		}
		
        /*
		 *
		 *	PROTECTED HELPER FUNCTIONS
		 * 
		 */
		
        protected void UpdateEmailRowState ()
		{
			if (FormHolder != null) {
//				FormHolder.FindViewById (Resource.Id.emailRow).Visibility = (enableEmail) ? ViewStates.Visible : ViewStates.Gone;
				var emailRow = FormHolder.FindViewById<FrameLayout> (Resource.Id.emailRow);
				emailRow.SetChildrenEnabled (false);


				if (!enableEmail) {


					float px = TypedValue.ApplyDimension (ComplexUnitType.Dip, 20, Activity.Resources.DisplayMetrics);

					var layoutParams = new LinearLayout.LayoutParams (LinearLayout.LayoutParams.FillParent, LinearLayout.LayoutParams.WrapContent);
					layoutParams.SetMargins (0, (int)px, 0, 0);
					FormHolder.FindViewById (Resource.Id.passwordRow).LayoutParameters = layoutParams;
				}
			}
//			if (!showEmail) {
//				FormHolder.FindViewById <TextView> (Resource.Id.emailTextView).Text = FormHolder.FindViewById<EditText> (Resource.Id.emailEditText).Text;
//			}
		}

		protected void SetupExclusiveToggle (List<int> RadioButtonIdList)
		{
			foreach (int RadioButtonId in RadioButtonIdList){
				var radioButton = FormHolder.FindViewById (RadioButtonId) as RadioButton;
				radioButton.CheckedChange += (object sender, CompoundButton.CheckedChangeEventArgs e) => {
					if (e.IsChecked){
						int currentRadioButtonId = RadioButtonId;
						foreach (int OtherRadioButtonId in RadioButtonIdList){
							if (currentRadioButtonId != OtherRadioButtonId){
								var otherRadioButton = FormHolder.FindViewById (OtherRadioButtonId) as RadioButton;
								otherRadioButton.Checked = false;
							}
						}
					}
				};

			}


		}

		protected void SetupRowToggle (int RadioButtonId, int rowID)
		{
			var radioButton = FormHolder.FindViewById (RadioButtonId) as RadioButton;
			var row = FormHolder.FindViewById (rowID) as LinearLayout;
			
			radioButton.CheckedChange += (object sender, CompoundButton.CheckedChangeEventArgs e) => {
				if (!e.IsChecked) {
					row.Visibility = ViewStates.Gone;
				} else {
					row.Visibility = ViewStates.Visible;
				}
			};
		}

		protected void UpdateEditTextValues (FrameLayout frame)
		{
			(frame.FindViewById (Resource.Id.firstNamEditText) as EditText).Text = User.Fname;
			(frame.FindViewById (Resource.Id.lastNameEditText) as EditText).Text = User.Lname;
            (frame.FindViewById (Resource.Id.emailEditText) as EditText).Text = User.Email;
		}

        protected void UpdateZipField()
        {
            if (FormHolder == null) return;

            if (!ShowZipCodeRow) 
            {
                FormHolder.FindViewById<FrameLayout> (Resource.Id.zipRow).Visibility = ViewStates.Gone;
            } 
            else 
            {
                FormHolder.FindViewById<FrameLayout> (Resource.Id.zipRow).Visibility = ViewStates.Visible;
                FormHolder.FindViewById<EditText> (Resource.Id.zipEditText).Enabled = enableZip;
                FormHolder.FindViewById<EditText> (Resource.Id.zipEditText).Text = User.Zip;
            }
        }

        protected void UpdateParentFields(FrameLayout frame)
        {
            var sp1 = frame.FindViewById<LinearLayout>(Resource.Id.sharePhone1Row);
            var sp2 = frame.FindViewById<LinearLayout>(Resource.Id.sharePhone2Row);
            var sa  = frame.FindViewById<LinearLayout>(Resource.Id.shareStreetRow);
            var p1  = frame.FindViewById<FrameLayout>(Resource.Id.phone1Row);
            var p2  = frame.FindViewById<FrameLayout>(Resource.Id.phone2Row);
            var a   = frame.FindViewById<FrameLayout>(Resource.Id.streetRow);

            if (!User.IsEmpty && User.IsParent)
            {
                sp1.Visibility = ViewStates.Visible;
                sp1.FindViewById <ToggleButton>(Resource.Id.sharePhone1Toggle).Checked = User.HidePhone1 ?? false;

                sp2.Visibility = ViewStates.Visible;
                sp2.FindViewById<ToggleButton> (Resource.Id.sharePhone2Toggle).Checked = User.HidePhone2 ?? false;

                sa.Visibility = ViewStates.Visible;
                sa.FindViewById<ToggleButton> (Resource.Id.shareStreetRadioButton).Checked = User.HideAddress ?? false;

                p1.Visibility = ViewStates.Visible;
                p1.FindViewById<EditText> (Resource.Id.phone1EditText).Text = User.Phone1;

                p2.Visibility = ViewStates.Visible;
                p2.FindViewById<EditText> (Resource.Id.phone2EditText).Text = User.Phone2;

                a.Visibility = ViewStates.Visible;
                a.FindViewById<EditText> (Resource.Id.streetEditText).Text = User.Street;
            }
            else
            {
                sp1.Visibility = ViewStates.Gone;
                sp2.Visibility = ViewStates.Gone;
                sa .Visibility = ViewStates.Gone;
                p1 .Visibility = ViewStates.Gone;
                p2 .Visibility = ViewStates.Gone;
                a  .Visibility = ViewStates.Gone;
            }
        }

		protected void InitSpinnerRows (FrameLayout frame)
		{
			InitSpinnerRow<County> (frame, Resource.Id.countySpinner, Counties, User.CountyID);
            InitSpinnerRow<Core.BL.Region> (frame, Resource.Id.regionSpinner, Regions, User.RegionID);
            InitStates (frame); 
		}

        protected void InitStates(FrameLayout frame)
        {
            var statesSpinner = frame.FindViewById (Resource.Id.stateSpinner) as Spinner;
            InitSpinner<Core.BL.State> (statesSpinner, States);

            var selectedIndex = States.FindIndex (x => x.Title == User.State);
            statesSpinner.SetSelection (selectedIndex);

            statesSpinner.ItemSelected += (sender, e) => {
                var selectedState = States [e.Position];
                User.State = selectedState.Title;
                if (selectedState.Counties.Count > 0)
                {
                    InitSpinnerRow<County> (frame, Resource.Id.countySpinner, Counties, User.CountyID);
                }
                else
                {
                    InitSpinnerRow<County> (frame, Resource.Id.countySpinner, new List<County> { new County(0, "No matching counties")}, User.CountyID);
                }

                if (selectedState.Regions.Count > 0)
                {
                    InitSpinnerRow<Core.BL.Region> (frame, Resource.Id.regionSpinner, Regions, User.RegionID);
                } 
                else
                {
                    InitSpinnerRow<Core.BL.Region> (frame, Resource.Id.regionSpinner, new List<Core.BL.Region> { new Core.BL.Region (0, "No matching regions") }, User.CountyID);
                }
            };
        }

		protected void InitSpinnerRow<T> (FrameLayout frame, int spinnerID, List<T> items, long itemID)
		{
			//setting spinner items and default selection
			var spinner = frame.FindViewById (spinnerID) as Spinner;
			InitSpinner<T> (spinner, items);
			var selectedIndex = GetAreaIndexByID (itemID, items);
			spinner.SetSelection (selectedIndex);
		}

		protected int GetAreaIndexByID<T> (long id, List<T> items)
		{
			for (int i = 0; i < items.Count; i++) {
				if ((items [i] as IArea).ID == id) {
					return i;
				}
			}
			return -1;
		}

		protected void InitSpinner<T> (Spinner targetSpinner, List<T> items)
		{
            if (items == null)
                return;
			
            var labels = new  JavaList<IDictionary<string, object>> ();

			foreach (var item in items) {
				var d = new JavaDictionary<string,object> ();
				d ["itemTitle"] = (item as IArea).Title;
				labels.Add (d);
			}

			var adapter = new RegionCountySpinnerAdapter (this.Activity.BaseContext, labels, Resource.Layout.SimpleSpinnerItem, new string[] { "itemTitle" }, new int[] { Resource.Id.titleTextView });
//			adapter.SetDropDownViewResource (Resource.Layout.SpinnerDropDownItem);

			targetSpinner.Adapter = adapter;
		}
	}

    //https://developer.xamarin.com/recipes/android/other_ux/camera_intent/take_a_picture_and_save_using_camera_app/
    public static class BitmapHelpers
    {
        public static Bitmap LoadAndResizeBitmap (this string fileName, int width, int height)
        {
            // First we get the the dimensions of the file on disk
            BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
            BitmapFactory.DecodeFile (fileName, options);

            // Next we calculate the ratio that we need to resize the image by
            // in order to fit the requested dimensions.
            int outHeight = options.OutHeight;
            int outWidth = options.OutWidth;
            int inSampleSize = 1;

            if (outHeight > height || outWidth > width)
            {
                inSampleSize = outWidth > outHeight
                                   ? outHeight / height
                                   : outWidth / width;
            }

            // Now we will load the image and have BitmapFactory resize it for us.
            options.InSampleSize = inSampleSize;
            options.InJustDecodeBounds = false;
            Bitmap resizedBitmap = BitmapFactory.DecodeFile (fileName, options);

            return resizedBitmap;
        }

        public static Bitmap RotateBitmap (Bitmap bitmap, int orientation)
        {
            Matrix matrix = new Matrix ();
           
            switch (orientation)
            {
                case (int)Android.Media.Orientation.Rotate180:
                    matrix.SetRotate (180);
                    break;
                case (int)Android.Media.Orientation.Rotate90: 
                    matrix.SetRotate (90);
                    break;
                case (int)Android.Media.Orientation.Rotate270:
                    matrix.SetRotate (270);
                    break;
                default:
                    break;
            }

            try
            {
                Bitmap bmRotated = Bitmap.CreateBitmap (bitmap, 0, 0, bitmap.Width, bitmap.Height, matrix, true);
                return bmRotated;
            } 
            catch (OutOfMemoryError e)
            {
                return null;
            }
        }
    }
}

