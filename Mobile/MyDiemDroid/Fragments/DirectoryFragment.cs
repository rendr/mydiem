using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using MyDiem.Core.Interfaces;
using MonkeyArms;
using MyDiem.Core.BL;
using MyDiem.Droid.Adapters;
using Xamarin.Android.OpenSource;
using Delegates;
using MyDiem.Core.BL.Managers;

namespace MyDiem.Droid.Fragments
{
    public class DirectoryFragment : BaseFragment, IParentDirectoryView
    {
        protected PinnedHeaderListView ParentList;
        protected List<MyDiem.Core.BL.Parent> Contacts;
        ProgressDialog LoadingIndictor;
        TextView NoSubscriptionsPrompt, NoMatchesPrompt;
        DirectoryFilterMenuDelegate FilterMenu;

        public DirectoryFragment()
        {
            ParentSelected = new Invoker();
            Search = new Invoker();
            Reset = new Invoker();
        }

        #region IParentDirectoryView implementation

        public void HideLoadingIndicator()
        {
            this.Activity.RunOnUiThread(() =>
                {
                    LoadingIndictor.Hide();
                    NoMatchesPrompt.Visibility = ViewStates.Gone;
                    NoSubscriptionsPrompt.Visibility = ViewStates.Gone;
                });
        }

        public void ShowNotSubscribedPrompt()
        {
            this.Activity.RunOnUiThread(() =>
                {
                    NoSubscriptionsPrompt.Visibility = ViewStates.Visible;

                });
        }

        public void ShowNoMatchesPrompt()
        {
            this.Activity.RunOnUiThread(() =>
                {
                    NoMatchesPrompt.Visibility = ViewStates.Visible;

                });
        }

        public Invoker ParentSelected
        {
            private set;
            get;
        }

        public Invoker Search
        {
            private set;
            get;
        }

        public Invoker Reset
        {
            private set;
            get;
        }

        public void UpdateContacts(List<MyDiem.Core.BL.Parent> contacts)
        {
            this.Contacts = contacts;

            this.Activity.RunOnUiThread(() =>
                {
                    UpdateListView(contacts);
                });

        }

        public void GoParentDetails()
        {

            Activity.StartActivity(typeof(ParentDetailsActivity));
        }

        public string SearchPlaceHolderText
        {
            get
            {
                return "";
            }
            set
            {

            }
        }

        public string SearchQuery
        {
            get
            {
                return SearchInput.Query;
            }
        }

        private Parent selectedParent;

        public MyDiem.Core.BL.Parent SelectedParent
        {
            get
            {
                return selectedParent;
            }
        }

        protected override void FilterButtonClicked()
        {

        }


        #endregion

        protected override void MapButtonClicked()
        {
            Activity.StartActivity(typeof(MapActivity));
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            this.ScreenTitle = GetString(Resource.String.settingsDirectoryLabel);
            this.SearchVisibility = ViewStates.Visible;
            this.FilterVisibility = ViewStates.Visible;
            this.MapVisibility = DI.Get<User>().CarpoolConnect ? ViewStates.Visible : ViewStates.Gone;
            this.LayoutID = Resource.Layout.Directory;
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        LayoutInflater myInflater;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            myInflater = inflater;
            var view = base.OnCreateView(inflater, container, savedInstanceState);
            ParentList = view.FindViewById(Resource.Id.parentsList) as PinnedHeaderListView;
            ParentList.ItemClick += HandleItemClick;


            NoSubscriptionsPrompt = view.FindViewById<TextView>(Resource.Id.notSubscribedPrompt);
            NoMatchesPrompt = view.FindViewById<TextView>(Resource.Id.noMatchesPrompt);

            if (LoadingIndictor == null)
            {
                LoadingIndictor = new ProgressDialog(this.Activity);
                LoadingIndictor.SetCancelable(false);
                LoadingIndictor.SetMessage(GetString(Resource.String.loadingDirectory));
            }
            return view;
        }

        public override void OnStart()
        {
            base.OnStart();
            DI.RequestMediator(this);
            if (Contacts == null && !LoadingIndictor.IsShowing)
            {

                LoadingIndictor.Show();
            }
        }

        public override void OnStop()
        {
            base.OnStop();
            DI.DestroyMediator(this);
        }

        public override void OnResume()
        {
            base.OnResume();
            if (FilterMenu == null)
            {
                FilterMenu = new DirectoryFilterMenuDelegate(FilterButton, this.Activity);
            }

        }

        void UpdateListView(List<Parent> contacts)
        {
            var parentDirectoryAdapter = new ParentDirectoryAdapter(this.Activity, contacts);

            ParentList.SetPinnedHeaderView(myInflater.Inflate(Resource.Layout.ParentDirectoryHeader, ParentList, false));
            ParentList.DividerHeight = 0;
            ParentList.FastScrollEnabled = true;
            ParentList.SetAdapter(parentDirectoryAdapter);
            ParentList.Adapter = parentDirectoryAdapter;

            ParentList.SetOnScrollListener(parentDirectoryAdapter);

        }

        void HandleItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            selectedParent = this.Contacts[e.Position];
            ParentSelected.Invoke(InvokerArgs.Empty);

        }

        override protected void SubmitSearch(string query)
        {
            Search.Invoke(InvokerArgs.Empty);
        }

        override protected void ResetSearch()
        {
            Reset.Invoke(InvokerArgs.Empty);
        }
    }
}

