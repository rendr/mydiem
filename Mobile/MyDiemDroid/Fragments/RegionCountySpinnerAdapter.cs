using System.Collections.Generic;
using Android.Content;
using Android.Views;
using Android.Widget;

namespace MyDiem.Droid.Fragments
{
	public class RegionCountySpinnerAdapter : SimpleAdapter
	{
		public RegionCountySpinnerAdapter (Context context, IList<IDictionary<string, object>> data, int resource, string[] from, int[] to) : base (context, data, resource, from, to)
		{
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			View view = base.GetView (position, convertView, parent);
			var textView = ((TextView)view);
			textView.SetTextColor (Android.Graphics.Color.Black);
			return view;
		}

		public override View GetDropDownView (int position, View convertView, ViewGroup parent)
		{
			View view = base.GetDropDownView (position, convertView, parent);
			((TextView)view).SetTextColor (Android.Graphics.Color.Black);
			return view;
		}
	}
}
