using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using MyDiem.Views;
using System.Globalization;
using MyDiem.Core.Interfaces;
using MyDiem.Core.BL;
using MonkeyArms;
using CustomEvents;

namespace MyDiem.Droid.Fragments
{
	public class CalendarWeekScreenFragment : BaseCalendarFragment, ICalendarWeekView
	{

		/*
		 * FIELDS
		 */
		protected View WeekView;
		
		protected int WeekOffset = 0;

		JavaList<IDictionary<string, object>> eventList;

		Dictionary<string, List<CalendarEvent>> daysDictionary;

		/*
		 * PROPERTIES
		 */

		/*
		 * CONSTRUCTOR
		 */
		
		public CalendarWeekScreenFragment()
		{
			this.ArrowButtonVisiblity = ViewStates.Visible;
			this.RightHeaderButtonVisiblity = ViewStates.Visible;
		}
		
		
		/*
		 * INTERFACE IMPLEMENTATIONS
		 */
		#region ICalendarWeekView implementation
		public event EventHandler WeekDateChanged = delegate {};
		public event EventHandler SelectedDateChanged = delegate {};
		public event EventHandler EventSelected = delegate {};

		public DateTime WeekDate {
			get {
				return DateTime.Today.AddDays (7 * WeekOffset);
			}
		}


		private DateTime selectedDate = DateTime.Today;
		public DateTime SelectedDate {
			get {
				return selectedDate;
			}
		}

		private List<CalendarEvent> weekEvents;
		public List<MyDiem.Core.BL.CalendarEvent> WeekEvents {
			get {
				return weekEvents;
			}
			set {
				weekEvents = value;
				foreach (var id in CellIDs) {
					var cell = WeekView.FindViewById<NotatedCalendarCell> (id);
					cell.Swatches = GetCellSwatches (cell, weekEvents);

				}

				if (EventsListView != null) {
					daysDictionary = InitDaysDictionary (weekEvents);
					eventList = InitMultiDayEventList (daysDictionary);
					EventsListView.Adapter = InitMultiDayEventListAdapter (eventList);
				}
			}
		}

		private List<CalendarEvent> selectedDateEvents;
		public List<CalendarEvent> SelectedDateEvents {
			get {
				return selectedDateEvents;
			}
			set {
				selectedDateEvents = value;
				UpdateEventList ();
			}
		}
		#endregion


		/*
		 * OVERRIDES
		 */
	

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{

			this.LayoutID = Resource.Layout.CalendarWeek;
			var view = base.OnCreateView (inflater, container, savedInstanceState);
			SetReferences (view);
			UpdateCalendar (DateTime.Today.AddDays(7 * WeekOffset));
			AddEventListeners ();
			RightHeaderButtonTitle = GetString (Resource.String.todayLabel);
			return view;

		}

		override protected void SetReferences (View view)
		{
			base.SetReferences (view);
			WeekView = view;

		}

		public override void OnStart ()
		{
			base.OnStart ();

			DI.RequestMediator(this);
		}

		public override void OnStop ()
		{
			base.OnStop ();
			DI.DestroyMediator(this);
		}

		protected override void RightHeaderButtonClicked ()
		{
			WeekOffset = 0;
			UpdateCalendar (DateTime.Today);
			WeekDateChanged(this, EventArgs.Empty);
		}

		/*
		 * HELPER FUNCTIONS
		 */


		protected void AddEventListeners ()
		{
			PreviousButton.Click += (object sender, EventArgs e) =>  {
				DeselectWeekCells(WeekView);
				WeekOffset--;
				UpdateCalendar (WeekDate);
				WeekDateChanged(this, EventArgs.Empty);
			};
			NextButton.Click += (object sender, EventArgs e) =>  {
				DeselectWeekCells(WeekView);
				WeekOffset++;
				UpdateCalendar (WeekDate);
				WeekDateChanged(this, EventArgs.Empty);
			};

			EventsListView.ItemClick += HandleItemClick;


		}

		void HandleItemClick (object sender, AdapterView.ItemClickEventArgs e)
		{
			var item = eventList [e.Position];
			EventSelected (this, new SelectedCalendarEventArgs (GetEventFromSectionAndRowIndex(GetSectionIndexForItem(item, daysDictionary), GetRowIndexForItem(item, daysDictionary),daysDictionary)));



		}

		protected void UpdateCalendar (DateTime date)
		{
			date = GetFirstDayOfWeek (date);
			UpdateTitle (date);
			UpdateCells (date);
		}



		protected DateTime GetFirstDayOfWeek (DateTime date)
		{
			while (date.DayOfWeek != DayOfWeek.Sunday) {
				date = date.AddDays (-1);
			}
			return date;
		}



		protected void UpdateCells (DateTime date)
		{
			for (var i = 0; i < 7; i++) {
				var cell = WeekView.FindViewById<NotatedCalendarCell> (CellIDs [i]);
				cell.Date = date;
				date = date.AddDays (1);
			}
		}

		protected void UpdateEventList ()
		{
			if (EventsListView != null && SelectedDateEvents != null) {
				daysDictionary = InitDaysDictionary (weekEvents);
				eventList = InitMultiDayEventList (daysDictionary);
				EventsListView.Adapter = InitMultiDayEventListAdapter (eventList);
			}
		}


	}
}

