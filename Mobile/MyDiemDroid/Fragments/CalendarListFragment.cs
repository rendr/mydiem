using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

using MyDiem.Core;
using MyDiem.Core.Interfaces;
using MonkeyArms;
using MyDiem.Core.BL;
using MyDiem.Droid.Adapters;
using CustomEvents;

namespace MyDiem.Droid.Fragments
{
	public class CalendarListFragment : BaseCalendarFragment, ICalendarEventListTableSource
	{
		/*
		*	FIELDS
		*/
		public event EventHandler EventSelected = delegate {};

		Dictionary<string, List<CalendarEvent>> Days;

		JavaList<IDictionary<string, object>> MultiDayEventList;


		ListView CalendarList;
		protected int TodayIndex;


		public CalendarListFragment ()
		{
			this.RightHeaderButtonVisiblity = ViewStates.Visible;
			this.rightHeaderButtonTitle = "Today";
		}

		/*
		*	INTERFACE IMPLEMENTATIONS
		*/
		#region ICalendarEventListTableSource implementation


		public void Init (Dictionary<string, List<MyDiem.Core.BL.CalendarEvent>> days, int todaySectionIndex, int todayRowIndex)
		{
			TitleText.Text = GetString (Resource.String.listView);
			this.Days = days;
			MultiDayEventList = InitMultiDayEventList (days);
		
			TodayIndex = 0;
			foreach (var list in days.Values) {
				foreach (var calendarEvent in list) {
					if (DateTime.Now > calendarEvent.StartDateTime) {
						TodayIndex++;
					} else {
						break;
					}
				}

			}


			(View.FindViewById (Resource.Id.listView) as ListView).Adapter = InitMultiDayEventListAdapter (MultiDayEventList);

		}



		public List<MyDiem.Core.BL.CalendarEvent> Events {
			get;
			set;
		}

	

		#endregion

		/*
		*	OVERRIDES
		*/

		public override void OnPause ()
		{
			base.OnPause ();
		}

		public override void OnStop ()
		{
			base.OnStop ();
			DI.DestroyMediator (this);
		}

		public override void OnCreate (Bundle savedInstanceState)
		{
			this.LayoutID = Resource.Layout.CalendarList;
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}


		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{

			var view = base.OnCreateView (inflater, container, savedInstanceState);
			CalendarList = (view.FindViewById (Resource.Id.listView) as ListView);
			CalendarList.ItemClick += HandleItemClick;
			return view;
		}

		protected override void RightHeaderButtonClicked ()
		{

			CalendarList.SetSelection (TodayIndex);
		}

		public override void OnStart ()
		{
			base.OnStart ();
			DI.RequestMediator(this);
			CalendarList.SetSelection (TodayIndex);

		}

		/*
		*	HELPER FUNCTIONS
		*/
		void HandleItemClick (object sender, AdapterView.ItemClickEventArgs e)
		{


			var item = MultiDayEventList [e.Position];
			EventSelected (this, new SelectedCalendarEventArgs (GetEventFromSectionAndRowIndex(GetSectionIndexForItem(item, Days), GetRowIndexForItem(item, Days),Days)));
		}



	}
}

