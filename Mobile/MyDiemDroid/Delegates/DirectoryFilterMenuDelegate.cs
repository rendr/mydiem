using System;
using Android.Views;
using Android.App;
using Android.Widget;
using MyDiem.Droid;
using Android.Content;
using MonkeyArms;
using MyDiem.Core.ViewModel;
using System.Collections.Generic;

namespace Delegates
{
	public class DirectoryFilterMenuDelegate: IInjectingTarget
	{
		[Inject]
		public DirectoryViewModel VM;
		protected View Target;
		protected Activity Activity;
		protected List<string> SelectedDirectories;
		protected List<string> SelectedGrades;
		protected List<string> AllDirectories;
		protected List<string> AllGrades;
		PopupMenu optionPopUp;

		public DirectoryFilterMenuDelegate (View view, Activity context)
		{
			this.Target = view;
			this.Activity = context;
			Target.Click += (object sender, EventArgs e) => {
				ViewClicked ();
			};
			DIUtil.InjectProps (this);

			if (VM.Contacts == null || VM.Contacts.Count == 0) {
				VM.ContactsUpdated.Invoked += HandleContactsUpdated;
			} else {
				HandleContactsUpdated (null, null);
			}


		}

		void HandleContactsUpdated (object sender, EventArgs e)
		{
			AllDirectories = VM.GetDirectoryNames ();
			AllDirectories.Insert (0, "All");

			AllGrades = VM.GetGrades ();
			AllGrades.Insert (0, "All");
			VM.ContactsUpdated.Invoked -= HandleContactsUpdated;
		}

		protected void ViewClicked ()
		{

			if (SelectedDirectories == null) {
				SelectedDirectories = AllDirectories;
			}
			if (SelectedGrades == null) {
				SelectedGrades = AllGrades;
			}

			if (optionPopUp == null) {
				optionPopUp = new PopupMenu (this.Activity, Target);
			
				optionPopUp.MenuInflater.Inflate (Resource.Menu.DirectoryFilterMenuItems, optionPopUp.Menu);
				optionPopUp.MenuItemClick += (object sender, PopupMenu.MenuItemClickEventArgs e) => {
					switch (e.Item.ItemId) {
					case Resource.Id.directoryOption:

						var directoryOptions = AllDirectories;

						CreateAndShowPicker (Resource.String.directories, directoryOptions.ToArray (), (object dialogSender, DialogClickEventArgs args) => {
							if (args.Which == 0) {
								SelectedDirectories = AllDirectories;
							} else {
								SelectedDirectories = new List<string> (){ directoryOptions [args.Which] };
							}
							SubmitFilter ();
						});
						break;
					case Resource.Id.gradeOption:
						var gradeOptions = AllGrades;

						CreateAndShowPicker (Resource.String.grades, AllGrades.ToArray (), (object dialogSender, DialogClickEventArgs args) => {
							if (args.Which == 0) {
								SelectedGrades = AllGrades;
							} else {
								SelectedGrades = new List<string> (){ gradeOptions [args.Which] };
							}
							SubmitFilter ();
						});
						break;
					}
				
				};
			}
			optionPopUp.Show ();

		}

		void SubmitFilter ()
		{
			var enabledItems = new List<string> ();
			enabledItems.AddRange (SelectedDirectories);

			foreach (var grade in SelectedGrades) {
				enabledItems.Add (grade.Replace ("GRADE ", ""));
			}

			VM.FilterContacts (enabledItems);
		}

		void CreateAndShowPicker (int title, string[] labels, EventHandler<DialogClickEventArgs> handler)
		{
			AlertDialog.Builder b = new AlertDialog.Builder (this.Activity);
			b.SetTitle (title);
			b.SetItems (labels, handler);

			var d = b.Create ();
			d.Show ();

		}
	}
}

