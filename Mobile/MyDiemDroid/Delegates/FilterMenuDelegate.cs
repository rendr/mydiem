using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyDiem.Droid;
using MyDiem.Core.Interfaces;
using MyDiem.Core.BL;
using MonkeyArms;

namespace Delegates
{
	class FilterMenuDelegate: IFilterMenu
	{

		public event EventHandler ShowFilteringPrompt = delegate {};

		#region IFilterMenu implementation
		public event EventHandler RegionSelected = delegate {};
		public event EventHandler CountySelected = delegate {};
		public event EventHandler TypeSelected = delegate {};
		public event EventHandler CategorySelected = delegate{};

		public List<string> Categories {get;set;}
		public List<Region> Regions {get;set;}
		public List<County> Counties {get;set;}
		public List<string> Types {get;set;}


		public Region SelectedRegion {private set;get;}


		public County SelectedCounty {private set;get;}


		public string SelectedType {get;private set;}

		public List<string> SelectedCategories{ get; private set;}

		#endregion

		protected View Target;
		protected Activity Activity;



		public FilterMenuDelegate(View view, Activity context)
		{
			this.Target = view;
			this.Activity = context;
			Target.Click += (object sender, EventArgs e) => {
				ViewClicked();
			};

			DI.RequestMediator(this);
		}

		bool[] SelectedCategoryBools;

		protected void ViewClicked ()
		{
			PopupMenu optionPopUp = new PopupMenu (this.Activity, Target);
			optionPopUp.MenuInflater.Inflate (Resource.Menu.FilterMenuItems,optionPopUp.Menu);
			optionPopUp.MenuItemClick += (object sender, PopupMenu.MenuItemClickEventArgs e) => {
				switch(e.Item.ItemId){
					case Resource.Id.countyOption:
					CreateAndShowPicker(Resource.String.countyLabel, Counties.Select(x=>x.Title).ToArray(), (object dialogSender, DialogClickEventArgs args) =>{
						SelectedCounty = Counties[args.Which];
						CountySelected(this, EventArgs.Empty);
						ShowFilteringPrompt(this, EventArgs.Empty);
					});
					break;
					case Resource.Id.regionOption:
					CreateAndShowPicker(Resource.String.regionLabel, Regions.Select(x=>x.Title).ToArray(), (object dialogSender, DialogClickEventArgs args) =>{
						SelectedRegion = Regions[args.Which];
						RegionSelected(this, EventArgs.Empty);
						ShowFilteringPrompt(this, EventArgs.Empty);
					});
					break;
					case Resource.Id.typeOption:
					CreateAndShowPicker(Resource.String.typeLabel, Types.ToArray(), (object dialogSender, DialogClickEventArgs args) =>{
						SelectedType = Types[args.Which];
						TypeSelected(this, EventArgs.Empty);
						ShowFilteringPrompt(this, EventArgs.Empty);
					});
					break;
					case Resource.Id.categoryOption:
					if(SelectedCategoryBools == null || SelectedCategoryBools.Length != Categories.Count){
						this.SelectedCategoryBools = new bool[Categories.Count];
						for (int i = 0; i < SelectedCategoryBools.Length; i++) {
							SelectedCategoryBools[i] = true;
						}
					}
					CreateAndShowMultiPicker(Resource.String.categoryLabel, Categories.ToArray(), SelectedCategoryBools, (object dialogSender, DialogMultiChoiceClickEventArgs args) =>{
						SelectedCategoryBools[args.Which] = args.IsChecked;


						SelectedCategories = new List<string>();

						for (int i = 0; i < SelectedCategoryBools.Length; i++) {
							var b = SelectedCategoryBools [i];
							if(b){
								SelectedCategories.Add(Categories[i]);
							}
						}


					}, (object dialogSender, DialogClickEventArgs args)=>{
						CategorySelected(this, EventArgs.Empty);
						ShowFilteringPrompt(this, EventArgs.Empty);

					});
					break;
				}
			};
			optionPopUp.Show ();

		}

		void CreateAndShowPicker(int title, string[] labels, EventHandler<DialogClickEventArgs> handler){
			AlertDialog.Builder b = new AlertDialog.Builder (this.Activity);
			b.SetTitle (title);
			b.SetItems (labels, handler);
			var d = b.Create ();
			d.Show ();
		}

		void CreateAndShowMultiPicker(int title, string[] labels, bool[] whichAreSelected, EventHandler<DialogMultiChoiceClickEventArgs> handler, EventHandler<DialogClickEventArgs> doneHandler){
			AlertDialog.Builder b = new AlertDialog.Builder (this.Activity);
			b.SetTitle (title);
			b.SetMultiChoiceItems(labels, whichAreSelected, handler);
			b.SetPositiveButton ("Done", doneHandler);
			var d = b.Create ();
			d.Show ();

		}
	}
}

