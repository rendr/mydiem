using System;
using Android.App;
using Android.Content;
using Android.Support.V4.App;
using Android.Media;

namespace MyDiem.Droid
{
	[BroadcastReceiver]
	public class ReminderAlarm:BroadcastReceiver
	{
		public override void OnReceive (Context context, Intent intent)
		{
			NotificationCompat.Builder mBuilder =
				new NotificationCompat.Builder (Application.Context)
					.SetSmallIcon (Resource.Drawable.AppIcon)
					.SetContentTitle (intent.GetStringExtra ("title"))
					.SetContentText (intent.GetStringExtra ("time"));
			// Creates an explicit intent for an Activity in your app
			Intent resultIntent = new Intent (Application.Context, typeof(EventDetailsActivity));
			var eventID = intent.GetIntExtra ("eventID", -1);
			resultIntent.PutExtra ("eventID", eventID);

			// The stack builder object will contain an artificial back stack for the
			// started Activity.
			// This ensures that navigating backward from the Activity leads out of
			// your application to the Home screen.
			TaskStackBuilder stackBuilder = TaskStackBuilder.Create (Application.Context);
			// Adds the back stack for the Intent (but not the Intent itself)

//			stackBuilder.AddParentStack (typeof(EventDetailsActivity).co`);
			// Adds the Intent that starts the Activity to the top of the stack
			stackBuilder.AddNextIntent (resultIntent);
			PendingIntent resultPendingIntent =
				stackBuilder.GetPendingIntent (
					0,
					(int)PendingIntentFlags.UpdateCurrent
				);
			mBuilder.SetContentIntent (resultPendingIntent);
			NotificationManager mNotificationManager =
				(NotificationManager)Application.Context.GetSystemService (Context.NotificationService);
			// mId allows you to update the notification later on.
			mNotificationManager.Notify (200, mBuilder.Build ());

			var notification = RingtoneManager.GetDefaultUri (RingtoneType.Notification);
			Ringtone r = RingtoneManager.GetRingtone (Application.Context, notification);
			r.Play ();


			/*
			PowerManager pm = (PowerManager) Application.Context.GetSystemService(Context.PowerService);
			Android.OS.PowerManager.WakeLock wakeLock = pm.NewWakeLock((WakeLockFlags.ScreenBright | WakeLockFlags.Full | WakeLockFlags.AcquireCausesWakeup), "TAG");
			wakeLock.Acquire();

			KeyguardManager keyguardManager = (KeyguardManager) Application.Context.GetSystemService(Context.KeyguardService); 
			Android.App.KeyguardManager.KeyguardLock keyguardLock =  keyguardManager.NewKeyguardLock("TAG");
			keyguardLock.DisableKeyguard();


			Toast.MakeText (context, Resource.String.setReminderLabel, ToastLength.Short).Show ();
			*/
		}
	}
}

