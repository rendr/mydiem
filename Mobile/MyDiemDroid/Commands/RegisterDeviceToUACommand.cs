using System;
using MonkeyArms;
using MyDiem.Core.SAL;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.Invokers;
using MyDiem.Core.BL;
using Xamarin.UrbanAirship.Push;

namespace MyDiem.iOS.Commands
{
	public class RegisterDeviceToUACommand:Command
	{
		[Inject]
		public UserManager UM;
		[Inject]
		public User AppUser;

		public RegisterDeviceToUACommand ()
		{

		}

		public override void Execute (InvokerArgs args)
		{

			string deviceToken = PushManager.Shared ().APID;
			var del = new AddDeviceToUrbanAirshipServiceDelegate ();

//			deviceToken will be null in simulator because registering for push notifications is not supported 
			if (deviceToken != null) {
				del.Add (AppUser.ID, deviceToken.ToString (), Android.OS.Build.Model, delegate {
				});
			}

		}
	}
}

