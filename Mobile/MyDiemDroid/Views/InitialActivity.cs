using System;
using System.Threading;
using Android.App;
using Android.OS;
using MonkeyArms;
using MyDiem.Core.Commands;
using MyDiem.Core.Invokers;

namespace MyDiem.Droid
{
    [Activity (Theme = "@style/Theme.Splash", Label = "MyDiemDroid", MainLauncher = true, NoHistory=true, ScreenOrientation=Android.Content.PM.ScreenOrientation.Portrait)]
	public class InitialActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			OverridePendingTransition (Resource.Animation.fadein, Resource.Animation.fadeout);
			SetContentView (Resource.Layout.Loading);

			new Thread(Start).Start();
		}

		void Start ()
		{
			//add any global invoker listeners
			DI.Get<SetInitialScreenInvoker>().Invoked += HandelSetInitialScreen;

			//Kickoff any Bootstrap commands/processes
			DI.MapCommandToInvoker<BaseBootStrapCommand, BootStrapInvoker> ().Invoke();
		}

		void HandelSetInitialScreen (object sender, EventArgs e)
		{
			var initialScreen = (e as SetInitialScreenInvokerArgs).InitialScreen;
			if (initialScreen == InitialScreenEnum.HomeScreen) {
				StartActivity (typeof(HomeActivity));
			}

			if (initialScreen == InitialScreenEnum.LoadingScreen) {
				StartActivity(typeof(LoadingActivity));
			}
			DI.Get<SetInitialScreenInvoker>().Invoked -= HandelSetInitialScreen;
		}
	}
}


