using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using Android.Text;

namespace MyDiem.Droid
{
	[Activity (Label = "TermsOfServiceActivity")]			
	public class TermsOfServiceActivity : BaseActivity
	{

		protected TextView TextView;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.About);
			TextView = FindViewById<TextView> (Resource.Id.aboutTextView);
			UpdateTextView ();

		}

		protected void UpdateTextView ()
		{
			TextView.SetText (Html.FromHtml (GetString (Resource.String.termsOfService)), TextView.BufferType.Normal);

		}
	}
}

