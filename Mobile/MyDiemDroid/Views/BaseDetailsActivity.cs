using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MyDiem.Droid
{
	[Activity (Label = "BaseDetailsActivity")]			
	public class BaseDetailsActivity : BaseActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
		}

		protected void HideItems (int[] ids)
		{
			foreach (var id in ids) {
				(FindViewById (id) as View).Visibility = ViewStates.Gone;
			}	
		}
	}
}

