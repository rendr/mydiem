using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;
using MonkeyArms;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.ViewModel;
using MyDiemCore;
using Newtonsoft.Json;
using Xamarin.Social;
using Xamarin.Social.Services;

namespace MyDiem.Droid
{
    [Activity(Label = "EventDetailsActivity")]			
    public class EventDetailsActivity : BaseDetailsActivity, IInjectingTarget
    {
        private static AlarmManager ALARM_MANAGER;

		 
        [Inject]
        public EventListViewModel VM;

        [Inject]
        public EventManager EM;

        protected override void OnCreate(Bundle bundle)
        {
            DIUtil.InjectProps(this);

            base.OnCreate(bundle);

            SetContentView(Resource.Layout.EventDetails);
            // Create your application here


        }

        protected override void OnStart()
        {
            base.OnStart();
            if (this.Intent.GetIntExtra("eventID", -1) != -1)
            {
                var eventID = this.Intent.GetIntExtra("eventID", -1);
                VM.SelectedEvent = EM.GetEventByID(eventID);
            }

            base.HideItems(new int[]
                {
                    Resource.Id.rightHeaderButton,
                    Resource.Id.settingsButton,
                    Resource.Id.filterButton,
                    Resource.Id.searchInput,
                    Resource.Id.leftArrowButton,
                    Resource.Id.rightArrowButton,
                    Resource.Id.mapButton

                });

            FindViewById<TextView>(Resource.Id.screenTitle).Text = VM.SelectedEvent.CalendarTitle;
            FindViewById<TextView>(Resource.Id.eventTitleTextView).Text = VM.SelectedEvent.Title;

            FindViewById<TextView>(Resource.Id.locationTextView).Text = VM.SelectedEvent.Location;

            var locationRow = FindViewById<LinearLayout>(Resource.Id.locationRow);
            locationRow.Visibility = String.IsNullOrEmpty(VM.SelectedEvent.Location) ? ViewStates.Gone : ViewStates.Visible;
            locationRow.Click += (object sender, EventArgs e) =>
            {
                String uri = String.Format("geo:?q={0}", VM.SelectedEvent.Location); // for example pass geo:75.333000,30.003030 to search for latitude = 75.333000 and longitude = 30.003030 as your default search query
                Intent intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(uri));
                StartActivity(intent);
                
            };

            FindViewById<TextView>(Resource.Id.eventDescriptionTextView).Text = VM.SelectedEvent.Description;
            FindViewById<TextView>(Resource.Id.reminderTextView).Text = GetReminderStatus();

            FindViewById<Button>(Resource.Id.facebookShareButton).Click += (object sender, EventArgs e) =>
            {
                ShareOnFacebook();
            };

            if (VM.SelectedEvent.AllDay)
            {
                FindViewById<LinearLayout>(Resource.Id.allDayRow).Visibility = ViewStates.Visible;

            }
            else
            {
                FindViewById<LinearLayout>(Resource.Id.allDayRow).Visibility = ViewStates.Gone;
            }

            FindViewById<LinearLayout>(Resource.Id.startsRow).Visibility = FindViewById<LinearLayout>(Resource.Id.endsRow).Visibility = (VM.SelectedEvent.AllDay) ? ViewStates.Gone : ViewStates.Visible;

            FindViewById<TextView>(Resource.Id.startTimeTextView).Text = VM.SelectedEvent.StartDateTime.ToString("t");
            FindViewById<TextView>(Resource.Id.endTimeTextView).Text = VM.SelectedEvent.EndDateTime.ToString("t");

            //			FindViewById<LinearLayout> (Resource.Id.reminderRow).Visibility = ViewStates.Gone;
            FindViewById<LinearLayout>(Resource.Id.reminderRow).Click += HandleReminderClick;
            ;
        }

        void HandleReminderClick(object sender, EventArgs e)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.SetTitle(Resource.String.setReminderLabel);
            builder.SetItems(Resource.Array.reminder_options, OnReminderOptionClicked);
            builder.Create().Show();
        }

        void OnReminderOptionClicked(object sender, DialogClickEventArgs args)
        {

            RemovePreviousReminderEntry();
			
            var labels = Resources.GetStringArray(Resource.Array.reminder_options);
            FindViewById<TextView>(Resource.Id.reminderTextView).Text = labels[args.Which];

            SetAlarm(args, labels);

            var reminderToAdd = new ReminderEntry (VM.SelectedEvent.ID, labels [args.Which]);

            var reminders = JsonConvert.DeserializeObject<List<ReminderEntry>> (Settings.Reminder);
            if (reminders == null)
            {
                Settings.Reminder = JsonConvert.SerializeObject (new List<ReminderEntry> { reminderToAdd });
            } 
            else
            {
                reminders.Add (reminderToAdd);
                Settings.Reminder = JsonConvert.SerializeObject (reminders);
            }
        }

        private string GetReminderStatus()
        {
            try
            {
                var reminders = JsonConvert.DeserializeObject<List<ReminderEntry>> (Settings.Reminder);
                var entry = reminders.First (x => x.ID == VM.SelectedEvent.ID);
                return entry.Label;
            }
            catch
            {
                return "None";
            }
        }

        private PendingIntent CreatePendingIntent(Intent intent, PendingIntentFlags flag)
        {


            Console.WriteLine("Event ID: " + VM.SelectedEvent.ID);
            return PendingIntent.GetBroadcast(this.Application.ApplicationContext, VM.SelectedEvent.ID, intent, flag);
        }

        private Intent CreateAlarmIntent()
        {
            Intent intent = new Intent(this, typeof(ReminderAlarm));
            intent.PutExtra("title", VM.SelectedEvent.Title);
            intent.PutExtra("time", VM.SelectedEvent.StartDateTime.ToString("t"));
            intent.PutExtra("eventID", VM.SelectedEvent.ID);
            return intent;
        }

        void SetAlarm(DialogClickEventArgs args, string[] labels)
        {
            PendingIntent pendingIntent = CreatePendingIntent(CreateAlarmIntent(), PendingIntentFlags.CancelCurrent);
            if (ALARM_MANAGER == null)
            {
                ALARM_MANAGER = (AlarmManager)GetSystemService(Android.Content.Context.AlarmService);
            }
            if (args.Which == 0)
            {
                ALARM_MANAGER.Cancel(pendingIntent);
                pendingIntent.Cancel();
                return;
            }
            var intent = CreateAlarmIntent();
            intent.PutExtra("reminderType", labels[args.Which]);
            pendingIntent = CreatePendingIntent(intent, PendingIntentFlags.CancelCurrent);
            var minutesArr = new int[]
            {
                5,
                10,
                15,
                30,
                60,
                60 * 24
            };
            var triggerTime = VM.SelectedEvent.StartDateTimeMilliseconds;
            triggerTime += minutesArr[args.Which - 1] * 60 * 1000;
            ALARM_MANAGER.Set(AlarmType.RtcWakeup, triggerTime, pendingIntent);
        }

        void RemovePreviousReminderEntry()
        {
            try
            {
                var reminders = JsonConvert.DeserializeObject<List<ReminderEntry>> (Settings.Reminder);
                reminders.RemoveAll (x => x.ID == VM.SelectedEvent.ID);
                Settings.Reminder = JsonConvert.SerializeObject (reminders);
            }
            catch
            {
            }
        }

        void ShareOnFacebook()
        {
            // 1. Create the service
            var facebook = new FacebookService { ClientId = "530690950333915" };

            // 2. Create an item to share

            var shareText = VM.SelectedEvent.Title;
            shareText += " - ";
            shareText += VM.SelectedEvent.StartDateTime.ToShortDateString();
            if (VM.SelectedEvent.AllDay)
            {
                shareText += " All Day";
            }
            else
            {
                shareText += " " + VM.SelectedEvent.StartDateTime.ToShortTimeString();
            }

            shareText += "\r" + VM.SelectedEvent.Description;

            var item = new Item { Text = shareText };
            item.Images.Add(new ImageData(BitmapFactory.DecodeResource(Resources, Resource.Drawable.AppIcon)));
            item.Links.Add(new Uri("http://mydiem.com"));

            // 3. Present the UI on Android
            var shareIntent = facebook.GetShareUI(this, item, result =>
                {
                    // result lets you know if the user shared the item or canceled
                });
            StartActivityForResult(shareIntent, 42);
        }
    }



}

