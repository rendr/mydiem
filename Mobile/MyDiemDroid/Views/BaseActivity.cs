﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

//using Google.Analytics.Tracking;

namespace MyDiem.Droid
{
    [Activity(Label = "BaseActivity")]			
    public class BaseActivity : Activity
    {
        protected override void OnStart()
        {
            base.OnStart();
            #if !DEBUG
            // Setup Google Analytics Easy Tracker
//			EasyTracker.GetInstance (this).ActivityStart (this);

            // By default, data is dispatched from the Google Analytics SDK for Android every 30 minutes.
            // You can override this by setting the dispatch period in seconds.
//			GAServiceManager.Instance.SetLocalDispatchPeriod (5);
            #endif
        }

        protected override void OnStop()
        {
            base.OnStop();
            #if !DEBUG
//            EasyTracker.GetInstance(this).ActivityStop(this);
            #endif
        }

        protected void TrackPoint(string category, string action, string label)
        {
//            #if !DEBUG
//            // May return null if a EasyTracker has not yet been initialized with a
//            // property ID.
//            var easyTracker = EasyTracker.GetInstance(this);
//
//            // MapBuilder.createEvent().build() returns a Map of event fields and values
//            // that are set and sent with the hit.
//            var gaEvent = MapBuilder.CreateEvent(category, action, label, null).Build();
//            easyTracker.Send(gaEvent);
//
//            Console.WriteLine("Event Sent, please see GA Events Console");
//            #endif
        }
    }
}

