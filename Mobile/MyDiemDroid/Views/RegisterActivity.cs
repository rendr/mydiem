using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyDiem.Droid.Fragments;
using MyDiem.Core.Interfaces;
using MonkeyArms;
using MyDiem.Core.BL;

namespace MyDiem.Droid
{
	[Activity (Label = "RegisterActivity")]			
	public class RegisterActivity : BaseActivity, IRegisterView
	{
		protected UserFormFragment UserForm;

		#region IRegisterView implementation

		public event EventHandler RegistrationSubmitted = delegate{};

		public void HandleSuccess (string message)
		{

			RunOnUiThread (() => {
				AlertDialog.Builder alert = new AlertDialog.Builder (this);
				alert.SetMessage (message);
				alert.SetPositiveButton ("Ok", (object sender, DialogClickEventArgs e) => {
					base.OnBackPressed ();
				});

				alert.Show ();


			});
		}

		public void ShowRegistrationError (string message)
		{
			RunOnUiThread (() => {
				Toast.MakeText (this, message, ToastLength.Long).Show ();
			});
		}

		public string SubmittedFirstName {
			get {
				return UserForm.GetEditTextValue (Resource.Id.firstNamEditText);
			}
		}

		public string SubmittedLastName {
			get {
				return UserForm.GetEditTextValue (Resource.Id.lastNameEditText);
			}
		}

		public string SubmittedEmail {
			get {
				return UserForm.GetEditTextValue (Resource.Id.emailEditText);
			}
		}

		public string SubmittedZip {
			get {
				return UserForm.GetEditTextValue (Resource.Id.zipEditText);
			}
		}

		public string SubmittedPassword {
			get {
				return UserForm.GetEditTextValue (Resource.Id.passwordEditText);
			}
		}

		public string SubmittedPasswordVerify {
			get {
				return UserForm.GetEditTextValue (Resource.Id.verifyEditText);
			}
		}

		public string SubmittedRegionTitle {
			get {
				return UserForm.GetSpinnerRowValue (Resource.Id.privateSchoolsRadioButton, Resource.Id.regionSpinner);
			}
		}

		public string SubmittedCountyTitle {
			get {
				return UserForm.GetSpinnerRowValue (Resource.Id.publicSchoolsRadioButton, Resource.Id.countySpinner);
			}
		}

		public List<MyDiem.Core.BL.County> Counties {
			get;
			set;
		}

		public List<MyDiem.Core.BL.Region> Regions {
			get;
			set;
		}

        public List<MyDiem.Core.BL.State> States {
            get;
            set;
        }

		#endregion

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Register);
			
			var transaction = FragmentManager.BeginTransaction ();
			UserForm = new UserFormFragment ();
            UserForm.HidePic = true;
			transaction.Replace (Resource.Id.registerUserFormLayout, UserForm);
			transaction.Commit ();
			
			(FindViewById (Resource.Id.submitButton) as Button).Click += (object sender, EventArgs e) => {
				Toast.MakeText (this, Resource.String.submittingRegistrationPrompt, ToastLength.Short).Show ();
				RegistrationSubmitted (this, new EventArgs ());
			};

			FindViewById<TextView> (Resource.Id.termsOfServiceLink).Click += (object sender, EventArgs e) => {
				StartActivity (typeof(TermsOfServiceActivity));
			};
			// Create your application here
		}

		protected override void OnStart ()
		{
			base.OnStart ();

			DI.RequestMediator (this);

			if (UserForm.User == null) 
            {
				UserForm.User = new MyDiem.Core.BL.User ();
				UserForm.Counties = Counties;
                UserForm.Regions = Regions;
                UserForm.States = States;
			}
		}

		protected override void OnStop ()
		{
			base.OnStop ();
			DI.DestroyMediator (this);
		}
	}
}

