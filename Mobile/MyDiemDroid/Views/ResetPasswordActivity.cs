﻿using System;
using System.Threading;
using Android.App;
using Android.OS;
using Android.Widget;
using MonkeyArms;
using MyDiemCore;

namespace MyDiem.Droid
{
    [Activity (Label = "ResetPasswordActivity")]
    public class ResetPasswordActivity : BaseActivity, IResetPasswordView
    {
        private Button ResetButton;
        private EditText EmailField;

        public event EventHandler ResetSubmitted;

        public string SubmittedEmail 
        {
            get 
            {
                return EmailField.Text;
            }
        }

        protected override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate (savedInstanceState);
            SetContentView (Resource.Layout.ResetPassword);

            ResetButton = FindViewById<Button>(Resource.Id.resetButton);
            EmailField = FindViewById<EditText>(Resource.Id.resetEmailAddressField);

            ResetButton.Click += (object sender, EventArgs e) => {
                ThreadPool.QueueUserWorkItem (state => {
                    ResetSubmitted (this, new EventArgs());
                });
                EmailField.ClearFocus ();
            };
        }

        protected override void OnStart ()
        {
            base.OnStart ();
            DI.RequestMediator (this);
        }

        protected override void OnStop ()
        {
            base.OnStop ();
            DI.DestroyMediator (this);
         } 

        public void ShowMessage (string message)
        {
            RunOnUiThread (() => {
                Toast.MakeText (this, message, ToastLength.Long).Show ();
            });
        }
    }
}

