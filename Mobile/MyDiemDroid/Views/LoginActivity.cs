using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MonkeyArms;
using MyDiem.Core.Interfaces;
using System.Threading;

namespace MyDiem.Droid
{
	[Activity (Label = "LoginActivity", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, NoHistory = true)]			
	public class LoginActivity : BaseActivity, ILoginView
	{
		ProgressDialog LoadingIndicator;

		#region ILoginView implementation

		public event EventHandler LoginSubmitted;



		public void ShowBadPasswordPrompt ()
		{

			RunOnUiThread (() => {
				LoadingIndicator.Hide ();
				Toast.MakeText (this, GetString (Resource.String.incorrectLogin), ToastLength.Short).Show ();
			});

		}

		public void DisableUIAndShowNetworkIndicator ()
		{
			RunOnUiThread (() => {
				SetUIEnabled (false);
			});
		}

		public void PromptInvalidatedUser ()
		{
			RunOnUiThread (() => {
				LoadingIndicator.Hide ();
				Toast.MakeText (this, GetString (Resource.String.validatePrompt), ToastLength.Short).Show ();
			});
		}

		public void GoCalendar ()
		{
			RunOnUiThread (() => {
				LoadingIndicator.Hide ();
				StartActivity (typeof(MainActivity));
			});
		}

		public void EnableControls ()
		{
			RunOnUiThread (() => {
				SetUIEnabled (true);
			});
		}

		public string SubmittedPassword {
			get {
				return PasswordField.Text;
			}
		}

		public string SubmittedEmail {
			get {
				return EmailField.Text;
			}
		}

		#endregion

		protected EditText EmailField, PasswordField;
        protected ImageView SubmitButton, RegisterButton;
        protected Button ResetButton;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Login);


			SetReferences ();
			SetEventListeners ();
		}

		protected override void OnStart ()
		{
			base.OnStart ();
			DI.RequestMediator (this);
		}

		protected override void OnStop ()
		{
			base.OnStop ();
			DI.DestroyMediator (this);

		}

		protected void SetReferences ()
		{
			SubmitButton = FindViewById (Resource.Id.loginButton) as ImageView;
            RegisterButton = FindViewById (Resource.Id.registerButton) as ImageView;
            ResetButton = FindViewById (Resource.Id.resetButton) as Button;

			EmailField = FindViewById (Resource.Id.emailTextInput) as EditText;
			PasswordField = FindViewById (Resource.Id.passwordTextField) as EditText;
		}

		protected void SetEventListeners ()
		{
			SubmitButton.Click += (object sender, EventArgs e) => {

				this.ShowLoadingIndicator ();

				ThreadPool.QueueUserWorkItem (state => {
					LoginSubmitted (this, new EventArgs ());
				});


				EmailField.ClearFocus ();
				PasswordField.ClearFocus ();

			};

			RegisterButton.Click += (object sender, EventArgs e) => {
				StartActivity (typeof(RegisterActivity));
			};

            ResetButton.Click += (object sender, EventArgs e) => {
                StartActivity (typeof (ResetPasswordActivity));
            };
		}

		protected void SetUIEnabled (bool enabled)
		{
			EmailField.Enabled = PasswordField.Enabled = SubmitButton.Enabled = RegisterButton.Enabled = enabled;
		}

		protected void ShowLoadingIndicator ()
		{
			LoadingIndicator = new ProgressDialog (this);
			LoadingIndicator.SetCancelable (false);
			LoadingIndicator.SetMessage (GetString (Resource.String.loggingIn));
			LoadingIndicator.Show ();
		}
	}
}

