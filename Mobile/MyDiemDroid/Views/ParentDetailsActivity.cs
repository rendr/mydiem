using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyDiem.Core.Interfaces;
using MyDiem.Droid.Adapters;
using MonkeyArms;
using Android.Locations;
using Android.Provider;

namespace MyDiem.Droid
{
	[Activity (Label = "ParentDetailsActivity", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]			
	public class ParentDetailsActivity : BaseDetailsActivity, IParentDetailsView
	{
		protected ListView DetailsListView;
		protected TextView ScreenTitleTextView;
        protected MyDiem.Core.BL.Parent SelectedParent;
        protected CircleImageView ProfilePicture;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.ParentInfo);
			DetailsListView = FindViewById (Resource.Id.detailsListView) as ListView;
            ScreenTitleTextView = FindViewById (Resource.Id.screenTitle) as TextView;
            ProfilePicture = FindViewById (Resource.Id.parentPicture) as CircleImageView;

			DetailsListView.ItemClick += HandleItemClick;

			base.HideItems (new int[] {

				Resource.Id.settingsButton,
				Resource.Id.filterButton,
				Resource.Id.searchInput,
				Resource.Id.leftArrowButton,
				Resource.Id.rightArrowButton,
				Resource.Id.mapButton

			});
			var addButton = FindViewById<Button> (Resource.Id.rightHeaderButton);
			addButton.Text = "Add";
			addButton.Click += (object sender, EventArgs e) => AddContact ();

		}

		protected override void OnStart ()
		{
			base.OnStart ();
			DI.RequestMediator (this);
		}

		protected override void OnStop ()
		{
			base.OnStop ();
			DI.DestroyMediator (this);
		}

		void HandleItemClick (object sender, AdapterView.ItemClickEventArgs e)
		{
			switch (e.Position) {
			case 0:
				ChoosePhoneOrText (SelectedParent.Phone1);
				break;
			case 1:
				ChoosePhoneOrText (SelectedParent.Phone2);
				break;
			case 2:
				OpenEmail (SelectedParent.Email);
				break;
			case 3:
			case 4:
				OpenMap ();
				break;
			}
		}

		protected void ChoosePhoneOrText (string phoneNumber)
		{
			//Put up the Yes/No message box
			EventHandler<DialogClickEventArgs> okhandler;
			var builder = new AlertDialog.Builder (this)
				.SetMessage ("Call or Message")
				.SetPositiveButton ("Call", (sender, args) => {
				MakePhoneCall (phoneNumber);
			})
				.SetNeutralButton ("Message", (sender, args) => {
				SendText (phoneNumber);
			})
				.SetTitle ("Do you wish to:");
			builder.Show ();
		}

		protected void MakePhoneCall (string phoneNumber)
		{
			var callIntent = new Intent (Intent.ActionCall);
			callIntent.SetData (Android.Net.Uri.Parse ("tel:" + phoneNumber));
			StartActivity (callIntent);
		}

		protected void SendText (string phoneNumber)
		{
			var smsUri = Android.Net.Uri.Parse ("smsto:" + phoneNumber.ToString ());
			var smsIntent = new Intent (Intent.ActionSendto, smsUri);
			StartActivity (smsIntent);
		}

		protected void OpenEmail (string emailAddress)
		{
			//ONLY WORKS ON DEVICE BECAUSE EMULATOR DOESN'T HAVE EMAIL APP
			var emailIntent = new Intent (Intent.ActionSend);
			emailIntent.SetType ("plan/text");
			emailIntent.PutExtra (Intent.ExtraEmail, new String[] { emailAddress });
			emailIntent.PutExtra (Intent.ExtraSubject, "subject");
			emailIntent.PutExtra (Intent.ExtraText, "mail body");
			StartActivity (emailIntent);
		}

		void OpenMap ()
		{
			try {
				var mapIntent = new Intent (Intent.ActionView, Android.Net.Uri.Parse ("geo:0,0?q=" + SelectedParent.Address + " " + SelectedParent.City + ", " + SelectedParent.State + " " + SelectedParent.Zip.ToString ()));
				StartActivity (mapIntent);
			} finally {

			}
		}

		#region IParentDetailsView implementation

		public void UpdateTableSource (MyDiem.Core.BL.Parent selectedParent)
		{
			this.SelectedParent = selectedParent;
			DetailsListView.Adapter = new ParentDetailsAdapter (this, selectedParent);
            if (!string.IsNullOrEmpty (selectedParent.PhotoUrl))
            {
                ProfilePicture.SetImageUrl (selectedParent.PhotoUrl);
            } 
            else
            {
                ProfilePicture.SetImageResource (Resource.Drawable.user);
            }
		}

		public string ScreenTitle {
			get {
				return "";
//				throw new NotImplementedException ();
			}
			set {
				ScreenTitleTextView.Text = value;
//				throw new NotImplementedException ();
			}
		}

		#endregion

		#region IBaseCalendarView implementation

		public void GoCalendarMonthView ()
		{
			throw new NotImplementedException ();
		}

		public void GoCalendarWeekView ()
		{
			throw new NotImplementedException ();
		}

		public void GoCalendarDayView ()
		{
			throw new NotImplementedException ();
		}

		public void GoSettingsView (MyDiem.Core.BL.User user)
		{
			throw new NotImplementedException ();
		}

		public void GoSubscribablesView (MyDiem.Core.BL.SchoolGroup category, string title)
		{
			throw new NotImplementedException ();
		}

		public void GoEventDetailsView (MyDiem.Core.BL.CalendarEvent selectedEvent)
		{
			throw new NotImplementedException ();
		}

		public void GoAboutView ()
		{
			throw new NotImplementedException ();
		}

		public void GoParentDirectory ()
		{
			throw new NotImplementedException ();
		}

        public void GoFacebookDirectory ()
        {
            throw new NotImplementedException ();
        }

        public void GoTwitterDirectory ()
        {
            throw new NotImplementedException ();
        }

		#endregion

		void AddContact ()
		{
			Intent intent = new Intent (Intent.ActionInsert);
			intent.SetType (ContactsContract.Contacts.ContentType);

			intent.PutExtra (ContactsContract.Intents.Insert.Name, SelectedParent.FirstName + " " + SelectedParent.LastName);
			intent.PutExtra (ContactsContract.Intents.Insert.Phone, SelectedParent.Phone1);
			intent.PutExtra (ContactsContract.Intents.Insert.SecondaryPhone, SelectedParent.Phone2);
			intent.PutExtra (ContactsContract.Intents.Insert.Postal, SelectedParent.Address + "\n" + SelectedParent.City + ", " + SelectedParent.State + " " + SelectedParent.Zip);
			StartActivity (intent);
		}
	}
}

