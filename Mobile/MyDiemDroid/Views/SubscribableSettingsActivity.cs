using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyDiem.Core.Interfaces;
using MonkeyArms;
using MyDiem.Core.BL;
using MyDiem.Droid.Adapters;

namespace MyDiem.Droid
{
	[Activity (Label = "SubscribableSettingsActivity")]			
	public class SubscribableSettingsActivity : BaseDetailsActivity, ISubscribableSettingsView
	{
		protected ProgressDialog UpdatingPrompt;
		protected bool UnSavedChanges = false;

		#region IBaseCalendarView implementation

		public void GoCalendarMonthView ()
		{
			//ignored on android
		}

		public void GoCalendarWeekView ()
		{
			//ignored on android
		}

		public void GoCalendarDayView ()
		{
			//ignored on android
		}

		public void GoSettingsView (MyDiem.Core.BL.User user)
		{
			//ignored on android
		}

		public void GoSubscribablesView (MyDiem.Core.BL.SchoolGroup category, string title)
		{
			//ignored on android
		}

		public void GoEventDetailsView (MyDiem.Core.BL.CalendarEvent selectedEvent)
		{
			//ignored on android
		}

		public void GoAboutView ()
		{
			//ignored on android
		}

		public void GoParentDirectory ()
		{
			//ignored on android
		}

        public void GoFacebookDirectory ()
        {
            //ignored on android
        }

        public void GoTwitterDirectory ()
        {
            //ignored on android
        }

		#endregion

		#region ISubscribableSettingsView implementation

		public event EventHandler SubscriptionChanged = delegate{};

		public List<CalendarColor> colors;

		public List<CalendarColor> Colors {
			get {
				return colors;
			}
			set {
				colors = value;
				if (SelectedSubscription.Color == null) {
					SelectedColorID = colors [0].ID;
				} else {
					foreach (var calendarColor in colors) {
						if (calendarColor.Color == SelectedSubscription.Color) {
							SelectedColorID = calendarColor.ID;
							break;
						}
					}
				}

				RunOnUiThread (() => {
					UpdateColorGrid ();
				});
			}
		}

		public void CreateColorPicker (List<MyDiem.Core.BL.CalendarColor> colors)
		{

		}

		private Subscribable selectedSubscribable;

		public Subscribable SelectedSubscribable {
			get {
				return selectedSubscribable;
			}
			set {
				selectedSubscribable = value;

			}
		}

		public Subscription SelectedSubscription {
			get;
			set;
		}

		public long SelectedColorID {
			get;
			set;
		}

		public void ShowUpdatedPrompt ()
		{
			RunOnUiThread (() => {
				UpdatingPrompt.Hide ();
				Toast.MakeText (this.BaseContext, "Updated.", ToastLength.Short).Show ();
				UnSavedChanges = false;
				base.OnBackPressed ();
			});
		}

		public void ShowUpdatingPrompt ()
		{
			UpdatingPrompt.Show ();
		}

		#endregion

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.SubscribableSettings);

			ConfigureHeader ();

			HideItems (new int[] {
				Resource.Id.leftArrowButton,
				Resource.Id.rightArrowButton,
				Resource.Id.mapButton
			});
			(FindViewById (Resource.Id.enabledCheckBox) as CheckBox).Click += HandleEnabledClicked;

			UpdatingPrompt = new ProgressDialog (this);
			UpdatingPrompt.SetCancelable (false);
			UpdatingPrompt.SetMessage ("Updating");
		}

		void HandleEnabledClicked (object sender, EventArgs e)
		{
			UpdateColorRowState ();
			SelectedSubscription.Enabled = (FindViewById (Resource.Id.enabledCheckBox) as CheckBox).Checked;
			UnSavedChanges = true;

		}

		protected override void OnStart ()
		{
			base.OnStart ();
			DI.RequestMediator (this);
			SetTitle ();
			(FindViewById (Resource.Id.enabledCheckBox) as CheckBox).Checked = this.SelectedSubscription.Enabled;
			UpdateColorRowState ();
			UnSavedChanges = false;
		}

		protected override void OnStop ()
		{
			base.OnStop ();
			DI.DestroyMediator (this);
		}

		void UpdateColorRowState ()
		{
			var enabledCheckbox = FindViewById (Resource.Id.enabledCheckBox) as CheckBox;

			if (enabledCheckbox.Checked && selectedSubscribable.type == Subscribable.TYPE_CALENDAR) {
				ShowColorPicker ();
				UpdateColorGrid ();
			} else {
				HideColorPicker ();
			}
		}

		void ShowColorPicker ()
		{
			(FindViewById (Resource.Id.colorRow) as FrameLayout).Visibility = ViewStates.Visible;
			(FindViewById (Resource.Id.colorGridView) as GridView).Visibility = ViewStates.Visible;
			var color = Android.Graphics.Color.ParseColor ("#" + SelectedSubscription.Color.Trim ());
			(FindViewById (Resource.Id.colorSwatchView) as View).SetBackgroundColor (color);
		}

		void HideColorPicker ()
		{
			(FindViewById (Resource.Id.colorRow) as FrameLayout).Visibility = ViewStates.Gone;
			(FindViewById (Resource.Id.colorGridView) as GridView).Visibility = ViewStates.Gone;
		}

		void UpdateColorGrid ()
		{
			var grid = FindViewById (Resource.Id.colorGridView) as GridView;
			grid.Adapter = new ColorListAdapter (colors);
			grid.ItemClick -= HandleColorClicked; 
			grid.ItemClick += HandleColorClicked;
		}

		void HandleColorClicked (object sender, AdapterView.ItemClickEventArgs e)
		{
			var selectedSwatchView = FindViewById (Resource.Id.colorSwatchView) as View;
			var colorValue = colors [e.Position].Color.Trim ();
			selectedSwatchView.SetBackgroundColor (Android.Graphics.Color.ParseColor ("#" + colorValue));

			SelectedSubscription.Color = colorValue;
			SelectedColorID = colors [e.Position].ID;
			UnSavedChanges = true;
		}

		void SetTitle ()
		{
			var titleView = (FindViewById (Resource.Id.screenTitle) as TextView);
			if (titleView != null) {
				titleView.Text = SelectedSubscribable.title;
			}
		}

		protected void ConfigureHeader ()
		{
			(FindViewById (Resource.Id.settingsButton) as View).Visibility = ViewStates.Gone;
			(FindViewById (Resource.Id.searchInput) as View).Visibility = ViewStates.Gone;
			(FindViewById (Resource.Id.filterButton) as View).Visibility = ViewStates.Gone;
			(FindViewById (Resource.Id.rightHeaderButton) as View).Visibility = ViewStates.Gone;
		}

		public override void OnBackPressed ()
		{
			if (UnSavedChanges) {
				SubscriptionChanged (this, new EventArgs ());
			} else {
				base.OnBackPressed ();
			}
		}
	}
}

