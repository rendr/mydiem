using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Views.Animations;
using MyDiem.Core.Interfaces;
using MyDiem.Droid.Fragments;
using MonkeyArms;
using MyDiem.Core.Invokers;
using System.Threading;
using System.Drawing;
using Xamarin.UrbanAirship.Push;
using Xamarin.InAppBilling;
using Xamarin.InAppBilling.Utilities;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.SAL;
using MyDiem.Core.BL;


namespace MyDiem.Droid
{
    [Activity(Label = "MainActivity", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]			
    public class MainActivity : BaseActivity, IBaseCalendarView
    {
        List<BaseFragment> FragmentCache = new List<BaseFragment>();
        protected BaseFragment CurrentScreen;
        GestureDetector GestureDetector;

        #region IBaseCalendarView implementation

        public void GoCalendarMonthView()
        {
            SetContentFragment(new CalendarMonthFragmentScreen());
            if (SettingsIsOpen)
            {
                CloseSettingsMenu();
            }
        }

        public void GoCalendarWeekView()
        {
            SetContentFragment(new CalendarWeekScreenFragment());
            CloseSettingsMenu();
        }

        public void GoCalendarDayView()
        {
            SetContentFragment(new CalendarListFragment());
            CloseSettingsMenu();
        }

        public void GoSettingsView(MyDiem.Core.BL.User user)
        {
            SetContentFragment(new ProfileFragment());
            CloseSettingsMenu();
        }

        public void GoSubscribablesView(MyDiem.Core.BL.SchoolGroup category, string title)
        {
            SetContentFragment(new SubscribablesFragment(title));
            CloseSettingsMenu();
        }

        public void GoEventDetailsView(MyDiem.Core.BL.CalendarEvent selectedEvent)
        {
            StartActivity(typeof(EventDetailsActivity));
        }

        public void GoAboutView()
        {
            SetContentFragment(new AboutFragment());
            CloseSettingsMenu();
        }

        public void GoParentDirectory()
        {
            SetContentFragment(new DirectoryFragment());
            CloseSettingsMenu();
        }

        public void GoFacebookDirectory ()
        {
            SetContentFragment (new FacebookFragment ());
            CloseSettingsMenu ();
        }

        public void GoTwitterDirectory ()
        {
            SetContentFragment (new TwitterFragment ());
            CloseSettingsMenu ();
        }

        #endregion

        protected View SettingsView, ContentContainer;
        protected ImageView SettingsButton;
        protected TranslateAnimation OpenSettingsAnimation, CloseSettingsAnimation;
        protected float ScreenWidth;
        protected bool SettingsIsOpen = false;
        protected InAppBillingServiceConnection _serviceConnection;
        IList<Xamarin.InAppBilling.Model.Product> StoreItems;

        protected override void OnCreate(Bundle bundle)
        {

            base.OnCreate(bundle);

            ScreenWidth = WindowManager.DefaultDisplay.Width;


            CreateUI();
            InitAnimations();

            GoCalendarMonthView();


            DI.Get<AddDeviceToUAInvoker>().Invoke();


        }

        protected override void OnStart()
        {
            base.OnStart();

            DI.RequestMediator(this);

            DI.Get<LinkCalendarInvoker>().Invoked += HandleLinkToCalendar;
            DI.Get<PurchaseCalendarConnectInvoker>().Invoked += HandlePurchaseCC;


            GestureDetector = new GestureDetector(this, new MainActivityGestureDetector(this));
            InitStore();


        }

        void HandlePurchaseCC(object sender, EventArgs e)
        {
            foreach (Xamarin.InAppBilling.Model.Product product in StoreItems)
            {
                if (product.ProductId == "calendar_connect_2014_2015")
                {
                    _serviceConnection.BillingHelper.LaunchPurchaseFlow(product);
                    return;
                }
            }
            _serviceConnection.BillingHelper.LaunchPurchaseFlow(StoreItems[0]);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            DI.Get<User>().CalendarConnect = true;
            UpdateUserLinkAccess del = new UpdateUserLinkAccess();
            del.Update(DI.Get<User>().ID,
                delegate
                {
                }
            );
            DI.Get<CalendarConnectPurchasedInvoker>().Invoke();
        }

        async void InitStore()
        {
            var publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3Sf6xE7NbVAaTbFV6Z+QyGBYbKMaE0XNPcPABRniM1CbArs8dfmcN1GVG31VCXQssPoaUaqsAM44k/2EOVfbRIIQf3NrtxZERHSK2EoXTLl4NvBALpkPe7caZ3gGAwOM1R/GdxVk7ZC+RnrbOF70YUePid+prUvdzNpPktYyJKd+7wayqXOaiMo8jU3Gsp3RBdAJmaCCPwxlZ8Ntwf2C1Dp0EV/CLyel3VtB565i7VpywpQxMjkv2TDnWVsAq92sFRMdTMaabaZ7jNPGaun4i3Lq8GMq1m7LUTuUrapE3QLHGX3ItJibgnQHqh0cxVVB+EhQxmJL0+2KE9aPSQjHHwIDAQAB";
            //Get available products
            _serviceConnection = new InAppBillingServiceConnection(this, publicKey);
            _serviceConnection.OnDisconnected += HandleOnDisconnected;
            _serviceConnection.OnConnected += HandleOnConnected;
            _serviceConnection.Connect();




        }

        void HandleOnConnected(object sender, EventArgs e)
        {
            GetStoreItems();
        }

        void HandleOnDisconnected(object sender, EventArgs e)
        {
            Console.WriteLine("connect to PlayStore failed");
        }

        async void GetStoreItems()
        {
            StoreItems = await _serviceConnection.BillingHelper.QueryInventoryAsync(new List<string>
                {
                    "calendar_connect_2013"
                }, ItemType.InApp);
            Console.WriteLine("Products Received from Store");
        }

        void HandleLinkToCalendar(object sender, EventArgs e)
        {

//            var args = e  as LinkCalendarInvokerArgs;
//            //webcal://www.mydiem.com/ical/0B0C78751A6753FE3C749AAE4BEF7680.ics
//            var ics = args.SubscriptionURL.Replace("webcal://www.mydiem.com/ical/", "");
//            var i = new Intent(Intent.ActionView);
//            i.SetData(Android.Net.Uri.Parse("http://www.google.com/calendar/render?cid=http%3A%2F%2Fwww.mydiem.com%2Fical%2F" + ics));
//            StartActivity(i);


        }

        protected override void OnPause()
        {
            base.OnPause();
            DI.DestroyMediator(this);
            DI.Get<LinkCalendarInvoker>().Invoked -= HandleLinkToCalendar;
            DI.Get<PurchaseCalendarConnectInvoker>().Invoked -= HandlePurchaseCC;
        }

        protected override void OnStop()
        {
            base.OnStop();
            DI.DestroyMediator(this);
            DI.Get<LinkCalendarInvoker>().Invoked -= HandleLinkToCalendar;
            DI.Get<PurchaseCalendarConnectInvoker>().Invoked -= HandlePurchaseCC;


        }

        public override void OnBackPressed()
        {
            if (FragmentCache.Count == 1)
            {
                base.OnBackPressed();
            }
            else
            {
                SetContentFromCache();

            }
        }

        public override bool OnTouchEvent(MotionEvent e)
        {

            return GestureDetector.OnTouchEvent(e);
        }

        void InitAnimations()
        {
            var slideAmount = ScreenWidth - 200f;
            var slideDuration = 250;

            OpenSettingsAnimation = new TranslateAnimation(0, slideAmount, 0, 0);
            OpenSettingsAnimation.Duration = slideDuration;
            OpenSettingsAnimation.FillEnabled = true;

            OpenSettingsAnimation.AnimationEnd += delegate
            {
                ContentContainer.SetX(slideAmount);
                SettingsIsOpen = true;
            };



            CloseSettingsAnimation = new TranslateAnimation(0, -1 * slideAmount, 0, 0);
            CloseSettingsAnimation.Duration = slideDuration;
            CloseSettingsAnimation.FillEnabled = true;
            CloseSettingsAnimation.AnimationEnd += delegate
            {
                ContentContainer.SetX(0);
                SettingsIsOpen = false;
                SettingsView.Visibility = ViewStates.Gone;

            };
            SettingsView.Visibility = ViewStates.Gone;
        }

        void CreateUI()
        {
            // Create your application here

            SetContentView(Resource.Layout.Main);
            CreateSettingsView();
            SettingsButton = FindViewById(Resource.Id.settingsButton) as ImageView;

            ContentContainer = FindViewById(Resource.Id.contentContainer) as View;
        }

        void CreateSettingsView()
        {
            SettingsView = FindViewById(Resource.Id.settingsLayout) as View;
            var settings = SettingsFragment.NewInstance();
            var fragTransacdtion = FragmentManager.BeginTransaction();
            fragTransacdtion.Replace(Resource.Id.settingsLayout, settings);
            fragTransacdtion.Commit();
        }

        void SetContentFromCache()
        {
            var lastIndex = FragmentCache.Count - 1;
            FragmentCache.RemoveAt(lastIndex);
            lastIndex--;
            var previousFragment = FragmentCache[lastIndex];
            SetContentFragment(previousFragment, false);
        }

        void SetContentFragment(Fragment fragment, bool cache = true)
        {
            if (CurrentScreen != null)
            {

                CurrentScreen.ToggleSettingsView -= HandleToggleSettingsView;
            }
            CurrentScreen = fragment as BaseFragment;
            CurrentScreen.ToggleSettingsView += HandleToggleSettingsView;

            var transaction = FragmentManager.BeginTransaction();
            transaction.Replace(Resource.Id.contentFrame, fragment);
            transaction.SetTransition(FragmentTransit.FragmentFade);
            transaction.Commit();

            if (cache)
            {
                FragmentCache.Add(CurrentScreen);
            }
        }

        void HandleToggleSettingsView(object sender, EventArgs e)
        {
            if (SettingsIsOpen)
            {
                CloseSettingsMenu();
            }
            else
            {
                OpenSettingsMenu();
            }
        }

        public void CloseSettingsMenu()
        {
            if (SettingsIsOpen)
            {
                ContentContainer.StartAnimation(CloseSettingsAnimation);
            }

        }

        public void OpenSettingsMenu()
        {
            ContentContainer.StartAnimation(OpenSettingsAnimation);
            SettingsView.Visibility = ViewStates.Visible;
        }

        class MainActivityGestureDetector:GestureDetector.SimpleOnGestureListener
        {
            MainActivity Target;

            public MainActivityGestureDetector(MainActivity activity)
            {
                Target = activity;

            }

            public override bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
            {

                if (velocityX > 0)
                {
                    Target.OpenSettingsMenu();
                }
                return base.OnFling(e1, e2, velocityX, velocityY);
            }
        }
    }
}

