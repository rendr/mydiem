﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyDiem.Core.BL;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using MyDiem.Core.Interfaces;
using MonkeyArms;
using Java.Util;
using Android.Locations;
using Android.Drm;



namespace MyDiem.Droid
{
	[Activity (Label = "MapActivity")]			
	public class MapActivity : BaseDetailsActivity, IMapView, GoogleMap.IInfoWindowAdapter, GoogleMap.IOnMapLoadedCallback, ILocationListener
	{
		public event EventHandler ParentSelected = delegate{};

		protected TextView ScreenTitleTextView;

		protected GoogleMap Map;
		protected View InfoWindow;

		protected Dictionary<int, String> ParentMarkerDictionary = new Dictionary<int, String> ();

		Location _currentLocation;
		LocationManager _locationManager;
		String _locationProvider;


		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Map);
			ScreenTitleTextView = FindViewById (Resource.Id.screenTitle) as TextView;
			ScreenTitleTextView.Text = "Map";

			base.HideItems (new int[] {
				Resource.Id.rightHeaderButton,
				Resource.Id.settingsButton,
				Resource.Id.filterButton,
				Resource.Id.searchInput,
				Resource.Id.mapButton,
				Resource.Id.leftArrowButton,
				Resource.Id.rightArrowButton

			});
			InitializeLocationManager ();

		}

		protected override void OnResume ()
		{
			base.OnResume ();
			DI.RequestMediator (this);
			initMap ();

		}

		protected override void OnPause ()
		{
			base.OnPause ();
			DI.DestroyMediator (this);
			_locationManager.RemoveUpdates (this);
		}

		void InitializeLocationManager ()
		{
			_locationManager = (LocationManager)GetSystemService (LocationService);
			Criteria criteriaForLocationService = new Criteria {
				Accuracy = Accuracy.Fine
			};
			IList<string> acceptableLocationProviders = _locationManager.GetProviders (criteriaForLocationService, true);

			if (acceptableLocationProviders.Any ()) {
				_locationProvider = acceptableLocationProviders.First ();
			} else {
				_locationProvider = String.Empty;
			}
		}

		private void initMap ()
		{

			MapFragment mapFrag = (MapFragment)FragmentManager.FindFragmentById (Resource.Id.map);
			if (mapFrag != null) {
				Map = mapFrag.Map;
				if (Map != null) {
					Map.SetInfoWindowAdapter (this);
					Map.InfoWindowClick += MapOnInfoWindowClick;
					Map.SetOnMapLoadedCallback (this);
					foreach (Parent parent in Contacts) {
						addParentMarker (parent);
					}
				}
				updateMapLocation ();
			}
		}

		private void addParentMarker (Parent parent)
		{
			if (Map != null) {
				MarkerOptions markerOpt1 = new MarkerOptions ();
				markerOpt1.SetPosition (new LatLng (parent.Lat, parent.Long));
				markerOpt1.SetTitle (parent.FirstName + " " + parent.LastName);
				Marker parentMarker = Map.AddMarker (markerOpt1);
				ParentMarkerDictionary.Add (parentMarker.GetHashCode (), parent.ID);
			}
		}

		private void updateMapLocation ()
		{
//			if (Map != null && _currentLocation != null) {
			try {
				var user = Contacts.First (u => u.Email == DI.Get<User> ().Email);
				CameraUpdate cu = CameraUpdateFactory.NewLatLng (new LatLng (user.Lat, user.Long));
				Map.MoveCamera (cu);
				CameraUpdate cu2 = CameraUpdateFactory.ZoomTo (16);
				Map.MoveCamera (cu2);
			} catch {
				Console.WriteLine ("couldn't find user's location");
			}
//			}
		}



		public List<Parent> Contacts {
			get;
			set;
		}

		public Parent OriginUser {
			get;
			set;
		}

		#region IMapView implementation

		public void GoParentDetails ()
		{
			StartActivity (typeof(ParentDetailsActivity));
		}


		public Parent SelectedParent {
			private set;
			get;
		}

		#endregion

		#region GoogleMap.IOnMapLoadedCallback implementation

		public void OnMapLoaded ()
		{
			//TODO: Ben fix this
			//setMapLocation (OriginUser.Lat, OriginUser.Long);
			updateMapLocation ();
		}

		#endregion

		#region GoogleMap.IInfoWindowAdapter implementation

		public View GetInfoWindow (Marker marker)
		{
			return null;
		}

		public View GetInfoContents (Marker marker)
		{
			if (InfoWindow == null) {
				InfoWindow = LayoutInflater.Inflate (Resource.Layout.MapInfoWindow, null);
			}
			TextView infoWindowText = (TextView)InfoWindow.FindViewById (Resource.Id.marker_title);
			infoWindowText.Text = marker.Title;
			return InfoWindow;
		}

		#endregion

		#region GoogleMap.IMapOnInfoWindowClick implementation

		private void MapOnInfoWindowClick (object sender, GoogleMap.InfoWindowClickEventArgs e)
		{
			Marker myMarker = e.P0;
			int hashCode = e.P0.GetHashCode ();
			if (ParentMarkerDictionary.ContainsKey (hashCode)) {
				String parentId = ParentMarkerDictionary [hashCode];
				SelectedParent = Contacts.First (contact => contact.ID == parentId);
				ParentSelected (this, EventArgs.Empty);
			}
		}

		#endregion

		public void OnLocationChanged (Location location)
		{

			_currentLocation = location;

			updateMapLocation ();

		}

		public void OnProviderDisabled (string provider)
		{
		}

		public void OnProviderEnabled (string provider)
		{
		}

		public void OnStatusChanged (string provider, Availability status, Bundle extras)
		{

		}

	}
}

