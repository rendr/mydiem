using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using MyDiem.Droid;
using Android.Graphics;


namespace MyDiem.Views
{
	public class NotatedCalendarCell : View
	{
		/*
		 * PROPS
		 */


		Color backgroundColor = new Color (255,255,255);

		public Color BackgroundColor {
			get {
				return backgroundColor;
			}
			set {
				backgroundColor = value;
				this.backgroundPaint.Color = backgroundColor;
			}
		}

		List<CellSwatch> swatches;

		public List<CellSwatch> Swatches {
			get {
				return swatches;
			}
			set {
				swatches = value;
				Invalidate ();
			}
		}

		DateTime date;
		public DateTime Date {
			get {
				return date;
			}
			set {
				date = value;
				Invalidate ();
			}
		}


		bool highlighted = false;
		public bool Highlighted {
			get{
				return highlighted;
			}
			set{
				highlighted = value;
				Invalidate ();
			}
		}

		private bool showText;

		public bool ShowText {
			get {
				return showText;
			}
			set {
				showText = value;
				Invalidate ();
				RequestLayout ();
			}
		}

		int textPosition;

		public int TextPosition {
			get {
				return textPosition;
			}
			set {
				textPosition = value;
				Invalidate ();
				RequestLayout ();
			}
		}

		/*
		 * FIELDS
		 */

		Paint textPaint, backgroundPaint, borderPaint;

		/*
		 * CONSTRUCTORS
		 */

		public NotatedCalendarCell (Context context) :
			base (context)
		{
			Initialize ();
		}

		public NotatedCalendarCell (Context context, IAttributeSet attrs) :
			base (context, attrs)
		{
			Initialize ();
			var attArray = Context.Theme.ObtainStyledAttributes (attrs, Resource.Styleable.NotatedCalendarCell, 0, 0);

			try{
				this.showText = attArray.GetBoolean(Resource.Styleable.NotatedCalendarCell_showText,false);
				this.textPosition = attArray.GetInt(Resource.Styleable.NotatedCalendarCell_labelPosition,0);
			}finally{
				attArray.Recycle ();
			}
		}

		public NotatedCalendarCell (Context context, IAttributeSet attrs, int defStyle) :
			base (context, attrs, defStyle)
		{
			Initialize ();
		}

		/*
		 * OVERRIDES
		 */
		protected override void OnDraw (Canvas canvas)
		{
			base.OnDraw (canvas);



			//draw the background
			canvas.DrawRect(new Rect(0,0,MeasuredWidth, MeasuredHeight), backgroundPaint);

			borderPaint.Color = (!highlighted)? new Color(208,208,208): new Color(240,129,35);
			borderPaint.StrokeWidth = (!highlighted)? 4f: 12f;
			//draw the border
			canvas.DrawRect(new Rect(0,0,MeasuredWidth, MeasuredHeight), borderPaint);

			//draw the label
			canvas.DrawText (Date.Day.ToString(), MeasuredWidth - 30, MeasuredHeight - 10, textPaint);


			if (swatches != null) {
				UpdateSwatches (canvas);
			}


		}

		protected override void OnMeasure (int widthMeasureSpec, int heightMeasureSpec)
		{
			// we pass the width as the height so we get a nice scalable square.
			var newHeight = (int)Math.Floor ((float)widthMeasureSpec);
			base.OnMeasure (widthMeasureSpec, newHeight);
		}

		/*
		 * HELPER FUNCTIONS
		 */
		void Initialize ()
		{
			this.textPaint = new Paint (PaintFlags.AntiAlias);
			this.textPaint.Color = new Color (0,0,0);
			this.textPaint.TextSize = 20f;
			this.textPaint.FakeBoldText = true;

			this.backgroundPaint = new Paint (PaintFlags.AntiAlias);
			this.backgroundPaint.Color = backgroundColor;
			this.backgroundPaint.SetStyle (Paint.Style.Fill);

			this.borderPaint = new Paint (0);
			this.borderPaint.SetStyle (Paint.Style.Stroke);

		}

		void UpdateSwatches (Canvas canvas)
		{
			var swatchWidth = 20;
			var swatchHeight = 20;
			const int padding = 10;
			var curX = padding;
			var curY = padding;

			foreach (var cellSwatch in swatches) {
				if (curX + swatchWidth > MeasuredWidth || cellSwatch.SpanWidth) {
					curX = padding;
					if (swatches.IndexOf (cellSwatch) != 0) {
						curY += swatchHeight + padding;
					}
					swatchWidth = MeasuredWidth - (padding * 2);
				} else {
					swatchWidth = 20;
				}
				var swatchPaint = new Paint (PaintFlags.AntiAlias);
				swatchPaint.Color = cellSwatch.Color;
				swatchPaint.SetStyle (Paint.Style.Fill);
				canvas.DrawRect (new Rect (curX, curY, swatchWidth + curX, 20 + curY), swatchPaint);
				curX += swatchWidth + padding;
			}
		}
	}

	public struct CellSwatch{
		public Color Color;
		public bool SpanWidth;
	}
}

