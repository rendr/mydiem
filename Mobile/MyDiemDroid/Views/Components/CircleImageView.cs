using System;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Runtime;
using Android.Util;
using Android.Widget;
using System.Net;

namespace MyDiem.Droid
{
    public class CircleImageView : ImageView
    {
        public CircleImageView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public CircleImageView(Context context) : base(context)
        {
        }

        protected CircleImageView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public override void SetImageDrawable(Drawable drawable)
        {
            var inputBitmap = Utils.DrawableToBitmap(drawable);
            var drawable2 = new CircleDrawable(inputBitmap);
            base.SetImageDrawable(drawable2);
        }

        public override void SetImageBitmap(Bitmap bm)
        {
            var drawable = new CircleDrawable(bm);
            base.SetImageDrawable(drawable);
        }

        public override void SetImageResource(int resId)
        {
            var inputBitmap = Utils.DrawableToBitmap(Resources.GetDrawable(resId));

            var drawable = new CircleDrawable(inputBitmap);
            base.SetImageDrawable(drawable);
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            base.OnMeasure(widthMeasureSpec, heightMeasureSpec);
            SetMeasuredDimension (MeasuredWidth, MeasuredWidth);
        }

        public void SetImageUrl(string url)
        {
            if (string.IsNullOrEmpty (url))
                return;
            
            Bitmap imageBitmap = null;

            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }

            this.SetImageBitmap(imageBitmap);
        }
    }
}