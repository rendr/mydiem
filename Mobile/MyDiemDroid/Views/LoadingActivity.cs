using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MonkeyArms;
using MyDiem.Core.Interfaces;
using MyDiem.Core.SAL;

namespace MyDiem.Droid
{
	[Activity (Label = "LoadingActivity", NoHistory = true, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]			
	public class LoadingActivity : BaseActivity, ILoadingView
	{
		ProgressDialog loadingIndicator;

		#region ILoadingView implementation

		public void GoMainView ()
		{
			RunOnUiThread (() => {

				StartActivity (typeof(MainActivity));
				loadingIndicator.Hide ();
				loadingIndicator.Dismiss ();
			});
		}

		#endregion

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			OverridePendingTransition (Resource.Animation.fadein, Resource.Animation.fadeout);
			// Create your application here
			SetContentView (Resource.Layout.Loading);
			loadingIndicator = new ProgressDialog (this);
			loadingIndicator.SetCancelable (false);
			loadingIndicator.SetMessage (Resources.GetString (Resource.String.loading));

			loadingIndicator.Show ();

			BaseAPIServiceDelegate.ServiceError += (object sender, EventArgs e) => RunOnUiThread (() => {
				AlertDialog.Builder dialog = new AlertDialog.Builder (this.BaseContext);
				dialog.SetTitle ("Server Error");
				dialog.SetMessage ("Sorry, there was an error communicating with the server.");
				dialog.Show ();
			});


		}

		protected override void OnStart ()
		{
			base.OnStart ();
			DI.RequestMediator (this);
		}

		protected override void OnStop ()
		{
			base.OnStop ();
			DI.DestroyMediator (this);
		}
	}
}

