using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using MyDiemDroid;
using MyDiem.Core.Interfaces;
using MonkeyArms;
using Android.Util;

namespace MyDiem.Droid
{
	[Activity (Label = "HomeActivity", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]			
	public class HomeActivity : BaseActivity, IHomeView
	{

		public event EventHandler Login = delegate {};

		public event EventHandler Register = delegate {};

        public event EventHandler Reset = delegate { };

        protected Button LoginButton, RegisterButton, ResetButton;

		protected override void OnCreate (Bundle bundle)
		{


			base.OnCreate (bundle);

			SetContentView (Resource.Layout.Home);
			SetReferences ();
			SetEventHandlers ();



			int cardWidth = (int)TypedValue.ApplyDimension (ComplexUnitType.Dip, 300, Resources.DisplayMetrics);
			int appWidth = Resources.DisplayMetrics.WidthPixels;
			var paddingToCenter = (appWidth / 2) - (cardWidth / 2);


			var firstCard = FindViewById<ImageView> (Resource.Id.introCard1);
			var firstCardLayoutParams = new Android.Widget.LinearLayout.LayoutParams (cardWidth, WindowManagerLayoutParams.FillParent);
			firstCardLayoutParams.LeftMargin = paddingToCenter;
			firstCard.LayoutParameters = firstCardLayoutParams;


			var lastCard = FindViewById<LinearLayout> (Resource.Id.loginCard);
			var lastCardLayoutParams = new Android.Widget.LinearLayout.LayoutParams (cardWidth, WindowManagerLayoutParams.FillParent);
			lastCardLayoutParams.RightMargin = paddingToCenter;
			lastCard.LayoutParameters = lastCardLayoutParams;


		}

		protected override void OnStart ()
		{
			base.OnStart ();
			DI.RequestMediator (this);
		}

		protected override void OnStop ()
		{
			base.OnStop ();
			DI.DestroyMediator (this);
		}

		public void GoLogin ()
		{
			StartActivity (typeof(LoginActivity));
		}

		public void GoRegister ()
		{
			StartActivity (typeof(RegisterActivity));
		}

        public void GoReset ()
        {
            StartActivity (typeof (ResetPasswordActivity));
        }

        void SetReferences ()
		{
            LoginButton = FindViewById (Resource.Id.loginButton) as Button;
            RegisterButton = FindViewById (Resource.Id.registerButton) as Button;
            ResetButton = FindViewById (Resource.Id.resetButton) as Button;
		}

		void SetEventHandlers ()
		{
			LoginButton.Click += (object sender, EventArgs e) => {
				Login (this, EventArgs.Empty);
			};

			RegisterButton.Click += (object sender, EventArgs e) => {
				Register (this, EventArgs.Empty);
			};

            ResetButton.Click += (object sender, EventArgs e) => {
                Reset (this, EventArgs.Empty);
            };
		}
	}
}

