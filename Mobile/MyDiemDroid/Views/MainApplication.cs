using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using MyDiem.Core.Config;
using MonkeyArms;
using MyDiem.Core.SAL;
using Service;
using MyDiem.Core.Invokers;
using MyDiem.iOS.Commands;
using Com.Urbanairship;
using Xamarin.UrbanAirship.Push;
using Mindscape.Raygun4Net;
using Java.IO;
using Android.Webkit;

namespace MyDiemDroid
{
    [Application]
    class MainApplication:Application
    {
        MyDiemCoreContext appContext;

        public MainApplication(IntPtr handle, JniHandleOwnership transfer)
            : base(handle, transfer)
        {

            RaygunClient.Attach("P7bOiG+JYrXQE2XUso87Kw==");

            //platform specific mappings
            DI.MapClassToInterface<WebAPIService, IWebAPIService>();

            //invoker/command mappings
            DI.MapCommandToInvoker<RegisterDeviceToUACommand, AddDeviceToUAInvoker>(); 

            //core mappings
            appContext = new MyDiemCoreContext();

            DI.Get<LinkCalendarInvoker>().Invoked += (object sender, EventArgs e) =>
            {

                var url = (e  as LinkCalendarInvokerArgs).SubscriptionURL;
				//BAD
				//https://calendar.google.com/calendar/render?cid=http://www.mydiem.com/ical/webcal://app.mydiem.com//ical/AFFEFE35FFE8B2C1E16AA4E13F6EF0E2.ics&pli=1#main_7
				//GOOD
				//https://www.google.com/calendar/render?cid=webcal://app.mydiem.com/ical%2F9BBECC03E7670D216F6319F881C89491.ics 

				//            url = "https://www.google.com/calendar/render?cid=" + url;

				url = url.Replace("webcal", "http");

				Intent i = new Intent(Intent.ActionView);
				i.SetDataAndType(Android.Net.Uri.Parse(url), "text/html");
				i.AddFlags(ActivityFlags.NewTask);
				StartActivity(i);
			};
        }

        public override void OnCreate()
        {
            base.OnCreate();
            InitUrbanAirship();
		

        }

        void InitUrbanAirship()
        {
            UAirship.TakeOff(this);
            PushManager.EnablePush();



        }
    }
}

