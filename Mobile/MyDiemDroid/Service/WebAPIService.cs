using System;
using MyDiem.Core.SAL;
using RestSharp;

namespace Service
{
	public class WebAPIService:IWebAPIService
	{
		public event EventHandler Result = delegate{};
		public event EventHandler Error = delegate{};

		private string responseText;

		public string ResponseText {
			get {
				return responseText;
			}
		}

		private string apiErrorMessage;

		public string APIErrorMessage {
			get {
				return apiErrorMessage;
			}
		}

		private string apiErrorURL;

		public string APIErrorURL {
			get {
				return apiErrorURL;
			}
		}

		public WebAPIService ()
		{

		}

        public void Send (string baseURL, bool post, string payload)
		{
            if (post)
            {
                SendPostRequest(baseURL, payload);
            }
            else
            {
                SendGetRequest(baseURL, payload);
            }
		}

        private void SendPostRequest(string baseURL, string payload)
        {
            var client = new RestClient (baseURL + "&json=true"); 

            var request = new RestRequest (baseURL, Method.POST);
            request.AddParameter("application/json; charset=utf-8", payload, ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            client.ExecuteAsync (request, response => {
                responseText = response.Content;
                Result (this, new EventArgs ());
            });  
        }

        private void SendGetRequest(string baseURL, string payload)
        {
            var client = new RestClient (baseURL + payload); 
            var request = new RestRequest ("", Method.GET);

            client.ExecuteAsync (request, response => {
                responseText = response.Content;
                Result (this, new EventArgs ());
            });
        }
	}
}

