﻿using System;
using Android.Views;
using Android.Widget;

namespace MyDiem.Droid
{
	public static class UIExtensions
	{
		public static void SetChildrenEnabled (this FrameLayout group, bool enabled)
		{
			for (int i = 0; i < group.ChildCount; i++) {
				View view = group.GetChildAt (i);
				view.Clickable = enabled;
				view.Enabled = enabled;
				view.Alpha = enabled ? 1 : .5f;
			}
		}
	}
}

