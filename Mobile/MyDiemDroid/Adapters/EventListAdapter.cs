using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyDiem.Core.BL;
using MonkeyArms;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.Interfaces;
using Android.Content.Res;

namespace MyDiem.Droid
{
	class EventListAdapter:BaseAdapter, IInjectingTarget, ICalendarEventListTableSource
	{

		[Inject]
		public UserManager UM;


		protected Activity Context;



		public EventListAdapter(Activity context, List<CalendarEvent> events):base()
		{
			this.Context = context;
			this.Events = events;
			DIUtil.InjectProps(this);
			DI.RequestMediator (this);


		}



		#region ICalendarEventListTableSource implementation
		public event EventHandler EventSelected = delegate {};

		public void Init (Dictionary<string, List<CalendarEvent>> days, int todaySectionIndex, int todayRowIndex)
		{

		}
		public List<CalendarEvent> Events {
			get;
			set;
		}
		public int SelectedSectionIndex {
			get {
				return 0;
			}
			set {

			}
		}
		public int SelectedRowIndex {
			get {
				return SelectedRowIndex;
			}
			set {
				SelectedRowIndex = value;
			}
		}
		#endregion


		#region implemented abstract members of BaseAdapter
		public override Java.Lang.Object GetItem (int position)
		{

			return null;
		}
		public override long GetItemId (int position)
		{
			return position;
		}
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			View view = convertView; // re-use an existing view, if one is available
			if (view == null) // otherwise create a new one
				view = Context.LayoutInflater.Inflate(Resource.Layout.EventListItem, null);

			var e = Events [position];

			if (e.AllDay) {
				(view.FindViewById (Resource.Id.startTimeTextView) as TextView).Text = this.Context.GetString (Resource.String.allDayLabel);
				(view.FindViewById (Resource.Id.detailTextView) as TextView).Visibility = ViewStates.Gone;
			} else {
				(view.FindViewById (Resource.Id.startTimeTextView) as TextView).Text = e.StartDateTime.ToString ("t").ToUpper ();
				(view.FindViewById (Resource.Id.detailTextView) as TextView).Text = e.EndDateTime.ToString("t").ToUpper();
				(view.FindViewById (Resource.Id.detailTextView) as TextView).Visibility = ViewStates.Visible;
			}

			(view.FindViewById (Resource.Id.titleTextView) as TextView).Text = e.Title;

			var swatchColor = UM.GetSubscriptionForCalendar ((int)e.CalendarID).Color;

			(view.FindViewById (Resource.Id.colorSwatchView) as View).SetBackgroundColor (Android.Graphics.Color.ParseColor ("#" + swatchColor));

			return view;
		}
		public override int Count {
			get {
				return Events.Count;
			}
		}
		#endregion

		public void HandleRowSelected()
		{
			EventSelected (this, new EventArgs ());
		}
	}
}

