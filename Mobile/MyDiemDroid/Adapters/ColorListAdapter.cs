using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MyDiem.Droid.Adapters
{
	class ColorListAdapter:BaseAdapter
	{
		Activity context;

		protected List<MyDiem.Core.BL.CalendarColor> Colors;

		public ColorListAdapter(List<MyDiem.Core.BL.CalendarColor> calendarColors):base()
		{
			this.Colors = calendarColors;
		}

		public override int Count {
			get {
				if (Colors != null) {
					return Colors.Count;
				} else {
					return 0;
				}
			}
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			var calendarColor = Colors [position];
			var v = new View (parent.Context);
			v.SetMinimumHeight (100);
			v.SetMinimumWidth (50);

			v.SetBackgroundColor (Android.Graphics.Color.ParseColor("#"+calendarColor.Color.Trim()));
			return v;
		}



		#region implemented abstract members of BaseAdapter
		public override Java.Lang.Object GetItem (int position)
		{
			return Colors [position].Color;

		}
		public override long GetItemId (int position)
		{
			return position;

		}
		#endregion
	}
}

