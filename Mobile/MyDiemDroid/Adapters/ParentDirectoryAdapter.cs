using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyDiem.Core.BL;
using Xamarin.Android.OpenSource;


namespace MyDiem.Droid.Adapters
{
	class ParentDirectoryAdapter:BaseAdapter<string>, ISectionIndexer, IPinnedHeaderAdapter, Android.Widget.AbsListView.IOnScrollListener
	{


		Activity context;

		List<Parent> Parents;

		protected Dictionary<string, int> AlphaIndex;
		protected string[] Sections;
		protected Java.Lang.Object[] SectionsObjects;



		public ParentDirectoryAdapter(Activity context, List<Parent> parents):base(){

			this.context = context;

			this.Parents = parents;


			InitAlphaIndex ();

		}

		void InitAlphaIndex ()
		{
			AlphaIndex = new Dictionary<string, int>();
			for (int i = 0; i < Parents.Count; i++) { // loop through items
				var key = Parents[i].LastName[0].ToString();
				if (!AlphaIndex.ContainsKey(key))
					AlphaIndex.Add(key, i); // add each 'new' letter to the index
			}
			Sections = new string[AlphaIndex.Keys.Count];
			AlphaIndex.Keys.CopyTo(Sections, 0); // convert letters list to string[]
			// Interface requires a Java.Lang.Object[], so we create one here
			SectionsObjects = new Java.Lang.Object[Sections.Length];
			for (int i = 0; i < Sections.Length; i++) {
				SectionsObjects[i] = new Java.Lang.String(Sections[i]);
			}
		}

		#region IPinnedHeaderAdapter implementation

		public void ConfigurePinnedHeader (View header, int position, int alpha)
		{

			var headerTextView = header as TextView;
			int section = GetSectionForPosition(position);
			String title = (String) GetSections()[section];

			headerTextView.Text =(title);

		}

		public PinnedHeaderState GetHeaderState(int position)
		{
			if (AlphaIndex == null || this.Count == 0) {
				return PinnedHeaderState.PINNED_HEADER_GONE;
			}

			if (position < 0) {
				return PinnedHeaderState.PINNED_HEADER_GONE;
			}

			// The header should get pushed up if the top item shown
			// is the last item in a section for a particular letter.
			int section = GetSectionForPosition(position);
			int nextSectionPosition = GetPositionForSection(section + 1);

			if (nextSectionPosition != -1 && position == nextSectionPosition - 1) {
				return PinnedHeaderState.PINNED_HEADER_PUSHED_UP;
			}

			return PinnedHeaderState.PINNED_HEADER_VISIBLE;
		}

		#endregion

		#region IOnScrollListener implementation

		public void OnScroll (AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
		{
			if (view is PinnedHeaderListView) {
				((PinnedHeaderListView) view).configureHeaderView(firstVisibleItem);
			}     
		}

		public void OnScrollStateChanged (AbsListView view, ScrollState scrollState)
		{

		}

		#endregion

		#region ISectionIndexer implementation
		public int GetPositionForSection (int section)
		{
			if (section >= Sections.Length) {
				return 0;
			}
			return AlphaIndex [Sections[section]];

		}
		public int GetSectionForPosition (int position)
		{
			var sectionKey = AlphaIndex.Last (startIndex => startIndex.Value <= position).Key;
			return AlphaIndex.Keys.ToList ().IndexOf (sectionKey);
		}
		public Java.Lang.Object[] GetSections ()
		{
			return SectionsObjects;
		}
		#endregion


		#region implemented abstract members of BaseAdapter
		public override long GetItemId (int position)
		{
			return position;
		}
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			View view = convertView; // re-use an existing view, if one is available
            if (view == null) // otherwise create a new one
            {
                view = context.LayoutInflater.Inflate (Resource.Layout.ParentDirectoryItem, null);
            
                var image = view.FindViewById<CircleImageView> (Resource.Id.profilePicture);
                var name = view.FindViewById<TextView> (Resource.Id.parentNameText);
                var child = view.FindViewById<TextView> (Resource.Id.childrenNameText);

                view.Tag = new ViewHolder () { Image = image, Title = name, Subtitle = child };
            }

			if (position == 0) {
				view.SetPadding (0, 50, 0, 0);
			} else {
				view.SetPadding (0, 0, 0, 0);
			}

			if (IsItemFirstOfSection (position)) {
				var headerTextView = view.FindViewById (Resource.Id.parentDirectoryHeader) as TextView;
				headerTextView.Text = Sections [GetSectionForPosition (position)];
				if (position != 0) {
					view.FindViewById (Resource.Id.parentDirectoryHeader).Visibility = ViewStates.Visible;
				} else {
					view.FindViewById (Resource.Id.parentDirectoryHeader).Visibility = ViewStates.Gone;
				}
			} else {
				view.FindViewById (Resource.Id.parentDirectoryHeader).Visibility = ViewStates.Gone;
			}

            var holder = (ViewHolder)view.Tag;
            holder.Title.Text = Parents[position].LastFirstName;
            holder.Subtitle.Text = Parents [position].ChildrenNames;

            if (string.IsNullOrEmpty (Parents [position].PhotoUrl))
            {
                holder.Image.SetImageResource (Resource.Drawable.user);
            } 
            else
            {
                holder.Image.SetImageUrl (Parents [position].PhotoUrl);
            }

            return view;
		}

		public override int Count {
			get {
				return Parents.Count;
			}
		}
		#endregion
		#region implemented abstract members of BaseAdapter
		public override string this [int position] {
			get {
				return Parents [position].LastFirstName;
			}
		}
		#endregion

		bool IsItemFirstOfSection (int position)
		{
			return position == 0 || GetSectionForPosition (position - 1) != GetSectionForPosition (position);
		}
	}

    public class ViewHolder : Java.Lang.Object
    {
        public TextView Title;
        public TextView Subtitle;
        public CircleImageView Image;
    }
}

