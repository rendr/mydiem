using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace MyDiem.Droid.Adapters
{
	public class GroupedSimpleAdapter:SimpleAdapter
	{
		public GroupedSimpleAdapter (Context context, IList<IDictionary<string, object>> data, int resource, string[] from, int[] to) : base (context, data, resource, from, to)
		{

		
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{

			var view= base.GetView (position, convertView, parent);;




			CheckToCollapseTextView (view, Resource.Id.groupHeaderTextView);
			CheckToCollapseTextView (view, Resource.Id.groupListItemLeftLabelTextView);

			var leftIcon = view.FindViewById<ImageView> (Resource.Id.leftIcon);
			if (leftIcon.Drawable == null) {
				leftIcon.Visibility = ViewStates.Gone;
			} else {
				leftIcon.Visibility = ViewStates.Visible;
			}
			var c = new Color (0x999999);
			leftIcon.SetColorFilter (c);


			var rightIcon = view.FindViewById<ImageView> (Resource.Id.rightIcon);
			if (rightIcon.Drawable == null) {
				rightIcon.Visibility = ViewStates.Gone;
			} else {
				rightIcon.Visibility = ViewStates.Visible;
			}
			rightIcon.SetColorFilter (c);
			return view;
		}

		static void CheckToCollapseTextView (View view, int textViewID)
		{
			var textView = view.FindViewById<TextView> (textViewID);
			if (String.IsNullOrEmpty (textView.Text)) {
				textView.Visibility = ViewStates.Gone;
			}
			else {
				textView.Visibility = ViewStates.Visible;
			}
		}
	}
}

