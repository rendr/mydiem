using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyDiem.Core.BL;
using MyDiemCore.Extensions;

namespace MyDiem.Droid.Adapters
{

    public class ParentDetailsAdapter : BaseAdapter<string>
    {

        protected Parent Contact;

        protected Activity Context;

        public ParentDetailsAdapter (Activity context, Parent parent)
        {
            this.Context = context;
            Contact = parent;
        }

        #region implemented abstract members of BaseAdapter

        public override long GetItemId (int position)
        {
            return position;
        }

        public override View GetView (int position, View convertView, ViewGroup parent)
        {
            View view = convertView; // re-use an existing view, if one is available
            if (view == null) // otherwise create a new one
                view = Context.LayoutInflater.Inflate (Resource.Layout.GroupedListItem, null);

            if (position < 5) {
                ConfigDetailRow (position, ref view);
            } else {
                GetChildRow (position, ref view);
            }
            return view;
        }

        public override int Count {
            get {
                //5 is for phone1, phone2, email, address, address2
                return Contact.Children.Count + 5;
            }
        }

        #endregion

        #region implemented abstract members of BaseAdapter

        public override string this [int position] {
            get {
                throw new NotImplementedException ();
            }
        }

        #endregion



        void ConfigDetailRow (int position, ref View view)
        {
            var header = view.FindViewById (Resource.Id.groupHeaderTextView) as TextView;
            header.Visibility = (position == 0) ? header.Visibility = ViewStates.Visible : header.Visibility = ViewStates.Gone;
            if (header.Visibility == ViewStates.Visible) {

                header.Text = Contact.DirectoryTitle;
            }

            var labels = new List<string> {
                Context.GetString(Resource.String.phone1Label),
                Context.GetString(Resource.String.phone2Label),
                Context.GetString(Resource.String.emailLabel),
                Context.GetString(Resource.String.addressLabel),
                ""
            };

            var details = new List<string> {
                Contact.Phone1.FormatPhoneNumber(),
                       Contact.Phone2.FormatPhoneNumber(),
                Contact.Email,
                Contact.HideAddress ? "Not listed" : Contact.Address,
                Contact.HideAddress ? ""           : string.Format("{0},  {1} {2}", Contact.City, Contact.State, Contact.Zip)
            };

            var rowLabel = view.FindViewById (Resource.Id.groupListItemCenterLabelTextView) as TextView;
            rowLabel.Text = labels [position];

            var rowDetails = view.FindViewById (Resource.Id.groupListItemRightTextView) as TextView;
            rowDetails.Text = details [position];
        }


        void GetChildRow (int position, ref View view)
        {
            var header = view.FindViewById (Resource.Id.groupHeaderTextView) as TextView;
            header.Visibility = (position == 5) ? header.Visibility = ViewStates.Visible : header.Visibility = ViewStates.Gone;
            if (header.Visibility == ViewStates.Visible) {
                header.Text = Context.GetString (Resource.String.childrenLabel);
            }

            var rowLabel = view.FindViewById (Resource.Id.groupListItemCenterLabelTextView) as TextView;
            rowLabel.Text = String.Format ("{0} {1}", Contact.Children [position - 5].FirstName, Contact.Children [position - 5].LastName);

            var rowDetails = view.FindViewById (Resource.Id.groupListItemRightTextView) as TextView;
            rowDetails.Text = String.Format ("Grade {0}", Contact.Children [position - 5].Grade);
        }
    }
}

