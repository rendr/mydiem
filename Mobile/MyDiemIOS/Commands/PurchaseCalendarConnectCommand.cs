using System;
using MonkeyArms;
using MyDiem.UI.Touch;
using MonoTouch.Foundation;
using MyDiem.Core.ViewModel;
using MyDiem.Core.SAL;
using MyDiem.Core.BL.Managers;

namespace MyDiem.iOS.Commands
{
	public class PurchaseCalendarConnectCommand:BaseIAPCommand
	{
		public override void Execute (InvokerArgs args)
		{
			base.Execute (args);
			IAPManager.PurchaseProduct ("calendarConnectV1");
		}

		protected override void HandleTransactionSucceded (NSNotification notification)
		{
			base.HandleTransactionSucceded (notification);
		}
	}

	public class PurchaseSubscribableCommand:BaseIAPCommand
	{
		[Inject]
		public SubscribableListViewModel SLVM;

		public override void Execute (InvokerArgs args)
		{
			base.Execute (args);
			IAPManager.PurchaseProduct (SLVM.SelectedSubscribable.sku);
		}

		protected override void HandleTransactionSucceded (NSNotification notification)
		{
			base.HandleTransactionSucceded (notification);
		}
	}

	public class BaseIAPCommand:Command
	{
		[Inject]
		public UserManager UM;
		protected InAppPurchaseManager IAPManager;
		protected NSObject TransactionSucceedObserver, TransactionFailedObserver;

		public BaseIAPCommand ()
		{

		}

		public override void Execute (InvokerArgs args)
		{
			IAPManager = new InAppPurchaseManager ();
			TransactionSucceedObserver = NSNotificationCenter.DefaultCenter.AddObserver (InAppPurchaseManager.InAppPurchaseManagerTransactionSucceededNotification, HandleTransactionSucceded);
			TransactionFailedObserver = NSNotificationCenter.DefaultCenter.AddObserver (InAppPurchaseManager.InAppPurchaseManagerTransactionFailedNotification, HandleTransactionFailed);

		}

		protected virtual void HandleTransactionSucceded (NSNotification notification)
		{
			RemoveObservables ();

//			UM.AppUser.CalendarConnect = true;
			UpdateUserLinkAccess del = new UpdateUserLinkAccess ();
//			SlideMenuViewModel.Reset ();
//			del.Update (UserManager.AppUser.ID,
//				delegate {
//				}
//			);
		}

		protected virtual void HandleTransactionFailed (NSNotification notification)
		{
			Console.WriteLine ("Transaction Failed");
			RemoveObservables ();
		}

		protected void RemoveObservables ()
		{
			try {
				NSNotificationCenter.DefaultCenter.RemoveObserver (TransactionSucceedObserver);
				NSNotificationCenter.DefaultCenter.RemoveObserver (TransactionFailedObserver);
			} catch {

			}
		}
	}
}

