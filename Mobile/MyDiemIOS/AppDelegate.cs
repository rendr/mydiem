﻿using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MyDiem.UI.Touch.ViewController;
using GoogleAnalytics.iOS;
using MonkeyArms;
using MyDiem.Core.Interfaces;
using MyDiem.UI.Touch.Database;
using MyDiem.Core.SAL;
using MyDiem.Core.Invokers;
using MyDiem.Core.Config;
using MyDiem.iOS.Commands;
using MyDiem.UI.Touch.Service;
using MyDiem.Core.Commands;
using MyDiem.UI.Touch.Mediators;
using MyDiem.UI.Touch.Views.RegionsPicker;
using MyDiem.UI.Touch.Views.CountiesPicker;

namespace MyDiem.UI.Touch
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		UIWindow window;
		UINavigationController rootNavigationController;
		LoadingViewController loadingScreen;
		HomeViewController homeScreen;
		// class-level declarations
		public override UIWindow Window {
			get;
			set;
		}

		public override void FinishedLaunching (UIApplication application)
		{
			this.window = new UIWindow (UIScreen.MainScreen.Bounds);
			this.rootNavigationController = new UINavigationController (); 

			this.window.RootViewController = this.rootNavigationController; 
			this.window.MakeKeyAndVisible (); 

			InitGA ();
			InitMA ();
			InitPush ();
		}

		public IGAITracker Tracker;
		public static readonly string TrackingId = "UA-50504303-1";

		protected void InitGA ()
		{
			// Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
			GAI.SharedInstance.DispatchInterval = 20;

			// Optional: automatically send uncaught exceptions to Google Analytics.
			GAI.SharedInstance.TrackUncaughtExceptions = true;

			// Initialize tracker.
			Tracker = GAI.SharedInstance.GetTracker (TrackingId);
		}

		protected void InitPush ()
		{
			PushNotifications.Subscribe ();
		}

		MyDiemCoreContext appContext;

		protected void InitMA ()
		{
			//platform specific mappings
			DI.MapClassToInterface<IOSInvokerMap, IInvokerMap> ();
			DI.MapClassToInterface<MyDiemDatabase, IMyDiemDatabase> ();
			DI.MapClassToInterface<WebAPIService, IWebAPIService> ();

			//invoker/command mappings
			DI.MapCommandToInvoker<RegisterDeviceToUACommand, AddDeviceToUAInvoker> (); 

			//mediator mappings
			DI.MapMediatorToClass<RegionPickerMediator, RegionsUIPickerView> ();
			DI.MapMediatorToClass<CountiesPickerMediator, CountiesUIPickerView> ();

			//core mappings
			appContext = new MyDiemCoreContext ();

			DI.Get<LinkCalendarInvoker> ().Invoked += (object sender, EventArgs e) => {

			};

			//add any global invoker listeners
			DI.Get<SetInitialScreenInvoker> ().Invoked += HandelSetInitialScreen;

			//Kickoff any Bootstrap commands/processes
			DI.MapCommandToInvoker<BaseBootStrapCommand, BootStrapInvoker> ().Invoke ();


		}

		void HandelSetInitialScreen (object sender, EventArgs e)
		{
			InvokeOnMainThread (() => {
				var initialScreen = (e as SetInitialScreenInvokerArgs).InitialScreen;

				if (initialScreen == InitialScreenEnum.HomeScreen) {
					homeScreen = new HomeViewController ();
					this.rootNavigationController.PushViewController (homeScreen, false); 
				}

				if (initialScreen == InitialScreenEnum.LoadingScreen) {
					loadingScreen = new LoadingViewController ();
					this.rootNavigationController.PushViewController (loadingScreen, false); 

				}
				DI.Get<SetInitialScreenInvoker> ().Invoked -= HandelSetInitialScreen;
			});
		}

		public override void RegisteredForRemoteNotifications (UIApplication application, NSData deviceToken)
		{
			Console.WriteLine ("RegisteredForRemoteNotifications");
			PushNotifications.EnsureDeviceRegistration (deviceToken);	
		
		}

		public override void FailedToRegisterForRemoteNotifications (UIApplication application, NSError error)
		{

			using (var alert = new UIAlertView ("FAIL", String.Format ("Failed to Register for Remote Notifications: {0}", error.LocalizedDescription), null, "OK", null)) {
				alert.Show ();	
			}
			Console.WriteLine ("Failed to Register for Remote Notifications: {0}", error.LocalizedDescription);
		}

		string launchWithCustomKeyValue;

		void processNotification (NSDictionary options, bool fromFinishedLaunching)
		{
			//Check to see if the dictionary has the aps key.  This is the notification payload you would have sent
			if (null != options && options.ContainsKey (new NSString ("aps"))) {
				//Get the aps dictionary
				NSDictionary aps = options.ObjectForKey (new NSString ("aps")) as NSDictionary;

				string alert = string.Empty;
				string sound = string.Empty;
				int badge = -1;

				//Extract the alert text
				//NOTE: If you're using the simple alert by just specifying "  aps:{alert:"alert msg here"}  "
				//      this will work fine.  But if you're using a complex alert with Localization keys, etc., your "alert" object from the aps dictionary
				//      will be another NSDictionary... Basically the json gets dumped right into a NSDictionary, so keep that in mind
				if (aps.ContainsKey (new NSString ("alert")))
					alert = (aps [new NSString ("alert")] as NSString).ToString ();

				//Extract the sound string
				if (aps.ContainsKey (new NSString ("sound")))
					sound = (aps [new NSString ("sound")] as NSString).ToString ();

				//Extract the badge
				if (aps.ContainsKey (new NSString ("badge"))) {
					string badgeStr = (aps [new NSString ("badge")] as NSObject).ToString ();
					int.TryParse (badgeStr, out badge);
				}

				//If this came from the ReceivedRemoteNotification while the app was running,
				// we of course need to manually process things like the sound, badge, and alert.
				if (!fromFinishedLaunching) {
					//Manually set the badge in case this came from a remote notification sent while the app was open
					if (badge >= 0)
						UIApplication.SharedApplication.ApplicationIconBadgeNumber = badge;

					//Manually play the sound
					if (!string.IsNullOrEmpty (sound)) {
						//This assumes that in your json payload you sent the sound filename (like sound.caf)
						// and that you've included it in your project directory as a Content Build type.
						var soundObj = MonoTouch.AudioToolbox.SystemSound.FromFile (sound);
						soundObj.PlaySystemSound ();
					}

					//Manually show an alert
					if (!string.IsNullOrEmpty (alert)) {
						UIAlertView avAlert = new UIAlertView ("Notification", alert, null, "OK", null);
						avAlert.Show ();
					}
				}

			}

			//You can also get the custom key/value pairs you may have sent in your aps (outside of the aps payload in the json)
			// This could be something like the ID of a new message that a user has seen, so you'd find the ID here and then skip displaying
			// the usual screen that shows up when the app is started, and go right to viewing the message, or something like that.
			if (options.ContainsKey (new NSString ("customKeyHere"))) {
				launchWithCustomKeyValue = (options [new NSString ("customKeyHere")] as NSString).ToString ();

				//You could do something with your customData that was passed in here
			}
		}

		public override void ReceivedRemoteNotification (UIApplication application, NSDictionary userInfo)
		{
			//This method gets called whenever the app is already running and receives a push notification
			// YOU MUST HANDLE the notifications in this case.  Apple assumes if the app is running, it takes care of everything
			// this includes setting the badge, playing a sound, etc.
			processNotification (userInfo, false);
		}
		// This method is invoked when the application is about to move from active to inactive state.
		// OpenGL applications should use this method to pause.
		public override void OnResignActivation (UIApplication application)
		{
		}
		// This method should be used to release shared resources and it should store the application state.
		// If your application supports background exection this method is called instead of WillTerminate
		// when the user quits.
		public override void DidEnterBackground (UIApplication application)
		{
		}
		// This method is called as part of the transiton from background to active state.
		public override void WillEnterForeground (UIApplication application)
		{
		}
		// This method is called when the application is about to terminate. Save data, if needed.
		public override void WillTerminate (UIApplication application)
		{
		}
	}
}

