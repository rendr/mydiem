using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;
using System.Net;
using System.Text;

//using UA=UrbanAirship;
using MyDiem.Core.SAL;
using MyDiem.Core.BL.Managers;

namespace MyDiem.UI.Touch.Service
{
	public static class PushNotifications
	{
		public static HttpStatusCode UAResponseCode;
		public static string DeviceTOkenString;

		public static event EventHandler Done = delegate{};

		static bool SubscribeInvoked = false;

		public static void Subscribe ()
		{
			if (SubscribeInvoked) {
				return;
			}
			SubscribeInvoked = true;

			UIApplication.SharedApplication.RegisterForRemoteNotificationTypes (
				UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge);
		}

		public static void Unsubscribe ()
		{
			UIApplication.SharedApplication.UnregisterForRemoteNotifications ();
		}

		public static void EnsureDeviceRegistration (NSData deviceToken)
		{

			var str = (NSString)Runtime.GetNSObject (
				                  Messaging.intptr_objc_msgSend (deviceToken.Handle, new Selector (
					                  "description").Handle));
			var deviceTokenString = str.ToString ().Replace ("<", "").Replace (">", "")
	            .Replace (" ", "");
	     
			var token = LoadFromSettings ();
	          
			if (String.IsNullOrEmpty (token) || token != deviceTokenString) {
				Console.WriteLine (String.Format ("EnsureDeviceRegistration: {0}, {1}", 
					deviceTokenString, deviceTokenString.Length));


	                
				var server = @"https://go.urbanairship.com";
				var url = String.Format ("{0}{1}{2}", server, "/api/device_tokens/", 
					                       deviceTokenString);
	                
				Console.WriteLine ("EnsureDeviceRegistration: URL: " + url);
	                     
				var putUrl = new Uri (url);
	                
				var request = (HttpWebRequest)HttpWebRequest.Create (putUrl);
				request.Method = "PUT";
				request.ContentType = "application/json";
				//DEBUG/DEV UA 
//					request.Credentials = new NetworkCredential(@"vyv2pZQWT4yL_tTrtTruNw", @"yH_LNVO3TqyAx9u_yhcn_w");
				request.Credentials = new NetworkCredential (@"pggv0Zt8SVyKYAM7drX9zQ", @"ygKf5fypRIGmOvfyN-3BGg");


				var payload = @"{""alias"": """ + UIDevice.CurrentDevice.Name + @"""}";
	                
				Console.WriteLine ("EnsureDeviceRegistration: Payload: " + payload);
	                
				var byteArray = Encoding.UTF8.GetBytes (payload);
				request.ContentLength = byteArray.Length;
				var dataStream = request.GetRequestStream ();
				dataStream.Write (byteArray, 0, byteArray.Length);
				dataStream.Close ();
	                
				var response = (HttpWebResponse)request.GetResponse ();
	                

				UAResponseCode = response.StatusCode;
				DeviceTOkenString = deviceTokenString;
				Done (null, new EventArgs ());
				Console.WriteLine ("EnsureDeviceRegistration: Status: " +
				response.StatusCode);
						                     
				SaveToSettings (deviceTokenString);

					
			} else {
					
				Console.WriteLine ("EnsureDeviceRegistration: device already registered.");
			}
		}

		private static string LoadFromSettings ()
		{
			var prefs = NSUserDefaults.StandardUserDefaults;
	          
			return prefs.StringForKey ("DeviceToken");
		}

		private static void SaveToSettings (string deviceTokenString)
		{
			var prefs = NSUserDefaults.StandardUserDefaults;
			var nSString = new NSString (deviceTokenString);
			var testSTr = nSString.ToString ();
			prefs ["DeviceToken"] = nSString;
		}
	}
}

