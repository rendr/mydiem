using System;
using RestSharp;
using MyDiem.Core.SAL;

namespace MyDiem.UI.Touch.Service
{
	public class WebAPIService:IWebAPIService
	{
		public event EventHandler Result = delegate{};
		public event EventHandler Error = delegate{};

		private string responseText;

		public string ResponseText {
			get {
				return responseText;
			}
		}

		private string apiErrorMessage;

		public string APIErrorMessage {
			get {
				return apiErrorMessage;
			}
		}

		private string apiErrorURL;

		public string APIErrorURL {
			get {
				return apiErrorURL;
			}
		}

		public WebAPIService ()
		{

		}

		public void Send (string baseURL, string methodURI)
		{
			var client = new RestClient (baseURL);
			var request = new RestRequest (methodURI, Method.GET);



			client.ExecuteAsync (request, response => {
				responseText = response.Content;

				Result (this, new EventArgs ());
			});
		}
	}
}

