﻿using System;
using MonkeyArms;
using MyDiem.UI.Touch.Views.CountiesPicker;
using Views;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.ViewModel;

namespace MyDiem.UI.Touch.Mediators
{
	public class CountiesPickerMediator:Mediator
	{
		[Inject]
		public CountyManager CM;
		[Inject]
		public FilterMenuViewModel FMVM;
		CountiesUIPickerView Picker;

		public CountiesPickerMediator (CountiesUIPickerView picker) : base (picker)
		{
			this.Picker = picker;
		}

		public override void Register ()
		{

			Picker.Counties = CM.Counties;
			FMVM.OpenCountyPicker += HandleOpenCountyPicker;

		}

		public override void Unregister ()
		{
			base.Unregister ();
			FMVM.OpenCountyPicker -= HandleOpenCountyPicker;
		}

		void HandleOpenCountyPicker (object sender, EventArgs e)
		{
			Picker.Open ();
		}
	}
}

