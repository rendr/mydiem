﻿using MonkeyArms;
using MyDiem.UI.Touch.Views.RegionsPicker;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.ViewModel;

namespace MyDiem.UI.Touch.Mediators
{
	public class RegionPickerMediator:Mediator
	{
		[Inject]
		public RegionManager RM;
		[Inject]
		public FilterMenuViewModel FMVM;
		RegionsUIPickerView Picker;

		public RegionPickerMediator (RegionsUIPickerView picker) : base (picker)
		{
			this.Picker = picker;
		}

		public override void Register ()
		{
			this.Picker.Regions = RM.Regions;
			FMVM.OpenRegionPicker += HandleOpenRegionPicker;
		}

		public override void Unregister ()
		{
			base.Unregister ();
			FMVM.OpenRegionPicker -= HandleOpenRegionPicker;
		}

		void HandleOpenRegionPicker (object sender, System.EventArgs e)
		{
			Picker.Open ();
		}
	}
}

