using System;
using System.Diagnostics;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MonkeyArms;

namespace MyDiem.UI.Touch.Util
{
	public static class LocalNotificationUtil
	{
		public static void SetNotification (DateTime targetFireDate, string title, NSDictionary userInfo)
		{

			UILocalNotification notification = new UILocalNotification {
				FireDate = targetFireDate,
				TimeZone = NSTimeZone.LocalTimeZone,
				AlertBody = title,
				RepeatInterval = 0,
				UserInfo = userInfo
			
			};
			Debug.WriteLine ("Setting reminder: " + targetFireDate.ToShortDateString () + ":" + targetFireDate.ToLongTimeString ());
			UIApplication.SharedApplication.ScheduleLocalNotification (notification);
			DI.Get<SubscribableManager> ().BroadcastReminderSet ();
		}

		public static string generateEventIDString (CalendarEvent targetEvent)
		{
			 
			return targetEvent.CalendarID + ":" + targetEvent.StartDateTime.ToShortDateString () + ":" + targetEvent.StartDateTime.ToShortTimeString () + ":" + targetEvent.EndDateTime.ToShortDateString () + ":" + targetEvent.EndDateTime.ToShortTimeString ();
		}

		public static bool IsReminderSet (string id, string offset)
		{
			foreach (UILocalNotification notification in UIApplication.SharedApplication.ScheduledLocalNotifications) {
				var storedID = notification.UserInfo.ValueForKey (new MonoTouch.Foundation.NSString ("eventID"));
				var storedOffset = notification.UserInfo.ValueForKey (new MonoTouch.Foundation.NSString ("optionID"));

					
				if (storedOffset.ToString () == offset && storedID.ToString () == id) {
					return true;
				}



			}
		

			return false;
		}

		public static string GetOptionLabelByEventID (string id)
		{
			foreach (UILocalNotification notification in UIApplication.SharedApplication.ScheduledLocalNotifications) {

				if ((notification.UserInfo.ValueForKey (new MonoTouch.Foundation.NSString ("eventID")) as NSString).ToString () == new MonoTouch.Foundation.NSString (id).ToString ()) {
					var optionLabel = notification.UserInfo.ValueForKey (new MonoTouch.Foundation.NSString ("optionLabel"));
					if (optionLabel != null) {
						return optionLabel.ToString ();
					}
				}
			}

			return "None";

		}

		public static void RemoveNotificationByEventID (string id)
		{
			foreach (UILocalNotification notification in UIApplication.SharedApplication.ScheduledLocalNotifications) {

				if ((notification.UserInfo.ValueForKey (new MonoTouch.Foundation.NSString ("eventID")) as NSString).ToString () == new MonoTouch.Foundation.NSString (id).ToString ()) {
					UIApplication.SharedApplication.CancelLocalNotification (notification);
				}
			}
		}

		public static bool IsThereReminderForEvent (CalendarEvent calEvent)
		{
			string id = generateEventIDString (calEvent);
			foreach (UILocalNotification notification in UIApplication.SharedApplication.ScheduledLocalNotifications) {

				if ((notification.UserInfo.ValueForKey (new MonoTouch.Foundation.NSString ("eventID")) as NSString).ToString () == id) {
					return true;
				}
			}

			return false;
		}
	}
}

