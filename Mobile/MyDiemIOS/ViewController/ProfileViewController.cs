using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MyDiem.Core.BL.Managers;
using MyDiem.UI.Touch.Views.Global;
using MyDiem.Core.BL;
using MyDiem.Core.Interfaces;
using MonkeyArms;

namespace MyDiem.UI.Touch.ViewController
{
	public partial class ProfileViewController : BaseUserFormViewController, IProfileView
	{
		protected UIButton CancelButton;
		protected UIButton DoneButton;
		UIButton SaveButton;

		public ProfileViewController () : base ("SettingsScreen", null)
		{
		}

		#region IProfileView implementation

		public event EventHandler Logout = delegate {};
		public event EventHandler UpdateSubmitted = delegate {};

		public void SetUserData (User appUser)
		{
			InitEntryVMs (appUser, false, false);
			AddProfileTable ();
			UpdateLayout ();
		}

		public void ShowUpdateSuccess ()
		{
			HideNetworkActivityIndicator ();
		}

		public void GoLogout ()
		{
			NavigationController.PushViewController (new LoginViewController (), true);
		}

		public string SubmittedPassword {
			get {
				return PasswordVM.Value;

			}
		}

		public string SubmittedFirstName {
			get {
				return FirstNameVM.Value;
			}
		}

		public string SubmittedLastName {
			get {
				return LastNameVM.Value;
			}
		}

		public string SubmittedEmail {
			get {
				return EmailVM.Value;
			}
		}

		public string SubmittedRegion {
			get {
				return DI.Get<RegionManager> ().GetRegionID (RegionVM.Value).ToString ();
			}
		}

		public string SubmittedCounty {
			get {
				return DI.Get<CountyManager> ().GetCountyID (CountyVM.Value).ToString ();
			}
		}

		public string SubmittedZip {
			get {
				return ZipVM.Value.ToString ();
			}
		}

		public List<Region> Regions {
			set {

			}
		}

		public List<County> Counties {
			set {

			}
		}

		#endregion

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			CreateSlideMenu ();
			CreateFormHolder ();
			AddHeader ("My Profile", null, ContentView);
			var logoutButton = CreateWhiteHeaderButton ("right", "Logout", ContentView);
			logoutButton.TouchUpInside += (sender, e) => Logout (this, EventArgs.Empty);
			CreateMenuButton ();


			AddRegionsPicker ();
			AddCountiesPicker ();
			AddEventListeners ();

			SaveButton = CreateGreenButton (40, 0, "Save", FormHolder);
			SaveButton.TouchUpInside += (sender, e) => {
				GenericTextCell.CloseAllKeyboards ();
				List<GenericTableCellViewModel> entries = tableSource.GetUpdatedTableItems ();

				if (!ValidateForm ()) {
					return;
				}
				UpdateSubmitted (this, EventArgs.Empty);
				ShowNetworkAcitivyIndicator ();
			};

			UpdateLayout ();

		}

		override protected void UpdateLayout ()
		{

			if (ProfileTable != null) {
				ProfileTable.Frame = new RectangleF (20, 74, 280, tableSource.RowHeight * entries.Count);
				SaveButton.Frame = new RectangleF (SaveButton.Frame.X, ProfileTable.Frame.Y + ProfileTable.Frame.Height + 30, SaveButton.Frame.Width, SaveButton.Frame.Height);
			}

			FormHolder.ContentSize = new Size (Convert.ToInt16 (View.Bounds.Width), Convert.ToInt16 (SaveButton.Frame.Y + SaveButton.Frame.Height + 20));
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			SendGATrackingPoint ("profile");
			DI.RequestMediator (this);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			DI.DestroyMediator (this);
		}
	}
}

