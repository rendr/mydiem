using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MyDiem.Core.BL;
using MyDiem.UI.Touch.Views.Global;
using MyDiem.UI.Touch.Views.Reminders;

namespace MyDiem.UI.Touch.ViewController
{
	public partial class EventReminderViewController : BaseCalendarViewController
	{
		protected CalendarEvent TargetEvent;

		public EventReminderViewController (CalendarEvent targetEvent) : base ("EventReminderScreen", null)
		{
			this.TargetEvent = targetEvent;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			CreateSlideMenu ();
			CreateContentView ();
			CreateThatchBackground (ContentView);
			CreateHeader ();
			CreateOptionsTable ();


			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			SendGATrackingPoint ("set-event-reminder");
		}

		protected void CreateHeader ()
		{
			AddHeader ("Reminder", null, ContentView);
			UIButton backButton = CreateHeaderBackButton ("Back", ContentView);
			backButton.TouchUpInside += (sender, e) => {
				NavigationController.PopViewControllerAnimated (true);
			};
		}

		protected UITableView CreateOptionsTable ()
		{
			List<ReminderOptionViewModel> entries = new List<ReminderOptionViewModel> ();
			entries.Add (new ReminderOptionViewModel (TargetEvent, -1, "None", true));
			entries.Add (new ReminderOptionViewModel (TargetEvent, 5, "5 minutes before", false));
			entries.Add (new ReminderOptionViewModel (TargetEvent, 30, "30 minutes before", false));
			entries.Add (new ReminderOptionViewModel (TargetEvent, 60, "1 hour before", false));
			entries.Add (new ReminderOptionViewModel (TargetEvent, 1440, "1 day before", false));
			entries.Add (new ReminderOptionViewModel (TargetEvent, 2880, "2 days before", false));
			entries.Add (new ReminderOptionViewModel (TargetEvent, 10080, "1 week before", false));


			int rowHeight = 48;
			UITableView detailsTable = new UITableView (new RectangleF (20, 75, 280, rowHeight * entries.Count));
			detailsTable.ScrollEnabled = false;
			detailsTable.RowHeight = rowHeight;
			var eventDetailsTableSource = new RemindersTableSource (entries);

			detailsTable.Source = eventDetailsTableSource;
			View.AddSubview (detailsTable);
			View.InsertSubviewBelow (new DropShadowView (detailsTable.Frame), detailsTable);
			return detailsTable;
		}
	}
}

