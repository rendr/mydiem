using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MyDiem.Core.BL.Managers;
using MyDiem.UI.Touch.Views.Global;
using MyDiem.Core.ViewModel;
using MyDiem.Core.Interfaces;
using MonkeyArms;

namespace MyDiem.UI.Touch.ViewController
{
	public partial class AboutViewController : BaseCalendarViewController, IAboutView
	{
		//protected UIButton MenuButton;
		protected UIWebView DescriptionLabel;
		protected UILabel TitleLabel;

		public AboutViewController () : base ("AboutScreen", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			CreateSlideMenu ();
			CreateContentView ();
			CreateThatchBackground (ContentView);

			AddHeader (ResourceManager.GetString ("about"), null, ContentView);
			CreateMenuButton ();

//			CreateTitle();
			CreateDescriptionText ();

		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			SendGATrackingPoint ("about");
		}

		protected void CreateTitle ()
		{
			TitleLabel = new UILabel (new RectangleF (12, 54, 308, 30)) {
				Font = UIFont.FromName ("OpenSans-Bold", 18),
				TextColor = UIColor.FromRGB (67, 67, 67),
				TextAlignment = UITextAlignment.Left,
				BackgroundColor = UIColor.Clear,
				Text = ResourceManager.GetString ("contact"),
				LineBreakMode = UILineBreakMode.WordWrap,
				Lines = 0
			};
			TitleLabel.SizeToFit ();
			ContentView.AddSubview (TitleLabel);
		}

		protected void CreateDescriptionText ()
		{

			DescriptionLabel = new UIWebView (new RectangleF (0, ScreenHeaderView.HeaderHeight, View.Frame.Width, ScreenHeight - ScreenHeaderView.HeaderHeight));
			DescriptionLabel.SizeToFit ();
			ContentView.AddSubview (DescriptionLabel);

			DescriptionLabel.Opaque = false;
			DescriptionLabel.BackgroundColor = UIColor.Clear;
			DescriptionLabel.LoadHtmlString (ResourceManager.GetString ("aboutText") + "<p>Build Version " + NSBundle.MainBundle.ObjectForInfoDictionary ("CFBundleVersion").ToString () + "</p>", NSUrl.FromString ("http://localhost"));

			DescriptionLabel.ShouldStartLoad = myHandler;
		}

		bool myHandler (UIWebView webView, NSUrlRequest request, UIWebViewNavigationType navType)
		{
			// Determine what to do here based on the @request and @navType
			if (navType == UIWebViewNavigationType.LinkClicked) {
				UIApplication.SharedApplication.OpenUrl (request.Url);
				return false;
			}

			return true;
		}
	}
}

