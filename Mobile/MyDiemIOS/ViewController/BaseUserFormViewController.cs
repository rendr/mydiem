using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MyDiem.UI.Touch.Views.RegionsPicker;
using MyDiem.UI.Touch.Views.CountiesPicker;
using MyDiem.UI.Touch.Views.Global;
using System.Collections.Generic;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.BL;
using Views.Tables;
using MonkeyArms;

namespace MyDiem.UI.Touch.ViewController
{
	public class BaseUserFormViewController:BaseCalendarViewController
	{
		protected bool RegionsOpen = false;
		protected bool CountiesOpen = false;
		protected UIScrollView FormHolder;
		protected bool ContentShrunk = false;
		NSObject _keyboardObserverWillShow, _keyboardObserverWillHide;
		protected List<GenericTableCellViewModel> entries;
		protected GenericTableCellViewModel FirstNameVM, LastNameVM, RegionEnabledVM, RegionVM, CountyEnabledVM, CountyVM, EmailVM, PasswordVM, PasswordVerifyVM, ZipVM;
		protected MyDiemUITableView ProfileTable;
		protected GenericTableSource tableSource;

		public BaseUserFormViewController (string ScreenClassName, NSBundle Bundle) : base (ScreenClassName, Bundle)
		{
		}

		protected void InitEntryVMs (User appUser, bool enableEmail, bool includeZip = false)
		{
			entries = new List<GenericTableCellViewModel> ();
			//first name
			FirstNameVM = new GenericTableCellViewModel ("First Name", "John", appUser.Fname);
			entries.Add (FirstNameVM);
			
			//last name
			LastNameVM = new GenericTableCellViewModel ("Last Name", "Doe", appUser.Lname);
			entries.Add (LastNameVM);
			
			//region
			bool privateEnabled = (appUser.RegionID > 0);
			RegionEnabledVM = new GenericTableCellViewModel ("Private Schools", "", privateEnabled.ToString (), false, false, false, true);
			entries.Add (RegionEnabledVM);
			RegionEnabledVM.ValueChanged += (object sender, EventArgs e) => {
				if (RegionEnabledVM.Value == "True") {
					CountyEnabledVM.Value = "False";
				}
			};
			
			
			
			Region region = DI.Get<RegionManager> ().GetRegionByID (appUser.RegionID);
			string regionTitle = null;
			if (region != null) {
				regionTitle = region.Title;
			}
			RegionVM = new GenericTableCellViewModel ("Region", "Select Region", regionTitle, false, true);

		
			if (appUser.RegionID > 0) {
				entries.Add (RegionVM);
			}
			
			//county
			bool publicEnabled = (appUser.CountyID > 0);
			CountyEnabledVM = new GenericTableCellViewModel ("Public Schools", "", publicEnabled.ToString (), false, false, false, true);
			CountyEnabledVM.ValueChanged += (object sender, EventArgs e) => {
				if (CountyEnabledVM.Value == "True") {
					RegionEnabledVM.Value = "False";
				}
			};

			entries.Add (CountyEnabledVM);
			
			
			
			
			County county = DI.Get<CountyManager> ().GetCountyByID (appUser.CountyID);
			string countyTitle = null;
			if (county != null) {
				countyTitle = county.Title;
			}
			CountyVM = new GenericTableCellViewModel ("County", "Select County", countyTitle, false, false, true);
			
			if (appUser.CountyID > 0) {
				entries.Add (CountyVM);
			}

			//zip
			if (includeZip) {
				ZipVM = new GenericTableCellViewModel ("Zip", "12345", null, false, false, false, false);
				entries.Add (ZipVM);
			}

			//email - do not allow editing of email
			if (!enableEmail) {
				EmailVM = new GenericTableCellViewModel (appUser.Email, "", "", false, false, false, false, enableEmail);
			} else {
				EmailVM = new GenericTableCellViewModel ("Email", "example@gmail.com", "", false, false, false, false, enableEmail);
			}
			entries.Add (EmailVM);
			
			//password
			PasswordVM = new GenericTableCellViewModel ("Password", "Secret", null, true, false, false);
			entries.Add (PasswordVM);
			
			//password verifiy
			PasswordVerifyVM = new GenericTableCellViewModel ("Verify", "Secret", null, true, false, false);
			entries.Add (PasswordVerifyVM);
		}

		void HandleCellBeganEditing (object sender, EventArgs e)
		{
			int rowIndex = (sender as BaseGenericCell).RowIndex; 
			if (entries [rowIndex].IsRegion) {
				OpenRegions ();
			} else if (entries [rowIndex].IsCounty) {
				OpenCounties ();
			} else if (entries [rowIndex].IsOnOffSwitch) {
				UpdateCountyRegionStates ();
				tableSource.CellBeganEditing -= HandleCellBeganEditing;
				tableSource = new GenericTableSource (entries, true);
				tableSource.CellBeganEditing += HandleCellBeganEditing;
				ProfileTable.Source = tableSource;
				UpdateLayout ();
				ProfileTable.ReloadData ();
			} else {
				FormHolder.ScrollRectToVisible (new RectangleF (0, 10000, View.Bounds.Width, View.Frame.Height), true);
			}
		}

		protected virtual void UpdateLayout ()
		{

		}

		protected void UpdateCountyRegionStates ()
		{

			if (CountyEnabledVM.Value == "True") {

				if (entries.IndexOf (CountyVM) == -1) {
					var enabledIndex = entries.IndexOf (CountyEnabledVM);
					CountyVM.Value = DI.Get<CountyManager> ().Counties [0].Title;
					entries.Insert (enabledIndex + 1, CountyVM);
				}
			} else {
				if (entries.IndexOf (CountyVM) != -1) {
					CountyVM.Value = "-1";
					entries.RemoveAt (entries.IndexOf (CountyVM));
				}
			}
			
			if (RegionEnabledVM.Value == "True") {

				if (entries.IndexOf (RegionVM) == -1) {
					var enabledIndex = entries.IndexOf (RegionEnabledVM);
					RegionVM.Value = DI.Get<RegionManager> ().Regions [0].Title;
					entries.Insert (enabledIndex + 1, RegionVM);
				}
			} else {
				if (entries.IndexOf (RegionVM) != -1) {
					RegionVM.Value = "-1";
					entries.RemoveAt (entries.IndexOf (RegionVM));
				}
			}
			
		}

		protected void AddProfileTable ()
		{
			
			tableSource = new GenericTableSource (entries, true);
			
			ProfileTable = new MyDiemUITableView (new RectangleF (20, 74, 280, tableSource.RowHeight * entries.Count));
			ProfileTable.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
			ProfileTable.ScrollEnabled = false;
			
			
			ProfileTable.Source = tableSource;
			FormHolder.AddSubview (ProfileTable);
			tableSource.CellBeganEditing += HandleCellBeganEditing;
		}

		protected void AddEventListeners ()
		{

			RegisterForKeyboardNotifications ();



			
			RegionsPicker.DoneClicked += (sender, e) => {
				CloseRegions ();
				
			};
			
			RegionsPicker.RegionSelected += (sender, e) => {
				RegionVM.Value = RegionsPicker.SelectedRegion.Title;
				ProfileTable.ReloadData ();
			};
			
			
			CountiesPicker.DoneClicked += (sender, e) => {
				CloseCounties ();
				
			};
			
			CountiesPicker.CountySelected += (sender, e) => {
				CountyVM.Value = CountiesPicker.SelectedCounty.Title;
				ProfileTable.ReloadData ();
			};
		}

		protected bool ValidateForm ()
		{
			UpdateCountyRegionStates ();
			if (RegionEnabledVM.Value == "False" && CountyEnabledVM.Value == "False") {
				AlertOK ("Update Error", "Please enable Private or Public schools");
				return false;
			}
			
			if (PasswordVM.Value != null && PasswordVM.Value != "" && PasswordVM.Value != PasswordVerifyVM.Value) {
				AlertOK ("Update Error", "Password values must match");
				return false;
			}
			
			return true;
		}

		protected void AlertOK (string title, string message)
		{
			using (var alert = new UIAlertView (title, message, null, "OK", null)) {
				alert.Show ();	
			}
		}

		protected void RemoveEventListeners ()
		{
			UnregisterKeyboardNotifications ();
		}

		protected void CreateFormHolder ()
		{
		
			CreateContentView ();
			CreateThatchBackground (ContentView);

			FormHolder = new UIScrollView (View.Bounds);
			
			
			FormHolder.ShowsVerticalScrollIndicator = true;
			FormHolder.ScrollEnabled = true;
			
			
			ContentView.AddSubview (FormHolder);
		}

		protected void OpenRegions ()
		{
			if (!RegionsOpen) {
				OpenPicker (RegionsPicker);
				RegionsOpen = true;
				CloseCounties ();

			}
		}

		protected void CloseRegions ()
		{
			if (RegionsOpen) {
				ClosePicker (RegionsPicker);
				RegionsOpen = false;

			}
		}

		protected void OpenCounties ()
		{
			if (!CountiesOpen) {
				OpenPicker (CountiesPicker);
				CountiesOpen = true;
				CloseRegions ();
			}
		}

		protected void CloseCounties ()
		{
			if (CountiesOpen) {
				ClosePicker (CountiesPicker);
				CountiesOpen = false;

			}
		}

		protected virtual void RegisterForKeyboardNotifications ()
		{
			_keyboardObserverWillShow = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, KeyboardWillShowNotification);
			_keyboardObserverWillHide = NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, KeyboardWillHideNotification);
		}

		protected virtual void UnregisterKeyboardNotifications ()
		{
			try {
				NSNotificationCenter.DefaultCenter.RemoveObserver (_keyboardObserverWillShow);
				NSNotificationCenter.DefaultCenter.RemoveObserver (_keyboardObserverWillHide);
			} catch (Exception e) {

			}
		}

		protected virtual void KeyboardWillShowNotification (NSNotification notification)
		{

			RectangleF keyboardBounds = UIKeyboard.FrameBeginFromNotification (notification);
			FormHolder.Frame = new RectangleF (0, 0, View.Bounds.Width, View.Bounds.Height - keyboardBounds.Size.Height);

		}

		protected virtual void KeyboardWillHideNotification (NSNotification notification)
		{

			
			FormHolder.Frame = new RectangleF (0, 0, View.Bounds.Width, View.Bounds.Height);

		}
	}
}

