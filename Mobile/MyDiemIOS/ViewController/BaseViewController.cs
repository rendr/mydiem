using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MyDiem.UI.Touch.Views.RegionsPicker;
using GoogleAnalytics.iOS;

namespace MyDiem.UI.Touch
{
	public class BaseViewController:UIViewController
	{
		protected ScreenHeaderView Header;

		public static bool IsIOS7 {
			get {

				var systemVersion = UIDevice.CurrentDevice.SystemVersion;
				var versionnNumber = float.Parse (systemVersion.ToCharArray () [0].ToString ());

				return (versionnNumber >= 7);
				
			}
		}

		public BaseViewController (string ScreenClassName, NSBundle Bundle) : base (null, null)
		{

		}

		protected void SendGATrackingPoint (string screenName)
		{
			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, screenName);
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateAppView ().Build ());
		}

		public override void LoadView ()
		{
			base.LoadView ();
//			FA.FlurryAnalytics.LogAllPageViews (this);

		
		}

		protected UIButton CreateImageButton (RectangleF bounds, string normalImagePath, string highlightedImagePath, UIView parentView = null)
		{
			UIButton NewButton = new UIButton (bounds);
			NewButton.SetImage (UIImage.FromBundle (normalImagePath), UIControlState.Normal);
			NewButton.SetImage (UIImage.FromBundle (highlightedImagePath), UIControlState.Highlighted);
			if (parentView == null) {
				View.AddSubview (NewButton);
			} else {
				parentView.AddSubview (NewButton);
			}
			return NewButton;
		}

		protected void MoveUIUp (int amount)
		{

			UIView.BeginAnimations ("moveLoginUIUp");
			UIView.SetAnimationDuration (0.4);
			UIView.SetAnimationDelay (0.1);
			UIView.SetAnimationCurve (UIViewAnimationCurve.EaseInOut);
			View.Frame = new RectangleF (0, -1 * amount, View.Bounds.Width, ScreenResolutionUtil.ScreenHeight);
			UIView.CommitAnimations ();
		}

		protected void MoveUIDown ()
		{
			UIView.BeginAnimations ("moveLoginUIUp");
			UIView.SetAnimationDuration (0.4);
			UIView.SetAnimationDelay (0.1);
			UIView.SetAnimationCurve (UIViewAnimationCurve.EaseInOut);
			View.Frame = new RectangleF (0, 0, View.Bounds.Width, ScreenResolutionUtil.ScreenHeight);
			UIView.CommitAnimations ();
		}

		protected UIButton CreateWhiteHeaderButton (string side, string title, UIView parentView = null)
		{
			int x;
			if (side == "left") {
				x = 10;
			} else {
				x = 250;
			}
			var headerButtonHeight = 25;
			UIButton newButton = new UIButton (new RectangleF (x, ScreenHeaderView.HeaderAlignY - headerButtonHeight / 2, 65, headerButtonHeight));
			newButton.BackgroundColor = UIColor.White;
			newButton.Layer.CornerRadius = 5;
			newButton.SetTitleColor (UIColor.Orange, UIControlState.Normal);

			newButton.SetTitle (title, UIControlState.Normal);
			newButton.TitleLabel.Font = UIFont.FromName ("Open Sans", 12);
			if (parentView == null) {
				View.AddSubview (newButton);
			} else {
				parentView.AddSubview (newButton);
			}
			return newButton;
		}

		protected UIButton CreateHeaderBackButton (string title, UIView parentView = null, string alignment = null)
		{
			UIButton newButton = new UIButton (new RectangleF (10, ScreenHeaderView.HeaderAlignY - 15, 88, 30));
			newButton.SetBackgroundImage (UIImage.FromBundle ("/Images/OrangeBackButton/OrangeBackButtonBackground"), UIControlState.Normal);
			newButton.SetTitleColor (UIColor.Orange, UIControlState.Normal);
			newButton.SetTitle (title, UIControlState.Normal);
			newButton.TitleLabel.Font = UIFont.FromName ("Open Sans", 12);
			RectangleF titleRect = newButton.TitleLabel.Frame;
			newButton.TitleLabel.Frame = new RectangleF (titleRect.X + 5, titleRect.Y, titleRect.Width, titleRect.Height);
			if (parentView == null) {
				View.AddSubview (newButton);
			} else {
				parentView.AddSubview (newButton);
			}
			return newButton;
		}

		protected UIButton CreateGreenButton (int x, int y, string title, UIView parentView = null)
		{
			UIButton newButton = new UIButton (new RectangleF (x, y, 242, 52));
			newButton.SetBackgroundImage (UIImage.FromBundle ("/Images/BaseScreen/GreenButtonBackground"), UIControlState.Normal);
			newButton.SetTitleColor (UIColor.White, UIControlState.Normal);
			newButton.TitleLabel.Font = UIFont.FromName ("OpenSans-Bold", 18);
			newButton.SetTitle (title, UIControlState.Normal);

			if (parentView != null) {
				parentView.AddSubview (newButton);
			} else {
				View.AddSubview (newButton);
			}

			return newButton;	
		}

		protected void AddHeader (string headerText, UIFont font = null, UIView parentView = null, string alignment = null)
		{
			if (font == null) {
				font = UIFont.FromName ("OpenSans", 20);
			}

			Header = new ScreenHeaderView (headerText, font, alignment);

			if (parentView == null) {
				View.AddSubview (Header);
			} else {
				parentView.AddSubview (Header);
			}
		}

		protected void CreateThatchBackground (UIView parentView = null)
		{
			CreateBackground ("/Images/BaseScreen/ThatchBackground", parentView);

		}

		protected void CreateBackground (string imagePath, UIView parentView = null)
		{
			UIImageView background = new UIImageView (new RectangleF (new PointF (0, 0), View.Frame.Size));//ScreenResolutionUtil.ScreenHeight
			background.Image = ScreenResolutionUtil.FromBundle16x9 (imagePath);

			if (parentView == null) {
				View.AddSubview (background);
			} else {
				parentView.AddSubview (background);
			}
		}

		protected void ShowNetworkAcitivyIndicator ()
		{
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;	
		}

		protected void HideNetworkActivityIndicator ()
		{
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;	
		}

		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{

			return (toInterfaceOrientation == UIInterfaceOrientation.Portrait || toInterfaceOrientation == UIInterfaceOrientation.PortraitUpsideDown);
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
	}
}

