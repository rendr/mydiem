using System;
using System.Drawing;
using MonoTouch.UIKit;
using MyDiem.Core.BL;
using MyDiem.Core.ViewModel;
using MyDiem.UI.Touch.Views.SubscribableList;
using MyDiem.UI.Touch.Views.Global;
using MyDiem.Core.BL.Managers;
using Views.Tables;
using MyDiem.UI.Touch.Views.SlideMenu;
using MyDiem.Core.Interfaces;
using MonkeyArms;

namespace MyDiem.UI.Touch.ViewController
{
	public partial class SubscribableListViewController : BaseCalendarViewController, ISubscribableListView
	{
		string ScreenTitle;
		MyDiemUITableView SubscribablesTable;
		protected string SelectedSubscribableType = "Any";
		SubscribableSettingsViewController SettingsViewController;
		SchoolGroup MainCategory;
		UIAlertView PasswordAlertView;
		protected UISearchBar SearchBar;

		public SubscribableListViewController (SchoolGroup cat, string title) : base ("CalendarListScreen", null)
		{
			this.MainCategory = cat;
			ScreenTitle = title;



		}

		public event EventHandler FilterSubscribables = delegate {};
		public event EventHandler ResetSubscribables = delegate {};
		public event EventHandler SubscribableSelected = delegate {};
		public event EventHandler PasswordSubmitted = delegate {};
		public event EventHandler EmailSubmitted = delegate {};

		public void UpdateSubscribablesList (System.Collections.Generic.List<SchoolGroup> schoolGroups, int[] userCalendarIDs, int[] userDirectoryIDs)
		{
			if (SubscribablesTable.Source == null) {
				var source = new SubscribableListTableViewSource ();
				source.SubscribableSelected += (Subscribable s) => {
					SelectedSubscribable = s;
					SubscribableSelected (this, EventArgs.Empty);
				};
				SubscribablesTable.Source = source;

			}
			((SubscribableListTableViewSource)SubscribablesTable.Source).Categories = schoolGroups;
			SubscribablesTable.ReloadData ();
		}

		public void PromptForSubscribablePassword ()
		{
			PasswordAlertView = new UIAlertView (){ Title = "Enter Password" };
			PasswordAlertView.AlertViewStyle = UIAlertViewStyle.SecureTextInput;
			PasswordAlertView.AddButton ("Cancel");
			PasswordAlertView.AddButton ("Ok");
			PasswordAlertView.Clicked += PasswordAlertButtonClicked;
			PasswordAlertView.Show ();
		}

		public void PromptForSubscribablePurchase (string sku)
		{

		}

		public void ShowWhiteListPrompt ()
		{

		}

		public void SubscribablePasswordFailed ()
		{
			UIAlertView alert = new UIAlertView{ Title = "Error", Message = "Sorry, the password you entered is incorrect" };
			alert.AddButton ("Ok");
			alert.Show ();
		}

		public void HandleSubscribableEmailFailed ()
		{
			UIAlertView alert = new UIAlertView{ Title = "Error", Message = "Sorry, the email you entered is incorrect" };
			alert.AddButton ("Ok");
			alert.Show ();
		}

		public void HandleSubscribableSelected (Subscribable subscribable, Subscription subscription)
		{
			NavigationController.PushViewController (new SubscribableSettingsViewController (subscribable, subscription), true);
		}

		public void ShowLoadingPrompt ()
		{
			this.ShowNetworkAcitivyIndicator ();
		}

		public string SearchPlaceHolderText {
			get {
				return SearchBar.Placeholder;
			}
			set {
				SearchBar.Placeholder = value;
			}
		}

		public string SearchQuery {
			get {
				return SearchBar.Text.ToLower ();
			}
		}

		public string SubmittedPassword {
			get;
			private set;
		}

		public string SubmittedEmail {
			get;
			private set;
		}

		public Subscribable SelectedSubscribable {
			get;
			set;
		}

		public override void ViewDidLoad ()
		{
			this.NavigationController.SetNavigationBarHidden (true, false);
			base.ViewDidLoad ();
			BuildUI ();


		}

		protected void AddEventListeners ()
		{

			RegionsPicker.DoneClicked += HandleRegionSelected;
			CountiesPicker.DoneClicked += HandleCountySelected;
			TypePicker.DoneClicked += HandleTypeSelected;

		}

		protected void RemoveEventListeners ()
		{

			RegionsPicker.DoneClicked -= HandleRegionSelected;
			CountiesPicker.DoneClicked -= HandleCountySelected;
			TypePicker.DoneClicked -= HandleTypeSelected;

		}

		void HandleCategoriesSelected (object sender, EventArgs e)
		{
//			SubscribableListViewModel.SelectedCategories = FilterMenuViewModel.SelectedCategories;
//			SubscribableListViewModel.ApplyFilters ();
		}

		protected void HandleTypeSelected (object sender, EventArgs e)
		{

			ClosePicker (TypePicker);
		}

		protected void HandleOpenTypePicker (object sender, EventArgs e)
		{
			ClosePicker (CountiesPicker);
			ClosePicker (RegionsPicker);
			OpenPicker (TypePicker);
		}

		void HandleCountySelected (object sender, EventArgs e)
		{

			ClosePicker (CountiesPicker);
		}

		void HandleRegionSelected (object sender, EventArgs e)
		{

			ClosePicker (RegionsPicker);

		}

		void HandleOpenRegionPicker (object sender, EventArgs e)
		{
			ClosePicker (TypePicker);
			ClosePicker (CountiesPicker);
			OpenPicker (RegionsPicker);
		}

		void HandleOpenCountyPicker (object sender, EventArgs e)
		{
			ClosePicker (TypePicker);
			ClosePicker (RegionsPicker);
			OpenPicker (CountiesPicker);
		}

		protected void HandlePromptForSubscribableEmail (object sender, EventArgs e)
		{
			var alert = new UIAlertView ("Restricted", "Access is not authorized. Please contact the school for access.", null, "Ok");
			alert.Show ();
		}

		void PasswordAlertButtonClicked (object sender, UIButtonEventArgs e)
		{
			if (e.ButtonIndex == 1) {
				this.SubmittedPassword = PasswordAlertView.GetTextField (0).Text;
				PasswordSubmitted (this, EventArgs.Empty);
			}
		}

		protected void CreateSearchBar ()
		{
			ContentView.AddSubview (new DropShadowView (new RectangleF (0, 40, View.Bounds.Width, 48)));

			SearchBar = new UISearchBar (new RectangleF (0, ScreenHeaderView.HeaderHeight, View.Bounds.Width, 46)) {
				BackgroundImage = UIImage.FromBundle ("/Images/SubscribableList/SearchBarBackground"),
				ClipsToBounds = false


			};

			SearchBar.ShouldBeginEditing = delegate {
				SearchBar.SetShowsCancelButton (true, true);
				return true;
			};

			SearchBar.TextChanged += (object sender, UISearchBarTextChangedEventArgs e) => {
				FilterSubscribables (this, EventArgs.Empty);
			};

			SearchBar.SearchButtonClicked += (sender, e) => {
				SearchBar.SetShowsCancelButton (false, true);
				FilterSubscribables (this, EventArgs.Empty);
				SearchBar.ResignFirstResponder ();
			};

			SearchBar.ShouldEndEditing = delegate {
				SearchBar.SetShowsCancelButton (false, true);
				SearchBar.ResignFirstResponder ();
				return true;
			};

			SearchBar.CancelButtonClicked += (sender, e) => {
				SearchBar.SetShowsCancelButton (false, true);
				ResetSubscribables (this, EventArgs.Empty);
				SearchBar.ResignFirstResponder ();
			};

			ContentView.AddSubview (SearchBar);
		}

		protected void BuildUI ()
		{
			CreateSlideMenu ();
			FilterMenu = CreateFilterMenu<SubscribableFilterMenu> ();
			CreateContentView ();
			CreateSearchBar ();
			CreateThatchBackground (ContentView);
			CreateCalendarsTable ();


			AddHeader (ScreenTitle, null, ContentView);

			CreateMenuButton ();
			if (ScreenTitle != "My Subscribed Calendars") {
				CreateFilterButton ();
			}

			AddRegionsPicker ();
			AddCountiesPicker ();
			AddTypePicker ();

			ContentView.BringSubviewToFront (SearchBar);
		}

		protected void CreateCalendarsTable ()
		{
			var tabelTopEdge = SearchBar.Frame.Y + SearchBar.Frame.Height;
			SubscribablesTable = new MyDiemUITableView (new RectangleF (0, tabelTopEdge, View.Bounds.Width, View.Frame.Height - tabelTopEdge), UITableViewStyle.Grouped);
			if (BaseViewController.IsIOS7) {
				SubscribablesTable.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
			}
			SubscribablesTable.BackgroundColor = UIColor.Clear;
			SubscribablesTable.BackgroundView = null;
			ContentView.AddSubview (SubscribablesTable);
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			if (SettingsViewController != null && SettingsViewController.SubscriptionEnabled) {
				foreach (var schoolGroup in MainCategory.SubCategories) {
					if (schoolGroup.Subscribables.IndexOf (SettingsViewController.SelectedSubscribable) != -1) {
						schoolGroup.Subscribables.Remove (SettingsViewController.SelectedSubscribable);

						break;
					}
				}
			}
			SettingsViewController = null;
			SendGATrackingPoint ("subscribables-list");
			DI.RequestMediator (this);
			AddEventListeners ();
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			DI.DestroyMediator (this);
			RemoveEventListeners ();
		}
	}
}

