using System;
using System.Drawing;
using MonoTouch.UIKit;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.SAL;
using MyDiem.Core.Interfaces;
using MonkeyArms;

namespace MyDiem.UI.Touch.ViewController
{
	public partial class LoginViewController : BaseViewController,ILoginView
	{
		protected RegisterViewController RegisterScreen;
		protected UITextField EmailTextField;
		protected UITextField PasswordTextField;
		protected UIButton LoginButton;
		protected UIButton RegisterButton;
		//		protected CalendarMonthViewController CalendarMonthScreen;
		public LoginViewController () : base ("LoginScreen", null)
		{
		}

		public override void ViewDidLoad ()
		{
			this.NavigationController.SetNavigationBarHidden (true, true);
			base.ViewDidLoad ();
			InitUI ();
			AddEventListeners ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			DI.RequestMediator (this);
		}

		public event EventHandler LoginSubmitted = delegate {};

		public void ShowBadPasswordPrompt ()
		{
			using (var alert = new UIAlertView ("Authentication Error", "Please check your email and password.", null, "OK", null)) {
				alert.Show ();	
			}
			EnableControls ();
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
		}

		public void DisableUIAndShowNetworkIndicator ()
		{
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
			DisableControls ();
		}

		public string SubmittedPassword {
			get {
				return PasswordTextField.Text;
			}
		}

		public string SubmittedEmail {
			get {
				return EmailTextField.Text;
			}
		}

		void AddEventListeners ()
		{
			BaseAPIServiceDelegate.ServiceError += (sender, e) => {
				InvokeOnMainThread (EnableControls);
			};
			this.LoginButton.TouchUpInside += (sender, e) => {
				Submit ();
			};
			this.RegisterButton.TouchUpInside += (sender, e) => {
				if (this.RegisterScreen == null) {
					this.RegisterScreen = new RegisterViewController ();
				}
				this.NavigationController.PushViewController (this.RegisterScreen, true);
			};
			this.EmailTextField.EditingDidBegin += delegate {
				MoveUIUp (175);
			};
			this.EmailTextField.ShouldReturn = delegate {
				this.PasswordTextField.BecomeFirstResponder ();
				return true;
			};
			this.PasswordTextField.EditingDidBegin += delegate {
				MoveUIUp (175);
			};
			this.PasswordTextField.ShouldReturn = delegate {
				Submit ();
				return true;
			};
		}

		public void PromptInvalidatedUser ()
		{
			var alert = new UIAlertView ("Authentication Error", "Please validate your account first.", null, "OK", null);
			alert.Show ();	

			EnableControls ();
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
		}

		protected void Submit ()
		{
		
			EmailTextField.ResignFirstResponder ();
			PasswordTextField.ResignFirstResponder ();
			MoveUIDown ();
			LoginSubmitted (this, EventArgs.Empty);
		
		}

		public void DisableControls ()
		{
			this.EmailTextField.Enabled = this.PasswordTextField.Enabled = this.RegisterButton.Enabled = this.LoginButton.Enabled = false;
			this.EmailTextField.Alpha = this.PasswordTextField.Alpha = this.RegisterButton.Alpha = this.LoginButton.Alpha = .5f;
		}

		public void EnableControls ()
		{
			this.EmailTextField.Enabled = this.PasswordTextField.Enabled = this.RegisterButton.Enabled = this.LoginButton.Enabled = true;
			this.EmailTextField.Alpha = this.PasswordTextField.Alpha = this.RegisterButton.Alpha = this.LoginButton.Alpha = 1;
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			SendGATrackingPoint ("login");
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			DI.DestroyMediator (this);
		}

		public void GoCalendar ()
		{
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
			EnableControls ();
			NavigationController.PushViewController (new CalendarMonthViewController (), true);
		}

		void InitUI ()
		{
			CreateBackground ("/Images/LoginScreen/LoginBackground", View);
			if (ScreenResolutionUtil.IsTall) {
				EmailTextField = new UITextField (new RectangleF (63, 220, 192, 30));
			} else {
				EmailTextField = new UITextField (new RectangleF (63, 224, 192, 30));
			}
			EmailTextField.Placeholder = "Email";
			EmailTextField.KeyboardType = UIKeyboardType.EmailAddress;
			EmailTextField.ReturnKeyType = UIReturnKeyType.Next;
			View.AddSubview (EmailTextField);
			if (ScreenResolutionUtil.IsTall) {
				PasswordTextField = new UITextField (new RectangleF (63, 270, 192, 30));
			} else {
				PasswordTextField = new UITextField (new RectangleF (63, 274, 192, 30));
			}
			PasswordTextField.Placeholder = "Password";
			PasswordTextField.SecureTextEntry = true;
			PasswordTextField.ReturnKeyType = UIReturnKeyType.Go;
			View.AddSubview (PasswordTextField);
			LoginButton = CreateImageButton (new Rectangle (35, 338, 250, 55), "/Images/LoginScreen/LoginButton", "/Images/LoginScreen/LoginButtonOver");
			View.AddSubview (LoginButton);
			RegisterButton = CreateImageButton (new Rectangle (132, 410, 58, 16), "/Images/LoginScreen/RegisterButton", "/Images/LoginScreen/RegisterButtonHighlight");
			View.AddSubview (RegisterButton);
		}
	}
}

