using System;
using System.Drawing;
using MonoTouch.UIKit;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.BL.Managers.Events;
using MyDiem.Core.BL;
using MyDiem.Core.Interfaces;
using MonkeyArms;

namespace MyDiem.UI.Touch.ViewController
{
	public partial class RegisterViewController : BaseUserFormViewController, IRegisterView
	{
		protected UIButton TermsOfUseButton, SubmitButton, HomeButton;

		public RegisterViewController () : base ("RegisterScreen", null)
		{

		}

		#region IRegisterView implementation

		public event EventHandler RegistrationSubmitted = delegate {};

		public void HandleSuccess (string message)
		{
			InvokeOnMainThread (() => new UIAlertView ("Success", message, null, "Ok").Show ());
		}

		public void ShowRegistrationError (string message)
		{
			InvokeOnMainThread (() => new UIAlertView ("Error", message, null, "Ok").Show ());
		}

		public System.Collections.Generic.List<County> Counties {
			get;
			set;
		}

		public System.Collections.Generic.List<Region> Regions {
			get;
			set;
		}

		public string SubmittedFirstName {
			get {
				return FirstNameVM.Value;

			}
		}

		public string SubmittedLastName {
			get {
				return LastNameVM.Value;

			}
		}

		public string SubmittedEmail {
			get {
				return EmailVM.Value;

			}
		}

		public string SubmittedZip {
			get {
				return ZipVM.Value;

			}
		}

		public string SubmittedPassword {
			get {
				return PasswordVM.Value;

			}
		}

		public string SubmittedPasswordVerify {
			get {
				return PasswordVerifyVM.Value;

			}
		}

		public string SubmittedRegionTitle {
			get {
				return RegionVM.Value;

			}
		}

		public string SubmittedCountyTitle {
			get {
				return CountyVM.Value;
			}
		}

		#endregion

		public override void ViewDidLoad ()
		{

			this.NavigationController.SetNavigationBarHidden (true, false);




			RegisterForKeyboardNotifications ();

			base.ViewDidLoad ();
			BuildUI ();
			
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			DI.RequestMediator (this);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			DI.DestroyMediator (this);
		}

		protected void HandleRegionManagerGetRegionsFinished (object sender, EventArgs e)
		{
			InvokeOnMainThread (BuildUI);
		}

		protected void BuildUI ()
		{
			CreateThatchBackground ();

			CreateFormHolder ();
			InitEntryVMs (new User (), true, true);
			AddProfileTable ();

			AddHeader ("Register", UIFont.FromName ("OpenSans", 20), ContentView);
			SubmitButton = CreateGreenButton (39, Convert.ToInt16 (ProfileTable.Frame.Y + ProfileTable.Frame.Height + 20), "Submit", FormHolder);

			SubmitButton.TouchUpInside += (sender, e) => {
				SubmitRegistration ();
			};

			TermsOfUseButton = CreateImageButton (new RectangleF (39, SubmitButton.Frame.Y + SubmitButton.Frame.Height + 20, 233, 28), 
				"/Images/RegisterScreen/RegisterTermsOfUseButton", 
				"/Images/RegisterScreen/RegisterTermsOfUseButtonHighlighted", FormHolder);

			TermsOfUseButton.TouchUpInside += (sender, e) => NavigationController.PushViewController (new TermsOfUseViewController (), true);

			HomeButton = CreateWhiteHeaderButton ("left", "Home", ContentView);
			HomeButton.TouchUpInside += HandleTouchUpInside;


			AddRegionsPicker ();
			AddCountiesPicker ();
			AddEventListeners ();
			

			UpdateLayout ();

		}

		void HandleTouchUpInside (object sender, EventArgs e)
		{

			NavigationController.PopViewControllerAnimated (true);
		}

		protected void HandleUserManagerRegisterUserFinished (object sender, EventArgs e)
		{
			InvokeOnMainThread (() => {
				MonoTouch.UIKit.UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
				SetSubmitButtonEnabled (true);
				var args = e as RegisterUserFinishedEvent;
				if (args.success == false) {
					AlertOK ("Registration Error", args.message);
				} else {
					OnRegistrationSuccess ();
				
				}
			});
		}

		protected void OnRegistrationSuccess ()
		{
			NavigationController.PopViewControllerAnimated (true);
			var av = new UIAlertView ("Almost Done"
			                          , "Check your email to complete registration."
			                          , null
			                          , "Ok, thanks!"
			                          , null);
			av.Show ();
		}

		override protected void UpdateLayout ()
		{
			
			ProfileTable.Frame = new RectangleF (20, 74, 280, tableSource.RowHeight * entries.Count);
			SubmitButton.Frame = new RectangleF (SubmitButton.Frame.X, ProfileTable.Frame.Y + ProfileTable.Frame.Height + 30, SubmitButton.Frame.Width, SubmitButton.Frame.Height);
			TermsOfUseButton.Frame = new RectangleF (39, SubmitButton.Frame.Y + SubmitButton.Frame.Height + 20, 233, 28);
			FormHolder.ContentSize = new Size (Convert.ToInt16 (View.Bounds.Width), Convert.ToInt16 (TermsOfUseButton.Frame.Y + TermsOfUseButton.Frame.Height + 20));
		}

		protected void SubmitRegistration ()
		{

			if (ValidateForm ()) {

				RegistrationSubmitted (this, EventArgs.Empty);

				ShowNetworkAcitivyIndicator ();
				SetSubmitButtonEnabled (false);
			} else {
				SetSubmitButtonEnabled (true);
			}
			
		}

		void SetSubmitButtonEnabled (bool enabled)
		{
			SubmitButton.Enabled = enabled;
			SubmitButton.Alpha = (enabled) ? 1 : .5f;
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			SendGATrackingPoint ("register");
		}
	}
}

