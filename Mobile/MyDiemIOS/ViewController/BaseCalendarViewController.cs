using System;
using System.Drawing;
using System.Diagnostics;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreAnimation;
using MyDiem.Core.BL.Managers;
using MyDiem.UI.Touch.Views.SlideMenu;
using MyDiem.Core.ViewModel;
using MyDiem.Core.CustomEvents;
using MyDiem.UI.Touch.Views.RegionsPicker;
using MyDiem.UI.Touch.Views.CountiesPicker;
using Views;
using MyDiem.Core.Interfaces;
using MyDiem.Core.Mediators;
using MonkeyArms;
using System.Reflection;

namespace MyDiem.UI.Touch.ViewController
{
	public class BaseCalendarViewController:BaseViewController, IBaseCalendarView
	{
		protected RegionsUIPickerView RegionsPicker;
		protected CountiesUIPickerView CountiesPicker;
		protected SubscribableTypeUIPickerView TypePicker;
		protected UIButton MenuButton;
		protected UIButton TodayButton;
		protected UIButton FilterButton;
		protected SlideMenu SettingsMenu;
		protected UIView ContentView;
		protected bool IsMenuOpen = false;
		protected SlideMenu FilterMenu;

		public BaseCalendarViewController (string ScreenClassName, NSBundle Bundle) : base (ScreenClassName, Bundle)
		{

		}

		protected void CreateContentView ()
		{
			ContentView = new UIView (new RectangleF (0, 0, 320, ScreenResolutionUtil.ScreenHeight));
			View.AddSubview (ContentView);
		}

		protected void CreateSlideMenu ()
		{
			SettingsMenu = new SlideMenu (new Rectangle (0, 0, 276, ScreenResolutionUtil.ScreenHeight));
			View.AddSubview (SettingsMenu);
		}

		protected TFilterMenu CreateFilterMenu<TFilterMenu> () where TFilterMenu:class
		{
			var FilterMenuInstance = Activator.CreateInstance (typeof(TFilterMenu)) as TFilterMenu;
			(FilterMenuInstance as UIView).Frame = new Rectangle (44, 0, 276, ScreenResolutionUtil.ScreenHeight);
			View.AddSubview (FilterMenuInstance as UIView);
			return FilterMenuInstance;
		}

		#region IBaseCalendarView implementation

		public void GoCalendarMonthView ()
		{
			CloseMenuOrPushController<CalendarMonthViewController> ();
		}

		public void GoCalendarWeekView ()
		{
			CloseMenuOrPushController<CalendarWeekViewController> ();
		}

		public void GoCalendarDayView ()
		{
			CloseMenuOrPushController<CalendarDayViewController> ();
		}

		public void GoSettingsView (MyDiem.Core.BL.User user)
		{
			CloseMenuOrPushController<ProfileViewController> ();
		}

		public void GoSubscribablesView (MyDiem.Core.BL.SchoolGroup category, string title)
		{
			NavigationController.PushViewController (new SubscribableListViewController (category, title), true);
		}

		public void GoEventDetailsView (MyDiem.Core.BL.CalendarEvent selectedEvent)
		{
			CloseMenuOrPushController<EventDetailsViewController> (selectedEvent);
		}

		public void GoAboutView ()
		{
			CloseMenuOrPushController<AboutViewController> ();
		}

		public void GoParentDirectory ()
		{
			CloseMenuOrPushController<ParentDirectoryViewController> ();
		}

		#endregion

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			if (IsIOS7) {
				SetNeedsStatusBarAppearanceUpdate ();
			}
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			RequestMediator (RegionsPicker);
			RequestMediator (CountiesPicker);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			DestroyMediator (RegionsPicker);
			DestroyMediator (CountiesPicker);
		}

		protected void RequestMediator (IMediatorTarget target)
		{
			if (target != null) {
				DI.RequestMediator (target);

			}
		}

		protected void DestroyMediator (IMediatorTarget target)
		{
			if (target != null) {
				DI.DestroyMediator (target);

			}
		}

		public override UIStatusBarStyle PreferredStatusBarStyle ()
		{
			return UIStatusBarStyle.LightContent;

		}

		protected bool IsCurrentControllerOfType<ClassType> () where ClassType : class
		{
			var controllers = NavigationController.ViewControllers;
			return controllers [controllers.Length - 1] is ClassType;
		}

		void CloseMenuOrPushController<TViewController> (object param = null) where TViewController:class
		{
			if (!IsCurrentControllerOfType<TViewController> ()) {
				if (param != null) {
					NavigationController.PushViewController (Activator.CreateInstance (typeof(TViewController), param) as BaseCalendarViewController, true); 
				} else {
					NavigationController.PushViewController (Activator.CreateInstance (typeof(TViewController)) as BaseCalendarViewController, true); 
				}
			} else {
				CloseSlideMenu ();
				IsMenuOpen = false;
			}
		}

		protected void CreateTodayButton ()
		{
		
			TodayButton = CreateWhiteHeaderButton ("right", "Today", ContentView);

		}

		public void CreateMenuButton ()
		{

			MenuButton = CreateImageButton (new Rectangle (0, ScreenHeaderView.HeaderAlignY - 20, 50, 40), "/Images/calendar/CalendarMenuButton", "/Images/calendar/CalendarMenuButton", ContentView);
			MenuButton.TouchUpInside += (sender, e) => {
				SettingsMenu.Hidden = false;
				if (FilterMenu != null) {
					FilterMenu.Hidden = true;
				}
				Debug.WriteLine ("IsMenuOpen {0}", IsMenuOpen);
				if (IsMenuOpen) {
					CloseSlideMenu ();
				} else {
					OpenSlideMenu ();
				}
				IsMenuOpen = !IsMenuOpen;
			};
		}

		public void CreateFilterButton ()
		{
			FilterButton = CreateImageButton (new Rectangle (265, ScreenHeaderView.HeaderAlignY - 20, 50, 40), "/Images/calendar/filterIcon", "/Images/calendar/filterIcon", ContentView);
			FilterButton.TouchUpInside += (sender, e) => {
				View.EndEditing (true);
				SettingsMenu.Hidden = true;
				if (FilterMenu != null) {
					FilterMenu.Hidden = false;
				}
				if (IsMenuOpen) {
					CloseSlideMenu ();
					CloseAllPickers ();
				} else {
					OpenSlideMenu (-1);
				}
				IsMenuOpen = !IsMenuOpen;
			};
		}

		protected void CloseAllPickers ()
		{
			ClosePicker (CountiesPicker);
			ClosePicker (RegionsPicker);
			ClosePicker (TypePicker);
		}

		protected void CreateTodayAndMenuButtons ()
		{
			CreateMenuButton ();
			CreateTodayButton ();
		}

		protected void CloseSlideMenu ()
		{
			Debug.WriteLine ("CloseSlideMenu");
			SlideContent (0);
		}

		protected void OpenSlideMenu (int direction = 1)
		{
			SlideContent (direction * 276);
		}

		protected void SlideContent (int offset)
		{
			UIView.BeginAnimations ("slideContent");
			UIView.SetAnimationDuration (.5);
			UIView.SetAnimationCurve (UIViewAnimationCurve.EaseOut);
			ContentView.Frame = new RectangleF (offset, 0, ContentView.Bounds.Width, ContentView.Bounds.Height);
			UIView.CommitAnimations ();

//			Rouse.To(ContentView, 0.5f, new{ positionx = ContentView.Frame.Width * 0.5 + offset }, null, Easing.EaseOutExpo);
//			ContentView.Frame = new RectangleF (offset, 0, ContentView.Bounds.Width, ContentView.Bounds.Height); //  TODO have Rouse do this also or make it unnecessary
		}

		protected void AddRegionsPicker ()
		{
			RegionsPicker = CreatePicker<RegionsUIPickerView> ();

		}

		protected void AddCountiesPicker ()
		{
			CountiesPicker = CreatePicker<CountiesUIPickerView> ();

		}

		protected void AddTypePicker ()
		{
			TypePicker = CreatePicker<SubscribableTypeUIPickerView> ();
		}

		protected TPicker CreatePicker<TPicker> () where TPicker:class
		{
			var picker = Activator.CreateInstance (typeof(TPicker), new RectangleF (0, 500, View.Bounds.Width, 200)) as BasePicker;
			picker.Hidden = true;
			View.AddSubview (picker);
			return picker as TPicker;
		}

		protected void OpenPicker (BasePicker picker)
		{
			MoveUIDown ();
			picker.Open ();
			
		}

		public static float ScreenHeight {
			get {
				return (IsIOS7) ? UIScreen.MainScreen.Bounds.Height : UIScreen.MainScreen.Bounds.Height - 20;
			}
		}

		protected void ClosePicker (UIView picker)
		{
			if (picker == null)
				return;

			UIView.BeginAnimations ("slideContent");
			UIView.SetAnimationDuration (.5);
			UIView.SetAnimationCurve (UIViewAnimationCurve.EaseOut);
			picker.Frame = new RectangleF (0, 600, picker.Bounds.Width, picker.Bounds.Height);
			UIView.CommitAnimations ();
		}
	}
}

