using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace MyDiem.UI.Touch.ViewController
{
	public partial class TermsOfUseViewController : BaseViewController
	{
		public TermsOfUseViewController () : base ("TermsOfUseScreen", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			this.NavigationController.SetNavigationBarHidden (true, false);
			CreateThatchBackground ();
			AddHeader ("Terms of Use", UIFont.FromName ("OpenSans-Bold", 18));

			UIButton closeButton = CreateWhiteHeaderButton ("left", "Close");
			closeButton.TouchUpInside += (sender, e) => {
				NavigationController.PopViewControllerAnimated (true);
			};

			CreateBodyText ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			SendGATrackingPoint ("terms-of-use");
		}

		protected void CreateBodyText ()
		{
			var style = "<style type='text/css'>body { color: #000000; background-color: transparent; font-family: 'HelveticaNeue-Light', Helvetica, Arial, sans-serif; padding: 20px; } h1, h2, h3, h4, h5, h6 { padding: 0px; margin: 0px; font-style: normal; font-weight: normal; } h2 { font-family: 'HelveticaNeue-Light', Helvetica, Arial, sans-serif; font-size: 24px; font-weight: normal; } h4 { font-size: 16px; } p { font-family: Helvetica, Verdana, Arial, sans-serif; line-height:1.5; font-size: 16px; } .esv-text { padding: 0 0 10px 0; }</style>";
			var html = style + "<body><h2>Terms and Conditions of Use</h2><br/><br/>";
			html += "<h3>Terms</h3><p>By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this web site are protected by applicable copyright and trade mark law.</p>";
			html += "<h3>Use License</h3><p>Permission is granted (as applicable) to temporarily download one copy of the materials (information or software) on MyDiem.com's web site for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:</p><ol><li>modify or copy the materials;</li><li>use the materials for any commercial purpose, or for any public display (commercial or non-commercial);</li><li>attempt to decompile or reverse engineer any software contained on MyDiem.com's web site;</li><li>remove any copyright or other proprietary notations from the materials; or transfer the materials to another person or \"mirror\" the materials on any other server.</li></ol><p>This license shall automatically terminate if you violate any of these restrictions and may be terminated by MyDiem.com at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.</p>";
			html += "<h3>Disclaimer</h3><p>The materials on MyDiem.com's web site are provided \"as is\". MyDiem.com makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, MyDiem.com does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its Internet web site or otherwise relating to such materials or on any sites linked to this site.</p>";
			html += "<h3>Limitations</h3><p>In no event shall MyDiem.com or its suppliers be liable for any damages (including, without limitation, damages for incorrect data, loss of data or profit, or due to business interruption,) arising out of the use or inability to use the materials on MyDiem.com's Internet site, even if MyDiem.com or a MyDiem.com authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.</p>";
			html += "<h3>Revisions and Errata</h3><p>The materials appearing on MyDiem.com's web site could include datum, technical, typographical, or photographic errors. MyDiem.com does not warrant that any of the materials on its web site are accurate, complete, or current. MyDiem.com may make changes to the materials contained on its web site at any time without notice. MyDiem.com does not, however, make any commitment to update the materials.</p>";
			html += "<h3>Links</h3><p>MyDiem.com has not reviewed all of the sites linked to its Internet web site and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by MyDiem.com of the site. Use of any such linked web site is at the user's own risk.</p>";
			html += "<h3>Terms of Use Modifications</h3><p>MyDiem.com may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Conditions of Use.</p>";
			html += "<h3>Governing Law</h3><p>Any claim relating to MyDiem.com's web site shall be governed by the laws of the State of California without regard to its conflict of law provisions. General Terms and Conditions applicable to Use of a Web Site.</p>";
			html += "</body>";

			UIWebView bodyText = new UIWebView (new RectangleF (0, 45, 320, 435));
			bodyText.Opaque = false;
			bodyText.BackgroundColor = UIColor.Clear;
			bodyText.LoadHtmlString (html, NSUrl.FromString ("http://localhost"));

			View.AddSubview (bodyText);
		}
	}
}

