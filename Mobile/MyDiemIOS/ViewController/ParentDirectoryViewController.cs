using System;
using System.Drawing;
using System.Diagnostics;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MyDiem.Core.BL.Managers;
using MyDiem.UI.Touch.Views.SlideMenu;
using MyDiem.Core.ViewModel;
using MyDiem.Core.CustomEvents;
using MyDiem.UI.Touch.Views.Directory;
using MyDiem.UI.Touch.Views.Global;
using Views.DirectoryFilterMenu;
using MyDiem.Core.Interfaces;
using MonkeyArms;
using MyDiem.Core.BL;

namespace MyDiem.UI.Touch.ViewController
{
	public partial class ParentDirectoryViewController : BaseCalendarViewController, IParentDirectoryView
	{
		UITableView ParentsTable;
		UIActivityIndicatorView uIActivityIndicatorView;
		UILabel NotSubscribedPrompt;

		public ParentDirectoryViewController () : base ("ParentDirectoryViewController", null)
		{

		}

		List<MyDiem.Core.BL.Parent> Contacts;

		public void UpdateContacts (List<MyDiem.Core.BL.Parent> contacts)
		{
			Contacts = contacts;
			InvokeOnMainThread (() => {
				UpdateTableSource ();
				FilterButton.Hidden = Contacts.Count == 0;
				MonoTouch.UIKit.UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
			});
		}

		public void GoParentDetails ()
		{
			NavigationController.PushViewController (new ParentDetailsViewController (), true);
		}

		public void HideLoadingIndicator ()
		{
			HideNetworkActivityIndicator ();
		}

		public void ShowNotSubscribedPrompt ()
		{
			NotSubscribedPrompt.Hidden = true;
		}

		public void ShowNoMatchesPrompt ()
		{

		}

		private Invoker parentSelected = new Invoker ();

		public Invoker ParentSelected {
			get {
				return parentSelected;
			}
		}

		private Invoker search = new Invoker ();

		public Invoker Search {
			get {
				return search;
			}
		}

		private Invoker reset = new Invoker ();

		public Invoker Reset {
			get {
				return reset;
			}
		}

		public string SearchPlaceHolderText {
			get {
				return searchBar.Placeholder;
			}
			set {
				searchBar.Placeholder = value;
			}
		}

		public string SearchQuery {
			get {
				return searchBar.Text.ToLower ();
			}
		}

		public Parent SelectedParent {
			get;
			private set;
		}

		public override void ViewDidLoad ()
		{
			this.NavigationController.SetNavigationBarHidden (true, false);
			base.ViewDidLoad ();
			BuildUI ();
		}

		public override void ViewWillAppear (bool animated)
		{

			base.ViewDidAppear (animated);

//			if (DirectoryViewModel.Contacts == null || DirectoryViewModel.Contacts.Count == 0) {
//				DirectoryViewModel.GetContacts ();
//				MonoTouch.UIKit.UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;	
//			} else {
//				uIActivityIndicatorView.Hidden = true;
//			}

			SendGATrackingPoint ("parent-directory");
			DI.RequestMediator (this);
		}

		protected void HandleContactsUpdated (object sender, EventArgs e)
		{
			InvokeOnMainThread (() => {
				UpdateTableSource ();
				FilterButton.Hidden = Contacts.Count == 0;
				HideNetworkActivityIndicator ();
			});

		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			DI.DestroyMediator (this);
		}

		protected void HandleGoParentDetails (object sender, EventArgs e)
		{
			if (!(NavigationController.ViewControllers [NavigationController.ViewControllers.Length - 1] is ParentDetailsViewController)) {
				NavigationController.PushViewController (new ParentDetailsViewController (), true);
			}
		}

		protected void BuildUI ()
		{
			CreateSlideMenu ();
			FilterMenu = CreateFilterMenu<DirectoryFilterMenu> ();

			CreateContentView ();
			CreateThatchBackground (ContentView);

			uIActivityIndicatorView = new UIActivityIndicatorView (new RectangleF (View.Center.X - 25, 350, 50, 50)) {
				Color = UIColor.Black
			};
			ContentView.AddSubview (uIActivityIndicatorView);
			uIActivityIndicatorView.StartAnimating ();


			CreateTable ();
			CreateSearchBar ();
			
			AddHeader ("Directory", null, ContentView);
			CreateMenuButton ();
			CreateFilterButton ();

			NotSubscribedPrompt = new UILabel (new RectangleF (15, searchBar.Frame.Bottom, View.Frame.Width - 30, 50));
			NotSubscribedPrompt.Text = "Sorry, no Directories associated with this email address.";
			NotSubscribedPrompt.AdjustsFontSizeToFitWidth = false;
			NotSubscribedPrompt.Lines = 0;
			NotSubscribedPrompt.TextAlignment = UITextAlignment.Center;
			ContentView.AddSubview (NotSubscribedPrompt);
			NotSubscribedPrompt.Hidden = true;
			
			
			
		}

		protected UISearchBar searchBar;

		protected void CreateSearchBar ()
		{
//			
			searchBar = new UISearchBar (new RectangleF (0, ScreenHeaderView.HeaderHeight, View.Bounds.Width, 48)) {
				BackgroundImage = UIImage.FromBundle ("/Images/SubscribableList/SearchBarBackground"),
				ClipsToBounds = false
				
			};
			
			searchBar.ShouldBeginEditing = delegate {
				searchBar.SetShowsCancelButton (true, true);
				return true;
			};
			
			searchBar.TextChanged += (object sender, UISearchBarTextChangedEventArgs e) => {
				Search.Invoke ();
			};
			
			searchBar.SearchButtonClicked += (sender, e) => {
				searchBar.SetShowsCancelButton (false, true);
				search.Invoke ();
				searchBar.ResignFirstResponder ();
			};
			
			searchBar.ShouldEndEditing = delegate {
				searchBar.SetShowsCancelButton (false, true);
				searchBar.ResignFirstResponder ();
				return true;
			};
			
			searchBar.CancelButtonClicked += (sender, e) => {
				searchBar.SetShowsCancelButton (false, true);
				Reset.Invoke ();
				searchBar.ResignFirstResponder ();
			};
			
			ContentView.AddSubview (searchBar);
		}

		protected void CreateTable ()
		{
			var topEdge = ScreenHeaderView.HeaderHeight + 48;
			ParentsTable = new UITableView (new RectangleF (0, topEdge, View.Bounds.Width, ScreenHeight - topEdge), UITableViewStyle.Grouped);
			ParentsTable.BackgroundColor = UIColor.Clear;
			ParentsTable.BackgroundView = null;
			ContentView.AddSubview (ParentsTable);
		}

		protected void UpdateTableSource ()
		{
			if (ParentsTable.Source == null) {
				var source = new DirectoryTableViewSource ();
				source.ParentSelected += (Parent p) => {
					this.SelectedParent = p;
					parentSelected.Invoke ();
				};
				ParentsTable.Source = source;
			}
			(ParentsTable.Source as DirectoryTableViewSource).Contacts = Contacts;
			ParentsTable.ReloadData ();
			uIActivityIndicatorView.Hidden = true;


		}
	}
}

