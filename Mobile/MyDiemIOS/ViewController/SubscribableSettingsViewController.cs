using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MyDiem.UI.Touch.ViewController;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.UI.Touch.Views.CalendarSettings;
using Views.Tables;
using MyDiem.Core.Interfaces;
using MonkeyArms;
using System.Collections.Generic;

namespace MyDiem.UI.Touch.ViewController
{
	public partial class SubscribableSettingsViewController : BaseCalendarViewController, ISubscribableSettingsView
	{
		public bool SubscribableDisabled = false;
		protected ColorSelectionView ColorPicker;
		protected SubscribableSettingsTableViewSource tableSource;
		protected MyDiemUITableView table;

		public SubscribableSettingsViewController (Subscribable subscribable, Subscription subscription) : base ("CalendarSettingsScreen", null)
		{
			this.selectedSubscribable = subscribable;
			this.SelectedSubscription = subscription;
		}

		public event EventHandler SubscriptionChanged;

		public void ShowUpdatedPrompt ()
		{
			HideNetworkActivityIndicator ();
		}

		public void ShowUpdatingPrompt ()
		{
			ShowNetworkAcitivyIndicator ();
		}

		private List<CalendarColor> colors;

		public List<CalendarColor> Colors {
			get {
				return colors;
			}
			set {
				colors = value;
				InvokeOnMainThread (CreateColorPicker);

			}
		}

		private Subscribable selectedSubscribable;

		public Subscribable SelectedSubscribable {
			get {
				return selectedSubscribable;
			}
			set {
				selectedSubscribable = value;
				UpdateTableSource ();
			}
		}

		void HandleColorClicked (object sender, EventArgs e)
		{
			MoveColorPickerUp (true);
		}

		Subscription selectedSubscription;

		public Subscription SelectedSubscription {
			get {
				return selectedSubscription;
			}
			set {
				selectedSubscription = value;
				UpdateTableSource ();
			}
		}

		public long SelectedColorID {
			get;
			set;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			BuildUI ();


		}

		void UpdateTableSource ()
		{


			if (SelectedSubscribable == null || SelectedSubscription == null || table == null) {
				return;
			}
			if (tableSource != null) {
				tableSource.ColorClicked -= HandleColorClicked;
				tableSource.Destroy ();
			}

			tableSource = new SubscribableSettingsTableViewSource (SelectedSubscribable, SelectedSubscription);
			tableSource.ColorClicked += HandleColorClicked;
			table.Source = tableSource;
			table.ReloadData ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			SendGATrackingPoint ("subscribable-settings");
			DI.RequestMediator (this);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			DI.DestroyMediator (this);
		}

		protected void CreateColorPicker ()
		{
			ColorPicker = new ColorSelectionView (Colors);

			View.AddSubview (ColorPicker);
			MoveColorPickerDown (false);
			ColorPicker.SelectedColorChanged += (sender, e) => {
				tableSource.UpdateColorSubscription (table, ColorPicker.SelectedColor);
				MoveColorPickerDown (true);
			};
		}

		protected void MoveColorPickerDown (bool animated)
		{
			if (animated) {
				UIView.BeginAnimations ("contentViewResize");
				UIView.SetAnimationDuration (.5);
				UIView.SetAnimationCurve (UIViewAnimationCurve.EaseOut);
			}
			ColorPicker.Frame = new RectangleF (0, ScreenResolutionUtil.ScreenHeight, 320, 230);
			if (animated) {
				UIView.CommitAnimations ();
			}
		}

		protected void MoveColorPickerUp (bool animated)
		{
			if (ColorPicker == null) {
				return;
			}

			if (animated) {
				UIView.BeginAnimations ("contentViewResize");
				UIView.SetAnimationDuration (.5);
				UIView.SetAnimationCurve (UIViewAnimationCurve.EaseOut);
			}

			ColorPicker.Frame = new RectangleF (0, View.Frame.Height - 160, 320, 230);

			if (animated) {
				UIView.CommitAnimations ();
			}
		}

		protected void BuildUI ()
		{
			CreateSlideMenu ();
			CreateContentView ();
			CreateThatchBackground (ContentView);
			AddHeader (SelectedSubscribable.title, UIFont.FromName ("OpenSans", 17), ContentView, "right");
			UIButton BackButton = CreateHeaderBackButton ("Schools", ContentView);
			CreateTable ();


			BackButton.TouchUpInside += (sender, e) => {
				NavigationController.PopViewControllerAnimated (true);
			};


		}

		protected void CreateTable ()
		{
			table = new MyDiemUITableView (new RectangleF (0, 55, View.Bounds.Width, 194), UITableViewStyle.Grouped);
			table.SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
			table.ScrollEnabled = false;


			table.BackgroundColor = UIColor.Clear;
			table.BackgroundView = null;
			ContentView.AddSubview (table);
			UpdateTableSource ();
		}

		public bool SubscriptionEnabled {
			get {
				return (table.Source as SubscribableSettingsTableViewSource).SelectedSubscription.Enabled;
			}
		}
	}
}

