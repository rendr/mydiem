using System;
using System.Drawing;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.BL.Managers.Events;
using MyDiem.Core.Interfaces;
using MonkeyArms;

namespace MyDiem.UI.Touch.ViewController
{
	public partial class HomeViewController : BaseViewController, IHomeView
	{
		protected UIScrollView scrollView;
		protected UIPageControl pageControl;
		protected List<UIView> pages;
		protected UIButton loginButton;
		protected UIButton registerButton;

		public HomeViewController () : base ("HomeScreen", null)
		{
		}

		#region IHomeView implementation

		public event EventHandler Login=delegate {};
		public event EventHandler Register = delegate {};

		public void GoLogin ()
		{

			this.NavigationController.PushViewController (new LoginViewController (), true);
		}

		public void GoRegister ()
		{
			this.NavigationController.PushViewController (new RegisterViewController (), true);
		}

		#endregion

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			InitUI ();
			AddEventListeners ();
			SetupGestureRecognisers ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			DI.RequestMediator (this);
			SendGATrackingPoint ("home");
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			DI.DestroyMediator (this);
		}

		protected void AddEventListeners ()
		{
			this.loginButton.TouchUpInside += (sender, e) => Login (this, EventArgs.Empty);
			this.registerButton.TouchUpInside += (sender, e) => Register (this, EventArgs.Empty);
		}

		protected void InitUI ()
		{
			this.NavigationController.SetNavigationBarHidden (true, false);
			CreateBackground ("/Images/HomeScreen/HomeBackground");
			scrollView = new UIScrollView (new RectangleF (0, View.Center.Y - 80, View.Bounds.Width, 284)) {
				AutoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleTopMargin,
				AutosizesSubviews = true,
				PagingEnabled = true,
				ShowsHorizontalScrollIndicator = false,
				ShowsVerticalScrollIndicator = false
			};
			pageControl = new UIPageControl (new RectangleF (0, ScreenResolutionUtil.ScreenHeight - 100, View.Bounds.Width, 36));
			pageControl.ValueChanged += delegate (object sender, EventArgs e) {
				var pc = (UIPageControl)sender;
				var toPage = pc.CurrentPage;
				var pageOffset = scrollView.Frame.Width * toPage - 10;
				PointF p = new PointF (pageOffset, 0);
				scrollView.SetContentOffset (p, true);
			};
			pageControl.Hidden = true;
			CreatePanels ();
			View.AddSubview (scrollView);
			View.AddSubview (pageControl);
		}

		void CreatePanels ()
		{
			scrollView.Scrolled += ScrollViewScrolled;

			int count = 5;
			RectangleF scrollFrame = scrollView.Frame;
			scrollFrame.Width = View.Bounds.Width * count;
			scrollView.ContentSize = scrollFrame.Size;

			pages = new List<UIView> ();

			for (int i = 0; i < count; i++) {
				UIView page = new UIView (new RectangleF (0, 0, View.Bounds.Width, scrollFrame.Height));

				UIImageView pageContent = new UIImageView (new RectangleF (30, 0, 263, 284));
				pageContent.Image = UIImage.FromBundle ("/Images/HomeScreen/HomePage" + (i + 1).ToString ());

				RectangleF frame = scrollView.Frame;
				PointF location = new PointF ();
				location.X = frame.Width * i;

				frame.Location = location;
				page.Frame = frame;
				page.AddSubview (pageContent);
				scrollView.AddSubview (page);

				pages.Add (page);
			}


			UIView loginPage = pages [4];
			loginButton = CreateGreenButton (35, 80, "Log In", loginPage);
			loginPage.AddSubview (loginButton);

			registerButton = CreateImageButton (new Rectangle (133, 155, 58, 16), "/Images/HomeScreen/RegisterButton", "/Images/HomeScreen/RegisterButtonHighlight");
			loginPage.AddSubview (registerButton);

			pageControl.Pages = count;

			UpdatePagePositions ();
		}

		protected void UpdatePagePositions ()
		{
			Debug.WriteLine ("UpdatePagePositions: " + pageControl.CurrentPage);

			if (pageControl.CurrentPage != 0) {
				pages [pageControl.CurrentPage - 1].Frame = new RectangleF (View.Bounds.Width * (pageControl.CurrentPage - 1) + 50, 0, 263, 284);
			}
			pages [pageControl.CurrentPage].Frame = new RectangleF (View.Bounds.Width * pageControl.CurrentPage, 0, 263, 284);
			if (pageControl.CurrentPage != pages.Count - 1) {
				pages [pageControl.CurrentPage + 1].Frame = new RectangleF (View.Bounds.Width * (pageControl.CurrentPage + 1) - 50, 0, 263, 284);
			}
		}

		private void ScrollViewScrolled (object sender, EventArgs e)
		{
			double page = Math.Floor ((scrollView.ContentOffset.X - scrollView.Frame.Width / 2) / scrollView.Frame.Width) + 1;
			pageControl.CurrentPage = (int)page;
			UpdatePagePositions ();

		}

		private void SetupGestureRecognisers ()
		{
			var doubleTapRecogniser = new UITapGestureRecognizer (this, new MonoTouch.ObjCRuntime.Selector ("ViewDoubleTapSelector"));
			doubleTapRecogniser.NumberOfTapsRequired = 3;
			View.AddGestureRecognizer (doubleTapRecogniser);
		}

		[Export ("ViewDoubleTapSelector")]
		protected void OnViewDoubleTapped (UIGestureRecognizer sender)
		{
			GoLogin ();
		}
	}
}

