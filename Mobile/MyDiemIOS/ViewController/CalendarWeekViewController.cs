using System;
using System.Drawing;
using System.Collections.Generic;
using System.Diagnostics;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.UI.Touch.ViewController;
using MyDiem.UI.Touch.Views.EventsTable;
using MyDiem.UI.Touch.Views.UICalendar;
using MyDiem.UI.Touch.Views.CalendarEventList;
using MyDiem.Core.ViewModel;
using MyDiem.Core.Interfaces;
using MonkeyArms;
using CustomEvents;

namespace MyDiem.UI.Touch.ViewController
{
	public partial class CalendarWeekViewController : BaseCalendarViewController, ICalendarWeekView
	{
		EventsTableView EventsTable;
		UICalendarWeek CalendarView;

		public CalendarWeekViewController () : base ("CalendarWeekScreen", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			BuildUI ();
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			SendGATrackingPoint ("calendar-week");
			DI.RequestMediator (this);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillAppear (animated);
			DI.DestroyMediator (this);
		}

		public event EventHandler WeekDateChanged = delegate {};
		public event EventHandler SelectedDateChanged = delegate {};
		public event EventHandler EventSelected = delegate {};

		public DateTime WeekDate {
			get {
				return CalendarView.CurrentWeekMonthYear;
			}
		}

		private DateTime selectedDate;

		public DateTime SelectedDate {
			get {
				return selectedDate;
			}
		}

		private List<CalendarEvent> weekEvents;

		public List<CalendarEvent> WeekEvents {
			get {
				return weekEvents;
			}
			set {
				weekEvents = value;
				UpdateEventsTableSource (value);
			}
		}

		private List<CalendarEvent> selectedDateEvents;

		public List<CalendarEvent> SelectedDateEvents {
			get {
				return selectedDateEvents;
			}
			set {
				selectedDateEvents = value;
				UpdateEventsTableSource (value);
			}
		}

		protected void BuildUI ()
		{
			CreateSlideMenu ();
			CreateContentView ();

			CreateBackground ("/Images/CalendarMonthScreen/CalendarMonthScreenBackground", ContentView);


			CalendarView = new UICalendarWeek ();

			CalendarView.OnDateSelected += (date) => {
				selectedDate = date;
				SelectedDateChanged (this, EventArgs.Empty);
			};
			ContentView.AddSubview (CalendarView);

			EventsTable = new EventsTableView (new RectangleF (0, CalendarView.Frame.Height, View.Frame.Width, (ScreenResolutionUtil.IsTall) ? 474 : 366));
			ContentView.AddSubview (EventsTable);

		
			CreateTodayAndMenuButtons ();
			TodayButton.TouchUpInside += (sender, e) => {
				CalendarView.GoToToday ();
			};
			CreateMenuButton ();


		}

		void UpdateEventsTableSource (List<CalendarEvent> value)
		{
			if (EventsTable.Source != null) {
//				(EventsTable.Source as CalendarEventsTableSource).EventSelected -= HandleEventSelected;
			}

			CalendarEventListTableSource eventsTableSource = new CalendarEventListTableSource (value);
//			eventsTableSource.EventSelected += HandleEventSelected;
			EventsTable.Source = eventsTableSource;
			EventsTable.ReloadData ();
		}

		void HandleEventSelected (CalendarEvent calendarEvent)
		{
			EventSelected (this, new SelectedCalendarEventArgs (calendarEvent));
		}
	}
}

