using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MyDiem.Core.Interfaces;
using MonkeyArms;

namespace MyDiem.UI.Touch.ViewController
{
	public partial class LoadingViewController : BaseViewController, ILoadingView
	{
		public LoadingViewController () : base ("LoadingScreen", null)
		{
		}

		#region ILoadingView implementation

		public void GoMainView ()
		{
			NavigationController.PushViewController (new CalendarMonthViewController (), true);
		}

		#endregion

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			this.NavigationController.SetNavigationBarHidden (true, false);
			CreateBackground ("/Default");


			UIActivityIndicatorView uIActivityIndicatorView = new UIActivityIndicatorView (new RectangleF (View.Center.X - 25, 350, 50, 50)) {
				Color = UIColor.Black
			};
			View.AddSubview (uIActivityIndicatorView);
			uIActivityIndicatorView.StartAnimating ();

		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			DI.RequestMediator (this);
			SendGATrackingPoint ("loading");
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			DI.DestroyMediator (this);
		}
	}
}

