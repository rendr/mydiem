using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MyDiem.UI.Touch.Views.CalendarEventList;
using MyDiem.Core.ViewModel;
using MonkeyArms;

namespace MyDiem.UI.Touch.ViewController
{
	public partial class CalendarDayViewController:BaseCalendarViewController
	{
		UITableView table;
		CalendarEventListTableSource calendarEventListTableSource;

		public CalendarDayViewController () : base ("CalendarDayScreen", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			BuildUI ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			DI.RequestMediator (this);
			SendGATrackingPoint ("calendar-about");
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			DI.DestroyMediator (this);
		}

		protected void BuildUI ()
		{
			CreateSlideMenu ();
			CreateContentView ();
			CreateThatchBackground (ContentView);

			AddHeader ("List View", null, ContentView);
			CreateTodayAndMenuButtons ();



			TodayButton.TouchUpInside += (sender, e) => {
				table.ScrollToRow (calendarEventListTableSource.PathToToday, UITableViewScrollPosition.Middle, false);
			};

			calendarEventListTableSource = new CalendarEventListTableSource (null);
			table = new UITableView (new RectangleF (0, ScreenHeaderView.HeaderHeight, View.Frame.Width, View.Frame.Height - ScreenHeaderView.HeaderHeight)) {
				RowHeight = 18,
				Source = calendarEventListTableSource

			};
			table.SeparatorStyle = UITableViewCellSeparatorStyle.None;
			ContentView.AddSubview (table);

			ContentView.BringSubviewToFront (Header);
			ContentView.BringSubviewToFront (TodayButton);
			ContentView.BringSubviewToFront (MenuButton);

			try {
				table.ScrollToRow (calendarEventListTableSource.PathToToday, UITableViewScrollPosition.Middle, false);
			} finally {

			}

		}
	}
}

