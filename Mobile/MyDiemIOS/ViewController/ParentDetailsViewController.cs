using System;
using System.Drawing;
using System.Diagnostics;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MyDiem.Core.BL.Managers;
using MyDiem.UI.Touch.Views.SlideMenu;
using MyDiem.Core.ViewModel;
using MyDiem.Core.CustomEvents;
using MyDiem.UI.Touch.Views.Directory;
using MyDiem.UI.Touch.Views.Global;
using MyDiem.Core.Interfaces;
using MyDiem.Core.BL;
using MonkeyArms;

namespace MyDiem.UI.Touch.ViewController
{
	public class ParentDetailsViewController: BaseCalendarViewController, IParentDetailsView
	{
		UITableView TableView;

		public ParentDetailsViewController () : base ("ParentDetailsViewController", null)
		{
		}

		public string ScreenTitle {
			get;
			set;
		}

		public override void ViewDidLoad ()
		{
			this.NavigationController.SetNavigationBarHidden (true, false);
			base.ViewDidLoad ();
			BuildUI ();
		}

		protected void BuildUI ()
		{
			CreateSlideMenu ();
			CreateContentView ();
			CreateThatchBackground (ContentView);
			CreateTable ();

//			string title = String.Format ("{0}, {1}", DirectoryViewModel.SelectedParent.LastName, DirectoryViewModel.SelectedParent.FirstName);

			AddHeader ("Details");
			CreateHeaderBackButton ("Back", null, null).TouchUpInside += HandleTouchUpInside;

		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			DI.RequestMediator (this);
			SendGATrackingPoint ("parent-details");
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			DI.DestroyMediator (this);
		}

		protected void HandleTouchUpInside (object sender, EventArgs e)
		{
			Console.WriteLine ("GO BACK");
			NavigationController.PopViewControllerAnimated (true);
		}

		protected void CreateTable ()
		{
			TableView = new UITableView (new RectangleF (0, 40, View.Bounds.Width, ScreenResolutionUtil.ScreenHeight - 40), UITableViewStyle.Grouped);
			TableView.BackgroundColor = UIColor.Clear;
			TableView.BackgroundView = null;
			ContentView.AddSubview (TableView);
		}

		public void UpdateTableSource (Parent selectedParent)
		{
			TableView.Source = new DirectoryDetailsTableViewSource (selectedParent);
			TableView.ReloadData ();
		}
	}
}

