using System;
using System.Drawing;
using System.Diagnostics;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.EventKit;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.ViewModel;
using MyDiem.UI.Touch.ViewController;
using MyDiem.UI.Touch.Views.EventsTable;
using MyDiem.UI.Touch.Views.UICalendar;
using MyDiem.Core.Interfaces;
using MonkeyArms;

namespace MyDiem.UI.Touch.ViewController
{
	public partial class CalendarMonthViewController : BaseCalendarViewController, ICalendarMonthView
	{
		EventsTableView EventsTable;
		UIImageView topInnerShadow;
		UICalendarMonth CalendarView;
		//EKEventStore eventStore;
		public CalendarMonthViewController () : base ("CalendarMonthScreen", null)
		{
		}

		#region ICalendarMonthView implementation

		public event EventHandler DateSelected = delegate {};
		public event EventHandler MonthChanged = delegate {};
		public event EventHandler EventSelected = delegate {};

		public DateTime SelectedDate {
			private set;
			get;
		}

		public DateTime SelectedMonth {
			get {
				return CalendarView.CurrentMonthYear;
			}
		}

		private List<CalendarEvent> selectedDateEvents;

		public List<CalendarEvent> SelectedDateEvents {
			get {
				return selectedDateEvents;
			}
			set {
				selectedDateEvents = value;
				UpdateEventsList (value);
			}
		}

		private List<CalendarEvent> selectedMonthEvents;

		public List<CalendarEvent> MonthEvents {
			get {
				return selectedMonthEvents;
			}
			set {
				selectedMonthEvents = value;
//				UpdateEventsList (value);
			}
		}

		#endregion

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			SendGATrackingPoint ("calendar-month");
			DI.RequestMediator (this);
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			DI.DestroyMediator (this);
		}

		protected void UpdateEventsList (List<CalendarEvent> events)
		{

		
			CalendarEventsTableSource eventsTableSource = new CalendarEventsTableSource (events);
			EventsTable.Source = eventsTableSource;
			EventsTable.ReloadData ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.NavigationController.SetNavigationBarHidden (true, false);

			CreateSlideMenu ();

			CreateContentView ();
			ContentView.BackgroundColor = UIColor.White;
			
			CreateCalendarView ();



			EventsTable = new EventsTableView (new RectangleF (0, 0, View.Frame.Width, 10f));
			UpdateEventsTableFrame (CalendarView.Frame);

			ContentView.AddSubview (EventsTable);

			topInnerShadow = new UIImageView (new RectangleF (0, GetCalendarBottomEdge (CalendarView.Frame), View.Frame.Width, 11));
			topInnerShadow.Image = UIImage.FromBundle ("/Images/EventsTable/TopInnerShadow");
			ContentView.AddSubview (topInnerShadow);



			CreateTodayAndMenuButtons ();

			TodayButton.TouchUpInside += (sender, e) => {
				CalendarView.GoToToday ();
			};
			CreateMenuButton ();

			// load initial data
			DateTime today = DateTime.Now;
//			List<CalendarEvent> events2 = EventManager.GetEventsForDate (today.Date);
			
//			CalendarEventsTableSource eventsTableSource2 = new CalendarEventsTableSource (events2);
//			EventsTable.Source = eventsTableSource2;
//			EventsTable.ReloadData ();




		}

		void CreateCalendarView ()
		{
			CalendarView = new UICalendarMonth ();
			CalendarView.OnCalendarResized += newFrame => {
				UpdateEventsTableFrame (newFrame);
				topInnerShadow.Frame = new RectangleF (0, GetCalendarBottomEdge (newFrame), View.Bounds.Width, 11);
			};

			CalendarView.OnDateSelected += (DateTime date) => {
				this.SelectedDate = date;
				DateSelected (this, EventArgs.Empty);
			};
			ContentView.AddSubview (CalendarView);
		}

		private float GetCalendarBottomEdge (RectangleF calendarFrame)
		{
			return calendarFrame.Y + calendarFrame.Height;
		}

		void UpdateEventsTableFrame (RectangleF newFrame)
		{
			var newHeight = View.Frame.Height - GetCalendarBottomEdge (newFrame);
			EventsTable.Frame = new RectangleF (0, GetCalendarBottomEdge (newFrame), View.Frame.Width, newHeight);
		}
	}
}

