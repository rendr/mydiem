using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Social;
using MyDiem.Core.BL;
using MyDiem.UI.Touch.Views.Global;
using MyDiem.UI.Touch.Util;
using MyDiem.Core.BL.Managers;
using Views.Tables;
using MonkeyArms;

namespace MyDiem.UI.Touch.ViewController
{
	public partial class EventDetailsViewController : BaseCalendarViewController
	{
		UILabel TitleLabel;
		UIWebView DescriptionLabel;
		protected CalendarEvent SelectedEvent;
		protected MyDiemUITableView detailsTable;
		protected UIScrollView ScrollView;
		protected UIButton FacebookButton, TwitterButton;

		public EventDetailsViewController (CalendarEvent selectedEvent) : base ("EventDetailScreen", null)
		{
			SelectedEvent = selectedEvent;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			CreateSlideMenu ();
			CreateContentView ();
			CreateThatchBackground (ContentView);
			CreateHeader ();

			ScrollView = new UIScrollView (new RectangleF (0, Header.Bounds.Height, View.Bounds.Width, View.Bounds.Height - Header.Bounds.Height));
			View.AddSubview (ScrollView);

			CreateEventTitle ();
			MyDiemUITableView detailsTable = CreateDetailsTable ();
			CreateDescriptionText (detailsTable);
			CreateSocialButtons ();

			DI.Get<SubscribableManager> ().ReminderSet += (sender, e) => {
				UpdateTable ();
			};


			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			SendGATrackingPoint ("event-details");
		}

		protected void UpdateTable ()
		{
			var entries = CreateEntries ();
			var eventDetailsTableSource = new GenericTableSource (entries, true);
			detailsTable.Source = eventDetailsTableSource;
			eventDetailsTableSource.CellSelected += (tableSender, ev) => {
				if (eventDetailsTableSource.SelectedRowIndex == entries.Count - 1) {
					NavigationController.PushViewController (new EventReminderViewController (SelectedEvent), true);
				}
			};
		}

		void UpdateFacebookButtonPosition ()
		{
			if (FacebookButton == null) {
				return;
			}

			var curY = DescriptionLabel.Frame.Y + DescriptionLabel.Frame.Height + 5;
			FacebookButton.Frame = new RectangleF (FacebookButton.Frame.X, curY, FacebookButton.Frame.Width, FacebookButton.Frame.Height);
			ScrollView.ContentSize = new SizeF (View.Bounds.Width, curY + 67);
		}

		protected void CreateDescriptionText (UITableView detailsTable)
		{
			DescriptionLabel = new UIWebView (new RectangleF (30, (detailsTable.Frame.Top + detailsTable.Frame.Height + 20), 260, 1));
			DescriptionLabel.LoadFinished += (object sender, EventArgs e) => {
				float height = float.Parse (DescriptionLabel.EvaluateJavascript ("document.height"));
				DescriptionLabel.Frame = new RectangleF (30, (detailsTable.Frame.Top + detailsTable.Frame.Height + 20), 260, height);

				UpdateFacebookButtonPosition ();
			};
			ScrollView.AddSubview (DescriptionLabel);
			
			DescriptionLabel.Opaque = false;
			DescriptionLabel.BackgroundColor = UIColor.Clear;

			string descriptionText = "<span style='font-family:Helvetica'>" + SelectedEvent.Description + "</span>";

			DescriptionLabel.LoadHtmlString (descriptionText, NSUrl.FromString ("http://localhost"));

			DescriptionLabel.SizeToFit ();

			/*
			DescriptionLabel = new UILabel (new RectangleF (30, (detailsTable.Frame.Top + detailsTable.Frame.Height + 20), 260, 30)) {
				Font = UIFont.FromName ("OpenSans", 14),
				TextColor = UIColor.FromRGB (67, 67, 67),
				TextAlignment = UITextAlignment.Left,
				BackgroundColor = UIColor.Clear,
				Text = SelectedEvent.Description,
				LineBreakMode = UILineBreakMode.WordWrap,

				Lines = 0
			};
			DescriptionLabel.SizeToFit ();
			ScrollView.AddSubview (DescriptionLabel);
			*/

		
		}

		void ShareEventOnFacebook ()
		{
			var slComposer = SLComposeViewController.FromService (SLServiceKind.Facebook);

			string messageText = SelectedEvent.Title;
			if (SelectedEvent.AllDay) {
				messageText += " - All Day: ";
			} else {
				messageText += " from " + SelectedEvent.StartDateTime.ToString ("t").ToUpper ();
				messageText += " to " + SelectedEvent.EndDateTime.ToString ("t").ToUpper ();

			}

			if (SelectedEvent.Location != null && SelectedEvent.Location != "") {
				messageText += " at " + SelectedEvent.Location;
			}

			if (SelectedEvent.Description != null && SelectedEvent.Description != "") {
				messageText += ": " + SelectedEvent.Description;
			}

			slComposer.SetInitialText (messageText);
			slComposer.AddImage (UIImage.FromFile ("Images/appIcons/AppIcon@2x.png"));
			slComposer.CompletionHandler += (IAsyncResult) => {
				InvokeOnMainThread (() => {
					DismissViewController (true, null);
				});
			};
			PresentViewController (slComposer, true, null);
		}

		protected void CreateSocialButtons ()
		{


			if (SLComposeViewController.IsAvailable (SLServiceKind.Facebook)) {
				FacebookButton = new UIButton (new RectangleF (20, 0, 282f, 57f));
				FacebookButton.SetBackgroundImage (UIImage.FromBundle ("Images/Social/FacebookButton"), UIControlState.Normal);
				FacebookButton.SetTitle ("Share on Facebook", UIControlState.Normal);
				FacebookButton.SetTitleColor (UIColor.White, UIControlState.Normal);
				FacebookButton.TitleEdgeInsets = new UIEdgeInsets (0, 15, 0, 0);

				FacebookButton.TouchUpInside += (object sender, EventArgs e) => {
					ShareEventOnFacebook ();
				};

				ScrollView.AddSubview (FacebookButton);

				UpdateFacebookButtonPosition ();
			}


			/*
			if (SLComposeViewController.IsAvailable (SLServiceKind.Twitter)) {
				TwitterButton = new UIButton (new RectangleF (20, curY, 282f, 57f));
				TwitterButton.SetBackgroundImage (UIImage.FromBundle ("Images/Social/TwitterButton"), UIControlState.Normal);
				TwitterButton.SetTitle ("Share on Twitter", UIControlState.Normal);
				TwitterButton.SetTitleColor (UIColor.White, UIControlState.Normal);
				TwitterButton.TitleEdgeInsets = new UIEdgeInsets (0, 15, 0, 0);
				ScrollView.AddSubview (TwitterButton);
			}
			*/


		}

		protected void CreateEventTitle ()
		{
			TitleLabel = new UILabel (new RectangleF (12, 0, 308, 30)) {
				Font = UIFont.FromName ("OpenSans-Bold", 18),
				TextColor = UIColor.FromRGB (67, 67, 67),
				TextAlignment = UITextAlignment.Left,
				BackgroundColor = UIColor.Clear,
				Text = SelectedEvent.Title,
				AdjustsFontSizeToFitWidth = true,
				LineBreakMode = UILineBreakMode.WordWrap,
				Lines = 0
			};
			TitleLabel.SizeToFit ();
			ScrollView.AddSubview (TitleLabel);
		}

		protected MyDiemUITableView CreateDetailsTable ()
		{

			int rowHeight = 48;
			List<GenericTableCellViewModel> entries = CreateEntries ();
			detailsTable = new MyDiemUITableView (new RectangleF (20, TitleLabel.Frame.Y + TitleLabel.Frame.Height + 10, 280, rowHeight * entries.Count));
			detailsTable.RowHeight = rowHeight;
			detailsTable.ScrollEnabled = false;
			var eventDetailsTableSource = new GenericTableSource (entries, false);
			eventDetailsTableSource.CellSelected += (sender, e) => {
				if (eventDetailsTableSource.SelectedRowIndex == entries.Count - 1) {
					NavigationController.PushViewController (new EventReminderViewController (SelectedEvent), true);
				}
			};
			detailsTable.Source = eventDetailsTableSource;
			ScrollView.AddSubview (detailsTable);
			ScrollView.InsertSubviewBelow (new DropShadowView (detailsTable.Frame), detailsTable);
			return detailsTable;
		}

		protected List<GenericTableCellViewModel> CreateEntries ()
		{
			List<GenericTableCellViewModel> entries = new List<GenericTableCellViewModel> ();
			if (SelectedEvent.AllDay) {

				entries.Add (InitCellVM ("ALL DAY", ""));
				entries.Add (InitCellVM ("Starts", SelectedEvent.StartDateTime.ToString ("g").ToUpper ()));
				entries.Add (InitCellVM ("Ends", SelectedEvent.EndDateTime.ToString ("g").ToUpper ()));

			} else {

				entries.Add (InitCellVM ("Starts", SelectedEvent.StartDateTime.ToString ("t").ToUpper ()));
				entries.Add (InitCellVM ("Ends", SelectedEvent.EndDateTime.ToString ("t").ToUpper ()));

			}

			if (!String.IsNullOrEmpty (SelectedEvent.CalendarTitle)) {
				entries.Add (InitCellVM ("Calendar", SelectedEvent.CalendarTitle));
			}

			if (!String.IsNullOrEmpty (SelectedEvent.Location)) {
				entries.Add (InitCellVM ("Location", SelectedEvent.Location));
			}
			var eventID = LocalNotificationUtil.generateEventIDString (SelectedEvent);
			entries.Add (InitCellVM ("Reminder", LocalNotificationUtil.GetOptionLabelByEventID (eventID)));
			return entries;
		}

		protected GenericTableCellViewModel InitCellVM (string label, string value)
		{
			return new GenericTableCellViewModel (label, value, "", false, false, false, false, false);
		}

		protected void CreateHeader ()
		{
			AddHeader ("Details", null, ContentView);
			UIButton backButton = CreateHeaderBackButton ("Back", ContentView);
			backButton.TouchUpInside += (sender, e) => {
				NavigationController.PopViewControllerAnimated (true);
			};
		}
	}
}

