using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using MyDiem.Core.Interfaces;
using MyDiem.UI.Touch.Database.SQLite;

namespace MyDiem.UI.Touch.Database
{
	public class MyDiemDatabase:IMyDiemDatabase
	{
		private SQLiteConnection db = null;
		private string dbLocation;
		static object locker = new object ();

		public MyDiemDatabase ()
		{
			dbLocation = DatabaseFilePath;
			db = new SQLiteConnection (dbLocation);
		}

		public IEnumerable<T> GetItems<T> () where T : BL.Contracts.IBusinessEntity, new()
		{
			lock (locker) {
				return (from i in db.Table<T> ()
				        select i).ToList ();
			}
		}

		public T GetItem<T> (int id) where T : BL.Contracts.IBusinessEntity, new()
		{
			lock (locker) {
				return (from i in db.Table<T> ()
				        where i.ID == id
				        select i).FirstOrDefault ();
			}
		}

		public int SaveItem<T> (T item) where T : BL.Contracts.IBusinessEntity
		{
			db.CreateTable<T> ();
			/*
            lock (locker) {
                if (item.ID != 0) {
                    me.Update (item);
                    return item.ID;
                } else {
                */
			return db.Insert (item);
			/*
                }
            }
            */
		}

		public void SaveItems<T> (IEnumerable<T> items) where T : BL.Contracts.IBusinessEntity
		{
			lock (locker) {
				db.BeginTransaction ();

				foreach (T item in items) {
					SaveItem<T> (item);
				}

				db.Commit ();
			}
		}

		public int DeleteItem<T> (int id) where T : BL.Contracts.IBusinessEntity, new()
		{
			lock (locker) {
				db.Execute (string.Format ("delete from \"{0}\" where id=" + id.ToString (), typeof(T).Name));
				return 0;
//                return db.Delete<T> (new T () { ID = id });
			}
		}

		public void ClearTable<T> () where T : BL.Contracts.IBusinessEntity, new()
		{
			lock (locker) {
				db.Execute (string.Format ("delete from \"{0}\"", typeof(T).Name));
			}
		}
		// helper for checking if database has been populated
		public int CountTable<T> () where T : BL.Contracts.IBusinessEntity, new()
		{
			lock (locker) {
				string sql = string.Format ("select count (*) from \"{0}\"", typeof(T).Name);
				var c = db.CreateCommand (sql, new object[0]);
				return c.ExecuteScalar<int> ();
			}
		}

		public static string DatabaseFilePath {
			get { 
#if SILVERLIGHT
            var path = "MyDiemDB.db3";
#else

#if __ANDROID__
            string libraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); ;
#else
				// we need to put in /Library/ on iOS5.1 to meet Apple's iCloud terms
				// (they don't want non-user-generated data in Documents)
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
				string libraryPath = Path.Combine (documentsPath, "../Library/");
#endif
				var path = Path.Combine (libraryPath, "MyDiemDB.db3");
#endif		
				return path;	
			}
		}
	}
}

