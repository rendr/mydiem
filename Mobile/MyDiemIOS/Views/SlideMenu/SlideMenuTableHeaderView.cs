using System;
using System.Drawing;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
namespace MyDiem.UI.Touch.Views.SlideMenu
{
	public class SlideMenuTableHeaderView:UIView
	{
		string Title;

		public SlideMenuTableHeaderView (RectangleF frame, string title):base(frame)
		{
			Title = title;
		}

		public override void LayoutSubviews ()
		{
			UIImageView background = new UIImageView(new RectangleF(0,0, 279, 23));
			background.Image = UIImage.FromBundle("/Images/SlideMenu/OptionCategoryHeaderBackground");
			AddSubview(background);

			int positionOffset = 7;
			UILabel titleLabel = new UILabel(new RectangleF(positionOffset,5, Bounds.Width-positionOffset, Bounds.Height-positionOffset)){
				Font = UIFont.BoldSystemFontOfSize(16),
				TextColor = UIColor.FromRGB(64,63,63),
				BackgroundColor = UIColor.Clear
			};

			titleLabel.Text = Title.ToUpper();
			AddSubview(titleLabel);
           
		}

	
	}
}

