using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using MyDiem.UI.Touch.Views;
using MyDiem.UI.Touch.Util;
using MyDiem.UI.Touch.Views.EventsTable;
using MyDiem.Core.ViewModel;
using MyDiem.UI.Touch.ViewController;
using MyDiem.Core.Interfaces;
using MonkeyArms;

namespace MyDiem.UI.Touch.Views.SlideMenu
{
	public class SlideMenu:UIView, ISlideMenuView
	{
		public SlideMenuTableView OptionsTable;
		protected UILabel LoadingLabel;

		public SlideMenu (RectangleF ViewRect) : base (ViewRect)
		{
			MonoTouch.UIKit.UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
		}

		#region ISlideMenuView implementation

		public void UpdateOptionsTableSource (List<MyDiem.Core.BL.SchoolGroup> categories)
		{

			SettingsMenuTableSource optionsTableSource = new SettingsMenuTableSource (categories);
			MonoTouch.UIKit.UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
			OptionsTable.Source = optionsTableSource;

			OptionsTable.ReloadData ();
			LoadingLabel.Hidden = true;
		}

		public void Enable ()
		{

		}

		#endregion

		public override void LayoutSubviews ()
		{
			UIImageView background = new UIImageView (new RectangleF (0, 0, 320, ScreenResolutionUtil.ScreenHeight));
			background.Image = ScreenResolutionUtil.FromBundle16x9 ("/Images/SlideMenu/SlideMenuBackground");

			AddSubview (background);
			AddLoadingLabel ();
			AddOptionsTable ();

			if (!(this is SubscribableFilterMenu)) {
				DI.RequestMediator (this);
			}
		
		}

		protected void AddLoadingLabel ()
		{
			LoadingLabel = new UILabel () {
				Font = UIFont.FromName ("Helvetica", 18),
				TextColor = UIColor.White,
				BackgroundColor = UIColor.Clear,
				TextAlignment = UITextAlignment.Center,
				Text = "Loading..."

			};
			LoadingLabel.Frame = new RectangleF (0 + 20, 200, Frame.Width, 30);
			AddSubview (LoadingLabel);

		}

		protected virtual void AddOptionsTable ()
		{
			CreateTable ();
			OptionsTable.Hidden = true;

		}

		protected void CreateTable ()
		{
			const int topEdge = 66;
			OptionsTable = new SlideMenuTableView (new RectangleF (0, topEdge, Bounds.Width, BaseCalendarViewController.ScreenHeight - topEdge));
			AddSubview (OptionsTable);
		}
	}
}

