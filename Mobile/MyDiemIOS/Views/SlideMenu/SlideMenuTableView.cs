using System;
using MonoTouch.UIKit;
using System.Drawing;
using Views.Tables;

namespace MyDiem.UI.Touch.Views.SlideMenu
{
	public class SlideMenuTableView:MyDiemUITableView
	{
		public SlideMenuTableView (RectangleF frame):base(frame)
		{
			Opaque = false;
			BackgroundColor = UIColor.Clear;
			RowHeight = 46;
			SeparatorColor = UIColor.FromRGB(89,87,83);
		}
	}
}

