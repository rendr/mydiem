using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using MonoTouch.UIKit;
using MyDiem.UI.Touch.Views.SlideMenu;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.ViewModel;
using MyDiem.Core.Interfaces;
using MonkeyArms;

namespace MyDiem.UI.Touch.Views.SlideMenu
{
	public class SettingsMenuTableSource:UITableViewSource, ISettingsMenuTableSource
	{
		Dictionary<string, List<string>> Options;
		string cellIdentifier = "TableCell";
		string[] keys;
		protected List<SchoolGroup> MainCalendarCategories;

		#region ISettingsMenuTableSource implementation

		public void SetOptions (Dictionary<string, List<string>> options)
		{

			Options = options;
			keys = Options.Keys.ToArray ();

		}

		public void EnableSubscribableOptions ()
		{

		}

		private Invoker RowSelectedInvoker = new Invoker ();

		public MonkeyArms.Invoker SettingsRowSelected {
			get {
				return RowSelectedInvoker;
			}
		}

		public int SelectedSectionIndex {
			get;
			set;
		}

		public int SelectedRowIndex {
			get;
			set;
		}

		public bool IsCalendarConnectPurchased {
			get;
			set;
		}

		#endregion

		public SettingsMenuTableSource (List<SchoolGroup> mainCalendarCategories)
		{
			MainCalendarCategories = mainCalendarCategories;
			Options = DI.Get<SlideMenuViewModel> ().OptionsDictionary;

			keys = Options.Keys.ToArray ();

			DI.RequestMediator (this);
		}

		public override int NumberOfSections (UITableView tableView)
		{
			return keys.Length;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			return Options [keys [section]].Count;

		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			return keys [section];
		}

		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			if (Options.Count == 0) {
				tableView.Hidden = true;
			} else {
				tableView.Hidden = false;
			}
			SlideMenuOptionCell cell = tableView.DequeueReusableCell (cellIdentifier) as SlideMenuOptionCell;
			if (cell == null)
				cell = new SlideMenuOptionCell (cellIdentifier);
			cell.UpdateCell (Options [keys [indexPath.Section]] [indexPath.Row]);
			return cell;

		}

		public override UIView GetViewForHeader (UITableView tableView, int section)
		{
			SlideMenuTableHeaderView header = new SlideMenuTableHeaderView (new RectangleF (0, 0, tableView.Bounds.Width, 20), keys [section]);
			return header;			                                              
		}

		public override void RowSelected (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			SelectedSectionIndex = indexPath.Section;
			SelectedRowIndex = indexPath.Row;
			tableView.DeselectRow (indexPath, true);
			RowSelectedInvoker.Invoke ();
		}
	}
}

