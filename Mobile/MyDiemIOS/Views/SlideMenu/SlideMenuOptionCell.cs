using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace MyDiem.UI.Touch.Views.SlideMenu
{


	public class SlideMenuOptionCell:UITableViewCell
	{

		protected UILabel optionLabel;

		public string Title{
			get{
				return optionLabel.Text;
			}
		}

		public SlideMenuOptionCell (string cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			Opaque = false;
			BackgroundColor = UIColor.Clear;
			optionLabel = new UILabel(){
				Font = UIFont.FromName("Helvetica", 18),
				TextColor = UIColor.FromRGB(177,177,177),
				BackgroundColor = UIColor.Clear
			};

			ContentView.Add(optionLabel);
		}

		public virtual void UpdateCell(string caption)
		{
			optionLabel.Text = caption;
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			optionLabel.Frame = new RectangleF(15,0,Bounds.Width-15,Bounds.Height);
		}


	}
}

