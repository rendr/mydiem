using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.UI.Touch;
using MyDiem.UI.Touch.Util;

namespace MyDiem.UI.Touch.Views.CalendarEventList
{
	public class EventsTableCell:UITableViewCell
	{
		UILabel TitleLabel, StartsLabel;
		UIImageView ReminderIcon;
		protected CalendarEvent CalEvent;

		public EventsTableCell (string cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			Opaque = false;
			BackgroundColor = UIColor.Clear;
			SelectedBackgroundView = new UIView ();

			TitleLabel = new UILabel (new RectangleF (90, 5, 175, 15)) {
				Font = UIFont.FromName ("Helvetica", 12),
				TextColor = UIColor.FromRGB (67, 67, 67),
				BackgroundColor = UIColor.Clear
			};
			AddSubview (TitleLabel);

		
			StartsLabel = new UILabel (new RectangleF (17, 5, 325, 15)) {
				Font = UIFont.BoldSystemFontOfSize (12),
				TextColor = UIColor.FromRGB (236, 126, 38),
				BackgroundColor = UIColor.Clear
			};
			AddSubview (StartsLabel);


			ReminderIcon = new UIImageView (new RectangleF (274, 5, 14, 14));
			ReminderIcon.Image = UIImage.FromBundle ("/Images/EventsTable/ReminderIcon");
			AddSubview (ReminderIcon);


//			SubscribableManager.ReminderSet += (sender, e) => {
//				UpdateCell(CalEvent);
//
//			};
		}

		public void UpdateCell (CalendarEvent calEvent)
		{
			CalEvent = calEvent;
			TitleLabel.Text = calEvent.Title;
			if (calEvent.AllDay) {
				StartsLabel.Text = "ALL DAY";
			} else {
				StartsLabel.Text = calEvent.StartDateTime.ToString ("t").ToUpper ();
			}
			ReminderIcon.Hidden = !LocalNotificationUtil.IsThereReminderForEvent (CalEvent);

		}

		public override void Draw (RectangleF rect)
		{
			base.Draw (rect);

		}

		public override void LayoutSubviews ()
		{

		}
	}
}

