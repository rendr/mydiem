using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MyDiem.Core.BL;
using MyDiem.Core.ViewModel;
using MonkeyArms;
using MyDiem.Core.Interfaces;
using CustomEvents;

namespace MyDiem.UI.Touch.Views.CalendarEventList
{
	public class CalendarEventListTableSource:UITableViewSource, IEventsTableSource
	{
		string cellIdentifier = "TableCell";
		string[] keys;
		public NSIndexPath PathToToday;
		protected Dictionary<string, List<CalendarEvent>> Days;

		public CalendarEventListTableSource (List<CalendarEvent> events)
		{
			//TODO: Get rid of reference to EventLisViewModel
			Days = DI.Get<EventListViewModel> ().InitDaysDictionary (events);

			PathToToday = NSIndexPath.Create (DI.Get<EventListViewModel> ().TodaySectionIndex, DI.Get<EventListViewModel> ().TodayRowIndex);
			keys = Days.Keys.ToArray ();

			DI.RequestMediator (this);

		}

		#region IEventsTableSource implementation

		public event EventHandler EventSelected;

		private List<CalendarEvent> items;

		public List<CalendarEvent> TableItems {
			get {
				return items;
			}
			set {
				items = value;
			}
		}

		#endregion

		public override int NumberOfSections (UITableView tableView)
		{
			return keys.Length;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			return Days [keys [section]].Count;

		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			return keys [section];
		}

		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			EventsTableCell cell = tableView.DequeueReusableCell (cellIdentifier) as EventsTableCell;
			if (cell == null)
				cell = new EventsTableCell (cellIdentifier);
			cell.UpdateCell (Days [keys [indexPath.Section]] [indexPath.Row]);
			return cell;

		}

		public override void RowSelected (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			EventSelected (this, new SelectedCalendarEventArgs (Days [keys [indexPath.Section]] [indexPath.Row]));
			tableView.DeselectRow (indexPath, true);

		}

		public override float GetHeightForHeader (UITableView tableView, int section)
		{
			return 30;
		}

		public override UIView GetViewForHeader (UITableView tableView, int section)
		{
			return new CalendarEventListHeader (new RectangleF (0, 0, 320, GetHeightForHeader (tableView, section)), keys [section]);
		}

		public override float GetHeightForFooter (UITableView tableView, int section)
		{
			return 25;
		}

		public override UIView GetViewForFooter (UITableView tableView, int section)
		{
			return new UIView ();
		}
	}
}

