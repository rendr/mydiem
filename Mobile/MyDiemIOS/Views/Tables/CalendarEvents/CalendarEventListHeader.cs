using System;
using System.Drawing;

using MonoTouch.UIKit;

namespace MyDiem.UI.Touch.Views.CalendarEventList
{
	public class CalendarEventListHeader:UIView
	{
		public CalendarEventListHeader (RectangleF frame, string dateString):base(frame)
		{
			//TODO: This breaks in UK cause their dates don't have commmas... 
			AddSubview(new UILabel(new RectangleF(10,7,320,20)){
				Text = dateString.Split(',')[0] + " - " + dateString.Split(',')[1] + " - " + dateString.Split(',')[2],
				Font = UIFont.FromName("OpenSans-Semibold", 17),
				TextColor = UIColor.FromRGB(96,96,96)
			});

			/*
			AddSubview(new UILabel(new RectangleF(120,7,320,20)){
				Text = dateString.Split(',')[1],
				Font = UIFont.FromName("OpenSans-Semibold", 17),
				TextColor = UIColor.FromRGB(154,154,154)
			});
			*/
		}
	}
}

