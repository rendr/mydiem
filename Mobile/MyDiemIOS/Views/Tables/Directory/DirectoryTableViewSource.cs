using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.UIKit;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.ViewModel;

namespace MyDiem.UI.Touch.Views.Directory
{
	public class DirectoryTableViewSource:UITableViewSource
	{
		public Action<Parent> ParentSelected;
		protected Dictionary<string, List<Parent>> TableIndex;
		protected string[] keys;
		protected string cellidentifier = "DirectoryTableCell";
		List<Parent> contacts;

		public List<Parent> Contacts {
			get {
				return contacts;
			}
			set {
				contacts = value;
				if (contacts != null) {
					BuildIndex ();
				} else {
					keys = new string[0];
				}
			}
		}

		public DirectoryTableViewSource ()
		{
	

		}

		public override int NumberOfSections (UITableView tableView)
		{
			return keys.Length;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			return TableIndex [keys [section]].Count;
		}

		public override string[] SectionIndexTitles (UITableView tableView)
		{
			return keys;
		}

		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (cellidentifier);
			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Subtitle, cellidentifier);
			}
			Parent p = TableIndex [keys [indexPath.Section]] [indexPath.Row];
			cell.TextLabel.Text = String.Format ("{0}, {1}", p.LastName, p.FirstName);
			cell.DetailTextLabel.Text = String.Join (", ", p.Children.Select (c => (c.FirstName + " " + c.LastName)).ToList ().ToArray ());
			cell.DetailTextLabel.Alpha = .5f;

			return cell;
		}

		protected void BuildIndex ()
		{
			

			TableIndex = new Dictionary<string, List<Parent>> ();
			foreach (var t in Contacts) {
				if (TableIndex.ContainsKey (t.LastName [0].ToString ())) {
					TableIndex [t.LastName [0].ToString ()].Add (t);
				} else {
					TableIndex.Add (t.LastName [0].ToString (), new List<Parent> () { t });
				}
			}
			keys = TableIndex.Keys.ToArray ();
		}

		public override void RowSelected (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			ParentSelected (TableIndex [keys [indexPath.Section]] [indexPath.Row]);
			tableView.DeselectRow (indexPath, true);
		}
	}
}

