using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.ViewModel;
using System.Text.RegularExpressions;

namespace MyDiem.UI.Touch.Views.Directory
{
	public class DirectoryDetailsTableViewSource:UITableViewSource
	{
		protected Parent Parent;

		public DirectoryDetailsTableViewSource (Parent parent)
		{
			Parent = parent;
		}

		public override int NumberOfSections (UITableView tableView)
		{
			return 2;
		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			if (section == 0) {
				return Parent.DirectoryTitle;
			} else {
				return "Children/Position";
			}
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			if (section == 0) {
				return 6;
			} else {
				return Parent.Children.Count;
			}
		}

		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			var cellidentifier = "DetailsCell";
			UITableViewCell cell = tableView.DequeueReusableCell (cellidentifier);
			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Value1, cellidentifier);
			}
			if (indexPath.Section == 0) {
				switch (indexPath.Row) {
				
				case 0:
					cell.TextLabel.Text = "Name";
					cell.DetailTextLabel.Text = Parent.FirstName + " " + Parent.LastName;
					break;
				case 1:
					cell.TextLabel.Text = "Cell/Work";

					cell.DetailTextLabel.Text = Parent.Phone1;
					break;
				case 2:
					cell.TextLabel.Text = "Home";
					cell.DetailTextLabel.Text = Parent.Phone2;
					break;

				case 3:
					cell.TextLabel.Text = "Email";
					cell.DetailTextLabel.Text = Parent.Email;
					break;

				case 4:
					cell.TextLabel.Text = "Address";
					cell.DetailTextLabel.Text = Parent.Address;
					break;
				case 5:
					cell.TextLabel.Text = "";
					cell.DetailTextLabel.Text = String.Format ("{0}, {1} {2}", Parent.City, Parent.State, Parent.Zip);
					break;
				
				}

			} else {
				Child child = Parent.Children [indexPath.Row];
				cell.TextLabel.Text = String.Format ("{0} {1}", child.FirstName, child.LastName);
				cell.TextLabel.AdjustsFontSizeToFitWidth = true;
				if (!string.IsNullOrEmpty (child.LastName)) {
					cell.DetailTextLabel.Text = "Grade " + child.Grade;
				}
			}

			return cell;
		}

		public override void RowSelected (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			if (indexPath.Section == 0) {
				switch (indexPath.Row) {
				case 1:
					if (IsUnListed (Parent.Phone1)) {
						tableView.DeselectRow (indexPath, false);
					} else {
						PhoneCall (Parent.Phone1);
					}
					break;
				case 2:
					if (IsUnListed (Parent.Phone2)) {
						tableView.DeselectRow (indexPath, false);
					} else {
						PhoneCall (Parent.Phone2);
					}
					break;

				case 3:
					if (IsUnListed (Parent.Email)) {
						tableView.DeselectRow (indexPath, false);
					} else {
						Email (Parent.Email);
					}
					break;

				case 4:
				case 5:
					Map (Parent.Address, Parent.Zip);
					break;
				}
			}
			tableView.DeselectRow (indexPath, false);
		}

		bool IsUnListed (string value)
		{
			return false;
//			return value == Parent.NOT_LISTED;
		}

		void Email (string email)
		{
			NSUrl url = new NSUrl (String.Format ("mailto:{0}", email));
			if (!UIApplication.SharedApplication.OpenUrl (url)) {
				var av = new UIAlertView ("Not supported"
				                         , "Email is not supported on this device"
				                         , null
				                         , "Ok"
				                         , null);
				av.Show ();
			}
		}

		void PhoneCall (string number)
		{
			NSUrl url = new NSUrl (String.Format ("tel:{0}", Parent.FormatNumberForIOSCall (number)));
			if (!UIApplication.SharedApplication.OpenUrl (url)) {
				var av = new UIAlertView ("Not supported"
				                         , "Phone calls are not supported on this device"
				                         , null
				                         , "Ok"
				                         , null);
				av.Show ();
			}
		}

		void Map (string address, string zip)
		{
			address = address.Replace (" ", "+");
			NSUrl url = new NSUrl (String.Format ("http://maps.apple.com/?q={0}+{1}", address, zip));
			if (!UIApplication.SharedApplication.OpenUrl (url)) {
				var av = new UIAlertView ("Not supported"
				                         , "Maps are not supported on this device"
				                         , null
				                         , "Ok"
				                         , null);
				av.Show ();
			}
		}
	}
}

