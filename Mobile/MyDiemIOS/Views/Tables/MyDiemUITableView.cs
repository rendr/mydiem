using System;
using MonoTouch.UIKit;
using MyDiem.UI.Touch;
using System.Drawing;

namespace Views.Tables
{
	public class MyDiemUITableView:UITableView
	{
		public MyDiemUITableView (RectangleF frame):base(frame)
		{
			if (BaseViewController.IsIOS7) {
				this.SeparatorInset = UIEdgeInsets.Zero;
			}

		}

		public MyDiemUITableView(RectangleF frame, UITableViewStyle style):base(frame, style){
			if (BaseViewController.IsIOS7) {
				this.SeparatorInset = UIEdgeInsets.Zero;
			}
		}
	}
}

