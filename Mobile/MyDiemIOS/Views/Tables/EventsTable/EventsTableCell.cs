using System;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.UI.Touch;
using MyDiem.UI.Touch.Util;
using MonkeyArms;

namespace MyDiem.UI.Touch.Views.EventsTable
{
	public class EventsTableCell:UITableViewCell
	{
		UILabel TitleLabel, StartsLabel, EndsLabel;
		UIImageView ReminderIcon;
		UIView ColorSwatch;
		protected CalendarEvent CalEvent;

		public EventsTableCell (string cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			Opaque = false;
			BackgroundColor = UIColor.Clear;
			SelectedBackgroundView = new UIView ();

			TitleLabel = new UILabel (new RectangleF (90, 12, 175, 15)) {
				Font = UIFont.FromName ("Helvetica", 12),
				TextColor = UIColor.FromRGB (67, 67, 67),
				BackgroundColor = UIColor.Clear
			};
			AddSubview (TitleLabel);

			EndsLabel = new UILabel (new RectangleF (90, 26, 325, 15)) {
				Font = UIFont.BoldSystemFontOfSize (9),
				TextColor = UIColor.FromRGB (171, 171, 171),
				BackgroundColor = UIColor.Clear
			};
			AddSubview (EndsLabel);

			StartsLabel = new UILabel (new RectangleF (17, 20, 325, 15)) {
				Font = UIFont.BoldSystemFontOfSize (12),
				TextColor = UIColor.FromRGB (115, 115, 115),
				BackgroundColor = UIColor.Clear,
				LineBreakMode = UILineBreakMode.CharacterWrap,
				Lines = 2
			};
			AddSubview (StartsLabel);


			ReminderIcon = new UIImageView (new RectangleF (274, 20, 14, 14));
			ReminderIcon.Image = UIImage.FromBundle ("/Images/EventsTable/ReminderIcon");
			AddSubview (ReminderIcon);
			ReminderIcon.Hidden = true;


			ColorSwatch = new UIView (new RectangleF (74, 15, 10, 10));
			AddSubview (ColorSwatch);

//			SubscribableManager.ReminderSet += (sender, e) => {
//				UpdateCell(CalEvent);
//
//			};

		}

		public void UpdateCell (CalendarEvent calEvent)
		{
			CalEvent = calEvent;
			TitleLabel.Text = calEvent.Title;
			EndsLabel.Text = "ENDS " + calEvent.EndDateTime.ToString ("t").ToUpper ();
			StartsLabel.Text = calEvent.StartDateTime.ToString ("t").ToUpper ();
			string color = DI.Get<UserManager> ().GetSubscriptionForCalendar (Convert.ToInt32 (calEvent.CalendarID)).Color;
			ColorSwatch.BackgroundColor = ColorUtil.UIColorFromHex (uint.Parse (color.Substring (0, 6), System.Globalization.NumberStyles.AllowHexSpecifier));

			ReminderIcon.Hidden = !LocalNotificationUtil.IsThereReminderForEvent (CalEvent);


			if (calEvent.AllDay) {
				StartsLabel.Frame = new RectangleF (17, 0, 50, 50);
				StartsLabel.Text = "ALL\rDAY";
				StartsLabel.TextAlignment = UITextAlignment.Center;
				StartsLabel.TextColor = UIColor.FromRGB (236, 126, 38);
				EndsLabel.Hidden = true;
			} else {
				StartsLabel.Frame = new RectangleF (17, 20, 325, 15);
				StartsLabel.TextAlignment = UITextAlignment.Left;
				StartsLabel.TextColor = UIColor.FromRGB (115, 115, 115);
				EndsLabel.Hidden = false;
			}
		}

		public override void Draw (RectangleF rect)
		{
			base.Draw (rect);
			if (!BaseViewController.IsIOS7) {
				var context = UIGraphics.GetCurrentContext ();

				UIColor.FromRGB (191, 191, 191).SetStroke ();

				CGPath path = new CGPath ();
				path.AddLines (new PointF[] {
					new PointF (0, Bounds.Height - 1),
					new PointF (Frame.Width, Bounds.Height - 1)

				});
				path.CloseSubpath ();
				context.AddPath (path);
				context.DrawPath (CGPathDrawingMode.Stroke);
				context.SaveState ();
			}
		}

		public override void LayoutSubviews ()
		{

		}
	}
}

