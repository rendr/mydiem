using System;
using System.Collections.Generic;
using MonoTouch.UIKit;
using MyDiem.Core.BL;
using MyDiem.Core.ViewModel;
using MyDiem.Core.Interfaces;
using MonoTouch.MediaPlayer;
using MonkeyArms;
using CustomEvents;

namespace MyDiem.UI.Touch.Views.EventsTable
{
	public class CalendarEventsTableSource:UITableViewSource, IEventsTableSource
	{
		public CalendarEvent SelectedEvent;
		List<CalendarEvent> tableItems;
		string cellIdentifier = "TableCell";

		public CalendarEventsTableSource (List<CalendarEvent> items)
		{
			tableItems = items;
			DI.RequestMediator (this);
		}

		#region IEventsTableSource implementation

		public event EventHandler EventSelected = delegate {};

		public List<CalendarEvent> TableItems {
			get {
				return tableItems;
			}
			set {
				tableItems = value;
			}
		}

		#endregion

		public override int RowsInSection (UITableView tableview, int section)
		{
			return tableItems.Count;
		}

		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			EventsTableCell cell = tableView.DequeueReusableCell (cellIdentifier) as EventsTableCell;
			if (cell == null)
				cell = new EventsTableCell (cellIdentifier);
			cell.UpdateCell (tableItems [indexPath.Row]);
			return cell;
		}

		public override void RowSelected (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{

			EventSelected (this, new SelectedCalendarEventArgs (tableItems [indexPath.Row]));
			tableView.DeselectRow (indexPath, false);
		}
	}
}

