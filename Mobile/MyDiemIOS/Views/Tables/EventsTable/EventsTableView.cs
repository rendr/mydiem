using System;
using System.Drawing;

using MonoTouch.UIKit;
using Views.Tables;

namespace MyDiem.UI.Touch.Views.EventsTable
{
	public class EventsTableView:MyDiemUITableView
	{
		public EventsTableView (RectangleF frame):base(frame)
		{
			SeparatorColor = UIColor.FromRGB(191,191,191);
			RowHeight = 58;

		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();


		}
	}
}

