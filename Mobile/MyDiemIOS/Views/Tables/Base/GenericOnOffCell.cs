using System;
using System.Drawing;

using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace MyDiem.UI.Touch.Views.Global
{
	public class GenericOnOffCell:BaseGenericCell
	{
		UISwitch OnOffSwitch;

		public GenericOnOffCell (string cellId) : base (cellId)
		{

			OnOffSwitch  = new UISwitch(new RectangleF(200,8,80,30)){
				
			};
			
			OnOffSwitch.ValueChanged += (sender, e) => {
				EntryModel.Value = OnOffSwitch.On.ToString();
				DispatchEditingBegan(e);

			};

			AddSubview(OnOffSwitch);

			EntryLabel.Frame = new RectangleF(20,0, 125, 40);
		}

		override public void UpdateCell (GenericTableCellViewModel entryModel)
		{
			base.UpdateCell (entryModel);
			OnOffSwitch.On = (entryModel.Value == "True");

		}
	}
}

