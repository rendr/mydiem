using System;
using System.Collections.Generic;

using MonoTouch.UIKit;
using MonoTouch.Foundation;

namespace MyDiem.UI.Touch.Views.Global
{
	public class GenericTableSource:UITableViewSource
	{

		public event EventHandler CellBeganEditing = delegate{};
		public event EventHandler TableEditingEnded = delegate{};
		public event EventHandler CellSelected = delegate{};



		List<GenericTableCellViewModel> tableItems;
		string cellIdentifier = "TableCell";

		public float RowHeight = 48f;

		public IGenericCell[] cellCache;

		public int SelectedRowIndex = -1;

		protected bool Editable;

		public GenericTableSource (List<GenericTableCellViewModel> entries, bool editable)
		{
			tableItems = entries;
			Editable = editable;
			cellCache = new IGenericCell[entries.Count];
		}

		public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
		{
			return RowHeight;
		}



		public override int RowsInSection (UITableView tableview, int section)
		{
			return tableItems.Count;
		}

		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
//			MyDiemEditableTableCell cell = tableView.DequeueReusableCell(cellIdentifier) as MyDiemEditableTableCell;
//			if(cell == null)
			GenericTableCellViewModel model = tableItems [indexPath.Row];

			IGenericCell cell;
			if (model.IsCounty || model.IsRegion) {
				cell = new GenericPickerCell (cellIdentifier);
			} else if (model.IsOnOffSwitch) {
				cell = new GenericOnOffCell (cellIdentifier);
			} else {
				cell = new GenericTextCell (cellIdentifier);
//				(cell as UserProfileTextCell).EntryInput.Enabled = Editable;
			}

			cell.UpdateCell(model);
			cellCache[indexPath.Row] = cell;

			cell.RowIndex = indexPath.Row;

			(cell as BaseGenericCell).EditingBegan += (sender, e) => {
				CellBeganEditing(sender, e);
			};
			(cell as BaseGenericCell).EditingEnded += (sender, e) => {

				MakeNextCellResponder((sender as GenericTextCell).RowIndex + 1);
			};



			return cell as UITableViewCell;
		}
		void MakeNextCellResponder (int cellIndex)
		{
			if(cellIndex < cellCache.Length){
				if(tableItems[cellIndex].IsRegion || tableItems[cellIndex].IsCounty){
					HandleLocationHasFocus(cellIndex);
				}else{
					IGenericCell nextCell = cellCache[cellIndex];
					nextCell.BecomeFirstResponder();
				}

			}else{
				TableEditingEnded(this, new EventArgs());
			}
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
//			if (!Editable) {
//				return;
//			}

			tableView.DeselectRow(indexPath,true);
			if(tableItems[indexPath.Row].IsRegion || tableItems[indexPath.Row].IsCounty){
				HandleLocationHasFocus(indexPath.Row);
			}

//			if(tableItems[indexPath.Row].Enabled){
//				return;
//			}

			SelectedRowIndex = indexPath.Row;
			CellSelected(this, new EventArgs());
		}

		protected void HandleLocationHasFocus(int index)
		{
			var cell = cellCache [index];
			CellBeganEditing (cell, new EventArgs ());
			BaseGenericCell.CloseAllKeyboards();
		}

		public List<GenericTableCellViewModel> GetUpdatedTableItems()
		{
			List<GenericTableCellViewModel> items = new List<GenericTableCellViewModel>();
			foreach(BaseGenericCell cell in cellCache){
				items.Add(cell.EntryModel);
			}
			return items;
		}

	}
}

