using System;
using MyDiem.UI.Touch.Views.Global;

namespace MyDiem.UI.Touch
{
	public interface IGenericCell
	{
		int RowIndex{ get; set; }
		void UpdateCell(GenericTableCellViewModel entryModel);
		bool BecomeFirstResponder();
	}
}

