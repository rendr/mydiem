using System;
using System.Drawing;

using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace MyDiem.UI.Touch.Views.Global
{
	public class GenericPickerCell:BaseGenericCell
	{
		UILabel ValueLabel;

		public GenericPickerCell (string cellId) : base (cellId)
		{
			ValueLabel = new UILabel(new RectangleF(0, 0, 260, Bounds.Height)){
				Font = UIFont.FromName("OpenSans-Italic", 14), 
				TextAlignment = UITextAlignment.Right,
				TextColor = UIColor.FromRGB(126,126,126),
				BackgroundColor = UIColor.Clear
			};
			AddSubview (ValueLabel);

		}

		public override void UpdateCell (GenericTableCellViewModel entryModel)
		{
			base.UpdateCell (entryModel);
			EntryLabel.Text = entryModel.Label;
			ValueLabel.Text = entryModel.Value;
		}
	}
}

