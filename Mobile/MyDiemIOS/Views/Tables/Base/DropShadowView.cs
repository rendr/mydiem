using System;
using System.Drawing;

using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace MyDiem.UI.Touch.Views.Global
{
	public class DropShadowView:UIView
	{



		public DropShadowView (RectangleF frame):base(new RectangleF(frame.X - 5, frame.Y - 5, frame.Width + 10, frame.Height + 10))
		{
			BackgroundColor = UIColor.Clear;


		}

		public override void Draw(RectangleF rect)
		{
			base.LayoutSubviews ();

		

	
			var ctx = UIGraphics.GetCurrentContext();
			ctx.ClearRect(Bounds);


	        SizeF shadowSize = new SizeF (0f, 3f);
			UIColor.White.SetFill();
	        //ctx.SetRGBFillColor (194f / 255f, 212f / 255f, 238f / 255f, 1f);
	        ctx.SetAllowsAntialiasing (true);
	        ctx.SetShadowWithColor (shadowSize, 5, UIColor.FromRGBA(0,0,0,.5f).CGColor);
	        RectangleF area = new RectangleF (5, 5, Bounds.Width - 10, Bounds.Height - 10);
	        ctx.FillRect (area);

	        

		}
	}
}

