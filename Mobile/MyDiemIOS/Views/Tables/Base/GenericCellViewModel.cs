using System;

namespace MyDiem.UI.Touch.Views.Global
{
	public class GenericTableCellViewModel
	{
		public event EventHandler ValueChanged = delegate{};

		public string Label;
		public string PlaceHolder;
		string vmValue;

		public string Value {
			get {
				return vmValue;
			}
			set {
				vmValue = value;
				ValueChanged (this, EventArgs.Empty);
			}
		}

		public bool IsPassword;
		public bool IsRegion;
		public bool IsCounty;
		public bool IsOnOffSwitch;
		public bool Enabled;

		public GenericTableCellViewModel (string label, string placeholder, string value, bool isPassword = false, bool isRegion = false, bool isCounty = false, bool isOnOffSwitch = false, bool enabled = true)
		{
			this.Label = label;
			this.PlaceHolder = placeholder;
			this.vmValue = value;
			this.IsPassword = isPassword;
			this.IsRegion = isRegion;
			this.IsCounty = isCounty;
			this.IsOnOffSwitch = isOnOffSwitch;
			this.Enabled = enabled;
		}
	}
}

