using System;
using System.Drawing;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace MyDiem.UI.Touch.Views.Global
{
	public class GenericTextCell:BaseGenericCell
	{
		public UITextField EntryInput;

		public GenericTextCell (string cellId) : base (cellId)
		{


			EntryInput = new UITextField (new RectangleF (0, 0, 260, Bounds.Height)) {
				Font = UIFont.FromName ("OpenSans-Italic", 14), 
				TextAlignment = UITextAlignment.Right,
				VerticalAlignment = UIControlContentVerticalAlignment.Center,
				TextColor = UIColor.FromRGB (126, 126, 126),
				BackgroundColor = UIColor.Clear
			};
			EntryInput.ShouldReturn = delegate {
				EntryInput.ResignFirstResponder ();
				DispatchEditinedEnded ();
				return true;
			};
			EntryInput.EditingDidBegin += (sender, e) => {
				DispatchEditingBegan (e);
			};
			EntryInput.EditingDidEnd += (sender, e) => {
				EntryModel.Value = EntryInput.Text;
				EntryInput.ResignFirstResponder ();
			};
			EntryInput.EditingChanged += (sender, e) => {
				EntryModel.Value = EntryInput.Text;
			};
			AddSubview (EntryInput);

			CloseAll += (sender, e) => {
				EntryInput.ResignFirstResponder ();
			};


		}

		override public bool BecomeFirstResponder ()
		{
			return EntryInput.BecomeFirstResponder ();

		}

		override public void UpdateCell (GenericTableCellViewModel entryModel)
		{


			base.UpdateCell (entryModel);


//			EntryLabel.Frame = new RectangleF(20,0, 75, 40);


			EntryInput.Placeholder = entryModel.PlaceHolder;

			EntryInput.Text = entryModel.Value;
			EntryInput.SecureTextEntry = entryModel.IsPassword;
			EntryInput.ReturnKeyType = UIReturnKeyType.Next;


			EntryInput.Enabled = entryModel.Enabled;

		}
	}
}

