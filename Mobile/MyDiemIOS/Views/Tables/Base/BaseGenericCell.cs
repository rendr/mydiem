using System;
using System.Drawing;

using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace MyDiem.UI.Touch.Views.Global
{
	public class BaseGenericCell:UITableViewCell, IGenericCell
	{

		public event EventHandler EditingBegan = delegate{};
		public event EventHandler EditingEnded = delegate{};

		public static event EventHandler CloseAll = delegate{};

		public GenericTableCellViewModel EntryModel;
		protected UILabel EntryLabel;

		private int _rowIndex;

		public int RowIndex {
			get {
				return _rowIndex;
			}
			set {
				_rowIndex = value;
			}
		}

		public BaseGenericCell (string cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			Opaque = false;
			BackgroundColor = UIColor.Clear;
			
			SelectedBackgroundView = new UIView();



			EntryLabel = new UILabel(new RectangleF(20,0, Frame.Width, Frame.Height)){
				Font = UIFont.FromName("Open Sans", 14), 
				TextColor = UIColor.FromRGB(126,126,126),
				BackgroundColor = UIColor.Clear,
				Enabled =  false
			};
			AddSubview(EntryLabel);
		}

		public void DispatchEditinedEnded ()
		{
			EditingEnded(this, new EventArgs());
		}

		public void DispatchEditingBegan (EventArgs e)
		{
			EditingBegan(this, e);
		}

		public virtual bool BecomeFirstResponder()
		{

			return false;
		}

		public static void CloseAllKeyboards()
		{
			CloseAll(null, null);
		}

		public virtual void UpdateCell(GenericTableCellViewModel entryModel)
		{
			EntryModel = entryModel;
			EntryLabel.Text = entryModel.Label;
		}
//
//		public override void Draw (RectangleF rect)
//		{
//			base.Draw (rect);
//			var context = UIGraphics.GetCurrentContext();
//			
//			UIColor.FromRGB(191,191,191).SetStroke();
//			
//			CGPath path = new CGPath();
//			path.AddLines(new PointF[]{
//				new PointF(0,Bounds.Height-1),
//				new PointF(Frame.Width, Bounds.Height-1),
//				new PointF(Frame.Width, Bounds.Height),
//				new PointF(0,Bounds.Height)
//					
//			});
//			path.CloseSubpath();
//			context.AddPath(path);
//			context.DrawPath(CGPathDrawingMode.Stroke);
//			context.SaveState();
//		}



		public override void LayoutSubviews ()
		{
			
		}
	}
}

