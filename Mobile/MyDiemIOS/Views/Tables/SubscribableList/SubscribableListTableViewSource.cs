using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.UIKit;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.ViewModel;

namespace MyDiem.UI.Touch.Views.SubscribableList
{
	public class SubscribableListTableViewSource:UITableViewSource
	{
		Dictionary<string, List<Subscribable>> TableItems;
		string[] keys;
		string cellIdentifier = "TableCell";
		public Action<Subscribable> SubscribableSelected;
		List<SchoolGroup> categories;

		public List<SchoolGroup> Categories {
			get {
				return categories;
			}
			set {
				categories = value;
				TableItems = new Dictionary<string, List<Subscribable>> ();


				foreach (SchoolGroup calendarCategory in categories) {
					if (!TableItems.ContainsKey (calendarCategory.Title)) {
						TableItems.Add (calendarCategory.Title, new List<Subscribable> ());
					}
					foreach (Subscribable calendar in calendarCategory.Subscribables.OrderBy(o => o.title)) { 
						TableItems [calendarCategory.Title].Add (calendar);
					}
				}
				keys = TableItems.Keys.ToArray ();
			}
		}

		public SubscribableListTableViewSource ()
		{

		}

		public override int NumberOfSections (UITableView tableView)
		{
			return keys.Length;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			return TableItems [keys [section]].Count;

		}

		public override UIView GetViewForHeader (UITableView tableView, int section)
		{
			return new SubscribableListSectionHeader (keys [section]);
		}

		public override float GetHeightForHeader (UITableView tableView, int section)
		{
			return 44f;
		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			return keys [section];
		}

		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			SubscribablesListCell cell;
			if (TableItems [keys [indexPath.Section]].Count - 1 == indexPath.Row) {
				cell = tableView.DequeueReusableCell (cellIdentifier + "EndCap") as SubscribablesListCell;
				if (cell == null)
					cell = new SubscribablesListCell (cellIdentifier + "EndCap");
			} else {
				cell = tableView.DequeueReusableCell (cellIdentifier) as SubscribablesListCell;
				if (cell == null)
					cell = new SubscribablesListCell (cellIdentifier);
			}

			cell.UpdateCell (TableItems [keys [indexPath.Section]] [indexPath.Row]);

			if (TableItems [keys [indexPath.Section]].Count - 1 == indexPath.Row) {
				cell.IsLastCellInSection = true;
			} else {
				cell.IsLastCellInSection = false;
			}
			return cell;

		}

		public override void RowSelected (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{

			SubscribableSelected (TableItems [keys [indexPath.Section]] [indexPath.Row]);
//			SelectedSubscribable = TableItems[keys[indexPath.Section]][indexPath.Row];
//
//			if (SelectedSubscribable.isPasswordProtected) {
//				SubscribableListViewModel.SelectPasswordProtectedSubscribable(SelectedSubscribable);
//			}else if(SelectedSubscribable.isEmailProtected){
//				SubscribableListViewModel.SelectEmailProtectedSubscribable(SelectedSubscribable);
//			}else if(SelectedSubscribable.isPremium && !UserManager.IsCalendarUsers(SelectedSubscribable.ID)){
//				SubscribableListViewModel.SelectPremiumSubscribable(SelectedSubscribable);
//			}else {
//				SubscribableListViewModel.SelectSubscribable (SelectedSubscribable);
//			}
			tableView.DeselectRow (indexPath, false);
		}
	}
}

