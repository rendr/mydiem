using System;
using System.Drawing;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.UI.Touch.Views.Global;
using System.Diagnostics;

namespace MyDiem.UI.Touch.Views.SubscribableList
{
	public class SubscribablesListCell:UITableViewCell
	{
		UILabel Label;
		UIImageView LockIcon, Arrow, CalendarIcon, DirectoryIcon;
		public bool IsLastCellInSection = false;
		public static int NumberOfCellInstances = 0;
		UILabel PriceLabel;

		public SubscribablesListCell (string cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			NumberOfCellInstances++;

//			Debug.WriteLine("Number of Cell instances: " + NumberOfCellInstances);

			Opaque = false;
			BackgroundColor = UIColor.White;

	
			SelectedBackgroundView = new UIView () {
				BackgroundColor = UIColor.White
			};


			Label = new UILabel (new RectangleF (55, 0, 180, Bounds.Height)) {
				Font = UIFont.FromName ("OpenSans", 12),
				TextColor = UIColor.FromRGB (126, 126, 126),
				BackgroundColor = UIColor.Clear,
				Opaque = false
			};

			AddSubview (Label);




			Arrow = new UIImageView (new RectangleF (280, 14, 9, 14));
			Arrow.Image = UIImage.FromBundle ("/Images/SubscribableList/Arrow");
			AddSubview (Arrow);

			LockIcon = new UIImageView (new RectangleF (277, 14, 12, 14));
			LockIcon.Image = UIImage.FromBundle ("/Images/SubscribableList/LockIcon");
			AddSubview (LockIcon);

			CalendarIcon = new UIImageView (new RectangleF (28, 11, 21, 21));
			CalendarIcon.Image = UIImage.FromBundle ("/Images/SubscribableList/CalendarIcon");
			AddSubview (CalendarIcon);

			DirectoryIcon = new UIImageView (new RectangleF (28, 11, 21, 21));
			DirectoryIcon.Image = UIImage.FromBundle ("/Images/SubscribableList/DirectoryIcon");
			AddSubview (DirectoryIcon);

			PriceLabel = new UILabel (new RectangleF (Bounds.Width - 70, 0, 50, Bounds.Height)) {
				Font = UIFont.FromName ("OpenSans", 12),
				TextColor = UIColor.FromRGB (126, 126, 126),
				BackgroundColor = UIColor.Clear,
				Opaque = false
			};
			
			AddSubview (PriceLabel);

		}

		public void UpdateCell (Subscribable subscribable)
		{
			Label.Text = subscribable.title;

			LockIcon.Hidden = Arrow.Hidden = PriceLabel.Hidden = true;


			CalendarIcon.Hidden = (subscribable.type != Subscribable.TYPE_CALENDAR);
			DirectoryIcon.Hidden = (subscribable.type != Subscribable.TYPE_DIRECTORY);
			LockIcon.Hidden = true;

			if (subscribable.isPremium) {
				Console.WriteLine ("test");
			}

//			if (subscribable.isPasswordProtected || subscribable.isEmailProtected && !subscribable.isPremium) {
//				LockIcon.Hidden = false;
//			} else if(subscribable.isPremium && !UserManager.IsCalendarUsers(subscribable.ID)){
//				PriceLabel.Hidden = false;
//				PriceLabel.Text = "$" + subscribable.price;
//			}else {
//				Arrow.Hidden = false;
//			}


		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();


		}
	}
}

