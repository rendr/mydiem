using System;
using System.Drawing;

using MonoTouch.UIKit;

using MyDiem.UI.Touch.Views.Global;

namespace MyDiem.UI.Touch.Views.SubscribableList
{
	public class SubscribableListSectionHeader:UIView
	{

		public SubscribableListSectionHeader (string title):base(new RectangleF(0,0,293,46))
		{

			BackgroundColor = UIColor.Clear;
			Opaque = false;

			UILabel TitleLabel = new UILabel(new RectangleF(25,5,Frame.Width-20, Frame.Height)){
				BackgroundColor = UIColor.Clear,
				Opaque = false,
				Font = UIFont.FromName("OpenSans-Semibold", 12),
				TextColor = UIColor.FromRGB(81,81,81),
				Text = title
			};
			AddSubview(TitleLabel);






		}


	}
}

