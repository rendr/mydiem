using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.UIKit;
using MyDiem.UI.Touch.Views.SubscribableList;
using MyDiem.Core.BL;

namespace MyDiem.UI.Touch.Views.CalendarSettings
{
	public class SubscribableSettingsTableViewSource:UITableViewSource
	{
		public event EventHandler ColorClicked = delegate{};
		public event EventHandler SubscriptionChanged = delegate{};

		Dictionary<string, List<SubscribableSettingsOptionModel>> Options;
		public Subscription SelectedSubscription;
		public Subscribable SelectedSubscribable;
		public string SelectedColor;
		string cellIdentifier = "TableCell";
		UITableView TableView;
		string[] keys;
		CalendarSettingsTableCell ColorCell;

		public SubscribableSettingsTableViewSource (Subscribable subscribable, Subscription subscription)
		{
			SelectedSubscribable = subscribable;
			SelectedSubscription = subscription;
			Options = new Dictionary<string, List<SubscribableSettingsOptionModel>> ();

			string VIEW_OPTIONS = "Options";


			Options.Add (VIEW_OPTIONS, new List<SubscribableSettingsOptionModel> ());
			if (!subscribable.isPremium) {
				Options [VIEW_OPTIONS].Add (new SubscribableSettingsOptionModel ("Enabled", SubscribableSettingsOptionModel.OptionType.OnOff, SelectedSubscription.Enabled));
			} else {
				Options [VIEW_OPTIONS].Add (new SubscribableSettingsOptionModel ("Hide", SubscribableSettingsOptionModel.OptionType.OnOff, SelectedSubscription.Suppressed));
			}
			if (SelectedSubscription.Enabled && subscribable.type == Subscribable.TYPE_CALENDAR) {
				Options [VIEW_OPTIONS].Add (new SubscribableSettingsOptionModel ("Choose Color", SubscribableSettingsOptionModel.OptionType.Color, false, SelectedSubscription.Color));
			}

			keys = Options.Keys.ToArray ();
		}

		public override int NumberOfSections (UITableView tableView)
		{
			return keys.Length;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			return Options [keys [section]].Count;

		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			return keys [section];
		}
		/*
		public override void CellDisplayingEnded (UITableView tableView, UITableViewCell cell, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/ios/tutorials/Events%2c_Protocols_and_Delegates 
			(cell as CalendarSettingsTableCell).ColorSwatchClicked += HandleColorSwatchClicked;
			(cell as CalendarSettingsTableCell).SwitchValueChanged -= HandleEnabledSwitchValueChanged;
			(cell as CalendarSettingsTableCell).SwitchValueChanged -= HandleSuppressSwitchValueChanged;
		}
		*/
		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			TableView = tableView;
			CalendarSettingsTableCell cell = tableView.DequeueReusableCell (cellIdentifier) as CalendarSettingsTableCell;
			var calendarSettingsOptionModel = Options [keys [indexPath.Section]] [indexPath.Row];
			if (cell == null) {
				cell = new CalendarSettingsTableCell (cellIdentifier);
				cell.ColorSwatchClicked += HandleColorSwatchClicked;
			}


			cell.SwitchValueChanged -= HandleEnabledSwitchValueChanged;
			cell.SwitchValueChanged -= HandleSuppressSwitchValueChanged;

			if (calendarSettingsOptionModel.Title == "Enabled") {
				cell.SwitchValueChanged -= HandleSuppressSwitchValueChanged;
				cell.SwitchValueChanged += HandleEnabledSwitchValueChanged;
			}
			if (calendarSettingsOptionModel.Title == "Hide") {
				cell.SwitchValueChanged -= HandleEnabledSwitchValueChanged;
				cell.SwitchValueChanged += HandleSuppressSwitchValueChanged;
			}

			cell.IsLastCellInSection = (indexPath.Row == Options [keys [indexPath.Section]].Count - 1);

			cell.Update (calendarSettingsOptionModel);
			return cell;

		}

		void HandleColorSwatchClicked (object sender, EventArgs e)
		{
			ColorCell = sender as CalendarSettingsTableCell;
			ColorClicked (sender, e);
		}

		void HandleSuppressSwitchValueChanged (object sender, EventArgs e)
		{
			SelectedSubscription.Suppressed = (sender as UISwitch).On;
//			UserManager.UpdateUserCalendarSubscription (UserManager.AppUser.ID, SelectedSubscription, CalendarColorManager.GetIDForColor (SelectedSubscription.Color));
//			EventManager.GetEventsForCalendars (UserManager.GetCalendarIDs ());
			Options ["Options"] [0].On = SelectedSubscription.Suppressed;
			TableView.ReloadData ();
		}

		void HandleEnabledSwitchValueChanged (object sender, EventArgs e)
		{
			SelectedSubscription.Enabled = (sender as UISwitch).On;
			SubscriptionChanged (this, EventArgs.Empty);
			TableView.ReloadData ();
		}

		public void UpdateColorSubscription (UITableView tableView, CalendarColor color)
		{
			Options [keys [0]] [1].Color = color.Color;
			SelectedSubscription.Color = color.Color;
			ColorCell.Update (Options [keys [0]] [1]);
			SubscriptionChanged (this, EventArgs.Empty);
		}

		public override UIView GetViewForHeader (UITableView tableView, int section)
		{
			return new SubscribableListSectionHeader ("Options");
		}

		public override float GetHeightForHeader (UITableView tableView, int section)
		{
			return 44f;
		}

		public override void RowSelected (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			tableView.DeselectRow (indexPath, false);
		}

		public void Destroy ()
		{

		}
	}
}

