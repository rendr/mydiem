using System;

namespace MyDiem.UI.Touch.Views.CalendarSettings
{
	public class SubscribableSettingsOptionModel
	{
		public enum OptionType {OnOff, Color};
		public string Title;
		public bool On;
		public OptionType Type;
		public string Color;
	

		public SubscribableSettingsOptionModel (string title, OptionType type, bool on = false, string color = "")
		{
			Title = title;
			Type = type;
			On = on;
			Color = color;
		}
	}


}

