using System;
using System.Drawing;

using System.Collections.Generic;

using MonoTouch.UIKit;

using MyDiem.Core.BL;
using MyDiem.UI.Touch;
using MyDiem.UI.Touch.Views.Global;

namespace MyDiem.UI.Touch.Views.CalendarSettings
{
	public class ColorSelectionView:UIView
	{
		public event EventHandler SelectedColorChanged = delegate{};

		public CalendarColor SelectedColor;
		protected List<UIButton> Buttons;
		protected List<CalendarColor> Colors;

		public ColorSelectionView (List<CalendarColor> colors):base(new RectangleF(0, 300,320,230))
		{
			Colors = colors;

			ClipsToBounds = true;

			int colWidth = 54;
			int rowHeight = 54;

			int curX = 0;
			int curY = 0;

			Buttons = new List<UIButton>();

			foreach(CalendarColor cc in colors){
				if(curX + colWidth>(Bounds.Width + 20)){
					curX = 0;
					curY += rowHeight;
				}

				UIButton swatchButton = new UIButton(new RectangleF(curX, curY, colWidth, rowHeight));
				Buttons.Add(swatchButton);

				swatchButton.TouchUpInside += (sender, e) => {
					SelectedColor = colors[Buttons.IndexOf(sender as UIButton)];
					SelectedColorChanged(sender, e);
				};
				swatchButton.BackgroundColor = ColorUtil.UIColorFromHex(uint.Parse(cc.Color.Substring(0,6) , System.Globalization.NumberStyles.AllowHexSpecifier));
				AddSubview(swatchButton);

				curX += colWidth;


			}
			AddSubview(new DropShadowView(new RectangleF(0,-10,320,10)));
			/*
			UIImageView topInnerShadow = new UIImageView(new RectangleF(0,0,320, 11));
				topInnerShadow.Image = UIImage.FromBundle("/Images/EventsTable/TopInnerShadow");
				AddSubview(topInnerShadow);
				*/
		}


	}
}

