using System;
using System.Drawing;
using System.Diagnostics;

using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.UI.Touch.Views.Global;
using MyDiem.UI.Touch;


namespace MyDiem.UI.Touch.Views.CalendarSettings
{
	public class CalendarSettingsTableCell:UITableViewCell
	{
		public event EventHandler SwitchValueChanged = delegate{};
		public event EventHandler ColorSwatchClicked = delegate{};

		public bool IsLastCellInSection = false;

		protected UILabel Label;

		protected UIView bottomBorder;

		protected UIButton ColorSwatch;

		protected UISwitch EnabledSwitch;

		protected UIButton ClickCatcher;



		public CalendarSettingsTableCell (string cellId) : base (UITableViewCellStyle.Default, cellId)
		{
		

			BackgroundColor = UIColor.White;


			SelectedBackgroundView = new UIView(Frame);
			Label = new UILabel(new RectangleF(35,0,200, Frame.Height)){
				Font = UIFont.FromName("OpenSans", 12),
				TextColor = UIColor.FromRGB(126,126,126),
				BackgroundColor = UIColor.Clear,
				Opaque = false
			};

			AddSubview(Label);


			EnabledSwitch = new UISwitch ();
			
			EnabledSwitch.Frame = new RectangleF (Frame.Width - EnabledSwitch.Frame.Width - 25, Frame.Height/2 - EnabledSwitch.Frame.Height/2,EnabledSwitch.Frame.Width, EnabledSwitch.Frame.Height);
			EnabledSwitch.ValueChanged += (sender, e) => {
				SwitchValueChanged(sender, new EventArgs());
			};
			AddSubview(EnabledSwitch);

			ColorSwatch = new UIButton(new RectangleF(265, 8, 26, 26));
			ColorSwatch.TouchUpInside += (sender, e) => {
				ColorSwatchClicked(this,new EventArgs());
			};
			AddSubview(ColorSwatch);


			ClickCatcher = UIButton.FromType(UIButtonType.RoundedRect);
			ClickCatcher.Frame = Frame;
			ClickCatcher.Alpha = .05f;
			ClickCatcher.Hidden = true;


			ClickCatcher.TouchUpInside += (sender, e) => {
				ColorSwatchClicked(this,new EventArgs());
			};

			AddSubview(ClickCatcher);



		}

		public void Update(SubscribableSettingsOptionModel model)
		{
			Label.Text = model.Title;
			EnabledSwitch.Hidden = !(model.Type == SubscribableSettingsOptionModel.OptionType.OnOff);
			EnabledSwitch.On = model.On;
			if(model.Color != null && model.Color != ""){
				ColorSwatch.BackgroundColor = ColorUtil.UIColorFromHex(uint.Parse(model.Color.Substring(0,6) , System.Globalization.NumberStyles.AllowHexSpecifier));
			}

			ClickCatcher.Hidden = ColorSwatch.Hidden = !(model.Type == SubscribableSettingsOptionModel.OptionType.Color);


		}




	}

}

