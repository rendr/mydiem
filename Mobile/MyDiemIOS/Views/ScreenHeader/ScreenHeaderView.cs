using System;
using System.Drawing;

using MonoTouch.UIKit;

namespace MyDiem.UI.Touch
{
	public class ScreenHeaderView:UIView
	{

		public UILabel TitleLabel;
		string Text;
		string Alignment;
		UIFont Font;

		public static int HeaderHeight = (BaseViewController.IsIOS7)?68:40;
		public static int HeaderAlignY = (BaseViewController.IsIOS7)?40:20;
		public const int DropShadowHeight = 7;


		public ScreenHeaderView (string text, UIFont font, string alignment):base(new RectangleF(0,0,320,HeaderHeight + DropShadowHeight))
		{
			Alignment = alignment;
			Text = text;
			Font = font;
		}

		public override void LayoutSubviews ()
		{
			UIImageView background = new UIImageView(Frame);
			background.Image = UIImage.FromBundle("/Images/HeaderView/HeaderViewBackground");
			AddSubview(background);

			int margin = 70;

			int labelWidth = 178;
			if(Alignment == "right"){
				labelWidth = 195;
				margin = 105;
			}


			TitleLabel = new UILabel(new RectangleF(margin, HeaderAlignY - Frame.Height/2, labelWidth, Frame.Height)){
				BackgroundColor = UIColor.Clear,
				Font = Font, 
				TextAlignment = UITextAlignment.Center,
				Text = this.Text,
				TextColor = UIColor.White
			};

			if(Alignment == "right"){
				TitleLabel.TextAlignment = UITextAlignment.Right;

			}
			AddSubview(TitleLabel);
		}
	}
}

