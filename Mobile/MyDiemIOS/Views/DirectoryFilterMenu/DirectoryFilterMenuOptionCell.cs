using System;
using MyDiem.UI.Touch.Views.SlideMenu;
using MonoTouch.UIKit;

namespace Views.DirectoryFilterMenu
{
	public class DirectoryFilterMenuOptionCell:SlideMenuOptionCell
	{
		UIImageView CheckMark;

		bool Selected;

		public DirectoryFilterMenuOptionCell (string cellID):base(cellID)
		{
		}

		public override void Draw (System.Drawing.RectangleF rect)
		{
			base.Draw (rect);
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			if (CheckMark == null) {
				CheckMark = new UIImageView (UIImage.FromBundle("/Images/Reminders/Checkmark"));
				CheckMark.Frame = new System.Drawing.RectangleF (Frame.Width - 17 - 10, Frame.Height/2 - 13/2, 17, 13);
				AddSubview (CheckMark);

			}
			optionLabel.Frame = new System.Drawing.RectangleF (optionLabel.Frame.X, optionLabel.Frame.Y, optionLabel.Frame.Width - 30, optionLabel.Frame.Height);
			CheckMark.Hidden = !Selected;
		}

		public void UpdateCell (string caption, bool selected)
		{
			base.UpdateCell (caption);
			Selected = selected;
			if (CheckMark != null) {
				CheckMark.Hidden = !Selected;
			}
		}

	}
}

