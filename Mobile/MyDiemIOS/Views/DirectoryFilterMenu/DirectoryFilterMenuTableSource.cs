using System;
using MonoTouch.UIKit;
using MyDiem.UI.Touch.Views.SlideMenu;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MyDiem.Core.ViewModel;

namespace Views.DirectoryFilterMenu
{
	public class DirectoryFilterMenuTableSource:UITableViewSource
	{
		protected string cellIdentifier = "directoryCell";
		List<string> DirectoryNames;
		public static Dictionary<string, bool> SelectedOptions;
		Dictionary<NSIndexPath, DirectoryFilterMenuOptionCell> CellMap = new Dictionary<NSIndexPath, DirectoryFilterMenuOptionCell> ();

		static DirectoryFilterMenuTableSource ()
		{
			SelectedOptions = new Dictionary<string, bool> ();
		}

		List<string> Grades;

		public DirectoryFilterMenuTableSource (List<string> directoryNames, List<string> grades)
		{
			this.DirectoryNames = directoryNames;
			foreach (var name in directoryNames) {
				if (!SelectedOptions.ContainsKey (name)) {
					SelectedOptions [name] = true;
				}
			}

			this.Grades = grades;
			for (int i = 0; i < grades.Count; i++) {
				if (grades [i].Length <= 2) {
					grades [i] = "GRADE " + grades [i];
				}
				var label = grades [i];

				if (!SelectedOptions.ContainsKey (label)) {
					SelectedOptions [label] = true;
				}
			}



		}

		public override int NumberOfSections (UITableView tableView)
		{
			return 2;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			if (section == 0) {
				return DirectoryNames.Count;
			} else {
				return Grades.Count;
			}
		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			switch (section) {
			case 0:
				return "DIRECTORY";

			case 1:
				return "GRADE/POSITION";


			}

			return "";
		}

		public override UIView GetViewForHeader (UITableView tableView, int section)
		{
			SlideMenuTableHeaderView header = new SlideMenuTableHeaderView (new RectangleF (0, 0, tableView.Bounds.Width, 20), TitleForHeader (tableView, section));
			return header;			                                              
		}

		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{


			var cell = tableView.DequeueReusableCell (cellIdentifier) as DirectoryFilterMenuOptionCell;
			if (cell == null) {
				cell = new DirectoryFilterMenuOptionCell (cellIdentifier);
			}

			if (indexPath.Section == 0) {
				var cellTitle = DirectoryNames [indexPath.Row];
				cell.UpdateCell (cellTitle, SelectedOptions [cellTitle]);
			} else if (indexPath.Section == 1) {

				var cellTitle = Grades [indexPath.Row];
				cell.UpdateCell (cellTitle, SelectedOptions [cellTitle]);
			}



			CellMap [indexPath] = cell;
			return cell;
		}

		public override void RowSelected (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			tableView.DeselectRow (indexPath, true);

			var cell = CellMap [indexPath];
			SelectedOptions [cell.Title] = !SelectedOptions [cell.Title];
			cell.UpdateCell (cell.Title, SelectedOptions [cell.Title]);




//			DirectoryViewModel.FilterContacts(GetEnabledItems());
		}

		protected List<string> GetEnabledItems ()
		{
			var items = new List<string> ();
			foreach (var keyValuePair in SelectedOptions) {
				if (keyValuePair.Value) {
					items.Add (keyValuePair.Key.Replace ("GRADE ", ""));
				}
			}
			return items;
		}
	}
}

