using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.UIKit;
using MyDiem.Core.BL;

namespace MyDiem.UI.Touch
{
	public class SubscribableTypeUIPickerViewDataSource:UIPickerViewModel
	{

		public event EventHandler TypeSelected = delegate{};
		
		public String SelectedType;

		protected List<String> Options = new List<String>(){"Any", Subscribable.TYPE_CALENDAR, Subscribable.TYPE_DIRECTORY};

		public SubscribableTypeUIPickerViewDataSource ()
		{

		}

		public override int GetComponentCount (UIPickerView pickerView)
		{
			return 1;
		}
		
		public override int GetRowsInComponent (UIPickerView pickerView, int component)
		{
			return 3;
		}
		
		public override string GetTitle (UIPickerView picker, int row, int component)
		{
			var title = Options [row];
			title = char.ToUpper(title[0]) + title.Substring(1);
			return title;
		}
		
		public override void Selected (UIPickerView picker, int row, int component)
		{

			SelectedType = Options[row];
			TypeSelected(this, new EventArgs());

		}
	}
}

