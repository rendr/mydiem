using System;
using MonoTouch.UIKit;
using System.Drawing;
using Views;

namespace MyDiem.UI.Touch
{
	public class SubscribableTypeUIPickerView:BasePicker
	{


		public event EventHandler TypeSelected = delegate{};
		
		public String SelectedType;
		

		protected SubscribableTypeUIPickerViewDataSource PickerDataSource;


		public SubscribableTypeUIPickerView (RectangleF bounds):base(bounds)
		{
		
		}

		override protected void BuildUI()
		{
			base.BuildUI ();
			var uIPickerView = BuildPickerView ();

			uIPickerView.Model = PickerDataSource =  new SubscribableTypeUIPickerViewDataSource();

			PickerDataSource.TypeSelected += (sender, e) => {
				SelectedType = PickerDataSource.SelectedType;
				TypeSelected(null, new EventArgs());
			};
		}
	}
}

