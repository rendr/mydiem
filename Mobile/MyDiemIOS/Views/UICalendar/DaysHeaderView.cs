using System;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.CoreGraphics;

namespace Views.UICalendar
{
	public class DaysHeader:UIView{

		public DaysHeader(RectangleF frame):base(frame){

		}

		public override void Draw (RectangleF rect)
		{
			base.Draw (rect);
			var context = UIGraphics.GetCurrentContext();
			DrawBackground (context);
			DrawLabels ();
			context.RestoreState();
		}

		void DrawBackground (CGContext context)
		{
			UIColor.FromRGB (126, 126, 126).SetFill ();
			CGPath path = new CGPath ();
			path.AddLines (new PointF[] {
				new PointF (0, 0),
				new PointF (Frame.Width, 0),
				new PointF (Frame.Width, Frame.Height),
				new PointF (0, Frame.Height),
				new PointF (0, 0)
			});
			path.CloseSubpath ();
			context.AddPath (path);
			context.DrawPath (CGPathDrawingMode.FillStroke);
			context.SaveState();
		}

		void DrawLabels ()
		{
			UIColor.White.SetColor ();
			var font = UIFont.BoldSystemFontOfSize (10);
			var i = 0;
			foreach (var d in Enum.GetNames (typeof(DayOfWeek))) {
				var labelWidth = (Bounds.Width / 7);
				DrawString (d.Substring (0, 3), new RectangleF (i * labelWidth, 2, 45, 10), font, UILineBreakMode.WordWrap, UITextAlignment.Center);
				i++;
			}
		}


	}
}

