//
//  CalendarMonthView.cs
//
//  Converted to MonoTouch on 1/22/09 - Eduardo Scoz || http://escoz.com
//  Originally reated by Devin Ross on 7/28/09  - tapku.com || http://github.com/devinross/tapkulibrary
//
/*
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */

using System;
using MonoTouch.UIKit;
using System.Drawing;
using MyDiem.UI.Touch;
using Views.UICalendar;

namespace MyDiem.UI.Touch.Views.UICalendar
{
	public delegate void DateSelected (DateTime date);
	public delegate void MonthChanged (DateTime monthSelected);
	public class UICalendarMonth : UIView
	{
		public Action<DateTime> OnDateSelected = delegate {
		};
		public Action<DateTime> OnFinishedDateSelection = delegate {
		};
		public Action<RectangleF> OnCalendarResized = delegate {
		};
		public Func<DateTime, bool> IsDayMarkedDelegate;
		public Func<DateTime, bool> IsDateAvailable;
		/*
		*	PROPS
		*/
		public DateTime currentMonthYear;

		public DateTime CurrentMonthYear {
			get {
				return currentMonthYear;
			}
			set {
				currentMonthYear = value;
				if (MainHeader != null) {
					MainHeader.CurrentMonthYear = value;
				}
			}
		}
		/*
		 * INTERNAL FIELDS
		 */
		protected DateTime CurrentDate { get; set; }

		protected CalendarHeader MainHeader;
		private UIScrollView _scrollView;
		private bool calendarIsLoaded;
		private MonthGridView _monthGridView;
		/*
		 * CONSTANTS
		 */
		public const int RowHeight = 45;
		const int scrollViewContentHeight = 400;
		const int calendarWidth = 320;
		/*
		*	CONSTRUCTOR
		*/
		public UICalendarMonth () : base (new RectangleF (0, 0, calendarWidth, scrollViewContentHeight))
		{
			CurrentDate = DateTime.Now.Date;
			currentMonthYear = new DateTime (CurrentDate.Year, CurrentDate.Month, 1);
		}
		/*
		*	OVERRIDES
		*/
		public override void Draw (RectangleF rect)
		{
			CreateMonthHeader ();
			CreateDaysHeader ();

		}

		public override void SetNeedsDisplay ()
		{
			base.SetNeedsDisplay ();
			if (_monthGridView != null)
				_monthGridView.Update ();
		}

		public override void LayoutSubviews ()
		{
			if (calendarIsLoaded)
				return;

			CreateScrollView ();
			LoadInitialGrids ();
			BackgroundColor = UIColor.Clear;
			GoToToday ();
			calendarIsLoaded = true;

		}
		/*
		 * Public Methods
		 */
		public void DeselectDate ()
		{
			if (_monthGridView != null)
				_monthGridView.DeselectDayView ();
		}

		void CreateScrollView ()
		{
			_scrollView = new UIScrollView (new RectangleF ()) {
				ContentSize = new SizeF (calendarWidth, scrollViewContentHeight),
				ScrollEnabled = false,
				Frame = new RectangleF (0, ScreenHeaderView.HeaderHeight + 16, calendarWidth, scrollViewContentHeight),
			};
			_scrollView.BackgroundColor = UIColor.Clear;
			_scrollView.Opaque = false;
			AddSubview (_scrollView);
		}

		public void GoToToday ()
		{
			DateTime today = DateTime.Now;
			
			bool animate = (today.Month != currentMonthYear.Month || today.Year != currentMonthYear.Year);
			
			while (today.Month != currentMonthYear.Month || today.Year != currentMonthYear.Year) {
				currentMonthYear = currentMonthYear.AddMonths ((today.Date < currentMonthYear.Date) ? -1 : 1);
				
			}
			
			MoveToCalendar (today.Date < currentMonthYear.Date, animate, 0);
			
		}

		public void MoveToCalendar (bool upwards, bool animated, int delta = 1)
		{
			
			UpdateCurrentMonthAndYear (upwards, delta);
			
			PreventUserInteraction ();
			
			MonthGridView newGrid = CreateNewGrid (currentMonthYear);
			
			var pointsToMove = CalculatePointsToMove (upwards, newGrid);
			
			StartAnimation (animated);
			
			UpdateGridCenterPoints (newGrid, pointsToMove);
			
			FadeOutCurrentGrid ();
			
			MoveScrollViewToShowGrid (newGrid);
			
			SetNeedsDisplay ();
			
			EndAnimation (animated);
			
			SetCurrentGridView (newGrid);
			
			EnableUserInteraction ();
			
			DispatchResultEvents ();
			
		}
		/*
		 * EVENT HANDLERS
		 */
		private void HandlePreviousMonthTouch (object sender, EventArgs e)
		{
			MoveToCalendar (false, true);
		}

		private void HandleNextMonthTouch (object sender, EventArgs e)
		{
			MoveToCalendar (true, true);
		}
		/*
		*	HELPER FUNCTIONS
		*/
		void SetCurrentGridView (MonthGridView newGrid)
		{
			_monthGridView = newGrid;
		}

		void UpdateCurrentMonthAndYear (bool upwards, int delta)
		{
			currentMonthYear = currentMonthYear.AddMonths (upwards ? 1 * delta : -1 * delta);
		}

		void MoveScrollViewToShowGrid (MonthGridView gridToMove)
		{
			_scrollView.Frame = new RectangleF (_scrollView.Frame.Location, new SizeF (_scrollView.Frame.Width, (gridToMove.Lines + 1) * RowHeight));
			_scrollView.ContentSize = _scrollView.Frame.Size;
		}

		void FadeOutCurrentGrid ()
		{
			_monthGridView.Alpha = 0;
		}

		void DispatchResultEvents ()
		{
			OnCalendarResized (new RectangleF (Frame.X, Frame.Y, _scrollView.Frame.Size.Width, _scrollView.Frame.Size.Height + _scrollView.Frame.Y));
			_monthGridView.DispatchOnDateSelected ();
		}

		void EnableUserInteraction ()
		{
			UserInteractionEnabled = true;
		}

		void PreventUserInteraction ()
		{
			UserInteractionEnabled = false;
		}

		void EndAnimation (bool animated)
		{
			if (animated)
				UIView.CommitAnimations ();
		}

		void UpdateGridCenterPoints (MonthGridView gridToMove, int pointsToMove)
		{
			_monthGridView.Center = new PointF (_monthGridView.Center.X, _monthGridView.Center.Y - pointsToMove);
			gridToMove.Center = new PointF (gridToMove.Center.X, gridToMove.Center.Y - pointsToMove);
		}

		void StartAnimation (bool animated)
		{
			if (animated) {
				UIView.BeginAnimations ("changeMonth");
				UIView.SetAnimationDuration (.5);
				UIView.SetAnimationCurve (UIViewAnimationCurve.EaseOut);
			}
		}

		int CalculatePointsToMove (bool upwards, MonthGridView gridToMove)
		{
			var pointsToMove = (upwards ? 0 + _monthGridView.Lines : 0 - _monthGridView.Lines) * RowHeight;
			if (upwards && gridToMove.weekdayOfFirst == 0)
				pointsToMove += RowHeight;
			if (!upwards && _monthGridView.weekdayOfFirst == 0)
				pointsToMove -= RowHeight;
			gridToMove.Frame = new RectangleF (new PointF (0, pointsToMove), gridToMove.Frame.Size);
			_scrollView.AddSubview (gridToMove);
			return pointsToMove;
		}

		private MonthGridView CreateNewGrid (DateTime date)
		{
			var grid = new MonthGridView (this, date);
			grid.CurrentDate = CurrentDate;
			grid.BuildGrid ();
			grid.Frame = new RectangleF (0, 15, 320, 400);
			return grid;
		}

		private void LoadInitialGrids ()
		{
			_monthGridView = CreateNewGrid (currentMonthYear);

			var rect = _scrollView.Frame;
			rect.Size = new SizeF { Height = (_monthGridView.Lines + 1) * RowHeight, Width = rect.Size.Width };
			_scrollView.Frame = rect;

			OnCalendarResized (new RectangleF (Frame.X, Frame.Y, _scrollView.Frame.Size.Width, _scrollView.Frame.Size.Height + _scrollView.Frame.Y));   
		}

		private void CreateMonthHeader ()
		{
			MainHeader = new CalendarHeader ();
			AddSubview (MainHeader);

			MainHeader.CurrentMonthYear = this.currentMonthYear;
			MainHeader.NextButton += HandleNextMonthTouch;
			MainHeader.PreviousButtonClicked += HandlePreviousMonthTouch;
		}

		private void CreateDaysHeader ()
		{
			AddSubview (new DaysHeader (new RectangleF (0, ScreenHeaderView.HeaderHeight, Bounds.Width, 16)));

		}
	}
}