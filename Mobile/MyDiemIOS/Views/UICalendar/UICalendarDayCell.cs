using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.UI.Touch;
using System.Diagnostics;
using MonkeyArms;

namespace MyDiem.UI.Touch.Views.UICalendar
{
	public class UICalendarDayCell : UIView
	{
		string _text;

		public DateTime Date { get; set; }

		bool _active, _today, _selected, _marked, _available;

		public bool Available {
			get { return _available; }
			set {
				_available = value;
				SetNeedsDisplay ();
			}
		}

		public string Text {
			get { return _text; }
			set {
				_text = value;
				SetNeedsDisplay ();
			}
		}

		public bool Active {
			get { return _active; }
			set {
				_active = value;
				SetNeedsDisplay ();
			}
		}

		public bool Today {
			get { return _today; }
			set {
				_today = value;
				SetNeedsDisplay ();
			}
		}

		public bool Selected {
			get { return _selected; }
			set {
				_selected = value;
				SetNeedsDisplay ();
			}
		}

		public bool Marked {
			get { return _marked; }
			set {
				_marked = value;
				SetNeedsDisplay ();
			}
		}

		public UICalendarDayCell ()
		{
			this.BackgroundColor = UIColor.Clear;
			this.Opaque = false;
		}

		public override void Draw (RectangleF rect)
		{
			UIImage img;
			UIColor color;



			if (!Active || !Available) {
				color = UIColor.FromRGBA (0.576f, 0.608f, 0.647f, 1f);
				img = UIImage.FromBundle ("Images/calendar/datecellInactive");
			} else if (Today && Selected) {
				color = UIColor.White;
				img = UIImage.FromBundle ("Images/calendar/todayselected");
			} else if (Today) {
				color = UIColor.White;
				img = UIImage.FromBundle ("Images/calendar/today");
			} else if (Selected) {
				color = UIColor.FromRGBA (0.275f, 0.341f, 0.412f, 1f);
				img = UIImage.FromFile ("Images/calendar/datecellselected.png");
			} else if (Marked) {
				color = UIColor.White;
				img = UIImage.FromFile ("Images/calendar/datecellmarked.png");
			} else {
				color = UIColor.FromRGBA (0.275f, 0.341f, 0.412f, 1f);
				img = UIImage.FromBundle ("Images/calendar/datecell");

			}

			if (Date < DateTime.Now.AddDays (-1)) {
				color = UIColor.FromRGBA (0.576f, 0.608f, 0.647f, 1f);
			}

			img.Draw (new Rectangle (0, 0, 46, UICalendarMonth.RowHeight));
            
			//color.SetColor();
			DrawString (Text, new Rectangle (28, 28, 15, 15), UIFont.BoldSystemFontOfSize (12), UILineBreakMode.WordWrap, UITextAlignment.Center);

			if (Active) {
				DrawEvents ();
			}




		}

		protected void DrawEvents ()
		{

			List<CalendarEvent> events = DI.Get<EventManager> ().GetEventsForDate (Date);
			int startX = 4;
			int startY = 4;
			int curX = startX;
			int curY = startY;
			int horSpace = 2;
			int vertSpace = 2;
			int swatchWidth = 5;
			int swatchHeight = 5;
			int rowCount = 1;
			foreach (CalendarEvent cevent in events) {
				if (curX + swatchWidth + horSpace > Bounds.Width - 10 || (cevent.AllDay && curX != startX)) {
					curX = startX;
					curY += swatchHeight + vertSpace;
					rowCount++;
				}

				if (rowCount > 6) {
					break;
				}

				if (cevent.AllDay) {
					swatchWidth = Convert.ToInt16 (Bounds.Width) - 10;
				} else {
					swatchWidth = 5;
				}
				Subscription sub = DI.Get<UserManager> ().GetSubscriptionForCalendar (Convert.ToInt32 (cevent.CalendarID));
//				Debug.WriteLine ("Swatch color: " + sub.Color);
				UIView swatch = new UIView (new RectangleF (curX, curY, swatchWidth, swatchHeight));
				curX += swatchWidth + horSpace;
				swatch.BackgroundColor = ColorUtil.UIColorFromHex (uint.Parse (sub.Color.Substring (0, 6), System.Globalization.NumberStyles.AllowHexSpecifier));
				AddSubview (swatch);
			}
		}
	}
}

