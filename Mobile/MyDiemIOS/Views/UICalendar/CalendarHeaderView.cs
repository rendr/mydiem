using System;
using MyDiem.UI.Touch;
using MonoTouch.UIKit;
using System.Drawing;

namespace Views.UICalendar
{
	public class CalendarHeader:ScreenHeaderView{


		public CalendarHeader():base("",  UIFont.FromName("OpenSans", 20), "center"){

		}

		/*
			*	FIELDS
			*/
		private UIButton _leftButton, _rightButton;

		DateTime currentMonthYear;

		public DateTime CurrentMonthYear {
			get {
				return currentMonthYear;
			}
			set {
				currentMonthYear = value;
				if (TitleLabel != null) {
					TitleLabel.Text = value.ToString ("MMM yyyy").ToUpper ();
				}
			}
		}

		public event EventHandler PreviousButtonClicked = delegate {}, NextButton = delegate {};



		/*
			*	OVERRIDES
			*/



		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			DrawButtons ();
			AddEventListeners ();
			TitleLabel.Text = CurrentMonthYear.ToString ("MMM yyyy");
		}

		/*
			*	HELPER FUNCTIONS
			*/
		void AddEventListeners ()
		{
			_leftButton.TouchUpInside += (object sender, EventArgs e) => PreviousButtonClicked (this, EventArgs.Empty);

			_rightButton.TouchUpInside += (object sender, EventArgs e) => NextButton (this, EventArgs.Empty);
		}



		protected void DrawButtons()
		{
			_leftButton = DrawButton (75, "Images/calendar/leftarrow");
			_rightButton = DrawButton (320 - 120, "Images/calendar/rightarrow");

		}

		protected UIButton DrawButton(int xPos, string imagePath)
		{
			var button = UIButton.FromType(UIButtonType.Custom);
			button.SetImage(UIImage.FromBundle(imagePath), UIControlState.Normal);
			AddSubview(button);
			const int buttonHeight = 42;
			button.Frame = new RectangleF (xPos, ScreenHeaderView.HeaderAlignY - buttonHeight/2, 44, buttonHeight);
			return button;
		}


	}
}

