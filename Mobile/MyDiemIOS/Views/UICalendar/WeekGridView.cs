using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;

using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.UI.Touch;
using System.Diagnostics;

namespace MyDiem.UI.Touch.Views.UICalendar
{
	public class WeekGridView:UIView
	{
		private UICalendarWeek _calendarWeekView;

        public DateTime CurrentDate {get;set;}
        private DateTime _currentWeek;
        protected readonly IList<UICalendarDayCell> _dayTiles = new List<UICalendarDayCell>();
		public int Lines = 1;
		protected UICalendarDayCell SelectedDayView;
		public int weekdayOfFirst;
        public IList<DateTime> Marks { get; set; }

        public WeekGridView(UICalendarWeek calendarWeekView, DateTime week)
        {
			_calendarWeekView = calendarWeekView;
            _currentWeek = week.Date;
        }

		public void Update(){
			foreach (var v in _dayTiles)
				updateDayView(v);

			this.SetNeedsDisplay();
		}

		public void updateDayView(UICalendarDayCell dayView){
			dayView.Marked = _calendarWeekView.IsDayMarkedDelegate == null ? 
							false : _calendarWeekView.IsDayMarkedDelegate(dayView.Date);
			dayView.Available = _calendarWeekView.IsDateAvailable == null ? 
							true : _calendarWeekView.IsDateAvailable(dayView.Date);
		}

        public void BuildGrid()
        {
			Debug.WriteLine("BuildGrid Week: " + _currentWeek.DayOfWeek);
			var startDateTime = _currentWeek;
			string[] DaysOfWeek = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
			
			_currentWeek = _currentWeek.AddDays(-1 * Array.IndexOf(DaysOfWeek, _currentWeek.DayOfWeek.ToString()));

            var position = weekdayOfFirst+1;
            var line = 0;

			DateTime currentDay = _currentWeek;
            // current week
            for (int i = 1; i <= 7; i++)
            {

                var dayView = new UICalendarDayCell
                  {
                      Frame = new RectangleF((position - 1) * 46 - 1, line * UICalendarMonth.RowHeight, 47, UICalendarMonth.RowHeight),
                      Today = false,
                      Text = currentDay.Day.ToString(),

                      Active = true,
                      Tag = i,
					  Selected = (currentDay.Day == CurrentDate.Day-1 )
                  };
				dayView.Date = currentDay;
				updateDayView(dayView);

				if (dayView.Selected)
					SelectedDayView = dayView;

                AddSubview(dayView);
                _dayTiles.Add(dayView);

                position++;
                currentDay = currentDay.AddDays(1);
            }


            Frame = new RectangleF(Frame.Location, new SizeF(Frame.Width, (line + 1) * UICalendarMonth.RowHeight));

			if (SelectedDayView!=null)
				this.BringSubviewToFront(SelectedDayView);


        }

		public void DispatchOnDateSelected ()
		{
			if(SelectedDayView != null){
				_calendarWeekView.OnDateSelected (SelectedDayView.Date);
			}
		}
		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			if (SelectDayView ((UITouch)touches.AnyObject) && _calendarWeekView.OnDateSelected != null)
				DispatchOnDateSelected ();
		}

		public override void TouchesMoved (NSSet touches, UIEvent evt)
		{
			base.TouchesMoved (touches, evt);
			if (SelectDayView ((UITouch)touches.AnyObject) && _calendarWeekView.OnDateSelected != null)
				DispatchOnDateSelected ();
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			if (_calendarWeekView.OnFinishedDateSelection==null) return;
			var date = new DateTime(_currentWeek.Year, _currentWeek.Month, SelectedDayView.Tag);
			if (_calendarWeekView.IsDateAvailable == null || _calendarWeekView.IsDateAvailable(date))
				_calendarWeekView.OnFinishedDateSelection(date);
		}

		public bool SelectDayView(UITouch touch){
			var p = touch.LocationInView(this);

			int index = ((int)p.Y / UICalendarMonth.RowHeight) * 7 + ((int)p.X / 46);
			if(index<0 || index >= _dayTiles.Count) return false;

			var newSelectedDayView = _dayTiles[index];
			if (newSelectedDayView == SelectedDayView) 
				return false;

			if (!newSelectedDayView.Active && touch.Phase!=UITouchPhase.Moved){
				var day = int.Parse(newSelectedDayView.Text);
				if (day > 15)
					_calendarWeekView.MoveCalendarWeeks(false, true);
				else
					_calendarWeekView.MoveCalendarWeeks(true, true);
				return false;
			} else if (!newSelectedDayView.Active && !newSelectedDayView.Available){
				return false;
			}

			if (SelectedDayView!=null)
				SelectedDayView.Selected = false;

			this.BringSubviewToFront(newSelectedDayView);
			newSelectedDayView.Selected = true;


			SelectedDayView = newSelectedDayView;
			Debug.WriteLine("SelectedDayView: " + SelectedDayView.Date.DayOfWeek);
			SetNeedsDisplay();
			return true;
		}

		public void DeselectDayView(){
			if (SelectedDayView==null) return;
			SelectedDayView.Selected= false;
			SelectedDayView = null;
			SetNeedsDisplay();
		}
	}
}

