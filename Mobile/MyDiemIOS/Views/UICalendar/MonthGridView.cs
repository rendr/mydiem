using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using System.Drawing;

namespace MyDiem.UI.Touch.Views.UICalendar
{
	public class MonthGridView : UIView
	{
		private UICalendarMonth _calendarMonthView;

		public DateTime CurrentDate { get; set; }

		private DateTime _currentMonth;
		protected readonly IList<UICalendarDayCell> _dayTiles = new List<UICalendarDayCell> ();

		public int Lines { get; set; }

		protected UICalendarDayCell SelectedDayView { get; set; }

		public int weekdayOfFirst;

		public IList<DateTime> Marks { get; set; }

		public MonthGridView (UICalendarMonth calendarMonthView, DateTime month)
		{
			_calendarMonthView = calendarMonthView;
			_currentMonth = month.Date;
		}

		public void Update ()
		{
			foreach (var v in _dayTiles)
				updateDayView (v);

			this.SetNeedsDisplay ();
		}

		public void updateDayView (UICalendarDayCell dayView)
		{
			dayView.Marked = _calendarMonthView.IsDayMarkedDelegate != null && _calendarMonthView.IsDayMarkedDelegate (dayView.Date);
			dayView.Available = _calendarMonthView.IsDateAvailable == null || _calendarMonthView.IsDateAvailable (dayView.Date);
		}

		public void BuildGrid ()
		{

			DateTime previousMonth = _currentMonth.AddMonths (-1);
			var daysInPreviousMonth = DateTime.DaysInMonth (previousMonth.Year, previousMonth.Month);
			var daysInMonth = DateTime.DaysInMonth (_currentMonth.Year, _currentMonth.Month);
			weekdayOfFirst = (int)_currentMonth.DayOfWeek;
			var lead = daysInPreviousMonth - (weekdayOfFirst - 1);


			// build last month's days
			for (int i = 1; i <= weekdayOfFirst; i++) {
				var viewDay = new DateTime (_currentMonth.Year, _currentMonth.Month, i);
				var dayView = new UICalendarDayCell ();
				dayView.Frame = new RectangleF ((i - 1) * 46 - 1, 0, 47, 45);
				dayView.Date = viewDay;
				dayView.Text = lead.ToString ();

				AddSubview (dayView);
				_dayTiles.Add (dayView);
				lead++;
			}

			var position = weekdayOfFirst + 1;
			var line = 0;

			// current month
			for (int i = 1; i <= daysInMonth; i++) {
				var viewDay = new DateTime (_currentMonth.Year, _currentMonth.Month, i);
				var dayView = new UICalendarDayCell {
					Frame = new RectangleF ((position - 1) * 46 - 1, line * UICalendarMonth.RowHeight, 47, UICalendarMonth.RowHeight),
					Today = (CurrentDate.Date == viewDay.Date),
					Text = i.ToString (),

					Active = true,
					Tag = i,
					Selected = (i == CurrentDate.AddDays (0).Day)
				};
				dayView.Date = viewDay;
				updateDayView (dayView);

				if (dayView.Selected)
					SelectedDayView = dayView;

				AddSubview (dayView);
				_dayTiles.Add (dayView);

				position++;
				if (position > 7) {
					position = 1;
					line++;
				}
			}

			//next month
			if (position != 1) {
				int dayCounter = 1;
				for (int i = position; i < 8; i++) {
					var viewDay = new DateTime (_currentMonth.Year, _currentMonth.Month, i);
					var dayView = new UICalendarDayCell {
						Frame = new RectangleF ((i - 1) * 46 - 1, line * UICalendarMonth.RowHeight, 47, UICalendarMonth.RowHeight),
						Text = dayCounter.ToString (),
					};
					dayView.Date = viewDay;
					updateDayView (dayView);

					AddSubview (dayView);
					_dayTiles.Add (dayView);
					dayCounter++;
				}
			}

			Frame = new RectangleF (Frame.Location, new SizeF (Frame.Width, (line + 1) * UICalendarMonth.RowHeight));

			Lines = (position == 1 ? line - 1 : line);

			if (SelectedDayView != null)
				this.BringSubviewToFront (SelectedDayView);


		}

		public void DispatchOnDateSelected ()
		{
			if (SelectedDayView != null && SelectedDayView.Tag != null && _calendarMonthView != null) {
				_calendarMonthView.OnDateSelected (new DateTime (_currentMonth.Year, _currentMonth.Month, SelectedDayView.Tag));
			}
		}

		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			if (SelectDayView ((UITouch)touches.AnyObject) && _calendarMonthView.OnDateSelected != null)
				DispatchOnDateSelected ();
		}

		public override void TouchesMoved (NSSet touches, UIEvent evt)
		{
			base.TouchesMoved (touches, evt);
			if (SelectDayView ((UITouch)touches.AnyObject) && _calendarMonthView.OnDateSelected != null)
				DispatchOnDateSelected ();
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			if (_calendarMonthView.OnFinishedDateSelection == null)
				return;
			var date = new DateTime (_currentMonth.Year, _currentMonth.Month, SelectedDayView.Tag);
			if (_calendarMonthView.IsDateAvailable == null || _calendarMonthView.IsDateAvailable (date))
				_calendarMonthView.OnFinishedDateSelection (date);
		}

		private bool SelectDayView (UITouch touch)
		{
			var p = touch.LocationInView (this);

			int index = ((int)p.Y / UICalendarMonth.RowHeight) * 7 + ((int)p.X / 46);
			if (index < 0 || index >= _dayTiles.Count)
				return false;

			var newSelectedDayView = _dayTiles [index];
			if (newSelectedDayView == SelectedDayView)
				return false;

			if (!newSelectedDayView.Active && touch.Phase != UITouchPhase.Moved) {
				var day = int.Parse (newSelectedDayView.Text);
				if (day > 15)
					_calendarMonthView.MoveToCalendar (false, true);
				else
					_calendarMonthView.MoveToCalendar (true, true);
				return false;
			} else if (!newSelectedDayView.Active && !newSelectedDayView.Available) {
				return false;
			}

			if (SelectedDayView != null)
				SelectedDayView.Selected = false;

			this.BringSubviewToFront (newSelectedDayView);
			newSelectedDayView.Selected = true;

			SelectedDayView = newSelectedDayView;
			SetNeedsDisplay ();
			return true;
		}

		public void DeselectDayView ()
		{
			if (SelectedDayView == null)
				return;
			SelectedDayView.Selected = false;
			SelectedDayView = null;
			SetNeedsDisplay ();
		}
	}
}

