using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;

using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.UI.Touch;
using System.Diagnostics;
using Views.UICalendar;

namespace MyDiem.UI.Touch.Views.UICalendar
{


    public class UICalendarWeek : UIView
    {
		public event EventHandler CurrentWeekChanged = delegate{};

		public delegate void DateSelected(DateTime date);
    	public delegate void WeekChanged(DateTime monthSelected);

		public Action<DateTime> OnDateSelected;
		public Action<DateTime> OnFinishedDateSelection;
		public Action<SizeF> OnCalendarResized;
		public Func<DateTime, bool> IsDayMarkedDelegate;
		public Func<DateTime, bool> IsDateAvailable;

        public DateTime CurrentWeekMonthYear;
        protected DateTime CurrentDate { get; set; }

        private UIScrollView _scrollView;
        
        private bool calendarIsLoaded;

		private WeekGridView _weekGridView;
        

		public static int RowHeight = 45;

		CalendarHeader Header;

        public UICalendarWeek() : base(new RectangleF(0, 0, 320, ScreenHeaderView.HeaderHeight + 63))
        {
            CurrentDate = DateTime.Now.Date;
			CurrentWeekMonthYear = new DateTime(CurrentDate.Year, CurrentDate.Month, 1);
		}

		public override void SetNeedsDisplay ()
		{
			base.SetNeedsDisplay();
			if (_weekGridView!=null)
				_weekGridView.Update();
		}

		public override void LayoutSubviews ()
		{
			BackgroundColor = UIColor.White;
			if (calendarIsLoaded) return;

			CreateHeader ();
			_scrollView = new UIScrollView(new RectangleF(0, ScreenHeaderView.HeaderHeight + 16, 320, RowHeight))
                  {
                      ContentSize = new SizeF(320, RowHeight* 2),
                      ScrollEnabled = false
                      
                  };

			_scrollView.BackgroundColor = UIColor.Clear;
			_scrollView.Opaque = false;


            LoadInitialGrids();

            BackgroundColor = UIColor.Clear;
            AddSubview(_scrollView);
           
			_scrollView.AddSubview(_weekGridView);

			calendarIsLoaded = true;
			GoToToday();// TODO perhaps we should know and start from the last date selected in any view.
        }

		public void DeselectDate(){
//			if (_weekGridView!=null)
//				_weekGridView.DeselectDayView();
		}

	

       

        private void HandlePreviousTouch(object sender, EventArgs e)
        {
            MoveCalendarWeeks(false, true);
        }
        private void HandleNextTouch(object sender, EventArgs e)
        {
            MoveCalendarWeeks(true, true);
        }

        public void MoveCalendarWeeks(bool upwards, bool animated)
        {

			CurrentWeekMonthYear = CurrentWeekMonthYear.AddDays(upwards? 7 : -7);
			Header.CurrentMonthYear = CurrentWeekMonthYear;


			UserInteractionEnabled = false;

			WeekGridView gridToMove = CreateNewGrid(CurrentWeekMonthYear);

			var pointsToMove = (upwards? 0 + _weekGridView.Lines : 0 - _weekGridView.Lines) * RowHeight;

			if (upwards && gridToMove.weekdayOfFirst==0)
				pointsToMove += RowHeight;
			if (!upwards && _weekGridView.weekdayOfFirst==0)
				pointsToMove -= RowHeight;

			gridToMove.Frame = new RectangleF(new PointF(0, pointsToMove), gridToMove.Frame.Size);

			_scrollView.AddSubview(gridToMove);

			if (animated){
				UIView.BeginAnimations("changeWEek");
				UIView.SetAnimationDuration(.5);
				UIView.SetAnimationCurve(UIViewAnimationCurve.EaseOut);
			}

			_weekGridView.Center = new PointF(_weekGridView.Center.X, _weekGridView.Center.Y - pointsToMove);
			gridToMove.Center = new PointF(gridToMove.Center.X, gridToMove.Center.Y - pointsToMove);

			_weekGridView.Alpha = 0;



            _scrollView.Frame = new RectangleF(
			               _scrollView.Frame.Location,
			               new SizeF(_scrollView.Frame.Width, (gridToMove.Lines + 1) * RowHeight));

			_scrollView.ContentSize = _scrollView.Frame.Size;

			SetNeedsDisplay();



			//OnCalendarResized( new SizeF(320, RowHeight));

			if (animated)
				UIView.CommitAnimations();

			_weekGridView = gridToMove;

            UserInteractionEnabled = true;

			_weekGridView.DispatchOnDateSelected();
			this.OnDateSelected(CurrentWeekMonthYear);

			CurrentWeekChanged(this, new EventArgs());

        }

		private WeekGridView CreateNewGrid(DateTime date){
			var grid = new WeekGridView(this, date);
			grid.CurrentDate = date;
			grid.BuildGrid();
			grid.Frame = new RectangleF(0, 0, 320, 400);
			return grid;
		}

        private void LoadInitialGrids()
        {
            _weekGridView = CreateNewGrid(CurrentWeekMonthYear);

            var rect = _scrollView.Frame;
            rect.Size = new SizeF { Height = (_weekGridView.Lines + 1) * RowHeight, Width = rect.Size.Width };
            _scrollView.Frame = rect;

            Frame = new RectangleF(Frame.X, Frame.Y, _scrollView.Frame.Size.Width, _scrollView.Frame.Size.Height+44);

            
        }

        public override void Draw(RectangleF rect)
        {
            
            DrawDayLabels(rect);

        }

        private void CreateHeader()
        {
			Header = new CalendarHeader ();
			Header.NextButton += HandleNextTouch;
			Header.PreviousButtonClicked += HandlePreviousTouch;
			Header.CurrentMonthYear = CurrentWeekMonthYear;
			AddSubview (Header);
        }

        private void DrawDayLabels(RectangleF rect)
        {
			AddSubview(new DaysHeader (new RectangleF(0, ScreenHeaderView.HeaderHeight, Bounds.Width, 16)));
        }

		public void GoToToday ()
		{
			string[] DaysOfWeek = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

			DateTime today = DateTime.Now;
			DateTime targetWeek = today.AddDays(-1 * Array.IndexOf(DaysOfWeek, today.DayOfWeek.ToString()) - 7);

			Debug.WriteLine("targetweek first day: " + targetWeek.DayOfWeek);
				

			CurrentWeekMonthYear= CurrentWeekMonthYear.AddDays(-1 * Array.IndexOf(DaysOfWeek, CurrentWeekMonthYear.DayOfWeek.ToString()));

			bool animate = (targetWeek.Month != CurrentWeekMonthYear.Month || targetWeek.Year != CurrentWeekMonthYear.Year);

			while(targetWeek.Month != CurrentWeekMonthYear.Month || targetWeek.Year != CurrentWeekMonthYear.Year || targetWeek.Day != CurrentWeekMonthYear.Day){

				if(targetWeek.Date < CurrentWeekMonthYear.Date){
					CurrentWeekMonthYear = CurrentWeekMonthYear.AddDays(-7);
				}else{
					CurrentWeekMonthYear = CurrentWeekMonthYear.AddDays(7);
				}
			}

			if(today.Date < CurrentWeekMonthYear.Date){
				MoveCalendarWeeks(false, animate);
			}else{
				MoveCalendarWeeks(true, animate);
			}
		}

		bool InSameWeek (DateTime today, DateTime currentWeekMonthYear)
		{
			string[] DaysOfWeek = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
			
			DateTime week1 = currentWeekMonthYear.AddDays(-1 * Array.IndexOf(DaysOfWeek, currentWeekMonthYear.DayOfWeek.ToString()));
			DateTime week2 = currentWeekMonthYear.AddDays(-1 * Array.IndexOf(DaysOfWeek, today.DayOfWeek.ToString()));

			return week1.Day == week2.Day ;
		}
    }
}

