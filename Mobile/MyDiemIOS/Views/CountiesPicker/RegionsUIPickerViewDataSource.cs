using System;
using System.Collections.Generic;
using System.Diagnostics;

using MonoTouch.UIKit;

using MyDiem.Core.BL;

namespace MyDiem.UI.Touch.Views.CountiesPicker
{
	public class CountiesUIPickerViewDataSource:UIPickerViewModel
	{
		public event EventHandler CountySelected = delegate{};
		
		public County SelectedCounty;
		
		protected List<County> Counties;
		
		public CountiesUIPickerViewDataSource (List<County> counties)
		{
			Counties = counties;
		}
		
		public override int GetComponentCount (UIPickerView pickerView)
		{
			return 1;
		}
		
		public override int GetRowsInComponent (UIPickerView pickerView, int component)
		{
			return Counties.Count;
		}
		
		public override string GetTitle (UIPickerView picker, int row, int component)
		{
			return Counties[row].Title;
		}
		
		public override void Selected (UIPickerView picker, int row, int component)
		{
			SelectedCounty = Counties[row];
			CountySelected(this, new EventArgs());
		}
		
	}
}

