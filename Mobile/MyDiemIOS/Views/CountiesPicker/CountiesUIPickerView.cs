using System;
using System.Drawing;
using MonoTouch.UIKit;
using MyDiem.Core.BL;
using Views;
using MonkeyArms;

namespace MyDiem.UI.Touch.Views.CountiesPicker
{
	public class CountiesUIPickerView:BasePicker, IMediatorTarget
	{
		public event EventHandler CountySelected = delegate{};

		public County SelectedCounty;
		protected CountiesUIPickerViewDataSource PickerDataSource;
		UIPickerView pickerView;

		public CountiesUIPickerView (RectangleF bounds) : base (bounds)
		{

		}

		public System.Collections.Generic.List<MyDiem.Core.BL.County> Counties {

			set {
				pickerView.Model = PickerDataSource = new CountiesUIPickerViewDataSource (value);

				SelectedCounty = value [0];

				PickerDataSource.CountySelected += (sender, e) => {
					SelectedCounty = PickerDataSource.SelectedCounty;
					CountySelected (null, new EventArgs ());
				};
			}
		}

		override protected void BuildUI ()
		{

			base.BuildUI ();
			
			pickerView = BuildPickerView ();
		}
	}
}

