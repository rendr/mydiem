using System;
using MonoTouch.UIKit;
using System.Drawing;
using MyDiem.UI.Touch;
using MyDiem.UI.Touch.ViewController;

namespace Views
{
	public class BasePicker:UIView
	{
		public event EventHandler DoneClicked = delegate{};

		protected UIButton DoneButton;

		public BasePicker (RectangleF bounds) : base (bounds)
		{

			BuildUI ();
		}

		protected virtual void BuildUI ()
		{

			SetBackgroundColor ();
			BuildDoneButton ();
			BuildPickerView ();
		}

		void SetBackgroundColor ()
		{
			BackgroundColor = (!BaseViewController.IsIOS7) ? UIColor.FromRGB (40, 42, 58) : UIColor.White;
		}

		void BuildDoneButton ()
		{
			DoneButton = new UIButton (new RectangleF (15, 165, 300, 31));
			DoneButton.SetImage (UIImage.FromBundle ("/Images/RegisterScreen/DoneButton"), UIControlState.Normal);
			DoneButton.SetImage (UIImage.FromBundle ("/Images/RegisterScreen/DoneButton"), UIControlState.Highlighted);
			AddSubview (DoneButton);
			DoneButton.TouchUpInside += (sender, e) => {
				DoneClicked (this, new EventArgs ());
			};
		}

		protected UIPickerView BuildPickerView ()
		{
			var uIPickerView = new UIPickerView (new RectangleF (0, 0, Bounds.Width, 40));
			uIPickerView.ShowSelectionIndicator = true;
			AddSubview (uIPickerView);
			return uIPickerView;
		}

		public void Open ()
		{
			Hidden = false;
			BeginAnimations ("slideContent");
			SetAnimationDuration (.5);
			SetAnimationCurve (UIViewAnimationCurve.EaseOut);

			var pickerHeight = Frame.Height;


			Frame = new RectangleF (0, BaseCalendarViewController.ScreenHeight - pickerHeight, Frame.Width, Frame.Height);
			UIView.CommitAnimations ();
		}
	}
}

