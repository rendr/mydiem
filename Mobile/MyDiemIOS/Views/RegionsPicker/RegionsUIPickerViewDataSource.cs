using System;
using System.Collections.Generic;
using System.Diagnostics;

using MonoTouch.UIKit;

using MyDiem.Core.BL;

namespace MyDiem.UI.Touch.Views.RegionsPicker
{
	public class RegionsUIPickerViewDataSource:UIPickerViewModel
	{
		public event EventHandler RegionSelected = delegate{};

		public Region SelectedRegion;

		protected List<Region> Regions;

		public RegionsUIPickerViewDataSource (List<Region> regions)
		{
			Regions = regions;
		}

		public override int GetComponentCount (UIPickerView pickerView)
		{
			return 1;
		}

		public override int GetRowsInComponent (UIPickerView pickerView, int component)
		{
			return Regions.Count;
		}

		public override string GetTitle (UIPickerView picker, int row, int component)
		{
			return Regions[row].Title;
		}

		public override void Selected (UIPickerView picker, int row, int component)
		{
			SelectedRegion = Regions[row];
			RegionSelected(this, new EventArgs());
		}

	}
}

