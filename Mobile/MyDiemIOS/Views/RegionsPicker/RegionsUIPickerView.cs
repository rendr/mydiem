using System;
using System.Drawing;
using MonoTouch.UIKit;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using Views;
using System.Collections.Generic;
using MonkeyArms;

namespace MyDiem.UI.Touch.Views.RegionsPicker
{
	public class RegionsUIPickerView:BasePicker, IMediatorTarget
	{
		public event EventHandler RegionSelected = delegate{};

		public Region SelectedRegion;
		protected RegionsUIPickerViewDataSource PickerDataSource;
		UIPickerView pickerView;

		public RegionsUIPickerView (RectangleF bounds) : base (bounds)
		{

		}

		public List<Region> Regions {

			set {
				pickerView.Model = PickerDataSource = new RegionsUIPickerViewDataSource (value);

				SelectedRegion = value [0];

				PickerDataSource.RegionSelected += (sender, e) => {
					SelectedRegion = PickerDataSource.SelectedRegion;
					RegionSelected (null, new EventArgs ());
				};
			}
		}

		override protected void BuildUI ()
		{
			base.BuildUI ();
			pickerView = BuildPickerView ();



		}
	}
}

