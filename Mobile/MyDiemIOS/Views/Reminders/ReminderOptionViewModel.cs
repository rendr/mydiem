using System;

using MyDiem.Core.BL;

namespace MyDiem.UI.Touch.Views.Reminders
{
	public class ReminderOptionViewModel
	{
		public CalendarEvent TargetEvent;
		public long reminderOffset;
		public string Label;
		public bool Selected = false;

		public ReminderOptionViewModel (CalendarEvent calendarEvent, long reminderOffset, string label, bool selected)
		{
			this.TargetEvent = calendarEvent;
			this.reminderOffset = reminderOffset;
			this.Label = label;
			this.Selected = selected;
		}
	}
}

