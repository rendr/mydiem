using System;
using System.Drawing;

using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace MyDiem.UI.Touch.Views.Reminders
{
	public class ReminderOptionTableCell:UITableViewCell
	{



		protected UILabel EntryLabel;
		protected UIImageView Checkmark;

		public int RowIndex;
		public ReminderOptionViewModel OptionModel;



		public ReminderOptionTableCell (string cellId) : base (UITableViewCellStyle.Default, cellId)
		{
			Opaque = false;
			BackgroundColor = UIColor.Clear;

			SelectedBackgroundView = new UIView ();

			EntryLabel = new UILabel (new RectangleF (20, 0, 150, Bounds.Height)){
				Font = UIFont.FromName("Open Sans", 12), 
				TextColor = UIColor.FromRGB(126,126,126)
			};
			AddSubview (EntryLabel);


			Checkmark = new UIImageView(new RectangleF (210, 15, 17, 13));
			Checkmark.Image = UIImage.FromBundle("/Images/Reminders/Checkmark");
			AddSubview(Checkmark);

		}

		public void UpdateCell (ReminderOptionViewModel entryModel)
		{

			OptionModel = entryModel;
			EntryLabel.Text = entryModel.Label;
			Checkmark.Hidden = !entryModel.Selected;
		}

		public override void Draw (RectangleF rect)
		{
			base.Draw (rect);
			var context = UIGraphics.GetCurrentContext ();

			UIColor.FromRGB (191, 191, 191).SetStroke ();

			CGPath path = new CGPath ();
			path.AddLines (new PointF[]{
				new PointF (0, Bounds.Height - 1),
				new PointF (Frame.Width, Bounds.Height - 1),
				new PointF (Frame.Width, Bounds.Height),
				new PointF (0, Bounds.Height)

			}
			);
			path.CloseSubpath ();
			context.AddPath (path);
			context.DrawPath (CGPathDrawingMode.Stroke);
			context.SaveState ();
		}

		public override void LayoutSubviews ()
		{

		}
	}
}

