using System;
using System.Collections.Generic;
using System.Diagnostics;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.UI.Touch.Util;
using MonkeyArms;

namespace MyDiem.UI.Touch.Views.Reminders
{
	public class RemindersTableSource:UITableViewSource
	{
		List<ReminderOptionViewModel> tableItems;
		string cellIdentifier = "TableCell";
		public ReminderOptionTableCell[] cellCache;
		public int SelectedRowIndex = -1;

		public RemindersTableSource (List<ReminderOptionViewModel> entries)
		{
			tableItems = entries;

			UpdateSelectedOptions ();

			cellCache = new ReminderOptionTableCell[entries.Count];
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			return tableItems.Count;
		}

		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			ReminderOptionTableCell cell = tableView.DequeueReusableCell (cellIdentifier) as ReminderOptionTableCell;
			if (cell == null)
				cell = new ReminderOptionTableCell (cellIdentifier);
			cell.UpdateCell (tableItems [indexPath.Row]);
			cellCache [indexPath.Row] = cell;
			cell.RowIndex = indexPath.Row;
			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			tableView.DeselectRow (indexPath, true);
			SelectedRowIndex = indexPath.Row;
			foreach (ReminderOptionViewModel model in tableItems) {
				model.Selected = false;
			}

			RemoveReminderForEvent (tableItems [SelectedRowIndex].TargetEvent);
			if (tableItems [SelectedRowIndex].reminderOffset != -1) {
				SetReminder (tableItems [SelectedRowIndex]);

			} else {
				DI.Get<SubscribableManager> ().BroadcastReminderSet ();
			}
			UpdateSelectedOptions ();

			foreach (ReminderOptionTableCell cell in cellCache) {
				cell.UpdateCell (cell.OptionModel);
			}




		}

		protected void UpdateSelectedOptions ()
		{
			foreach (ReminderOptionViewModel model in tableItems) {
				var id = LocalNotificationUtil.generateEventIDString (model.TargetEvent);
				var offset = model.reminderOffset.ToString ();
				model.Selected = LocalNotificationUtil.IsReminderSet (id, offset);

				if (model.reminderOffset == -1 && !LocalNotificationUtil.IsThereReminderForEvent (model.TargetEvent)) {
					model.Selected = true;
				}

			}
		}

		protected void RemoveReminderForEvent (CalendarEvent targetEvent)
		{
			LocalNotificationUtil.RemoveNotificationByEventID (LocalNotificationUtil.generateEventIDString (targetEvent));


		}

		protected void SetReminder (ReminderOptionViewModel model)
		{
			var keys = new object[] { "eventID", "optionID", "optionLabel" };
			var objects = new object[] {
				LocalNotificationUtil.generateEventIDString (model.TargetEvent),
				model.reminderOffset.ToString (),
				model.Label
			};

			LocalNotificationUtil.SetNotification (model.TargetEvent.StartDateTime.AddMinutes (-1 * model.reminderOffset), model.TargetEvent.Title, NSDictionary.FromObjectsAndKeys (objects, keys));
		}
	}
}

