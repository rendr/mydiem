using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using MyDiem.UI.Touch.Views;
using MyDiem.UI.Touch.Util;
using MyDiem.UI.Touch.Views.EventsTable;
using MyDiem.Core.ViewModel;
using MyDiem.UI.Touch.Views.FilterMenu;
using MonkeyArms;
using MyDiem.Core.Interfaces;

namespace MyDiem.UI.Touch.Views.SlideMenu
{
	public class SubscribableFilterMenu:SlideMenu, IFilterMenu
	{
		public SubscribableFilterMenu () : base (new RectangleF ())
		{

		}

		public SubscribableFilterMenu (RectangleF viewRect) : base (viewRect)
		{

		}

		#region IFilterMenu implementation

		public event System.EventHandler RegionSelected;
		public event System.EventHandler CountySelected;
		public event System.EventHandler TypeSelected;
		public event System.EventHandler CategorySelected;

		public List<MyDiem.Core.BL.Region> Regions {
			get;
			set;
		}

		public List<MyDiem.Core.BL.County> Counties {
			get;
			set;
		}

		public List<string> Types {
			get;
			set;
		}

		public List<string> Categories {
			get;
			set;
		}

		public List<string> SelectedCategories {
			get;
			private set;
		}

		public MyDiem.Core.BL.Region SelectedRegion {
			get;
			private set;
		}

		public MyDiem.Core.BL.County SelectedCounty {
			get;
			private set;
		}

		public string SelectedType {
			get;
			private set;
		}

		#endregion

		protected override void AddOptionsTable ()
		{
			CreateTable ();
			UpdateOptionsTableSource ();

			LoadingLabel.Hidden = true;

		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			OptionsTable.Hidden = false;
		}

		protected void UpdateOptionsTableSource ()
		{
			FilterMenuTableSource optionsTableSource = new FilterMenuTableSource (DI.Get<SubscribableListViewModel> ().GetCategories ());
			optionsTableSource.CategoriesChanged += HandleCategoriesChanged;

			OptionsTable.Source = optionsTableSource;
			OptionsTable.ReloadData ();

		}

		public void Refresh (long RegionID = -1, long CountyID = -1, string SelectedType = "Any")
		{
			(OptionsTable.Source as FilterMenuTableSource).RegionID = RegionID;
			(OptionsTable.Source as FilterMenuTableSource).CountyID = CountyID;
			(OptionsTable.Source as FilterMenuTableSource).SelectedSubscribableType = SelectedType;
			OptionsTable.ReloadData ();

		}

		void HandleCategoriesChanged (object sender, System.EventArgs e)
		{
			var enabledCategories = (OptionsTable.Source as FilterMenuTableSource).GetEnabledCategories ();
			DI.Get<FilterMenuViewModel> ().FilterCategories (enabledCategories);
		}
	}
}

