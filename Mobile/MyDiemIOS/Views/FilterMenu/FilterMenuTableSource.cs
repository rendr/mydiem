using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using MonoTouch.UIKit;
using MyDiem.UI.Touch.Views.SlideMenu;
using MyDiem.Core.BL;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.ViewModel;
using Views.DirectoryFilterMenu;
using MonkeyArms;

namespace MyDiem.UI.Touch.Views.FilterMenu
{
	public class FilterMenuTableSource:UITableViewSource
	{
		public event EventHandler CategoriesChanged = delegate{};

		string optionCellIdentifier = "FilterOptionCell";
		string categoryCellIdentifier = "CategoryOptionCell";
		public long RegionID = -1, CountyID = -1;
		public string SelectedSubscribableType;
		private static Dictionary<string, bool> enabledCategories = new Dictionary<string, bool> ();
		Dictionary<string, DirectoryFilterMenuOptionCell> CellMap = new Dictionary<string, DirectoryFilterMenuOptionCell> ();
		List<string> Categories;

		public FilterMenuTableSource (List<string> categories)
		{
			this.Categories = categories;

			foreach (var cat in categories) {
				if (!enabledCategories.ContainsKey (cat)) {
					enabledCategories [cat] = true;
				}
			}
		}

		public override int NumberOfSections (UITableView tableView)
		{
			return 4;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			if (section != 3) {
				return 1;
			} else {
				return Categories.Count;
			}

		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			switch (section) {
			case 0:
				return "REGION";
			
			case 1:
				return "COUNTY";
			case 2:
				return "TYPE";
			case 3:
				return "CATEGORY";
			
			}

			return "";
		}

		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			if (indexPath.Section != 3) {
				return GetFilterOptionCell (tableView, indexPath);
			} else {
				return GetCategoryOptionCell (tableView, indexPath);
			}





		}

		DirectoryFilterMenuOptionCell GetCategoryOptionCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			DirectoryFilterMenuOptionCell cell = tableView.DequeueReusableCell (categoryCellIdentifier) as DirectoryFilterMenuOptionCell;
			if (cell == null)
				cell = new DirectoryFilterMenuOptionCell (categoryCellIdentifier);

			var categoryTitle = Categories [indexPath.Row];
			cell.UpdateCell (categoryTitle, enabledCategories [categoryTitle]);
			CellMap [categoryTitle] = cell;
			return cell;
		}

		SlideMenuOptionCell GetFilterOptionCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			SlideMenuOptionCell cell = tableView.DequeueReusableCell (optionCellIdentifier) as SlideMenuOptionCell;
			if (cell == null)
				cell = new SlideMenuOptionCell (optionCellIdentifier);
			if (indexPath.Section == 0) {
				Region region;
				if (RegionID == -1) {
					region = DI.Get<RegionManager> ().GetRegionByID (DI.Get<User> ().RegionID);
				} else {
					region = DI.Get<RegionManager> ().GetRegionByID (RegionID);
				}
				string RegionText = (DI.Get<User> ().RegionID != -1 && region != null) ? region.Title : "Click to set region";
				cell.UpdateCell (RegionText);
			}
			if (indexPath.Section == 1) {
				County county;
				if (CountyID == -1) {
					county = DI.Get<CountyManager> ().GetCountyByID (DI.Get<User> ().CountyID);
				} else {
					county = DI.Get<CountyManager> ().GetCountyByID (CountyID);
				}
				string CountyText = (DI.Get<User> ().CountyID != -1 || county != null) ? county.Title : "Click to set county";
				cell.UpdateCell (CountyText);
			}
			if (indexPath.Section == 2) {
				string LabelText = "Any";
				if (SelectedSubscribableType != null) {
					LabelText = char.ToUpper (SelectedSubscribableType [0]) + SelectedSubscribableType.Substring (1);
				}
				cell.UpdateCell (LabelText);
			}
			return cell;
		}

		public override UIView GetViewForHeader (UITableView tableView, int section)
		{
			SlideMenuTableHeaderView header = new SlideMenuTableHeaderView (new RectangleF (0, 0, tableView.Bounds.Width, 20), TitleForHeader (tableView, section));
			return header;			                                              
		}

		public override void RowSelected (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			if (indexPath.Section == 0 && indexPath.Row == 0) {
				DI.Get<FilterMenuViewModel> ().RegionClicked ();
			}
			if (indexPath.Section == 1 && indexPath.Row == 0) {
				DI.Get<FilterMenuViewModel> ().CountyClicked ();
			}
			if (indexPath.Section == 2 && indexPath.Row == 0) {
				DI.Get<FilterMenuViewModel> ().TypeClicked ();
			}
			if (indexPath.Section == 3) {
				var categoryTitle = Categories [indexPath.Row];
				var cell = CellMap [categoryTitle];
				enabledCategories [categoryTitle] = !enabledCategories [categoryTitle];
				cell.UpdateCell (categoryTitle, enabledCategories [categoryTitle]);
				CategoriesChanged (this, EventArgs.Empty);
			}
			tableView.DeselectRow (indexPath, true);
		}

		public List<string> GetEnabledCategories ()
		{
			return enabledCategories.Keys.Where (key => enabledCategories [key] == true).ToList ();
		}
	}
}

