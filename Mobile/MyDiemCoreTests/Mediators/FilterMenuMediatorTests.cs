﻿using NUnit.Framework;
using System;
using Ploeh.AutoFixture;
using MyDiem.Core.BL.Managers;
using MyDiem.Core.Mediators;
using FakeItEasy;
using MyDiem.Core.Interfaces;
using MyDiem.Core.ViewModel;
using System.Collections.Generic;
using MyDiem.Core.BL;
using Should;
using Xunit.Extensions;
using MonkeyArms;
using System.ComponentModel.Design;

namespace MyDiem.Core.Tests.Mediators
{
	[TestFixture ()]
	public class FilterMenuMediatorTests:BaseMediatorTest
	{
		[Test]
		public void MediatorRegister_ShouldSet_MenuCounties ()
		{
			FakeMenu.Counties.ShouldEqual (MediatorUT.CM.Counties);
		}

		[Test]
		public void MediatorRegister_ShouldSet_MenuRegions ()
		{
			FakeMenu.Regions.ShouldEqual (MediatorUT.RM.Regions);
		}

		[Test]
		public void MediatorRegister_ShouldSet_MenuTypes ()
		{
			FakeMenu.Types.ShouldEqual (new List<String> () { "Any", "Calendar", "Directory" });
		}

		[Test]
		public void MenuRaisesCountySelected_Mediator_ShouldCall_ViewModelGetAllSubscribables_With_AppUser_Region_County ()
		{
			NullSelections ();
			RaiseCountySelected ();
			VerifyGetAllSubscribablesWasCalledWithAppUserValues ();
		}

		[Test]
		public void MenuRaisesRegionSelected_Mediator_ShouldCall_ViewModelGetAllSubscribables_With_AppUser_Region_County ()
		{
			NullSelections ();
			RaiseRegionSelected ();
			VerifyGetAllSubscribablesWasCalledWithAppUserValues ();
		}

		[Test]
		public void MenuRaisesCountySelected_Mediator_ShouldCall_ViewModelGetAllSubscribables_With_ViewSelectedRegionCounty ()
		{
			SelectRegion ();
			SelectCounty ();
			RaiseCountySelected ();
			VerifyGetAllSubscribablesWasCalledWithSelectedValues ();
		}

		[Test]
		public void MenuRaisesRegionSelected_Mediator_ShouldCall_ViewModelGetAllSubscribables_With_ViewSelectedRegionCounty ()
		{
			SelectRegion ();
			SelectCounty ();
			RaiseRegionSelected ();
			VerifyGetAllSubscribablesWasCalledWithSelectedValues ();
		}

		[Test]
		public void MenuRaisesTypeSelected_Mediator_ShouldSet_ViewModelSelectedFilterType ()
		{
			SelectType ();
			RaiseTypeSelected ();
			VerifyTypeSetOnVM ();
			VerifyGetAllSubscribablesWasCalledWithSelectedValues ();
		}

		IFilterMenu FakeMenu;
		FilterMenuMediator MediatorUT;

		[SetUp]
		public void Init ()
		{
			FakeMenu = A.Fake<IFilterMenu> ();


			MediatorUT = CreateMediatorUT<FilterMenuMediator> (FakeMenu);
			MediatorUT.Register ();

		}

		void RaiseRegionSelected ()
		{
			FakeMenu.RegionSelected += Raise.WithEmpty ().Now;
		}

		void RaiseCountySelected ()
		{
			FakeMenu.CountySelected += Raise.WithEmpty ().Now;
		}

		void RaiseTypeSelected ()
		{
			FakeMenu.TypeSelected += Raise.WithEmpty ().Now;
		}

		void NullSelections ()
		{
			A.CallTo (() => FakeMenu.SelectedRegion).Returns (null);
			A.CallTo (() => FakeMenu.SelectedCounty).Returns (null);
		}

		void SelectType ()
		{
			A.CallTo (() => FakeMenu.SelectedType).Returns ("Directory");
		}

		void SelectRegion ()
		{
			var FakeRegion = DataFixture.Create<Region> ();
			A.CallTo (() => FakeMenu.SelectedRegion).Returns (FakeRegion);
		}

		void SelectCounty ()
		{
			var FakeCounty = DataFixture.Create<County> ();
			A.CallTo (() => FakeMenu.SelectedCounty).Returns (FakeCounty);
		}

		void VerifyGetAllSubscribablesWasCalledWithAppUserValues ()
		{
			A.CallTo (() => MediatorUT.ViewModel.GetAllSubscribables (MediatorUT.ViewModel.AppUser.RegionID, MediatorUT.ViewModel.AppUser.CountyID)).MustHaveHappened ();
		}

		void VerifyGetAllSubscribablesWasCalledWithSelectedValues ()
		{
			A.CallTo (() => MediatorUT.ViewModel.GetAllSubscribables (FakeMenu.SelectedRegion.ID, FakeMenu.SelectedCounty.ID)).MustHaveHappened ();
		}

		void VerifyTypeSetOnVM ()
		{
			MediatorUT.ViewModel.SelectedFilterType.ShouldEqual (FakeMenu.SelectedType);
		}
	}
}

