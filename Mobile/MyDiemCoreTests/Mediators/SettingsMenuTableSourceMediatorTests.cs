﻿using NUnit.Framework;
using System;
using FakeItEasy;
using MyDiem.Core.Interfaces;
using MyDiem.Core.Mediators;
using System.Net.Mime;
using Should;
using NUnit.Framework.SyntaxHelpers;
using Ploeh.AutoFixture;
using System.Collections.Generic;
using MyDiem.Core.BL;
using MyDiem.Core.ViewModel;

namespace MyDiem.Core.Tests.Mediators
{
	[TestFixture ()]
	public class SettingsMenuTableSourceMediatorTests:BaseMediatorTest
	{
		[Test]
		public void WhenRegisterCalled_Mediator_ShouldSet_TableSourceIsCalendarConnectPurchased ()
		{
			MediatorUT.AppUser.CalendarConnect = true;
			CallRegister ();
			FakeTableSource.IsCalendarConnectPurchased.ShouldEqual (MediatorUT.AppUser.CalendarConnect);
		}

		[Test]
		public void WhenRegisterCalled_AND_VMCalendarCategories_NotNull_Mediator_ShouldCall_TableSourceEnableSubscribableOptions ()
		{
			PopulateVMCategories ();
			CallRegister ();
			A.CallTo (() => FakeTableSource.EnableSubscribableOptions ()).MustHaveHappened (Repeated.Exactly.Once);
		}

		[Test]
		public void WhenRegisterCalled_AND_VMCalendarCategories_IsNull_Mediator_ShouldNotCall_TableSourceEnableSubscribableOptions ()
		{
			CallRegister ();
			A.CallTo (() => FakeTableSource.EnableSubscribableOptions ()).MustNotHaveHappened ();
		}

		[Test]
		public void When_VMCalendarCategoriesChanged_Mediator_ShouldCall_ViewEnableSubscribableOptions ()
		{
			CallRegister ();
			MediatorUT.VM.CalendarCategoriesChangedEvent.Invoke ();
			A.CallTo (() => FakeTableSource.EnableSubscribableOptions ()).MustHaveHappened (Repeated.Exactly.Once);
		}

		ISettingsMenuTableSource FakeTableSource;
		SettingsMenuTableSourceMediator MediatorUT;

		[SetUp]
		public void Init ()
		{
			FakeTableSource = A.Fake<ISettingsMenuTableSource> ();
			MediatorUT = CreateMediatorUT<SettingsMenuTableSourceMediator> (FakeTableSource);
		}

		void CallRegister ()
		{
			MediatorUT.Register ();
		}

		void PopulateVMCategories ()
		{
			MediatorUT.VM = A.Fake<SlideMenuViewModel> ();
			var categories = new List<SchoolGroup> ();
			A.CallTo (() => MediatorUT.VM.CalendarCategories).Returns (categories);
		}
	}
}

