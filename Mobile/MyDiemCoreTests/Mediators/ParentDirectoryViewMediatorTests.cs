﻿using NUnit.Framework;
using System;
using MyDiem.Core.Mediators;
using FakeItEasy;
using MyDiem.Core.Interfaces;
using System.Collections.Generic;
using MyDiem.Core.BL;
using MonkeyArms;
using MyDiem.Core.ViewModel;
using Ploeh.AutoFixture;

namespace MyDiem.Core.Tests.Mediators
{
	[TestFixture ()]
	public class ParentDirectoryViewMediatorTests:BaseMediatorTest
	{
		ParentDirectoryViewMediator MediatorUT;
		IParentDirectoryView FakeView;

		[Test]
		public void Mediator_Register_ShouldCall_VM_GetContacts_if_VM_Contacts_Null ()
		{
			NullVMContacts ();
			InvokeRegister ();
			VerifyGetContactsCalled ();
			VerifyUpdateContactsNotCalled ();
		}

		[Test]
		public void Mediator_Register_ShouldCall_VM_GetContacts_if_VM_Contacts_Length_0 ()
		{
			EmptyVMContacts ();
			InvokeRegister ();
			VerifyGetContactsCalled ();
			VerifyUpdateContactsNotCalled ();
		}

		[Test]
		public void Mediator_Register_ShouldCall_View_UpdateContacts_if_VM_Contacts_Not_Empty ()
		{
			PopulateVMContacts ();
			InvokeRegister ();
			VerifyGetContactsNotCalled ();
			VerifyUpdateContactsCalled ();
		}

		[Test]
		public void AfterRegister_Mediator_ShouldCall_View_UpdateContacts_when_VM_Raises_ContactsUpdated ()
		{
			InvokeRegister ();
			PopulateVMContacts ();
			InvokeVMContactsUpdated ();
			VerifyUpdateContactsCalled ();
		}

		[Test]
		public void AfterRegister_Mediator_ShouldCall_View_HideLoadingIndicator_when_VM_Raises_ContactsUpdated ()
		{
			InvokeRegister ();
			PopulateVMContacts ();
			InvokeVMContactsUpdated ();
			VerifyHideLoadingIndicatorCalled ();
		}

		[Test]
		public void AfterRegister_Mediator_ShouldCall_View_ShowNotSubscribedPrompt_when_VM_Raises_ContactsUpdated_VM_Contacts_Empty_AND_Search_Query_Empty ()
		{
			InvokeRegister ();
			EmptyVMContacts ();
			InvokeVMContactsUpdated ();
			VerifyShowNotSubscribedPromptCalled ();
			VerifyShowNoMatchesPromptNotCalled ();
		}

		[Test]
		public void AfterRegister_Mediator_ShouldCall_View_ShowNoMatchesPrompt_when_VM_Raises_ContactsUpdated_VM_Contacts_Empty_AND_Search_Qeury_Exists ()
		{
			InvokeRegister ();
			EmptyVMContacts ();
			PopulateSearchQuery ();
			InvokeVMContactsUpdated ();
			VerifyShowNotSubscribedPromptNotCalled ();
			VerifyShowNoMatchesPromptCalled ();
		}

		[Test]
		public void When_ViewSearchInvoked_Mediator_ShouldCall_VMFilterContacts_with_ViewSearchQuery ()
		{
			InvokeRegister ();
			PopulateSearchQuery ();
			PopulateVMContacts ();
			InvokeViewSearch ();
			VerifyVMFilterCalledWithQuery ();
		}

		[Test, Ignore]
		public void When_ViewResetInvoked_Mediator_ShouldCall_VMResetContacts ()
		{
			InvokeRegister ();
			PopulateVMContacts ();
			InvokeViewReset ();
			VerifyVMRestContactsCalled ();
		}

		[Test]
		public void When_ViewParentSelectedInvoked_Mediator_ShouldCall_VMSelectParent ()
		{
			InvokeRegister ();
			SetSelectedParent ();
			InvokeViewParentSelected ();
			VerifyVMSelectParentCalledWithViewSelectedParent ();
		}

		[Test]
		public void When_ViewParentSelectedInvoked_Mediator_ShouldCall_ViewGoParentDetails ()
		{
			InvokeRegister ();
			SetSelectedParent ();
			InvokeViewParentSelected ();
			VerifyViewGoParentDetailsCalled ();
		}

		[SetUp]
		public void Init ()
		{
			FakeView = A.Fake<IParentDirectoryView> ();
			MediatorUT = CreateMediatorUT<ParentDirectoryViewMediator> (FakeView);

		}

		void InvokeRegister ()
		{
			MediatorUT.Register ();
		}

		void VerifyGetContactsCalled ()
		{
			A.CallTo (() => MediatorUT.VM.GetContacts ()).MustHaveHappened (Repeated.Exactly.Once);
		}

		void VerifyGetContactsNotCalled ()
		{
			A.CallTo (() => MediatorUT.VM.GetContacts ()).MustNotHaveHappened ();
		}

		void VerifyUpdateContactsCalled ()
		{
			A.CallTo (() => FakeView.UpdateContacts (MediatorUT.VM.Contacts)).MustHaveHappened (Repeated.Exactly.Once);
		}

		void VerifyUpdateContactsNotCalled ()
		{
			A.CallTo (() => FakeView.UpdateContacts (MediatorUT.VM.Contacts)).MustNotHaveHappened ();
		}

		void VerifyHideLoadingIndicatorCalled ()
		{
			A.CallTo (() => FakeView.HideLoadingIndicator ()).MustHaveHappened (Repeated.Exactly.Once);
		}

		void VerifyShowNotSubscribedPromptCalled ()
		{
			A.CallTo (() => FakeView.ShowNotSubscribedPrompt ()).MustHaveHappened (Repeated.Exactly.Once);
		}

		void VerifyShowNotSubscribedPromptNotCalled ()
		{
			A.CallTo (() => FakeView.ShowNotSubscribedPrompt ()).MustNotHaveHappened ();
		}

		void VerifyShowNoMatchesPromptNotCalled ()
		{
			A.CallTo (() => FakeView.ShowNoMatchesPrompt ()).MustNotHaveHappened ();
		}

		void VerifyShowNoMatchesPromptCalled ()
		{
			A.CallTo (() => FakeView.ShowNoMatchesPrompt ()).MustHaveHappened (Repeated.Exactly.Once);
		}

		void VerifyVMFilterCalledWithQuery ()
		{
			A.CallTo (() => MediatorUT.VM.FilterContacts (FakeView.SearchQuery)).MustHaveHappened (Repeated.Exactly.Once);
		}

		void VerifyVMRestContactsCalled ()
		{
			A.CallTo (() => MediatorUT.VM.ResetContacts ()).MustHaveHappened (Repeated.Exactly.Once);
		}

		void VerifyVMSelectParentCalledWithViewSelectedParent ()
		{
			A.CallTo (() => MediatorUT.VM.SelectParent (FakeView.SelectedParent)).MustHaveHappened (Repeated.Exactly.Once);
		}

		void VerifyViewGoParentDetailsCalled ()
		{
			A.CallTo (() => FakeView.GoParentDetails ()).MustHaveHappened (Repeated.Exactly.Once);
		}

		void NullVMContacts ()
		{
			MediatorUT.VM.Contacts = null;
		}

		void EmptyVMContacts ()
		{
			MediatorUT.VM.Contacts = new List<Parent> ();
		}

		void PopulateVMContacts ()
		{
			MediatorUT.VM.Contacts = DataFixture.Create<List<Parent>> ();
		}

		void PopulateSearchQuery ()
		{
			A.CallTo (() => FakeView.SearchQuery).Returns (DataFixture.Create<string> ());
		}

		void SetSelectedParent ()
		{
			var selectedParent = DataFixture.Create<Parent> ();
			A.CallTo (() => FakeView.SelectedParent).Returns (selectedParent);
		}

		void InvokeVMContactsUpdated ()
		{
			MediatorUT.VM.ContactsUpdated.Invoke ();
		}

		void InvokeViewSearch ()
		{
			FakeView.Search.Invoked += Raise.WithEmpty ().Now;
		}

		void InvokeViewReset ()
		{
			FakeView.Reset.Invoked += Raise.WithEmpty ().Now;
		}

		void InvokeViewParentSelected ()
		{
			FakeView.ParentSelected.Invoked += Raise.WithEmpty ().Now;
		}
	}
}

