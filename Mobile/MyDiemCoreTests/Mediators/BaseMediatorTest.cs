﻿using NUnit.Framework;
using System;
using Ploeh.AutoFixture;
using MyDiem.Core.BL.Managers;
using FakeItEasy;
using MyDiem.Core.ViewModel;
using System.Collections.Generic;
using MyDiem.Core.BL;
using Ploeh.AutoFixture.AutoFakeItEasy;
using MonkeyArms;
using MyDiem.Core.SAL;
using MyDiem.Core.Invokers;

namespace MyDiem.Core.Tests.Mediators
{
	public class BaseMediatorTest
	{
		protected Fixture DataFixture = new Fixture ();
		InjectionMap map;

		public BaseMediatorTest ()
		{
			map = new InjectionMap ();
			DataFixture.Customize (new AutoFakeItEasyCustomization ());	

			DI.MapInstanceToSingleton<IWebAPIService> (A.Fake<IWebAPIService> ());

			FixtureInjection<CountyManager> ();
			FixtureInjection<RegionManager> ();
			FixtureInjection<User> ();
			FixtureInjection<ResolveSlideMenuRowTouchedInvoker> ();
//			FakeInjection<SlideMenuViewModel> ();
			FakeInjection<DirectoryViewModel> ();

			map.Add<SubscribableListViewModel> (() => {
				var vm = A.Fake<SubscribableListViewModel> ();
				A.CallTo (() => vm.AppUser).Returns (DataFixture.Create<User> ());
				return vm;
			});

		}

		protected void FixtureInjection<TFake> () where TFake:class
		{
			map.Add<TFake> (DataFixture.Create<TFake>);
		}

		protected void FakeInjection<TFake> () where TFake:class
		{
			map.Add<TFake> (A.Fake<TFake>);
		}

		protected TMediator CreateMediatorUT<TMediator> (IMediatorTarget target) where TMediator:class
		{
			Mediator mediator = Activator.CreateInstance (typeof(TMediator), target) as Mediator;
			DIUtil.InjectProps (mediator, map);
			return mediator as TMediator;
		}
	}
}

